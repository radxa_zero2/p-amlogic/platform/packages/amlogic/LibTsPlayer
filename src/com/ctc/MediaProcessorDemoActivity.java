package com.ctc;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
//import android.os.SystemProperties;
import android.text.method.ScrollingMovementMethod;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.EditText;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Surface;
import android.view.View;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log; 

import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.graphics.Typeface;

import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

public class MediaProcessorDemoActivity extends Activity {

	private static String TAG="MediaProcessorDemoActivity";
	private Surface mySurface = null;
	private SurfaceHolder myHolder = null;
	private SurfaceView mySurfaceView = null;
	private MediaProcessor mMediaProcessor = null;
	private PropertieList propList = new PropertieList();
	String url = getUrl();
	private static String getUrl() {
		return (String)getProperties("iptv.demo.url",
				"");
	}
	public static Object getProperties(String key, String def) {
            String defVal = def;
            try {
                Class properClass = Class.forName("android.os.SystemProperties");
                Method getMethod = properClass.getMethod("get",String.class,String.class);
                defVal = (String)getMethod.invoke(null,key,def);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                Log.d(TAG,"getProperties(" + key + "," + def + ") ret= "+defVal);
                return defVal;
            }

    }

    private String mCurrentPlayUrl = null;
	private Button playButton = null;
	private Button stopButton = null;
	private Button pauseResumeButton = null;
	private Button browseButton = null;
	private CheckBox mStreamTypeBox = null;
	private CheckBox mUseOmxBox = null;
	private CheckBox mUseAmMediaPlayerBox = null;
	private SeekBar mSeekBar = null;
	private TextView mCurrentPositionText = null;
	private TextView mDurationText = null;
	private Spinner mAudioTrackSelector = null;
	private ArrayAdapter<TrackItem> mAudioAdapter = null;
	private Spinner mSubtitleTrackSelector = null;
	private ArrayAdapter<TrackItem> mSubtitleAdapter = null;
    private EditText urlEditText = null;
    private TextView eventView = null;
	private EventHandler mEventHandler = null;
	private EventCollector mEventCollector = new EventCollector();

	private boolean mPaused = false;
	private int mDurationMs = -1;
    private boolean ignoreUpdateProgressbar = false;

	private static final int DRAW_EVENT = 1;
	private static final int SEEK_EVENT = 2;
	private static final int UPDATE_POSITION_EVENT = 3;
	private static final int SELECT_TRACK_EVENT = 4;
	private static final int PLAYBACK_COMPLETE_EVENT = 5;

	//keep sync with MediaPlayer
	private static final int MEDIA_NOP = 0; // interface test message
	private static final int MEDIA_PREPARED = 1;
	private static final int MEDIA_PLAYBACK_COMPLETE = 2;
	private static final int MEDIA_BUFFERING_UPDATE = 3;
	private static final int MEDIA_SEEK_COMPLETE = 4;
	private static final int MEDIA_SET_VIDEO_SIZE = 5;
	private static final int MEDIA_STARTED = 6;
	private static final int MEDIA_PAUSED = 7;
	private static final int MEDIA_STOPPED = 8;
	private static final int MEDIA_SKIPPED = 9;
	private static final int MEDIA_NOTIFY_TIME = 98;
	private static final int MEDIA_TIMED_TEXT = 99;
	private static final int MEDIA_ERROR = 100;
	private static final int MEDIA_INFO = 200;
	private static final int MEDIA_SUBTITLE_DATA = 201;
	private static final int MEDIA_META_DATA = 202;
	private static final int MEDIA_DRM_INFO = 210;
	private static final int MEDIA_TIME_DISCONTINUITY = 211;
	private static final int MEDIA_AUDIO_ROUTING_CHANGED = 10000;

	/** Called when the activity is first created. */
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		Log.i(TAG, "onCreate");
		urlEditText = (EditText)findViewById(R.id.urlText);
		mySurfaceView = (SurfaceView)this.findViewById(R.id.SurfaceView01);
		mySurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mySurface = holder.getSurface();
                Log.d(TAG, "surfaceCreated: " + mySurface);
            }

			@Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

			@Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });

        //play
		playButton = (Button)findViewById(R.id.play);
		playButton.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v) {
                String playUrl = urlEditText.getText().toString();
                String propUrl = getUrl();
                if (!propUrl.isEmpty()) {
                    Log.i(TAG, "propUrl: " + propUrl);
                    playUrl = propUrl;
                }

                startPlay(playUrl);
				mCurrentPlayUrl = playUrl;
			}
		});

		//stop
		stopButton = (Button)findViewById(R.id.stop);
		stopButton.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v) {
			    stopPlay();
			}
		});

		pauseResumeButton = (Button)findViewById(R.id.pauseResume);
		pauseResumeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				pauseOrResumePlay();
			}
		});

		browseButton = (Button)findViewById(R.id.browse);
		browseButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent tIntent=new Intent();
				ComponentName tComponentName=new ComponentName("com.amlogic.multimediaplayer", "com.amlogic.multimediaplayer.FileManagerActivity");
				         tIntent.setComponent(tComponentName);
//				         tIntent.setAction("android.intent.action.MAIN");
                         tIntent.putExtra("path", "/storage");
                         tIntent.putExtra("mediaplayerType", "MediaPlayer");
				         startActivityForResult(tIntent, 0x10);
			}

		});

		eventView = (TextView)findViewById(R.id.eventView);
		eventView.setMovementMethod(ScrollingMovementMethod.getInstance());

		mStreamTypeBox = (CheckBox)findViewById(R.id.streamType);
		mUseOmxBox = (CheckBox)findViewById(R.id.useOmx);
		mUseAmMediaPlayerBox = (CheckBox)findViewById(R.id.useAmMediaPlayer);

		mCurrentPositionText = (TextView)findViewById(R.id.currentPosition);
		mDurationText = (TextView)findViewById(R.id.duration);
		mSeekBar = (SeekBar)findViewById(R.id.seekBar);
		mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
                    //Log.i(TAG, "changed fromUser:" + fromUser + ", progress:" + progress);
                    ignoreUpdateProgressbar = true;

                    if (mEventHandler != null) {
						mEventHandler.removeMessages(SEEK_EVENT);

						Message msg = mEventHandler.obtainMessage(SEEK_EVENT);
						msg.arg1 = progress;
						mEventHandler.sendMessageDelayed(msg, 500);
					}
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				Log.i(TAG, "start tracking");
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				Log.i(TAG, "stop tracking");
			}
		});

		mAudioTrackSelector = (Spinner)findViewById(R.id.audioTrack);
		mAudioTrackSelector.setDropDownWidth(400);
		mAudioTrackSelector.setDropDownHorizontalOffset(100);
		mAudioTrackSelector.setDropDownVerticalOffset(100);

		//String[] spinnerItems = {"10","200","400"};
		//ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,
				//R.layout.item_select, spinnerItems);
		//spinnerAdapter.setDropDownViewResource(R.layout.item_drop);
		//mAudioTrackSelector.setAdapter(spinnerAdapter);

		mAudioTrackSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				Log.i(TAG, "onNothing selected!");
			}

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (mAudioAdapter == null)
					return;

				TrackItem trackItem = mAudioAdapter.getItem(position);
				Log.i(TAG, "select audio, position:" + position + ", audio index:" + trackItem.mIndex);

				if (mEventHandler != null) {
					Message msg = mEventHandler.obtainMessage(SELECT_TRACK_EVENT);
					msg.arg1 = trackItem.mIndex;
					msg.arg2 = MediaProcessor.TrackInfo.MEDIA_TRACK_TYPE_AUDIO;
					mEventHandler.sendMessage(msg);
				}
			}
		});

		mSubtitleTrackSelector = (Spinner)findViewById(R.id.subtitleTrack);
		mSubtitleTrackSelector.setDropDownWidth(400);
		mSubtitleTrackSelector.setDropDownHorizontalOffset(100);
		mSubtitleTrackSelector.setDropDownVerticalOffset(100);
		//mSubtitleTrackSelector.setAdapter(spinnerAdapter);

		mSubtitleTrackSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (mSubtitleAdapter == null)
					return;

				TrackItem trackItem = mSubtitleAdapter.getItem(position);
				Log.i(TAG, "select subtitle, position:" + position + ", subtitle index:" + trackItem.mIndex);

				if (mEventHandler != null) {
					Message msg = mEventHandler.obtainMessage(SELECT_TRACK_EVENT);
					msg.arg1 = trackItem.mIndex;
					msg.arg2 = MediaProcessor.TrackInfo.MEDIA_TRACK_TYPE_SUBTITLE;
					mEventHandler.sendMessage(msg);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});


		//restore settings
        SharedPreferences pref = getSharedPreferences("data", MODE_PRIVATE);
        String savedUrl = pref.getString("playUrl", "");
        boolean useOmx =  pref.getBoolean("useOmx", false);
        boolean tsMode = pref.getBoolean("streamType", false);
        boolean useAmMediaPlayer = pref.getBoolean("useAmMediaPlayer", true);
        if (!savedUrl.isEmpty()) {
            urlEditText.setText(savedUrl);
        }
        mUseOmxBox.setChecked(useOmx);
        mStreamTypeBox.setChecked(tsMode);
        mUseAmMediaPlayerBox.setChecked(useAmMediaPlayer);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "onStart()");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume()");
	}

	@Override
	public void onPause(){
		super.onPause();
		Log.i(TAG, "onPause");
	}

	@Override
	public void onStop(){
		super.onStop();
		Log.i(TAG, "onStop");

        SharedPreferences.Editor editor = getSharedPreferences("data", MODE_PRIVATE).edit();
        String playUrl = urlEditText.getText().toString();
        if (playUrl != null) {
            editor.putString("playUrl", playUrl);
        }
        editor.putBoolean("useOmx", mUseOmxBox.isChecked());
        editor.putBoolean("streamType", mStreamTypeBox.isChecked());
        editor.putBoolean("useAmMediaPlayer", mUseAmMediaPlayerBox.isChecked());
        editor.apply();

		Date a = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Log.i(TAG, "stopPlay start: " + sdf.format(a));
		stopPlay();
		Date b = new Date();
		Log.i(TAG, "stopPlay end: " + sdf.format(b));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "onDestroy");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.i(TAG, "onRestart");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case 0x10:
				if (data != null) {
					String filePath = data.getStringExtra("path");
					Log.i(TAG, "path = " + filePath);
					urlEditText.setText(filePath);
				}
				break;
			default:
				super.onActivityResult(requestCode, resultCode, data);
				break;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent msg) {
        Log.d(TAG, "onKeyDown() " + keyCode);
		if(keyCode == KeyEvent.KEYCODE_POWER){
			return true;
		}
		return super.onKeyDown(keyCode, msg);
	}

	private void startPlay(String playUrl) {
		if (mEventHandler == null) {
            mEventHandler = new EventHandler();
        }

        if (mMediaProcessor != null) {
            return;
        }

		String result = "StartPlay: " + playUrl;
        mEventCollector.add(result);

        boolean isTsSource = mStreamTypeBox.isChecked();
        boolean useOmx = mUseOmxBox.isChecked();
        boolean useAmMediaPlayer = mUseAmMediaPlayerBox.isChecked();
		Log.i(TAG, "isTsSource:" + isTsSource + ", useOmx:" + useOmx + ", useAmMediaPlayer:" + useAmMediaPlayer);
        mMediaProcessor = new MediaProcessor(isTsSource, useOmx, useAmMediaPlayer);
		mMediaProcessor.setDataSource(playUrl);
		mMediaProcessor.setSurface(mySurface);
		mMediaProcessor.setEventListener(new MediaProcessor.EventListener() {
			@Override
			public void onEvent(int event, int ext1, int ext2) {
				if (event == MEDIA_PLAYBACK_COMPLETE) {
					Log.i(TAG, "MEDIA_PLAYBACK_COMPLETE!");

					if (mEventHandler != null) {
						Message msg = mEventHandler.obtainMessage();
						msg.what = PLAYBACK_COMPLETE_EVENT;
						mEventHandler.sendMessage(msg);
					}
				}
			}

			@Override
			public void onCTCEvent(int event, int ext1, int ext2, String extras) {
				//Log.i(TAG, "event:" + event + ", extras:" + extras);
			    mEventCollector.add(event, ext1, ext2, extras);
			}
		});

		if (mMediaProcessor.prepare() == 0)
			Log.i(TAG, "prepare success!");
		else {
			Log.i(TAG, "prepare error!");
            stopPlay();

            mEventCollector.add("Play failed!");
			return;
		}

		updateTracksInfo();
		mMediaProcessor.startPlay();
		postUpdatePositionMessage();
	}

	private void stopPlay() {
        mPaused = false;
        pauseResumeButton.setText("Pause");
		mAudioTrackSelector.setAdapter(null);
		mSubtitleTrackSelector.setAdapter(null);

        if (mEventHandler != null) {
            mEventHandler.removeMessages(UPDATE_POSITION_EVENT);
            mEventHandler.removeMessages(SEEK_EVENT);
        }

		if (mMediaProcessor == null) {
			return;
		}

		mMediaProcessor.stop();
		mMediaProcessor.release();
		mMediaProcessor = null;

		mEventCollector.add("Play Stopped!");
	}

	private void pauseOrResumePlay() {
		if (mMediaProcessor == null)
			return;

		if (mPaused) {
			mMediaProcessor.resume();
			mPaused = false;
			pauseResumeButton.setText("Pause");
		} else {
			mMediaProcessor.pause();
			mPaused = true;
			pauseResumeButton.setText("Resume");
		}
	}

	private class EventHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			//Log.i(TAG, "handle message " + msg.what);
			switch (msg.what) {
				case DRAW_EVENT:
					eventView.setText(mEventCollector.toString());
					break;

				case SEEK_EVENT:
                    updateDuration();
					if (mDurationMs <= 0) {
						break;
					}

					int progress = msg.arg1;
					int msec = progress * mDurationMs / 100;
					doSeek(msec);
					break;

				case UPDATE_POSITION_EVENT:
                    updateDuration();

					int positionMs = mMediaProcessor.getCurrentPosition();
				    mCurrentPositionText.setText(secToTime(positionMs/1000));

                    if (!ignoreUpdateProgressbar && mDurationMs > 0) {
                        int step = positionMs * 100 / mDurationMs;
                        mSeekBar.setProgress(step);
                    }

                    mEventHandler.removeMessages(UPDATE_POSITION_EVENT);
				   	postUpdatePositionMessage();
					break;

				case SELECT_TRACK_EVENT:
					int trackIndex = msg.arg1;
					if (mMediaProcessor != null) {
						int currentSelectedIndex = mMediaProcessor.getSelectedTrack(msg.arg2);
						if (currentSelectedIndex == trackIndex) {
							break;
						}

						mMediaProcessor.selectTrack(trackIndex);
					}
					break;

				case PLAYBACK_COMPLETE_EVENT:
				    Log.i(TAG, "Playback completed!");
					// restart play
					stopPlay();
					if (mCurrentPlayUrl != null && !mCurrentPlayUrl.isEmpty()) {
						startPlay(mCurrentPlayUrl);
					} else {
						Log.i(TAG, "current play url is invalid!");
					}
					break;

				default:
				    break;
			}
		}
	}

	private void doSeek(int msec) {
		mEventHandler.removeMessages(UPDATE_POSITION_EVENT);

		mMediaProcessor.seekTo(msec);

        ignoreUpdateProgressbar = false;
		postUpdatePositionMessage();
	}

    private void updateDuration() {
        mDurationMs = mMediaProcessor.getDuration();
        if (mDurationMs < 0) {
            return;
        }

        mDurationText.setText(secToTime(mDurationMs/1000));
    }

	private void postUpdatePositionMessage() {
		Message updatePositionMsg = mEventHandler.obtainMessage(UPDATE_POSITION_EVENT);
		mEventHandler.sendMessageDelayed(updatePositionMsg, 500);
	}

	private String secToTime (int i) {
		String retStr = null;
		int hour = 0;
		int minute = 0;
		int second = 0;
		if (i <= 0) {
			return "00:00:00";
		}
		else {
			minute = i / 60;
			if (minute < 60) {
				second = i % 60;
				retStr = "00:" + unitFormat (minute) + ":" + unitFormat (second);
			}
			else {
				hour = minute / 60;
				if (hour > 99) {
					return "99:59:59";
				}
				minute = minute % 60;
				second = i % 60;
				retStr = unitFormat (hour) + ":" + unitFormat (minute) + ":" + unitFormat (second);
			}
		}
		return retStr;
	}

	private String unitFormat (int i) {
		String retStr = null;
		if (i >= 0 && i < 10) {
			retStr = "0" + Integer.toString (i);
		}
		else {
			retStr = Integer.toString (i);
		}
		return retStr;
	}

	private class TrackItem {
		final int mIndex;
		final String mName;

		TrackItem(int index, String name) {
			mIndex = index;
			mName = name;
		}

		@Override
		public String toString() {
		    return mName;
		}
	}

	private void updateTracksInfo() {
		MediaProcessor.TrackInfo trackInfos[] = mMediaProcessor.getTrackInfo();

		Vector<TrackItem> audioItems = new Vector<>();
		Vector<TrackItem> subtitleItems = new Vector<>();

		Log.i(TAG, "trackInfos length = " + trackInfos.length);
		for (int i = 0; i < trackInfos.length; ++i) {
			MediaProcessor.TrackInfo info = trackInfos[i];
			if (info.mTrackType == MediaProcessor.TrackInfo.MEDIA_TRACK_TYPE_AUDIO) {
				audioItems.add(new TrackItem(i, info.getLanguage()));
			} else if (info.mTrackType == MediaProcessor.TrackInfo.MEDIA_TRACK_TYPE_SUBTITLE) {
				subtitleItems.add(new TrackItem(i, info.getLanguage()));
			}
		}

		if (!audioItems.isEmpty()) {
			mAudioAdapter = new ArrayAdapter<TrackItem>(this,
					R.layout.item_select, audioItems);
			mAudioAdapter.setDropDownViewResource(R.layout.item_drop);
			mAudioTrackSelector.setAdapter(mAudioAdapter);
		}

		if (!subtitleItems.isEmpty()) {
			mSubtitleAdapter = new ArrayAdapter<TrackItem>(this,
					R.layout.item_select, subtitleItems);
			mSubtitleAdapter.setDropDownViewResource(R.layout.item_drop);
			mSubtitleTrackSelector.setAdapter(mSubtitleAdapter);
		}
	}

	private class EventCollector {
		private LinkedList<String> mEvents;
		final static int kMaxEvents = 100;

		EventCollector() {
			mEvents = new LinkedList<>();
		}

		void add(int event, int ext1, int ext2, String extras) {
			String result = extras;
			if (result == null) {
				StringBuilder b = new StringBuilder();
				b.append("event=");
				b.append(event);
				b.append(", ext1=");
				b.append(ext1);
				b.append(", ext2=");
				b.append(ext2);
				result = b.toString();
			}

			add(result);
		}

		void add(String event) {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
			String time = sdf.format(date);

			StringBuilder builder = new StringBuilder();
			builder.append(time);
			builder.append(" ");
			builder.append(event);

			mEvents.addFirst(builder.toString());
			if (mEvents.size() > kMaxEvents) {
			    mEvents.removeLast();
			}

			if (mEventHandler == null) {
				return;
			}

			Message msg = mEventHandler.obtainMessage(DRAW_EVENT);
			mEventHandler.sendMessage(msg);
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			ListIterator iterator = mEvents.listIterator();
			while (iterator.hasNext()) {
				builder.append(iterator.next());
				if (iterator.hasNext()) {
					builder.append("\n");
				}
			}

			return builder.toString();
		}
	}
}
