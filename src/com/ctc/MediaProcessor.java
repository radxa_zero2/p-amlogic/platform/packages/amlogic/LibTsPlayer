package com.ctc;

import android.media.MediaFormat;
import android.os.Handler;
import android.os.Message;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.Surface;


public class MediaProcessor {
    private static String TAG = "MediaProcessor";

    static {
        System.loadLibrary("CTC_MediaProcessorjni");
        nativeInit();
    }

    private long mNativeContext;
    private EventListener mEventListener = null;
    private EventHandler mEventHandler = null;

    private static final int INVOKE_ID_GET_TRACK_INFO = 1;
    private static final int INVOKE_ID_ADD_EXTERNAL_SOURCE = 2;
    private static final int INVOKE_ID_ADD_EXTERNAL_SOURCE_FD = 3;
    private static final int INVOKE_ID_SELECT_TRACK = 4;
    private static final int INVOKE_ID_DESELECT_TRACK = 5;
    private static final int INVOKE_ID_SET_VIDEO_SCALE_MODE = 6;
    private static final int INVOKE_ID_GET_SELECTED_TRACK = 7;

    private static final int MEDIA_PROCESSOR_EVENT = 1;

    public MediaProcessor(boolean isTsSource, boolean useOmx, boolean useAmMediaPlayer) {
        Looper looper;
        if ((looper = Looper.myLooper()) != null) {
            mEventHandler = new EventHandler(looper);
        } else if ((looper = Looper.getMainLooper()) != null) {
            mEventHandler = new EventHandler(looper);
        } else {
            mEventHandler = null;
        }

        nativeSetup(isTsSource, useOmx, useAmMediaPlayer);
    }

    public int setDataSource(String url) {
        return nativeSetDataSource(url);
    }

    public int prepare() {
        return nativePrepare();
    }

    public int setSurface(Surface surface) {
        return nativeSetSurface(surface);
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    public int startPlay() {
        return nativeStartPlay();
    }

    public int pause() {
        return nativePause();
    }

    public int resume() {
        return nativeResume();
    }

    public int seekTo(int msec) {
        return nativeSeekTo(msec);
    }

    public int getCurrentPosition() {
        return nativeGetCurrentPosition();
    }

    public int getDuration() {
        return nativeGetDuration();
    }

    public int stop() {
        return nativeStop();
    }

    public void release() {
        nativeRelease();
    }

    public int invoke(Parcel request, Parcel reply) {
        return nativeInvoke(request, reply);
    }

    static public class TrackInfo implements Parcelable {
        public int getTrackType() {
            return mTrackType;
        }

        public String getLanguage() {
            String language = mFormat.getString(MediaFormat.KEY_LANGUAGE);
            return language == null ? "und" : language;
        }

        public MediaFormat getFormat() {
            if (mTrackType == MEDIA_TRACK_TYPE_TIMEDTEXT
                    || mTrackType == MEDIA_TRACK_TYPE_SUBTITLE) {
                return mFormat;
            }
            return null;
        }

        public static final int MEDIA_TRACK_TYPE_UNKNOWN = 0;
        public static final int MEDIA_TRACK_TYPE_VIDEO = 1;
        public static final int MEDIA_TRACK_TYPE_AUDIO = 2;
        public static final int MEDIA_TRACK_TYPE_TIMEDTEXT = 3;
        public static final int MEDIA_TRACK_TYPE_SUBTITLE = 4;
        public static final int MEDIA_TRACK_TYPE_METADATA = 5;

        final int mTrackType;
        final MediaFormat mFormat;

        TrackInfo(Parcel in) {
            mTrackType = in.readInt();
            String mime = in.readString();
            String language = in.readString();
            mFormat = MediaFormat.createSubtitleFormat(mime, language);

            if (mTrackType == MEDIA_TRACK_TYPE_SUBTITLE) {
                mFormat.setInteger(MediaFormat.KEY_IS_AUTOSELECT, in.readInt());
                mFormat.setInteger(MediaFormat.KEY_IS_DEFAULT, in.readInt());
                mFormat.setInteger(MediaFormat.KEY_IS_FORCED_SUBTITLE, in.readInt());
            }
        }

        TrackInfo(int type, MediaFormat format) {
            mTrackType = type;
            mFormat = format;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(mTrackType);
            dest.writeString(mFormat.getString(MediaFormat.KEY_MIME));
            dest.writeString(getLanguage());

            if (mTrackType == MEDIA_TRACK_TYPE_SUBTITLE) {
                dest.writeInt(mFormat.getInteger(MediaFormat.KEY_IS_AUTOSELECT));
                dest.writeInt(mFormat.getInteger(MediaFormat.KEY_IS_DEFAULT));
                dest.writeInt(mFormat.getInteger(MediaFormat.KEY_IS_FORCED_SUBTITLE));
            }
        }

        @Override
        public String toString() {
            StringBuilder out = new StringBuilder(128);
            out.append(getClass().getName());
            out.append('{');
            switch (mTrackType) {
                case MEDIA_TRACK_TYPE_VIDEO:
                    out.append("VIDEO");
                    break;
                case MEDIA_TRACK_TYPE_AUDIO:
                    out.append("AUDIO");
                    break;
                case MEDIA_TRACK_TYPE_TIMEDTEXT:
                    out.append("TIMEDTEXT");
                    break;
                case MEDIA_TRACK_TYPE_SUBTITLE:
                    out.append("SUBTITLE");
                    break;
                default:
                    out.append("UNKNOWN");
                    break;
            }
            if (mFormat != null) {
                out.append(", " + mFormat.toString());
            }
            out.append("}");
            return out.toString();
        }

        static final Parcelable.Creator<TrackInfo> CREATOR
                = new Parcelable.Creator<TrackInfo>() {
            @Override
            public TrackInfo createFromParcel(Parcel in) {
                return new TrackInfo(in);
            }

            @Override
            public TrackInfo[] newArray(int size) {
                return new TrackInfo[size];
            }
        };
    }

    public TrackInfo[] getTrackInfo() {
        TrackInfo trackInfo[] = getInbandTrackInfo();

        return trackInfo;
    }

    public void selectTrack(int index) {
        selectOrDeselectInbandTrack(index, true);
    }

    public void deselectTrack(int index) {
        selectOrDeselectInbandTrack(index, false);
    }

    private void selectOrDeselectInbandTrack(int index, boolean select) {
        Parcel request = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        try {
            request.writeInt(select ? INVOKE_ID_SELECT_TRACK : INVOKE_ID_DESELECT_TRACK);
            request.writeInt(index);
            invoke(request, reply);
            reply.setDataPosition(0);
        } finally {
            request.recycle();
            reply.recycle();
        }
    }

    public int getSelectedTrack(int trackType) {
        Parcel request = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        try {
            request.writeInt(INVOKE_ID_GET_SELECTED_TRACK);
            request.writeInt(trackType);
            invoke(request, reply);
            reply.setDataPosition(0);
            int inbandTrackIndex = reply.readInt();

            return inbandTrackIndex;
        } finally {
            request.recycle();
            reply.recycle();
        }
    }

    private TrackInfo[] getInbandTrackInfo() {
        Parcel request = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        try {
            request.writeInt(INVOKE_ID_GET_TRACK_INFO);
            invoke(request, reply);
            reply.setDataPosition(0);
            //Log.i(TAG, "reply datasize:"+reply.dataSize() + ", dataPosition:" + reply.dataPosition());
            TrackInfo trackInfo[] = reply.createTypedArray(TrackInfo.CREATOR);
            return trackInfo;
        } finally {
            request.recycle();
            reply.recycle();
        }
    }

    private static native void nativeInit();

    private native void nativeSetup(boolean isTsSource, boolean useOmx, boolean useAmMediaPlayer);

    private native void nativeRelease();

    private native int nativeSetDataSource(String url);

    private native int nativePrepare();

    private native int nativeSetSurface(Surface surface);

    private native int nativeStartPlay();

    private native int nativePause();

    private native int nativeResume();

    private native int nativeSeekTo(int msec);

    private native int nativeStop();

    private native int nativeGetCurrentPosition();

    private native int nativeGetDuration();

    private native int nativeInvoke(Parcel request, Parcel reply);

    private static void postEventFromNative(Object ctc, int what, int arg1, int arg2, String extras, boolean isCTCEvent) {
        final MediaProcessor mediaProcessor = (MediaProcessor) ctc;
        if (mediaProcessor == null) {
            Log.e(TAG, "postEventFromNative null object!");
            return;
        }

        if (mediaProcessor.mEventHandler != null) {
            Message msg = mediaProcessor.mEventHandler.obtainMessage(MEDIA_PROCESSOR_EVENT);

            MediaProcessorEvent ctcEvent = mediaProcessor.new MediaProcessorEvent (
                    what, arg1, arg2, extras, isCTCEvent);

            msg.obj = ctcEvent;
            mediaProcessor.mEventHandler.sendMessage(msg);
        } else {
            Log.e(TAG, "mEventHandler is null, postEventFromNative:" + what);
        }
    }

    public interface EventListener {
        void onEvent(int event, int ext1, int ext2);
        void onCTCEvent(int event, int ext1, int ext2, String extras);
    }

    public class MediaProcessorEvent {
        MediaProcessorEvent(int _what, int _arg1, int _arg2, Object _obj, boolean _isCTCEvent) {
            what = _what;
            arg1 = _arg1;
            arg2 = _arg2;
            obj = _obj;
            isCTCEvent = _isCTCEvent;
        }

        int what;
        int arg1;
        int arg2;
        Object obj;
        boolean isCTCEvent;
    }

    private class EventHandler extends Handler {
        public EventHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MEDIA_PROCESSOR_EVENT:
                    MediaProcessorEvent event = (MediaProcessorEvent)msg.obj;
                    if (mEventListener != null) {
                        if (event.isCTCEvent) {
                            mEventListener.onCTCEvent(event.what, event.arg1, event.arg2, (String)event.obj);
                        } else {
                            mEventListener.onEvent(event.what, event.arg1, event.arg2);
                        }
                    } else {
                        Log.e(TAG, "mEventListener is null!");
                    }
                    break;
            }
    }
    }
}
