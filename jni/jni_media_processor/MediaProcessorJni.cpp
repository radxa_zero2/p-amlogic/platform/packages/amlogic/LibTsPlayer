/**
 * @file 		iptv_player_jni.cpp
 * @author    	zhouyj
 * @date      	2012/9/5
 * @version   	ver 1.0
 * @brief     	定义CTC_MediaProcessor类中方法的jni接口，供上层调用。
 * @attention
*/
#define  LOG_TAG    "MediaProcessorJni"
#include <android_runtime/AndroidRuntime.h>
//#include "android_runtime/android_view_Surface.h"
#include <utils/Log.h>
#include <utils/Mutex.h>
#if ANDROID_PLATFORM_SDK_VERSION <= 27
#include "player.h"
#include "player_type.h"
#include "vformat.h"
#include "aformat.h"
#include <gui/Surface.h>
#endif
#include "android/log.h"
#include "jni.h"
#include "stdio.h"
#include <android/bitmap.h>
#include <string.h>
#include <gui/Surface.h>
#include <android/native_window_jni.h>
#include <system/window.h>
#include <utils/StrongPointer.h>
#include "../media_processor/CTC_Utils.h"
#include "../media_processor/CTC_Log.h"
#include "../CTCMediaPlayer/CTCMediaPlayer.h"
#include <media/stagefright/SurfaceUtils.h>
#include <nativehelper/JNIHelp.h>
#include <media/IMediaHTTPService.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/ammediaplayerext.h>
#include <MediaPlayerFactory.h>
#include "../include/CTC_Common.h"

namespace android {
extern Parcel* parcelForJavaObject(JNIEnv* env, jobject obj);
}

using namespace android;

///////////////////////////////////////////////////////////////////////////////
struct fields_t {
    jfieldID context;
    jmethodID post_event;
};
static fields_t fields;

class JniCTCMediaPlayer {
public:
    JniCTCMediaPlayer(JNIEnv* env, jobject thiz, bool isTsSource, bool useOmx, bool useAmMediaPlayer);
    ~JniCTCMediaPlayer();
    void setDataSource(const char* url);
    int prepare();
    int setSurface(ANativeWindow* nativeWindow);
    int start();
    int stop();
    int pause();
    int resume();
    int seekTo(int msec);
    int getCurrentPosition(int* msec);
    int getDuration(int* msec);
    int invoke(const Parcel& requst, Parcel* reply);
    void reset(JNIEnv* env);

private:
    struct Listener : public MediaPlayerBase::Listener {
        Listener(JniCTCMediaPlayer* cookie)
        : mPlayer(cookie)
        {

        }

        virtual ~Listener() {
            MLOG();
        }

        virtual void notify(int msg, int ext1, int ext2, const Parcel* obj) override {
            mPlayer->notify(msg, ext1, ext2, false);
        }

    private:
        JniCTCMediaPlayer* mPlayer = nullptr;
    };

private:
    static void eventCb(void* handler, aml::IPTV_PLAYER_EVT_E evt, uint32_t param1, uint32_t param2);
    void notify(int msg, int ext1, int ext2, bool isCTCEvent = true);
    void setCTCEventCb(aml::IPTV_PLAYER_EVT_CB cb, void* handler);

    jclass mClass = nullptr;
    jobject mObject = nullptr;
    sp<MediaPlayerBase> mPlayer;
};

JniCTCMediaPlayer::JniCTCMediaPlayer(JNIEnv* env, jobject thiz, bool isTsSource, bool useOmx, bool useAmMediaPlayer)
{
    jclass clazz = env->GetObjectClass(thiz);
    if (clazz == nullptr) {
        ALOGE("can't find com/ctc/MediaProcessor");
        jniThrowException(env, "java/lang/Exception", nullptr);
        return;
    }

    mClass = (jclass)env->NewGlobalRef(clazz);
    mObject = env->NewGlobalRef(thiz);

    player_type type = useAmMediaPlayer ? (player_type)AMMEDIA_PLAYER : (player_type)CTC_MEDIAPLAYER;
    mPlayer = MediaPlayerFactory::createPlayer(type, new Listener(this), getpid());
    if (mPlayer == NULL) {
        ALOGE("createPlayer failed!");
    } else {
        int ret = OK;
        Parcel request;

        request.setDataPosition(0);
        request.writeInt32(useOmx);
        request.setDataPosition(0);
        ret = mPlayer->setParameter(KEY_PARAMETER_CTC_PLAYER_SET_USE_OMX, request);
        if (ret != OK) {
            ALOGE("set useOmx failed!");
        }

        request.setDataPosition(0);
        request.writeInt32(isTsSource);
        request.setDataPosition(0);
        ret = mPlayer->setParameter(KEY_PARAMETER_AML_PLAYER_SET_TS_OR_ES, request);
        if (ret != OK) {
            ALOGE("set useOmx failed!");
        }

        setCTCEventCb(&JniCTCMediaPlayer::eventCb, this);
    }
}

void JniCTCMediaPlayer::eventCb(void* handler, aml::IPTV_PLAYER_EVT_E evt, uint32_t param1, uint32_t param2)
{
    JniCTCMediaPlayer* ctcPlayer = (JniCTCMediaPlayer*)handler;
    ctcPlayer->notify(evt, param1, param2);
}

void JniCTCMediaPlayer::setCTCEventCb(aml::IPTV_PLAYER_EVT_CB cb, void* handler)
{
    int ret = OK;
    Parcel request;
    request.setDataPosition(0);
    request.writeInt64((int64_t)cb);
    request.writeInt64((int64_t) handler);
    request.setDataPosition(0);
    ret = mPlayer->setParameter(KEY_PARAMETER_CTC_PLAYER_SET_EVENT_CB, request);
    if (ret != OK) {
        ALOGE("set event cb failed!");
    }
}

JniCTCMediaPlayer::~JniCTCMediaPlayer()
{
    MLOG("dtor begin, mPlayer = %p", mPlayer.get());

    mPlayer.clear();

    MLOG("dtor end");
}

void JniCTCMediaPlayer::setDataSource(const char* url)
{
    if (mPlayer == nullptr) {
        return;
    }

    mPlayer->setDataSource(nullptr, url, nullptr);
}

int JniCTCMediaPlayer::prepare()
{
    return mPlayer->prepare();
}

int JniCTCMediaPlayer::setSurface(ANativeWindow* nativeWindow)
{
    sp<android::Surface> surface = static_cast<android::Surface*>(nativeWindow);

    if (mPlayer != nullptr) {
        mPlayer->setVideoSurfaceTexture(surface->getIGraphicBufferProducer());
    }

    return 0;
}

int JniCTCMediaPlayer::start()
{
    int ret = 0;

    if (mPlayer != nullptr) {
        ret = mPlayer->start();
    }

    return ret;
}

int JniCTCMediaPlayer::stop()
{
    int ret = 0;

    if (mPlayer != nullptr) {
        ret = mPlayer->stop();
    }

    return ret;
}

int JniCTCMediaPlayer::pause()
{
    return mPlayer->pause();
}

int JniCTCMediaPlayer::resume()
{
    return mPlayer->start();
}

int JniCTCMediaPlayer::seekTo(int msec)
{
    return mPlayer->seekTo(msec);
}

int JniCTCMediaPlayer::getCurrentPosition(int* msec)
{
    if (mPlayer == nullptr) {
        *msec = 0;
        return INVALID_OPERATION;
    }

    return mPlayer->getCurrentPosition(msec);
}

int JniCTCMediaPlayer::getDuration(int* msec)
{
    if (mPlayer == nullptr) {
        *msec = 0;
        return INVALID_OPERATION;
    }

    return mPlayer->getDuration(msec);
}

int JniCTCMediaPlayer::invoke(const Parcel& requst, Parcel* reply)
{
    if (mPlayer == nullptr) {
        return INVALID_OPERATION;
    }

    return mPlayer->invoke(requst, reply);
}

void JniCTCMediaPlayer::reset(JNIEnv* env)
{
    if (mPlayer != nullptr) {
        mPlayer->setNotifyCallback(0);
        setCTCEventCb(0, 0);
        mPlayer->reset();
    }

    env->DeleteGlobalRef(mObject);
    env->DeleteGlobalRef(mClass);
}

void JniCTCMediaPlayer::notify(int msg, int ext1, int ext2, bool isCTCEvent)
{
    JNIEnv* env = AndroidRuntime::getJNIEnv();
    JavaVM* vm = nullptr;
    bool needDetach = false;

    if (env == nullptr) {
        vm = AndroidRuntime::getJavaVM();
        vm->AttachCurrentThread(&env, nullptr);
        needDetach = true;
    }

    jstring extras = nullptr;
    if (isCTCEvent) {
        std::string details = aml::iptvPlayerEvt2Str((aml::IPTV_PLAYER_EVT_E)msg);
        extras = env->NewStringUTF(details.c_str());
    } else {
        extras = env->NewStringUTF("");
    }

    env->CallStaticVoidMethod(mClass, fields.post_event, mObject,
        msg, ext1, ext2, extras, isCTCEvent);

    if (env->ExceptionCheck()) {
        ALOGE("exception occurred while notifying event:%d", msg);
        env->ExceptionClear();
    }

    if (extras != nullptr) {
        env->DeleteLocalRef(extras);
    }

    if (needDetach) {
        vm->DetachCurrentThread();
    }
}

static Mutex sLock;
static void setJniCTCMediaPlayer(JNIEnv* env, jobject thiz, JniCTCMediaPlayer* ctcPlayer)
{
    AutoMutex _l(sLock);
    env->SetLongField(thiz, fields.context, (jlong)ctcPlayer);
}

static JniCTCMediaPlayer* getJniCTCMediaPlayer(JNIEnv* env, jobject thiz)
{
    AutoMutex _l(sLock);

    JniCTCMediaPlayer* ctcPlayer  = (JniCTCMediaPlayer*)env->GetLongField(thiz, fields.context);
    return ctcPlayer;
}

static void nativeInit(JNIEnv* env)
{
    MLOG();

    jclass clazz;
    clazz = env->FindClass("com/ctc/MediaProcessor");
    if (clazz == nullptr) {
        ALOGE("FindClass failed!");
        return;
    }

    fields.context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (fields.context == nullptr) {
        ALOGE("get mNativeContext failed!");
        return;
    }
    ALOGI("field.context:%p", fields.context);

    fields.post_event = env->GetStaticMethodID(clazz, "postEventFromNative", "(Ljava/lang/Object;IIILjava/lang/String;Z)V");
    if (fields.post_event == nullptr) {
        ALOGE("get postEventFromNative failed!");
        return;
    }

    env->DeleteLocalRef(clazz);
}

static void nativeSetup(JNIEnv* env, jobject thiz, jboolean isTsSource, jboolean useOmx, jboolean useAmMediaPlayer)
{
    MLOG("isTsSource:%d, useOmx:%d, useAmMediaPlayer:%d", isTsSource, useOmx, useAmMediaPlayer);

    JniCTCMediaPlayer* ctcPlayer = new JniCTCMediaPlayer(env, thiz, isTsSource, useOmx, useAmMediaPlayer);
    setJniCTCMediaPlayer(env, thiz, ctcPlayer);
}

static void nativeRelease(JNIEnv* env, jobject thiz)
{
    MLOG();

    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);
    setJniCTCMediaPlayer(env, thiz, 0);

    ctcPlayer->reset(env);
    delete ctcPlayer;
}

static int nativeSetDataSource(JNIEnv* env, jobject thiz, jstring path)
{
    MLOG();

    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);
    ALOGI("ctcPlayer:%p", ctcPlayer);
    if (ctcPlayer == nullptr) {
        jniThrowException(env, "java/lang/IllegalArgumentException", nullptr);
        return -1;
    }

    const char* tmp = env->GetStringUTFChars(path, nullptr);
    if (tmp == nullptr) {
        return -1;
    } else if (strlen(tmp) == 0) {
        ALOGE("path is empty!");
        return -1;
    }

    ALOGI("path:%s", tmp);
    ctcPlayer->setDataSource(tmp);

    env->ReleaseStringUTFChars(path, tmp);

    return 0;
}

static int nativePrepare(JNIEnv* env, jobject thiz)
{
    MLOG();
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int ret = ctcPlayer->prepare();

    return ret;
}

static int nativeSetSurface(JNIEnv* env, jobject thiz, jobject jsurface)
{
    MLOG();
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    ANativeWindow* window = ANativeWindow_fromSurface(env, jsurface);

    int ret = ctcPlayer->setSurface(window);

    return ret;
}

static int nativeStartPlay(JNIEnv* env, jobject thiz)
{
    MLOG();
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int ret = ctcPlayer->start();

    return ret;
}

static int nativePause(JNIEnv* env, jobject thiz)
{
    MLOG();
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int ret = ctcPlayer->pause();

    return ret;
}

static int nativeResume(JNIEnv* env, jobject thiz)
{
    MLOG();
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int ret = ctcPlayer->resume();

    return ret;
}

static int nativeSeekTo(JNIEnv* env, jobject thiz, jint msec)
{
    MLOG("msec:%d", msec);
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int ret = ctcPlayer->seekTo(msec);

    return ret;
}

static int nativeStop(JNIEnv* env, jobject thiz)
{
    MLOG();
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int ret = ctcPlayer->stop();

    return ret;
}

static int nativeGetCurrentPosition(JNIEnv* env, jobject thiz)
{
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int msec = 0;

    ctcPlayer->getCurrentPosition(&msec);

    return msec;
}

static int nativeGetDuration(JNIEnv* env, jobject thiz)
{
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);

    int msec = 0;

    ctcPlayer->getDuration(&msec);

    return msec;
}

static int nativeInVoke(JNIEnv* env, jobject thiz, jobject java_request, jobject java_reply)
{
    JniCTCMediaPlayer* ctcPlayer = getJniCTCMediaPlayer(env, thiz);
    Parcel* request = parcelForJavaObject(env, java_request);
    Parcel* reply = parcelForJavaObject(env, java_reply);
    request->setDataPosition(0);

    return ctcPlayer->invoke(*request, reply);
}

static const JNINativeMethod gMethods[] = {
    {"nativeInit", "()V", (void*)nativeInit},
    {"nativeSetup", "(ZZZ)V", (void*)nativeSetup},
    {"nativeRelease", "()V", (void*)nativeRelease},
    {"nativeSetDataSource", "(Ljava/lang/String;)I", (void*)nativeSetDataSource},
    {"nativePrepare", "()I", (void*)nativePrepare},
    {"nativeSetSurface", "(Landroid/view/Surface;)I", (void*)nativeSetSurface},
    {"nativeStartPlay", "()I", (void*)nativeStartPlay},
    {"nativePause", "()I", (void*)nativePause},
    {"nativeResume", "()I", (void*)nativeResume},
    {"nativeSeekTo", "(I)I", (void*)nativeSeekTo},
    {"nativeStop", "()I", (void*)nativeStop},
    {"nativeGetCurrentPosition", "()I", (void*)nativeGetCurrentPosition},
    {"nativeGetDuration", "()I", (void*)nativeGetDuration},
    {"nativeInvoke", "(Landroid/os/Parcel;Landroid/os/Parcel;)I", (void*)nativeInVoke},
};

void registerExtensions();

jint JNI_OnLoad(JavaVM* vm, void* /* reserved */)
{
    MLOG();

    JNIEnv* env = nullptr;
    jint result = -1;

    if (vm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("ERROR: GetEnv failed!");
        goto bail;
    }

    if ( jniRegisterNativeMethods(env, "com/ctc/MediaProcessor", gMethods, sizeof(gMethods)/sizeof(*gMethods)) < 0) {
        ALOGE("register native methods failed!");
        goto bail;
    }

    registerExtensions();

    result = JNI_VERSION_1_4;

bail:
    return result;
}
