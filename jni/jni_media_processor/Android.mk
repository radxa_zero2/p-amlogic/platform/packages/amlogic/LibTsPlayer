LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS := -Wno-unused-parameter
LOCAL_CFLAGS += -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE    := libCTC_MediaProcessorjni

LOCAL_SRC_FILES := MediaProcessorJni.cpp

HARDWARE_PATH := $(TOP)/hardware/amlogic

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../include/android9 \
	$(TOP)/frameworks/native/include \
	frameworks/av/media/libmediaplayerservice \
	vendor/amlogic/common/frameworks/av/mediaextconfig/include \
	$(HARDWARE_PATH)/media/amcodec/include \
	$(HARDWARE_PATH)/LibAudio/amadec/include \
	$(HARDWARE_PATH)/media/amavutils/include \
	$(TOP)/vendor/amlogic/common/external/ffmpeg \
#	$(TOP)/vendor/amlogic/common/iptvmiddlewave/AmIptvMedia/contrib/ffmpeg40 \

LOCAL_SHARED_LIBRARIES += \
			libcutils \
			libutils \
			libbinder \
			liblog \
			libCTC_MediaProcessor \
			libgui \
			libandroid \
			libnativehelper \
			libandroid_runtime \
			libstagefright_foundation \
			libmediaplayerservice

#LOCAL_HEADER_LIBRARIES := libnativebase_headers
#LOCAL_STATIC_LIBRARIES := libarect
ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 29))
LOCAL_STATIC_LIBRARIES := libregistermsext_aml
else ifeq ($(PLATFORM_SDK_VERSION), 28)
LOCAL_STATIC_LIBRARIES := libregistermsext
endif

include $(BUILD_SHARED_LIBRARY)
