#ifndef SUBTITLE_SERVICE_UTILS_H
#define SUBTITLE_SERVICE_UTILS_H

typedef enum {
    SUBTITLE_UNAVAIABLE,
    SUBTITLE_AVAIABLE,
    CC_ADD,
    CC_REMOVED
} SUBTITLE_STATE;

typedef void (*SUBTITLELIS)(SUBTITLE_STATE state, int val);

namespace ctc_subtitle {
void subtitleCreat();
void subtitleShow();
void subtitleOpen(char* path, void* pthis);
void subtitleOpenIdx(int idx);
void subtitleClose();
int subtitleGetTotal();
void subtitleNext();
void subtitlePrevious();
void subtitleShowSub(int pos);
void subtitleOption();
int subtitleGetType();
char* subtitleGetTypeStr();
int subtitleGetTypeDetial();
void subtitleSetTextColor(int color);
void subtitleSetTextSize(int size);
void subtitleSetGravity(int gravity);
void subtitleSetTextStyle(int style);
void subtitleSetPosHeight(int height);
void subtitleSetImgRatio(float ratioW, float ratioH, int maxW, int maxH);
void subtitleClear();
void subtitleResetForSeek();
void subtitleHide();
void subtitleDisplay();
char* subtitleGetCurName();
char* subtitleGetName(int idx);
const char* subtitleGetLanguage();
void subtitleLoad(char* path);
void subtitleSetSurfaceViewParam(int x, int y, int w, int h);
void subtitleSetSubPid(int pid);
int subtitleGetSubHeight();
int subtitleGetSubWidth();
void subtitleSetSubType(int type, int total);
void registerSubtitleMiddleListener();
void subtitle_register(SUBTITLELIS sub);
unsigned int getSubState();
unsigned int getClosedCaptionEnable();
unsigned int getClosedCaptionSelect();
unsigned int getClosedCaptionRemoved();
unsigned int getClosedCaptionAdded();

}

#endif
