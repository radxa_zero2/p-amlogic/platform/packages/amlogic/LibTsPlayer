#ifndef _LEGACY_CTC_MEDIAPROCESSOR_H_
#define _LEGACY_CTC_MEDIAPROCESSOR_H_

#include "CTsPlayer.h"
#include "CTC_Common.h"

namespace aml {
class CTC_MediaProcessor;
};

class CTC_MediaProcessor//:public CTsPlayer
{
public:
		~CTC_MediaProcessor();
		int  GetMediaControlVersion();//获取版本
		int  GetPlayMode();//取得播放模式
		int  SetVideoWindow(int x,int y,int width,int height);//设置视频显示的位置，以及视频显示的宽高
		int  VideoShow(void);//显示视频图像
		int  VideoHide(void);//隐藏视频图像
		void InitVideo(PVIDEO_PARA_T pVideoPara);//初始化视频参数
		void InitAudio(PAUDIO_PARA_T pAudioPara);//初始化音频参数
		bool StartPlay();//开始播放
		bool StartRender();
		int  WriteData(unsigned char* pBuffer, unsigned int nSize);
        int  WriteData(PLAYER_STREAMTYPE_E type, uint8_t* pBuffer, uint32_t nSize, int64_t pts);
		bool Pause();//暂停
		bool Resume();//暂停后的恢复
		bool Fast();//快进或者快退
		bool StopFast();//停止快进或者快退
		bool Stop();//停止
		bool Seek();//定位
		bool SetVolume(int volume);//设定音量
		int  GetVolume();//获取音量
		bool SetRatio(int nRatio);//设定视频显示比例
		int  GetAudioBalance();//获取当前声道
		bool SetAudioBalance(int nAudioBalance);//设置声道
		void GetVideoPixels(int& width, int& height);//获取视频分辩率
		bool IsSoftFit();//判断是否由软件拉伸
		void SetEPGSize(int w, int h);//设置EPG大小
		void SetSurface(android::Surface* pSurface);//设置显示用的surface
		long  GetCurrentPlayTime();
		void InitSubtitle(PSUBTITLE_PARA_T sParam);
		void SwitchSubtitle(int pid);//设置显示用的surface
		bool SubtitleShowHide(bool bShow);
		void playerback_register_evt_cb(IPTV_PLAYER_EVT_CB pfunc, void *hander);
		void RegisterParamEvtCb(void *hander, IPTV_PLAYER_PARAM_Evt_e enEvt, IPTV_PLAYER_PARAM_EVENT_CB  pfunc);
		int playerback_getStatusInfo(IPTV_ATTR_TYPE_e enAttrType, int *value);
		void SwitchAudioTrack_ZTE(PAUDIO_PARA_T pAudioPara);
		void ClearLastFrame() ;
		void BlackOut(int EarseLastFrame) ;
		bool SetErrorRecovery(int mode);
		void GetAvbufStatus(PAVBUF_STATUS pstatus);
		int GetRealTimeFrameRate();
		int GetVideoFrameRate();
		int GetCurrentVidPTS(unsigned long long *pPTS);
        int GetAVPTSDiff(int *avdiff);
		void GetVideoInfo (int *width,int *height ,int  *ratio,int *frameformat);
		int GetPlayerInstanceNo(void);
		void ExecuteCmd(const char* cmd);
		int  EnableAudioOutput();
		int  DisableAudioOutput();
		int  StartVideo();
		int  StopVideo();
		int  StartAudio();
		int  StopAudio();
		int  StartSubtitle();
		int  StopSubtitle();
		int  ClosedCaptionsSelect(int id);
		int ClosedCaptionsEnable();
		void GetDecodeFrameInfo(void *DecoderInfo, TIF_HAL_PlaybackType_t eType);
		int SetEcmCallback(tSetEcmCallback cb);
		void SetSubtitleCallback(SUBTITLELIS sub);
		void SetSurface_ANativeWindow(ANativeWindow* pSurface);
		int GetVideoPTS();
		int GetRatio();
		int GetHDMIState();
		int SetSurfaceVideo(int x ,int y, int width, int height);
		int ClosedCaptionsRemoved();
		void SwitchSubtitle(int pid, int type);
		void SetAudioDelay(char* mode);
		void SwitchVideoTrack(PVIDEO_PARA_T pVideoPara);
		/*end add*/

private:
        friend CTC_MediaProcessor* GetMediaProcessor(int use_omx_decoder, int isDrm);
		CTC_MediaProcessor(int use_omx_decoder = 0);
        aml::SubtitleType convertSubType(SUB_TYPE subType);
        void internalEventCb(aml::IPTV_PLAYER_EVT_E, uint32_t param1, uint32_t param2);

		//CTC_MediaControl * ctc_MediaControl;
        //sp<ITsPlayer> ctc_MediaControl;
        aml::CTC_MediaProcessor* ctc_MediaControl = nullptr;
        void* opaque = nullptr;

        SUBTITLELIS mSubtitleCb = nullptr;

        IPTV_PLAYER_EVT_CB mEventCb = nullptr;
        void* mEventHandler = nullptr;

private:
        CTC_MediaProcessor(const CTC_MediaProcessor&) = delete;
        CTC_MediaProcessor& operator=(const CTC_MediaProcessor&) = delete;
};

// 获取CTC_MediaProcessor 派生类的实例对象。在CTC_MediaProcessor () 这个接口的实现中，需要创建一个
// CTC_MediaProcessor 派生类的实例，然后返回这个实例的指针
//CTC_MediaProcessor* GetMediaProcessor(int use_omx_decoder);  // { return NULL; }

CTC_MediaProcessor* GetMediaProcessor(int use_omx_decoder = 0, int isDrm = 0);

// 获取底层模块实现的接口版本号。将来如果有多个底层接口定义，使得上层与底层之间能够匹配。本版本定义
// 返回为1
int GetMediaProcessorVersion();  //  { return 0; }


#endif

