#ifndef _AML_HAL_UTILS_H__
#define _AML_HAL_UTILS_H__

#include <amports/aformat.h>
#include <amports/vformat.h>
#include "AmlHalPlayerCommon.h"

namespace aml {

AMLOGIC_AUDIO_CODEC_TYPE getAmlAudioCodec(aformat_t aFmt);
AMLOGIC_VIDEO_CODEC_TYPE getAmlVideoCodec(vformat_t vFmt);

const char* convertAudioCodecType2Mime(AMLOGIC_AUDIO_CODEC_TYPE type);
const char* convertVideoCodecType2Mime(AMLOGIC_VIDEO_CODEC_TYPE type);




}

#endif
