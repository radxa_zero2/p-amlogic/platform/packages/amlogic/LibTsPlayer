#ifndef _DVB_H_20200102
#define _DVB_H_20200102
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef int (*dvb_ca_callback) (int type, void* para);

typedef struct _dvb_ca dvb_ca_t;
typedef struct _aml_dvb_init_para aml_dvb_init_para_t;

enum dvb_ca_type
{
    CA_TYPE_UNKNOWN=0,
    CA_TYPE_CLEAR=1,
    CA_TYPE_VMX=2,
    CA_TYPE_CONTAX=3
};

enum init_cas_value
{
    CA_INIT_OK = 0,
    CA_INIT_ERR = -1,
    CA_NOT_SUPPORT_VMX = -2,
    CA_NOT_SUPPORT_CONTAX = -3
};

typedef struct _vmx_init_para
{
    int vpid;
    int vfmt;
    int apid;
    int afmt;
    int spid;
    int sfmt;
    int ecmpid;
    int emmpid;
    int programnum;
    char key_file_path[100];
    char server_ip[100];
    unsigned short server_port;
} vmx_init_para_t;

typedef struct _clear_init_para
{
    int vpid;
    int vfmt;
    int apid;
    int afmt;
    int pkgfmt;
} clear_init_para_t;

struct am_cas_ops {
	void (*ca_lib_init)();
	dvb_ca_t* (*ca_create)(aml_dvb_init_para_t* init_para, int* err);
	int (*ca_start)(dvb_ca_t* ca);
	int (*ca_writedata)(dvb_ca_t* ca, uint8_t* buf, size_t len, void* userdata);
	int (*ca_callback)(dvb_ca_t* ca, dvb_ca_callback cb);
	int (*ca_stop)(dvb_ca_t* ca);
	int (*ca_destory)(dvb_ca_t* ca);
};


struct _aml_dvb_init_para {
    enum dvb_ca_type type;
	struct am_cas_ops ops;
    union {
        clear_init_para_t   clear_para;
        vmx_init_para_t     vmx_para;
    };
};

int AM_register_CTC_CAS(aml_dvb_init_para_t * pCasPara);


/**
 * @brief AmlDVB库初始化，调用一次就可以
 *
 */
void AM_MP_DVB_lib_init();

/**\brief 创建dvb ca实例
 * \param type dvb ca类型
 * \param init_para void*类型, vmx_init_para_t
 * \param[out] err 错误码
 * \return
 *   - 非空 成功, dvb ca实例句柄
 *   - NULL 在err中返回错误代码
 */
dvb_ca_t* AM_MP_DVB_create(aml_dvb_init_para_t* init_para, int* err);

/**
 * @brief 销毁dvb ca实例
 *
 * @param ca dvb ca实例
 * @return int 返回0，成功；其他，失败
 */
int AM_MP_DVB_destory(dvb_ca_t* ca);

/**
 * @brief start play
 *
 * @param ca dvb ca实例
 * @return int 返回0，成功；其他，失败
 */
int AM_MP_DVB_start(dvb_ca_t* ca);

int AM_MP_DVB_stop(dvb_ca_t* ca);

/**
 * @brief write data to dvb
 *
 * @param ca dvb_ca_t handle
 * @param buf input data buffer
 * @param len intput data len
 * @param userdata user data
 * @return int successed write bytes
 */
int AM_MP_DVB_write(dvb_ca_t* ca, uint8_t* buf, size_t len, void* userdata);

int AM_MP_DVB_set_callback(dvb_ca_t* ca, dvb_ca_callback cb);

#ifdef __cplusplus
}
#endif

#endif
