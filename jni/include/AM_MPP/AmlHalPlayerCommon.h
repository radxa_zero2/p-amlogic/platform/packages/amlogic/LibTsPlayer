#ifndef _AML_HAL_PLAYER_COMMON_H_
#define _AML_HAL_PLAYER_COMMON_H_

#include <stdint.h>

namespace aml {
//XXX: MUST keep sync with IHalPlayer::Event
typedef enum AML_AVPLAY_EVENT_E {
    EVENT_VIDEO_STREAM_INFO_CHANGED,
	EVENT_AUDIO_STREAM_INFO_CHANGED,

	EVENT_FIRST_IFRAME_DISPLAYED,
	EVENT_FRAME_DISPLAYED,	//	event->data is HalPtsInfo

	EVENT_VIDEO_END_OF_STREAM,
	EVENT_AUDIO_END_OF_STREAM,

	EVENT_SUBTITLE_DATA,

	EVENT_DECODE_ERROR,

	// used in SKB
	EVENT_AUDIO_RECONFIG,
    //AML_AVPLAY_EVENT_EOS,

    // added in SKB
    EVENT_AUDIO_PCM_DATA,
    EVENT_AV_SYNC_LOCKED,
    EVENT_AUDIO_FIRST_DECODED,
    EVENT_USERDATA_READY,
    EVENT_PLAY_COMPLETED,

    EVENT_BAD_DECODING,
    EVENT_BUFFER_FULL,

} AML_AVPLAY_EVENT_E;


typedef void (*AML_TVS_EVENT)(void* handler, AML_AVPLAY_EVENT_E evt, unsigned long param1, int param2);

typedef int (*AML_SECTION_FILTER_CB)(unsigned int u32AcquiredNum, unsigned char* pstBuf, void* pUserData);

typedef enum
{
    NORMAL          = 0,
    BT              = 1,
    AUDIOMIRROR     = 2,
    REMOTE_SUBMIX   = 3,
    UNKNOWN         = 4,
} AUDIO_OUTPUT_TYPE;

/////////////////////////////////////////////////////////////
// copy from AmlCommon.h
typedef enum AMLOGIC_VIDEO_CODEC_TYPE_e {
	AMLOGIC_VCODEC_TYPE_UNKNOWN = 0,
	AMLOGIC_VCODEC_TYPE_MPEG2 = 1,
	AMLOGIC_VCODEC_TYPE_MPEG4,
	AMLOGIC_VCODEC_TYPE_H263,
	AMLOGIC_VCODEC_TYPE_H264,
	AMLOGIC_VCODEC_TYPE_HEVC,
	AMLOGIC_VCODEC_TYPE_VC1,
	AMLOGIC_VCODEC_TYPE_AVS,
	AMLOGIC_VCODEC_TYPE_AVS2,
} AMLOGIC_VIDEO_CODEC_TYPE;

typedef enum AMLOGIC_AUDIO_CODEC_TYPE_e {
	AMLOGIC_ACODEC_TYPE_UNKNOWN = 0,
	AMLOGIC_ACODEC_TYPE_MP2,
	AMLOGIC_ACODEC_TYPE_AAC,        //      0x0F, 0x11
	AMLOGIC_ACODEC_TYPE_ADPCM,      //      AMLOGIC_ACODEC_TYPE_BLYRAYLPCM?
	AMLOGIC_ACODEC_TYPE_DOLBY_AC3, // ??
	AMLOGIC_ACODEC_TYPE_DOLBY_TRUEHD,
	AMLOGIC_ACODEC_TYPE_DOLBY_AC3PLUS,
	AMLOGIC_ACODEC_TYPE_DTSHD,
} AMLOGIC_AUDIO_CODEC_TYPE;

#define AML_INVALID_HANDLE	(0)
#define AML_NULL			(NULL)

typedef void * AML_HANDLE;
typedef unsigned int AML_U32;
typedef int AML_S32;
typedef void AML_VOID;

typedef int32_t AML_HADECODE_OPENPARAM_S;
typedef uint8_t AML_DMX_DATA_S;

#define AML_SUCCESS		(0)
#define AML_FAILURE	    (-1)
#define AML_TRUE		(1)
#define AML_FALSE		(0)

}

#endif /* _AML_TVS_PLAYER_COMMON_H_ */
