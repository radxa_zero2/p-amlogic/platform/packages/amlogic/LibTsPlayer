#ifndef AML_HALPLAYER_INTF_H_
#define AML_HALPLAYER_INTF_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <log/log.h>
#include <cutils/native_handle.h>
//#include <android/native_window.h>

#include "AmlHalPlayerCommon.h"

#define AUDIO_MAX_TRACKS (10) //keep sync with CTC
struct ANativeWindow;

namespace aml
{

////////////////////////////////////////
typedef struct{
    uint32_t u32AudBufSize;
    uint32_t u32VidBufSize;
} AML_AVPLAY_BUF_SIZE_S;

typedef struct{
    uint32_t u32DemuxId;
    AML_AVPLAY_BUF_SIZE_S stStreamAttr;
} AML_AVPLAY_ATTR_S;

typedef enum {
    AML_AVPLAY_STREAM_TYPE_NONE = -1,
    AML_AVPLAY_STREAM_TYPE_TS,
    AML_AVPLAY_STREAM_TYPE_ES,
} AML_AVPLAY_STREAM_TYPE_E;

////////////////////////////////////////
//add for audio or video change
typedef enum {
    AML_AVPLAY_ATTR_ID_NONE,
    AML_AVPLAY_ATTR_ID_SYNC,
    AML_AVPLAY_ATTR_ID_PCR_PID,
} AML_AVPLAY_ATTR_ID_E;

//add for av sync
typedef enum {
    AML_UNF_SYNC_REF_NONE,
    AML_UNF_SYNC_REF_AUDIO,
    AML_UNF_SYNC_REF_VIDEO,
    AML_UNF_SYNC_REF_PCR,
} AML_AVPLAY_SYNC_MODE_E;

typedef struct{
    AML_AVPLAY_SYNC_MODE_E enSyncRef;
    AML_HANDLE mDemux;
    AML_U32 pcrPid;
} AML_UNF_SYNC_ATTR_S;

typedef union {
    AML_UNF_SYNC_ATTR_S syncAttr;
    AML_U32 pcrPid;
} AML_AVPLAY_ATTR_U;

////////////////////////////////////////
//add for pause
typedef struct{
    uint32_t u32Reserved;
} AML_AVPLAY_PAUSE_OPT_S;

////////////////////////////////////////
typedef struct{
} AML_AVPLAY_RESUME_OPT_S;

////////////////////////////////////////
typedef struct{
} AML_AVPLAY_START_OPT_S;

////////////////////////////////////////
typedef struct{
} AML_AVPLAY_STOP_OPT_S;

////////////////////////////////////////
typedef struct{
} AML_AVPLAY_FLUSH_STREAM_OPT_S;

////////////////////////////////////////
//add for reset
typedef struct{
    uint32_t u32SeekPtsMs;
} AML_AVPLAY_RESET_OPT_S;

////////////////////////////////////////
// add for fast play
typedef enum {
    AML_AVPLAY_TPLAY_DIRECT_NONE,
    AML_AVPLAY_TPLAY_DIRECT_BACKWARD,
    AML_AVPLAY_TPLAY_DIRECT_FORWARD,
} AML_AVPLAY_TPLAY_DIRECT_E;

typedef struct{
    AML_AVPLAY_TPLAY_DIRECT_E enTplayDirect;
    uint32_t u32SpeedInteger;
    uint32_t u32SpeedDecimal;
} AML_AVPLAY_TPLAY_OPT_S;

////////////////////////////////////////
typedef enum {
    AML_VCODEC_MODE_NONE,
    AML_VCODEC_MODE_I,
} AML_AVPLAY_VCODEC_MODE_E;

////////////////////////////////////////
//add for get av info and avsync info
typedef struct {
    int64_t u32LastAudPts;
    int64_t u32LastVidPts;
} AML_AVPLAY_SYNC_STATUS_S;

typedef struct {
    uint32_t usedSize;
    uint32_t totalSize;
    int64_t  bufferedUs;
} AML_AVPLAY_BUFFER_STATUS_S;

typedef struct {
    AML_AVPLAY_SYNC_STATUS_S stSyncStatus;
    AML_AVPLAY_BUFFER_STATUS_S videoBufferStatus;
    AML_AVPLAY_BUFFER_STATUS_S audioBufferStatus;
} AML_AVPLAY_STATUS_INFO_S;

////////////////////////////////////////
//for amlvideo and amlaudio
typedef enum {
    AML_PLAYER_MEDIA_CHAN_NONE,
    AML_PLAYER_MEDIA_CHAN_VID,
    AML_PLAYER_MEDIA_CHAN_AUD,
    AML_PLAYER_MEDIA_CHAN_SUB,
} AML_PLAYER_MEDIA_CHAN_E;

typedef enum {
    AML_PLAYER_ATTR_ID_NONE,
    AML_PLAYER_ATTR_ID_VDEC,
    AML_PLAYER_ATTR_ID_AUD_PID,
    AML_PLAYER_ATTR_ID_VID_PID,
    AML_PLAYER_ATTR_ID_MULTIAUD,
    AML_PLAYER_ATTR_ID_EOSREACHED,
} AML_PLAYER_ATTR_ID_E;

typedef enum {
    AML_UNF_VCODEC_MODE_NORMAL,
} AML_PLAYER_VCODEC_MODE_E;

typedef struct {
    AMLOGIC_VIDEO_CODEC_TYPE enType;
    AML_PLAYER_VCODEC_MODE_E enMode;
    uint32_t u32ErrCover;
    uint32_t u32Priority;
    uint32_t u32Pid;
    uint32_t zorder;
    uint32_t width;
    uint32_t height;
    void* extraData;
    uint32_t extraDataSize;
} AML_UNF_VCODEC_ATTR_S;

typedef struct {
    AMLOGIC_AUDIO_CODEC_TYPE enType;
    AML_HADECODE_OPENPARAM_S stDecodeParam;
    uint32_t u32CurPid;
    uint16_t zorder;
    uint16_t nChannels;
    uint16_t nSampleRate;
    bool trackChanged;
    bool audioFocusedStart;
    void* extraData;
    uint32_t extraDataSize;
} AML_UNF_ACODEC_ATTR_S;

typedef struct {

} AML_UNF_SUB_ATTR_S;

typedef struct {
    AMLOGIC_AUDIO_CODEC_TYPE enType;
    AML_UNF_ACODEC_ATTR_S pstAcodecAttr[AUDIO_MAX_TRACKS];
    uint32_t u32PidNum;
    uint32_t u32CurPid;
    unsigned int pu32AudPid[AUDIO_MAX_TRACKS];
} AML_PLAY_MULTIAUD_ATTR_S;

typedef union {
    AML_UNF_VCODEC_ATTR_S vcodecAttr;
    AML_UNF_ACODEC_ATTR_S audioAttr;
    AML_U32 videoPid;
    AML_S32 eosReached;
    AML_PLAY_MULTIAUD_ATTR_S multiAudioAttr;
} AML_PLAYER_ATTR_U;

////////////////////////////////////////
typedef enum {
    AML_PLAYER_SCREEN_MODE_FIT,
    AML_PLAYER_SCREEN_MODE_STRETCH,
} AML_PLAYER_SCREEN_MODE_E;

typedef enum {
    AML_PLAYER_LAST_FRAME_MODE_BLACK,
    AML_PLAYER_LAST_FRAME_MODE_STILL,
} AML_PLAYER_LAST_FRAME_MODE_E;

typedef struct {
    AML_PLAYER_LAST_FRAME_MODE_E enMode;
    AML_PLAYER_SCREEN_MODE_E screenMode;
} AML_PLAYER_VIDEO_OPEN_OPT_S;

typedef struct {
    bool audioOnly;
} AML_PLAYER_AUDIO_OPEN_OPT_S;

typedef union {
    AML_PLAYER_VIDEO_OPEN_OPT_S videoOptions;
    AML_PLAYER_AUDIO_OPEN_OPT_S audioOptions;
} AML_PLAYER_OPEN_OPT_U;

////////////////////////////////////////
typedef struct {

} AML_PLAYER_VIDEO_START_OPT_S;

typedef struct {

} AML_PLAYER_AUDIO_START_OPT_S;

typedef union {
    AML_PLAYER_VIDEO_START_OPT_S videoOptions;
    AML_PLAYER_AUDIO_START_OPT_S audioOptions;
} AML_PLAYER_START_OPT_U;

////////////////////////////////////////
typedef struct {
    AML_PLAYER_LAST_FRAME_MODE_E enMode;
    uint32_t u32TimeoutMs;
} AML_PLAYER_VIDEO_STOP_OPT_S;

typedef struct {

} AML_PLAYER_AUDIO_STOP_OPT_S;

typedef union {
    AML_PLAYER_VIDEO_STOP_OPT_S videoOptions;
    AML_PLAYER_AUDIO_STOP_OPT_S audioOptions;
} AML_PLAYER_STOP_OPT_U;

////////////////////////////////////////
typedef struct {
    AML_PLAYER_LAST_FRAME_MODE_E enMode;
} AML_PLAYER_VIDEO_CLOSE_OPT_S;

typedef struct {

} AML_PLAYER_AUDIO_CLOSE_OPT_S;

typedef union {
    AML_PLAYER_VIDEO_CLOSE_OPT_S videoOptions;
    AML_PLAYER_AUDIO_CLOSE_OPT_S audioOptions;
} AML_PLAYER_CLOSE_OPT_U;

////////////////////////////////////////
typedef enum {
    AML_PLAYER_CONFIG_AUDIO_MUTE,
} AML_PLAYER_CONFIG_E;

typedef struct {
    bool mute;
} AML_PLAYER_CONFIG_AUDIO_MUTE_S;

typedef union {
    AML_PLAYER_CONFIG_AUDIO_MUTE_S configAudioMute;
} AML_PLAYER_CONFIG_OPT_U;

#ifdef __cplusplus
extern "C" {
#endif

int AML_AVPLAY_GetVersion(const char** versionString = nullptr);
int AML_AVPLAY_GetDefaultConfig(AML_AVPLAY_ATTR_S *attr, AML_AVPLAY_STREAM_TYPE_E stream_type);
int AML_AVPLAY_Create(AML_AVPLAY_ATTR_S *attr, AML_HANDLE *mAVPlay, int zorder);
int AML_AVPLAY_SetStreamType(AML_HANDLE mAVPlay, AML_AVPLAY_STREAM_TYPE_E stream_type);
int AML_AVPLAY_RegisterEvent64(AML_HANDLE mAVPlay, AML_TVS_EVENT event_func, AML_VOID *para);
int AML_AVPLAY_UnRegisterEvent(AML_HANDLE mAVPlay, AML_AVPLAY_EVENT_E event);
int AML_AVPLAY_GetAttr(AML_HANDLE mAVPlay, AML_AVPLAY_ATTR_ID_E attr_id, AML_AVPLAY_ATTR_U *attr);
int AML_AVPLAY_SetAttr(AML_HANDLE mAVPlay, AML_AVPLAY_ATTR_ID_E attr_id, AML_AVPLAY_ATTR_U *attr);
int AML_AVPLAY_Pause(AML_HANDLE mAVPlay, AML_AVPLAY_PAUSE_OPT_S *options);
int AML_AVPLAY_Resume(AML_HANDLE mAVPlay, AML_AVPLAY_RESUME_OPT_S *options);
int AML_AVPLAY_Start(AML_HANDLE mAVPlay, AML_AVPLAY_START_OPT_S *options);
int AML_AVPLAY_Stop(AML_HANDLE mAVPlay, AML_AVPLAY_STOP_OPT_S *options);
int AML_AVPLAY_FlushStream(AML_HANDLE mAVPlay, AML_AVPLAY_FLUSH_STREAM_OPT_S *options);
int AML_AVPLAY_Reset(AML_HANDLE mAVPlay, AML_AVPLAY_RESET_OPT_S *options);
int AML_AVPLAY_Destroy(AML_HANDLE mAVPlay, int zorder);
int AML_AVPLAY_SetDecodeMode(AML_HANDLE mAVPlay, AML_AVPLAY_VCODEC_MODE_E vcodec_mode);
int AML_AVPLAY_GetPlaySpeed(AML_HANDLE mAVPlay, float* speed);
int AML_AVPLAY_Tplay(AML_HANDLE mAVPlay, AML_AVPLAY_TPLAY_OPT_S *options);
int AML_AVPLAY_GetStatusInfo(AML_HANDLE mAVPlay, AML_AVPLAY_STATUS_INFO_S *status);
int AML_AVPLAY_SetVideoSize(AML_HANDLE mAVPlay, int32_t x, int32_t y, int32_t w, int32_t h);
native_handle_t* AML_PLAYER_Get_SideBandHandle(AML_HANDLE mAVPlay); // add for getsideband
int AML_AVPLAY_SetSurface(AML_HANDLE mAVPlay, ANativeWindow* surface);
int AML_AVPLAY_SetNetworkJitter(AML_HANDLE mAVPlay, AML_HANDLE demux, int delayMs);
int AML_AVPLAY_SetAudioOutputType(AML_HANDLE mAVPlay, AUDIO_OUTPUT_TYPE audioOutputType);
int AML_AVPLAY_SetAudioPtsOffset(AML_HANDLE mAVPlay, int ptsOffset);
int AML_AVPLAY_SetVideoPtsOffset(AML_HANDLE mAVPlay, int ptsOffset);
int AML_AVPLAY_SetScreenMode(AML_HANDLE mAVPlay, int screenMode);
int AML_AVPLAY_PushEsBuffer(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E chn, const uint8_t* data, size_t nSize, int64_t us);
int AML_AVPLAY_PushEsBuffer90K(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E chn, const uint8_t* data, size_t nSize, int64_t pts);
int AML_AVPLAY_SetVolume(AML_HANDLE mAVPlay, float volume);

int AML_PLAYER_ChnOpen(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E change_type, AML_PLAYER_OPEN_OPT_U *options);
int AML_PLAYER_ChnClose(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E change_type, AML_PLAYER_CLOSE_OPT_U* options);
int AML_PLAYER_GetAttr(AML_HANDLE mAVPlay, AML_PLAYER_ATTR_ID_E attr, AML_PLAYER_ATTR_U *options);
int AML_PLAYER_SetAttr(AML_HANDLE mAVPlay, AML_PLAYER_ATTR_ID_E attr, AML_PLAYER_ATTR_U *options);
int AML_PLAYER_Start(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E change_type, AML_PLAYER_START_OPT_U *options);
int AML_PLAYER_Stop(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E change_type, AML_PLAYER_STOP_OPT_U *options);
int AML_PLAYER_GetConfig(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E change_type, AML_PLAYER_CONFIG_E config_type, AML_PLAYER_CONFIG_OPT_U* options);
int AML_PLAYER_SetConfig(AML_HANDLE mAVPlay, AML_PLAYER_MEDIA_CHAN_E change_type, AML_PLAYER_CONFIG_E config_type, AML_PLAYER_CONFIG_OPT_U* options);
int AML_PLAYER_GetCCEsData(AML_HANDLE mAVPlay, uint8_t *data, int nSize);

AML_HANDLE AML_DMX_OpenDemux(int zorder);
int AML_DMX_CloseDemux(AML_HANDLE demux);
int AML_DMX_StartDemux(AML_HANDLE demux);
int AML_DMX_StopDemux(AML_HANDLE demux);
int AML_DMX_FlushDemux(AML_HANDLE demux);

//channel represent one pid data
// filter use on table_id
AML_HANDLE AML_DMX_CreateChannel(AML_HANDLE demux, int pid);
int AML_DMX_DestroyChannel(AML_HANDLE demux, AML_HANDLE channel);
int AML_DMX_OpenChannel(AML_HANDLE demux, AML_HANDLE channel);
int AML_DMX_CloseChannel(AML_HANDLE demux, AML_HANDLE channel);
AML_HANDLE AML_DMX_CreateFilter(AML_HANDLE demux, AML_SECTION_FILTER_CB cb, AML_VOID* pUserData, int id);
int AML_DMX_DestroyFilter(AML_HANDLE demux, AML_HANDLE filter);
int AML_DMX_AttachFilter(AML_HANDLE demux, AML_HANDLE filter, AML_HANDLE channel);
int AML_DMX_DetachFilter(AML_HANDLE demux, AML_HANDLE filter, AML_HANDLE channel);

// add for amldemux;
int AML_DMX_PushTsBuffer(AML_HANDLE mAVPlay, AML_HANDLE demux, const uint8_t *data, size_t nSize);

//add for audiosystem
int AML_AUDIOSYSTEM_GetParameters(const char *keys);
int AML_AUDIOSYSTEM_SetParameters(const char *keyValuePairs);

#ifdef __cplusplus
}
#endif

}

#endif
