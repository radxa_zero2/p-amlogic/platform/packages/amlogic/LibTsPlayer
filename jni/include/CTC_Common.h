/*
 * Copyright (C) 2019 Amlogic, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef _CTC_COMMON_H
#define _CTC_COMMON_H

#include <stdint.h>
#include <amports/vformat.h>
#include <amports/aformat.h>

namespace aml {
///////////////////////////////////////////////////////////////////////////////
typedef void * AML_HANDLE;

#define AML_INVALID_HANDLE  (0)

#define AML_SUCCESS     (0)
#define AML_FAILURE     (-1)

#ifndef CTC_DEPRECATED
#define CTC_DEPRECATED __attribute__((deprecated))
#endif

const int64_t kUnknownPTS = INT64_MIN;

///////////////////////////////////////////////////////////////////////////////
/**
 * \brief initial parameters used for CTC_MediaProcessor object
 */
struct CTC_InitialParameter {
    int             userId{-1};             //user identifier
    uint8_t         useOmx{0};             //0 is auto mode, 1 is suggest to use omx decoder.
    uint8_t         isEsSource{0};         //0 is TS source, 1 is ES source
    uint32_t        flags{0};              //opaque flags
    uint8_t         extensionSize{0};      //extension data size
    uint8_t         extension[0];
};

#define CTC_LEGACY_MODE     (1 << 0)

///////////////////////////////////////////////////////////////////////////////
typedef struct{
	uint16_t		pid;			//pid
	uint32_t		nVideoWidth;
	uint32_t		nVideoHeight;
	uint32_t		nFrameRate;
	vformat_t		vFmt;
	uint32_t		nExtraSize;
	uint8_t			*pExtraData;
}VIDEO_PARA_T, *PVIDEO_PARA_T;

#define MAX_VIDEO_PARAM_SIZE 10

typedef struct{
	uint16_t		pid;			//pid
	uint32_t		nChannels;
	uint32_t        nSampleRate;
	aformat_t		aFmt;
    uint32_t        block_align;
    uint32_t        bit_per_sample;
	uint32_t        nExtraSize;
	uint8_t         *pExtraData;
}AUDIO_PARA_T, *PAUDIO_PARA_T;

#define MAX_AUDIO_PARAM_SIZE 10

///////////////////////////////////////////////////////////////////////////////
enum SubtitleType : uint32_t {
    UNKNOWN_SUBTYPE = 0,
    CC              = 2,
    SCTE27          = 3,
    DVB_SUBTITLE    = 4,
    TELETEXT        = 5,
};

typedef struct {
    int Channel_ID;
    vformat_t vfmt;  //Video format
} CC_Param;

typedef struct {
    int SCTE27_PID;
} SCTE27_Param;

typedef struct  {
    int Sub_PID;
    int Composition_Page;
    int Ancillary_Page;
}Dvb_Subtitle_Param;

typedef struct {
    int Teletext_PID;
    int Magzine_No;
    int Page_No;
    int SubPage_No;
} TELETEXT_Param;

typedef struct {
	SubtitleType   sub_type;
    union {
        CC_Param cc_param;
        Dvb_Subtitle_Param dvb_sub_param;
        SCTE27_Param scte27_param;
        TELETEXT_Param tt_param;
    };
}SUBTITLE_PARA_T, *PSUBTITLE_PARA_T;

#define MAX_SUBTITLE_PARAM_SIZE 10

///////////////////////////////////////////////////////////////////////////////
// CTC avsync source
enum CTC_SyncSource {
    CTC_SYNCSOURCE_DEFAULT = 0,
    CTC_SYNCSOURCE_VIDEO,
    CTC_SYNCSOURCE_AUDIO,
    CTC_SYNCSOURCE_PCR,
    CTC_SYNCSOURCE_MAX,
};

typedef enum {
    IPTV_PLAYER_EVT_STREAM_VALID=0, //input stream is invalid
    IPTV_PLAYER_EVT_FIRST_PTS,      //first frame decoded event
    IPTV_PLAYER_EVT_AUDIO_FIRST_DECODED, //audio first frame decoded event
    IPTV_PLAYER_EVT_USERDATA_READY,

    IPTV_PLAYER_EVT_VID_FRAME_ERROR =0x200,
    IPTV_PLAYER_EVT_VID_DISCARD_FRAME,
    IPTV_PLAYER_EVT_VID_DEC_OVERFLOW,
    IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW,
    IPTV_PLAYER_EVT_VID_PTS_ERROR,
    IPTV_PLAYER_EVT_VID_INVALID_DATA,

    IPTV_PLAYER_EVT_VID_BUFF_UNLOAD_START,
    IPTV_PLAYER_EVT_VID_BUFF_UNLOAD_END,
    IPTV_PLAYER_EVT_VID_MOSAIC_START,
    IPTV_PLAYER_EVT_VID_MOSAIC_END,

    IPTV_PLAYER_EVT_AUD_FRAME_ERROR=0x300,
    IPTV_PLAYER_EVT_AUD_DISCARD_FRAME,
    IPTV_PLAYER_EVT_AUD_DEC_OVERFLOW,
    IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW,
    IPTV_PLAYER_EVT_AUD_PTS_ERROR,
    IPTV_PLAYER_EVT_AUD_INVALID_DATA,

    //subtitle
    IPTV_PLAYER_EVT_Subtitle_Available = 0x400,
    IPTV_PLAYER_EVT_Subtitle_Unavailable,
    IPTV_PLAYER_EVT_CC_Removed,
    IPTV_PLAYER_EVT_CC_Added,

    IPTV_PLAYER_EVT_MAX,
} IPTV_PLAYER_EVT_E;
const char* iptvPlayerEvt2Str(IPTV_PLAYER_EVT_E evt);

typedef void (*IPTV_PLAYER_EVT_CB)(void* handler, IPTV_PLAYER_EVT_E evt, uint32_t param1, uint32_t param2);

typedef enum {
    PLAYER_STREAMTYPE_NULL = -1,
    PLAYER_STREAMTYPE_TS,
    PLAYER_STREAMTYPE_VIDEO,
    PLAYER_STREAMTYPE_AUDIO,
    PLAYER_STREAMTYPE_SUBTITLE,

    PLAYER_STREAMTYPE_ES = (PLAYER_STREAMTYPE_VIDEO +
                            PLAYER_STREAMTYPE_AUDIO +
                            PLAYER_STREAMTYPE_SUBTITLE),

    PLAYER_STREAMTYPE_MAX,
} PLAYER_STREAMTYPE_E;
const char* streamType2Str(PLAYER_STREAMTYPE_E type);

typedef enum {
    CONTENTMODE_NULL = -1,
    CONTENTMODE_FULL,               //full screen
    CONTENTMODE_LETTERBOX,          //origin ratio
    CONTENTMODE_WIDTHFULL,          //horizontal scale
    CONTENTMODE_HEIGHFULL,          //vertical scale
    CONTENTMODE_MAX,
} CONTENTMODE_E;

typedef enum {
    ABALANCE_NULL = 0,
    ABALANCE_LEFT,
    ABALANCE_RIGHT,
    ABALANCE_STEREO,
    ABALANCE_MAX,
} ABALANCE_E;

enum CTC_KEY_PARAMETER {
    CTC_KEY_PARAMETER_UNKNOWN           = 0,
    CTC_KEY_PARAMETER_START_VIDEO       = 1,
    CTC_KEY_PARAMETER_STOP_VIDEO        = 2,
    CTC_KEY_PARAMETER_START_AUDIO       = 3,
    CTC_KEY_PARAMETER_STOP_AUDIO        = 4,
    CTC_KEY_PARAMETER_START_SUBTITLE    = 5,
    CTC_KEY_PARAMETER_STOP_SUBTITLE     = 6,
    CTC_KEY_PARAMETER_HDMI_STATE        = 7,
    CTC_KEY_PARAMETER_VIDEO_DELAY       = 8,
    CTC_KEY_PARAMETER_AUDIO_DELAY       = 9,
    CTC_KEY_PARAMETER_CC_ENABLE         = 10,
    CTC_KEY_PARAMETER_CC_REMOVED        = 11,
    CTC_KEY_PARAMETER_PLAYSPEED         = 12,
    CTC_KEY_PARAMETER_GET_AUDIOMUTE     = 13,
    CTC_KEY_PARAMETER_SET_AUDIOMUTE     = 14,
    CTC_KEY_PARAMETER_DEMUX_PCRPID      = 15,

    CTC_KEY_PARAMETER_USER_BASE = 0X1000,
};

typedef struct {
    int nVideoWidth;
    int nVideoHeight;
    int interlaced;
    int nFrameRate;
    int nBitRate;
    int nAspect;            //picture aspect ratio
    uint8_t activeFormatDescription;
    //
    uint8_t enabled;
    int nDecoded;           //total decoded frames
    int nDisplayed;
    int nDecodeErrors;      //decoded error frames count
    int nDecoderOverflows;
    int nDecoderUnderflows;
    int nFramesDropped;
} VIDEO_INFO_T,*PVIDEO_INFO_T;

enum AudioSourceChannelConfiguration {
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_UNKNOWN = 0,
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_C, /**< Center */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_MONO = AUDIO_SOURCE_CHANNEL_CONFIGURATION_C,
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R,  /**< Left and right speakers */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO = AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R,
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R, /**< Left, center and right speakers */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_S, /**< Left, right and surround speakers */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_S, /**< Left, center right and surround speakers */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_SL_RS, /**< Left, right, surround left and surround right */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR, /**< Left, center, right, surround left and surround right */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_LFE, /**< Left, center, right, surround left, surround right and lfe*/
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_5_1 = AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_LFE,
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_RL_RR_LFE, /**< Left, center, right, surround left, surround right, rear left, rear right and lfe */
    AUDIO_SOURCE_CHANNEL_CONFIGURATION_7_1 = AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_RL_RR_LFE,
};

typedef struct {
    int nSampleRate;
    int nBitDepth;          //bits per sample
    int nChannels;
    AudioSourceChannelConfiguration channelConfiguration;
    //
    uint8_t enabled;
    int nDecoded;
    int nOutputSamples;
    int nDecoderErrors;
    int nDecoderOverflows;
    int nDecoderUnderFlows;
    int nSamplesDiscarded;
} AUDIO_INFO_T,*PAUDIO_INFO_T;

typedef struct {
    int width;
    int height;
    int iso639Code[4];
    //
    uint8_t enabled;
    int nDecodeErrors;
} SUBTITLE_INFO_T, *PSUBTITLE_INFO_T;

typedef struct {
    int abuf_size;
    int abuf_data_len;
    int abuf_ms;
    int vbuf_size;
    int vbuf_data_len;
    int vbuf_ms;
} AVBUF_STATUS, *PAVBUF_STATUS;

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief amldvb type define
 *
 */
typedef enum
{
    AMLDVB_CA_TYPE_UNKNOWN=0,
    AMLDVB_CA_TYPE_CLEAR=1,
    AMLDVB_CA_TYPE_VMX=2,
    AMLDVB_CA_TYPE_CONTAX=3
} DVB_CAS_TYPE_E;

typedef struct
{
} DVB_CAS_CLEAR_INIT_PARA;

typedef struct
{
    int ecmpid;
    int emmpid;
    int programnum;
    char key_file_path[100];
    char server_ip[100];
    unsigned short server_port;
} DVB_CAS_VMX_INIT_PARA;

typedef struct
{
} DVB_CAS_CONTAX_INIT_PARA;

typedef struct
{
    DVB_CAS_TYPE_E type;
    union {
        DVB_CAS_CLEAR_INIT_PARA      p_clear_init;
        DVB_CAS_VMX_INIT_PARA        p_vmx_init;
        DVB_CAS_CONTAX_INIT_PARA     p_contax_init;
    };
} DVB_CAS_INIT_PARA;


typedef int (*SECTION_FILTER_CB)(unsigned int u32AcquiredNum, unsigned char* pstBuf, void* pUserData);

///////////////////////////////////////////////////////////////////////////////
/**
 * \brief CTC GetParameter/SetParameter parameters
 */

// add for fast play
typedef enum {
    CTC_PLAYSPEED_DIRECT_NONE,
    CTC_PLAYSPEED_DIRECT_BACKWARD,
    CTC_PLAYSPEED_DIRECT_FORWARD,
} CTC_PLAYSPEED_DIRECT_E;

typedef struct{
    CTC_PLAYSPEED_DIRECT_E enPlaySpeedDirect;
    uint32_t u32SpeedInteger;
    uint32_t u32SpeedDecimal;
} CTC_PLAYSPEED_OPT_S;

// add for av offest
typedef struct{
    uint64_t u64offest;
} CTC_AVOFFEST_OPT_S;

//add for media config - audio mute
typedef struct {
    bool mute;
} CTC_MEDIACONFIG_AUDIOMUTE_S;

//add for hdmi state
typedef struct{
    int state;
} CTC_HDMISTATE_OPT_S;

//add for cc enable state
typedef struct{
    int state;
} CTC_CCENABLESTATE_OPT_S;

//add for cc remove state
typedef struct{
    int state;
} CTC_CCREMOVESTATE_OPT_S;

//add for demux pcrpid
typedef struct{
    unsigned int pcrpid;
} CTC_DEMUX_PCRPID_OPT_S;
}

///////////////////////////////////////////////////////////////////////////////
// for mediaplayer setParameter
namespace android {
enum {
    KEY_PARAMETER_CTC_PLAYER_SET_USE_OMX    = 2101,
    KEY_PARAMETER_CTC_PLAYER_SET_EVENT_CB   = 2102,
};
}
#endif /* ifndef _CTC_COMMON_H */
