#ifndef _TSPLAYER_H_
#define _TSPLAYER_H_
#include <android/log.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <gui/Surface.h>
#include <gui/SurfaceComposerClient.h>
//using namespace android;
extern "C" {
//#include "am_dsc.h"
#include <amports/vformat.h>
#include <amports/aformat.h>
#include <amports/amstream.h>
#include <codec.h>
}

#include <string.h>
#include <utils/Timers.h>
#include <CTC_Common.h>

#include "subtitleservice.h"


#define lock_t          pthread_mutex_t
#define lp_lock_init(x,v)   pthread_mutex_init(x,v)
#define lp_lock(x)      pthread_mutex_lock(x)
#define lp_unlock(x)    pthread_mutex_unlock(x)



#define IN
#define OUT

typedef struct{
	void *(*init)(IN int flags);
  int (*read)(IN void *handle, IN unsigned char *buf, IN int size);
  int (*ready)(IN void *handle);
  int (*exit)(IN void *handle);
}wiptv_callback;


#define TRICKMODE_NONE       0x00
#define TRICKMODE_I          0x01
#define TRICKMODE_I_HEVC     0x07
#define TRICKMODE_FFFB       0x02

typedef enum {
	TIF_HAL_PLAYBACK_TYPE_ALL,
    TIF_HAL_PLAYBACK_TYPE_VIDEO,
    TIF_HAL_PLAYBACK_TYPE_AUDIO,
    TIF_HAL_PLAYBACK_TYPE_SUBTITLES,
    TIF_HAL_PLAYBACK_TYPE_RESERVED
} TIF_HAL_PlaybackType_t;

//for contax
typedef enum
{
	e_DES = 1,
	e_3DES = 2,
	e_AES = 4
}E_EncryptAlgorithm;

typedef enum
{
	e_64_bits = 1,
	e_128_bits = 2,
	e_192_bits = 4,
	e_256_bits = 8
}E_EncryptKeyBits;

typedef enum {
    e_KEY_TYPE_EVEN = 0,   /**< ????*/
    e_KEY_TYPE_ODD = 1,     /**< ????*/
    e_KEY_TYPE_AES_EVEN = 2,
    e_KEY_TYPE_AES_ODD = 3,
    e_KEY_TYPE_AES_IV_EVEN = 4,
    e_KEY_TYPE_AES_IV_ODD = 5,
    e_KEY_FROM_KL = (1<<7)
} E_EncryptKeyType;

typedef enum {
	DSC_KEY_TYPE_EVEN = 0,        /**< DVB-CSA even control word*/
	DSC_KEY_TYPE_ODD = 1,         /**< DVB-CSA odd control word*/
	DSC_KEY_TYPE_AES_EVEN = 2,    /**< AES even control word*/
	DSC_KEY_TYPE_AES_ODD = 3,     /**< AES odd control word*/
	DSC_KEY_TYPE_AES_IV_EVEN = 4, /**< AES-CBC even control word's IV data*/
	DSC_KEY_TYPE_AES_IV_ODD = 5,  /**< AES-CBC odd control word's IV data*/
	DSC_KEY_FROM_KL = (1<<7)      /**< Read the control word from hardware keyladder*/
}DSC_KeyType_t;
//
typedef enum
{
    PLAYER_STREAMTYPE_NULL		= -1,
	PLAYER_STREAMTYPE_TS,					//TS数据
    PLAYER_STREAMTYPE_VIDEO,				//ES Video数据
    PLAYER_STREAMTYPE_AUDIO,				//ES Audio数据
    PLAYER_STREAMTYPE_SUBTITLE,
    PLAYER_STREAMTYPE_MAX,
}PLAYER_STREAMTYPE_E;

typedef struct{
	unsigned short	pid;//pid
	int				nVideoWidth;//��Ƶ����
	int				nVideoHeight;//��Ƶ�߶�
	int				nFrameRate;//֡��
	vformat_t		vFmt;//��Ƶ��ʽ
	unsigned long	cFmt;//�����ʽ
	int stream_index;
	uint32_t		nExtraSize;
	uint8_t			*pExtraData;
}VIDEO_PARA_T, *PVIDEO_PARA_T;
typedef struct{
	unsigned short	pid;//pid
	int				nChannels;//������
	int				nSampleRate;//������
	aformat_t		aFmt;//��Ƶ��ʽ
	int				nExtraSize;
	unsigned char*	pExtraData;
	int stream_index;
}AUDIO_PARA_T, *PAUDIO_PARA_T;

typedef enum {
	/* subtitle codecs */
    CODEC_ID_DVD_SUBTITLE= 0x17000,
    CODEC_ID_DVB_SUBTITLE,
    CODEC_ID_TEXT,  ///< raw UTF-8 text
    CODEC_ID_XSUB,
    CODEC_ID_SSA,
    CODEC_ID_MOV_TEXT,
    CODEC_ID_HDMV_PGS_SUBTITLE,
    CODEC_ID_DVB_TELETEXT,
    CODEC_ID_SRT,
    CODEC_ID_MICRODVD,
    CODEC_ID_SCTE27,
    CODEC_ID_CLOSEDCAPTION,
}SUB_TYPE;

typedef struct{
	unsigned short	pid;//pid
	int sub_type;
	int stream_index;

}SUBTITLE_PARA_T, *PSUBTITLE_PARA_T;

typedef struct ST_LPbuffer{
    unsigned char *rp;
    unsigned char *wp;
    unsigned char *buffer;
    unsigned char *bufferend;
    int valid_can_read;
    bool enlpflag;
}LPBUFFER_T;

typedef enum
{
    IPTV_PLAYER_EVT_STREAM_VALID=0,
    IPTV_PLAYER_EVT_FIRST_PTS,   //�����һ֡
    IPTV_PLAYER_EVT_VOD_EOS,    //VOD�������
    IPTV_PLAYER_EVT_ABEND,         //Ϊ�ϱ������¼������ӵ�����
    IPTV_PLAYER_EVT_PLAYBACK_ERROR,	// ���Ŵ���

    IPTV_PLAYER_EVT_VID_FRAME_ERROR = 0x200,// ��Ƶ�������
    IPTV_PLAYER_EVT_VID_DISCARD_FRAME,// ��Ƶ���붪֡
    IPTV_PLAYER_EVT_VID_DEC_OVERFLOW,
    IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW,// ��Ƶ��������
    IPTV_PLAYER_EVT_VID_INVALID_TIMESTAMP,// ��Ƶ����Pts����
    IPTV_PLAYER_EVT_VID_INVALID_DATA,
    IPTV_PLAYER_EVT_VID_OTHER,

    IPTV_PLAYER_EVT_AUD_FRAME_ERROR = 0x300,// ��Ƶ�������
    IPTV_PLAYER_EVT_AUD_DISCARD_FRAME,// ��Ƶ���붪��
    IPTV_PLAYER_EVT_AUD_DEC_OVERFLOW,
    IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW,//��Ƶ��������ssss
    IPTV_PLAYER_EVT_AUD_INVALID_TIMESTAMP,
    IPTV_PLAYER_EVT_AUD_INVALID_DATA,
    IPTV_PLAYER_EVT_AUD_OTHER,

	IPTV_PLAYER_EVT_SUB_INVALID_TIMESTAMP = 0x400,
	IPTV_PLAYER_EVT_SUB_INVALID_DATA,
	IPTV_PLAYER_EVT_SUB_OTHER,
}IPTV_PLAYER_EVT_e;

typedef struct
{
    E_EncryptAlgorithm etype; //?????
    int keySupport;  //???????,???1,???0
    int keyBits;  //??????????
} Encrypt_info, *pEncrypt_info;

typedef enum {
    IPTV_PLAYER_ATTR_VID_ASPECT=0,  /* ��Ƶ���߱� 0--640*480��1--720*576��2--1280*720��3--1920*1080,4--3840*2160,5--others�ȱ�ʶָ���ֱ���*/
    IPTV_PLAYER_ATTR_VID_RATIO,     //��Ƶ���߱�, 0����4��3��1����16��9
    IPTV_PLAYER_ATTR_VID_SAMPLETYPE,     //֡��ģʽ, 1��������Դ��0��������Դ
    IPTV_PLAYER_ATTR_VIDAUDDIFF,     //����Ƶ����diff
    IPTV_PLAYER_ATTR_VID_BUF_SIZE,     //��Ƶ��������С
    IPTV_PLAYER_ATTR_VID_USED_SIZE,     //��Ƶ������ʹ�ô�С
    IPTV_PLAYER_ATTR_AUD_BUF_SIZE,     //��Ƶ��������С
    IPTV_PLAYER_ATTR_AUD_USED_SIZE,     //��Ƶ��������ʹ�ô�С
    IPTV_PLAYER_ATTR_AUD_SAMPLERATE,     //��Ƶ��������ʹ�ô�С
    IPTV_PLAYER_ATTR_AUD_BITRATE,     //��Ƶ��������ʹ�ô�С
    IPTV_PLAYER_ATTR_AUD_CHANNEL_NUM,     //��Ƶ��������ʹ�ô�С
    IPTV_PLAYER_ATTR_VID_FRAMERATE = 18, //video frame rate
    IPTV_PLAYER_ATTR_BUTT,
    IPTV_PLAYER_ATTR_V_HEIGHT, //video height
    IPTV_PLAYER_ATTR_V_WIDTH,  //video width
    IPTV_PLAYER_ATTR_STREAM_BITRATE,//stream bitrate
    IPTV_PLAYER_ATTR_CATON_TIMES,  //the num of caton
    IPTV_PLAYER_ATTR_CATON_TIME,    //the time of caton total time
    IPTV_PLAYER_ATTR_TS_ERROR,//cc error
    IPTV_PLAYER_ATTR_IMPAIRMENT_DEGREE, // impairment degree
    IPTV_PLAYER_ATTR_IMPAIRMENT_FRE,    //  impairment fre
    IPTV_PLAYER_ATTR_BLOCKING_FRE,    //    blocking fre
}IPTV_ATTR_TYPE_e;

typedef void (*IPTV_PLAYER_EVT_CB)(IPTV_PLAYER_EVT_e evt, void *handler);
typedef enum {
    IPTV_PLAYER_PARAM_EVT_VIDFRM_STATUS_REPORT = 0,
    IPTV_PLAYER_PARAM_EVT_BUTT
}IPTV_PLAYER_PARAM_Evt_e;

typedef void (*IPTV_PLAYER_PARAM_EVENT_CB)( void *hander, IPTV_PLAYER_PARAM_Evt_e enEvt, void *pParam);

typedef struct {
    int abuf_size;
    int abuf_data_len;
    int abuf_free_len;
    int vbuf_size;
    int vbuf_data_len;
    int vbuf_free_len;
}AVBUF_STATUS, *PAVBUF_STATUS;

typedef struct {
    uint32_t height; /**< Height of the display resolution for which the subtitles source in stream has been created in pixels units
                          Can be extracted from DDS info in DVB subtitles (ETSI EN 300 743 V1.5.1 Section 7.2.1)
                          or from display_standard in case SCTE-27 subtitles (SCTE 27 2011 Section 5.3)
                      */
    uint32_t width; /**< Width of the display resolution for which the subtitles source in stream has been created in pixels units
                          Can be extracted from DDS info in DVB subtitles (ETSI EN 300 743 V1.5.1 Section 7.2.1)
                          or from display_standard in case SCTE-27 subtitles (SCTE 27 2011 Section 5.3)
                     */
    char iso639Code[3+1]; /**< Null terminated string containing iso639 language code extracted from the subtitle stream.
                               Only applies on some subtitle codecs (i.e SCTE-27).
                               When subtitles language is not present in the stream empty stream ( iso639Code[0] = '\0') must be returned
                           */
} TIF_HAL_PlaybackSubtitlesSourceInformation_t;

typedef enum {
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_UNKNOWN = 0,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_C, /**< Center */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_MONO = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_C,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R,  /**< Left and right speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R, /**< Left, center and right speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_S, /**< Left, right and surround speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_S, /**< Left, center right and surround speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_SL_RS, /**< Left, right, surround left and surround right */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR, /**< Left, center, right, surround left and surround right */    
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_LFE, /**< Left, center, right, surround left, surround right and lfe*/
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_5_1 = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_LFE,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_RL_RR_LFE, /**< Left, center, right, surround left, surround right, rear left, rear right and lfe */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_7_1 = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_RL_RR_LFE,
} TIF_HAL_Playback_AudioSourceChannelConfiguration_t;

typedef struct {
    uint32_t height; /**< Height of the video source in pixels units */
    uint32_t width; /**< Width of the video source in pixels units */
    float frameRate; /**< Frame rate of the video source in frame per second (or in hz) units, float value to support rational frame rates (i.e 29,97 or 59,94) */
    float pixelAspectRatio; /**< Ratio of a pixel's width to its height. 1.0 indicates square pixel. For some those video formats such as 720x480 4:3 and 720x480 16:9
                                 where pixels are not square floating point value must be set. Examples
                                 - SD 4:3 source:  (4.0f * height) / (3.0f * width) must be returned
                                 - SD 16:9 source: (16.0f * height) / (9.0f * width) must be returned
                                 - HD sources 1920x1080 (16:9) or 1440x1080 (4:3) where pixel aspect is square, 1.0f must be returned.
                             */
    uint8_t  activeFormatDescription; /**< Video Active Format description as defined in ETSI TS 101 154 V1.7.1 Annex B, ATSC A/53 Part 4 and SMPTE 2016-1-2007. */
} TIF_HAL_PlaybackVideoSourceInformation_t;

typedef struct {
    uint32_t sampleRate; /**< Sample rate of the audio source in sample per seconds (or in hz) units */
    uint32_t numChannels; /**< Number of channels of the audio source (i.e 2 for stereo, 6 for 5.1 surround).  See channelConfiguration to more detailed information about speaker layout */
    TIF_HAL_Playback_AudioSourceChannelConfiguration_t channelConfiguration; /**< Configuration or speaker layout of the audio source channels */
} TIF_HAL_PlaybackAudioSourceInformation_t;

typedef struct {
    struct 
    {
        uint8_t enabled; /**< Video decoding is enabled (running) */
        TIF_HAL_PlaybackVideoSourceInformation_t info;
        struct {
            uint32_t numDecodedFrames;
            uint32_t numDisplayedFrames;
            uint32_t numDecoderErrors;    
            uint32_t numDecoderOverflows;
            uint32_t numDecoderUnderflows;
            uint32_t numFramesDropped;
        } statistics;
    } video;
    struct 
    {
        uint8_t enabled; /**< Audio decoding is enabled (running) */
        TIF_HAL_PlaybackAudioSourceInformation_t info;
        struct {
            uint32_t numDecodedSamples;
            uint32_t numOutputSamples;
            uint32_t numDecoderErrors;
            uint32_t numDecoderOverflows;
            uint32_t numDecoderUnderflows;
            uint32_t numSamplesDiscarded;
        } statistics;
    } audio;
    struct 
    {
        uint8_t enabled; /**< Subtitles decoding is enabled (running) */
        TIF_HAL_PlaybackSubtitlesSourceInformation_t info;
        struct {
            uint32_t numDecoderErrors;            
        } statistics;
    } subtitles;
}TIF_HAL_PlaybackStatus_t,*p_DecodeFrameInfo;

typedef enum {
    TIF_HAL_PLAYBACK_ERROR_REASON_OVERFLOW = 1,
    TIF_HAL_PLAYBACK_ERROR_REASON_UNDERFLOW,
    TIF_HAL_PLAYBACK_ERROR_REASON_INVALID_TIMESTAMP,
    TIF_HAL_PLAYBACK_ERROR_REASON_INVALID_DATA,
    TIF_HAL_PLAYBACK_ERROR_REASON_OTHER = 100
} TIF_HAL_PlaybackErrorReason_t;

typedef int (*tSetEcmCallback) (void *ret);

int enable_gl_2xscale(const char *);

int Active_osd_viewport(int , int );

class CTsPlayer;
class ITsPlayer : public android::RefBase{
public:
	ITsPlayer(){}
	virtual ~ITsPlayer(){}
public:
	virtual int  GetPlayMode()=0;
	//��ʾ����
	virtual int  SetVideoWindow(int x,int y,int width,int height)=0;
	//x��ʾ��Ƶ
	virtual int  VideoShow(void)=0;
	//������Ƶ
	virtual int  VideoHide(void)=0;
	//��ʼ����Ƶ����
	virtual void InitVideo(PVIDEO_PARA_T pVideoPara)=0;
	//��ʼ����Ƶ����
	virtual void InitAudio(PAUDIO_PARA_T pAudioPara)=0;
	//��ʼ����
	virtual bool StartPlay()=0;
	//��ts��д��
	virtual int WriteData(unsigned char* pBuffer, unsigned int nSize)=0;
	
	//virtual int WriteData(PLAYER_STREAMTYPE_E type, unsigned char *pBuffer, unsigned int nSize, unsigned long int timestamp);
	virtual int WriteData(PLAYER_STREAMTYPE_E type, unsigned char *pBuffer, unsigned int nSize, unsigned long int timestamp)=0;
	//��ͣ
	virtual bool Pause()=0;
	//��������
	virtual bool Resume()=0;
	//�������
	virtual bool Fast()=0;
	//ֹͣ�������
	virtual bool StopFast()=0;
	//ֹͣ
	virtual bool Stop()=0;
    //��λ
    virtual bool Seek()=0;
    //�趨����
	//�趨����
	virtual bool SetVolume(int volume)=0;
	//��ȡ����
	virtual int GetVolume()=0;
	//�趨��Ƶ��ʾ����
	virtual bool SetRatio(int nRatio)=0;
	//��ȡ��ǰ����
	virtual int GetAudioBalance()=0;
	//��������
	virtual bool SetAudioBalance(int nAudioBalance)=0;
	//��ȡ��Ƶ�ֱ���
	virtual void GetVideoPixels(int& width, int& height)=0;
	virtual bool IsSoftFit()=0;
	virtual void SetEPGSize(int w, int h)=0;
    virtual void SetSurface(android::Surface* pSurface)=0;
	virtual void SetSurface_ANativeWindow(ANativeWindow* pSurface) = 0;
	virtual int UpdateVmxKey()=0;
	
	//16λɫ����Ҫ����colorkey��͸����Ƶ��
     virtual void SwitchAudioTrack(int pid) = 0;
     virtual void SwitchSubtitle(int pid) = 0;
     virtual void SwitchSubtitle(int pid, int type) = 0;
	 virtual bool SubtitleShowHide(bool bShow) = 0;
     virtual void SetProperty(int nType, int nSub, int nValue) = 0;
     virtual int64_t GetCurrentPlayTime() = 0;
     virtual void leaveChannel() = 0;
     virtual void playerback_register_evt_cb(IPTV_PLAYER_EVT_CB pfunc, void *hander) = 0;
	/*ZTEDSP 20140905 ����760D ����ԭʼ�ӿ��л�����������*/
     virtual void SwitchAudioTrack_ZTE(PAUDIO_PARA_T pAudioPara)= 0;
    virtual void SwitchVideoTrack(PVIDEO_PARA_T pVideoPara) = 0;
    virtual void ClearLastFrame() = 0;
    virtual void BlackOut(int EarseLastFrame)= 0;
    virtual bool SetErrorRecovery(int mode) = 0;
    virtual void GetAvbufStatus(PAVBUF_STATUS pstatus) = 0;
    virtual void SetVideoHole(int x,int y,int w,int h) = 0;
    virtual void writeScaleValue() = 0;
    virtual int GetRealTimeFrameRate() = 0;
    virtual int GetVideoFrameRate() = 0;
    virtual int GetVideoDropNumber() = 0;
    virtual int GetVideoTotalNumber() = 0;
	virtual void InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara)=0;
    virtual int GetVideoPTS() = 0;
	virtual int GetCurrentVidPTS(unsigned long long  *pPTS)=0;
    virtual int GetAVPTSDiff(int *avdiff) = 0;
	virtual  void GetVideoInfo (int *width,int *height ,int  *ratio, int *frame_format)=0;
	virtual  int GetPlayerInstanceNo() = 0;
	virtual void ExecuteCmd(const char* cmd_str) = 0;

    // //��audio���
     virtual int EnableAudioOutput() = 0;
    // //�ر�audio���
     virtual int DisableAudioOutput() = 0;
    //��video����
    virtual int StartVideo() = 0;
    //�ر�video����
    virtual int StopVideo() = 0;
    //��audio����
     virtual int StartAudio() = 0;
    // //�ر�audio����
     virtual int StopAudio() = 0;
    //��subtitle����
    virtual int StartSubtitle() = 0;
    //�ر�subtitle����
    virtual int StopSubtitle() = 0;
    virtual int ClosedCaptionsSelect(int id) = 0;
	virtual int  ClosedCaptionsEnable() = 0;
	virtual void GetDecodeFrameInfo(void *DecoderInfo, TIF_HAL_PlaybackType_t eType) = 0;
	virtual int SetEcmCallback(tSetEcmCallback) = 0;
    virtual void SetSubtitleCallback(SUBTITLELIS sub) = 0;
    virtual int GetRatio() = 0;
	virtual int GetChannelConfiguration(int numChannels, int acmod) = 0;
	virtual int GetHDMIState() = 0;
    virtual int ClosedCaptionsAdded() = 0;
    virtual int ClosedCaptionsRemoved() = 0;
    virtual void SetAudioDelay(char* mode) = 0;
    /*end add*/
};

class CTsPlayer : public ITsPlayer
{
public:
	CTsPlayer();
    CTsPlayer(bool omx_player);
	virtual ~CTsPlayer();
public:
	//ȡ�ò���ģʽ
	virtual int  GetPlayMode();
	//��ʾ����
	virtual int  SetVideoWindow(int x,int y,int width,int height);
	//x��ʾ��Ƶ
	virtual int  VideoShow(void);
	//������Ƶ
	virtual int  VideoHide(void);
	//��ʼ����Ƶ����
	virtual void InitVideo(PVIDEO_PARA_T pVideoPara);
	//��ʼ����Ƶ����
	virtual void InitAudio(PAUDIO_PARA_T pAudioPara);
	//
	virtual void InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara);
	//��ʼ����
	virtual bool StartPlay();
	//��ts��д��
	virtual int WriteData(unsigned char* pBuffer, unsigned int nSize);
	virtual int WriteData(PLAYER_STREAMTYPE_E type, unsigned char *pBuffer, unsigned int nSize, unsigned long int timestamp);
	//��ͣ
	virtual bool Pause();
	//��������
	virtual bool Resume();
	//�������
	virtual bool Fast();
	//ֹͣ�������
	virtual bool StopFast();
	//ֹͣ
	virtual bool Stop();
	//��λ
	virtual bool Seek();
	//�趨����
	//�趨����
	virtual bool SetVolume(int volume);
	//��ȡ����
	virtual int GetVolume();
	//�趨��Ƶ��ʾ����
	virtual bool SetRatio(int nRatio);
	//��ȡ��ǰ����
	virtual int GetAudioBalance();
	//��������
	virtual bool SetAudioBalance(int nAudioBalance);
	//��ȡ��Ƶ�ֱ���
	virtual void GetVideoPixels(int& width, int& height);
	virtual bool IsSoftFit();
	virtual void SetEPGSize(int w, int h);
	virtual void SetSurface(android::Surface* pSurface);
	virtual void SetSurface_ANativeWindow(ANativeWindow* pSurface);
	virtual int UpdateVmxKey();
	//16λɫ����Ҫ����colorkey��͸����Ƶ��

    virtual void SwitchAudioTrack(int pid) ;

    virtual void SwitchSubtitle(int pid) ;
    virtual void SwitchSubtitle(int pid, int type);
    virtual bool SubtitleShowHide(bool bShow);
    virtual void SetProperty(int nType, int nSub, int nValue) ;
    
    virtual int64_t GetCurrentPlayTime() ;
    
    virtual void leaveChannel() ;
	virtual void playerback_register_evt_cb(IPTV_PLAYER_EVT_CB pfunc, void *hander);
    /*ZTEDSP 20140905 ����760D ����*/
    virtual void SwitchAudioTrack_ZTE(PAUDIO_PARA_T pAudioPara);
    virtual void SwitchVideoTrack(PVIDEO_PARA_T pVideoPara);
    virtual void ClearLastFrame();

    virtual void BlackOut(int EarseLastFrame) ;
    virtual bool SetErrorRecovery(int mode);

    virtual void GetAvbufStatus(PAVBUF_STATUS pstatus);
    virtual void SetVideoHole(int x,int y,int w,int h);
    virtual void writeScaleValue();
    virtual int GetRealTimeFrameRate();
    virtual int GetVideoFrameRate();
    virtual int GetVideoDropNumber();
    virtual int GetVideoTotalNumber();
    virtual int GetVideoPTS();
	virtual int GetCurrentVidPTS(unsigned long long  *pPTS);
    virtual int GetAVPTSDiff(int *avdiff);
	virtual void GetVideoInfo (int *width,int *height ,int  *ratio, int *frame_format);
	virtual  int GetPlayerInstanceNo();
	virtual void ExecuteCmd(const char* cmd_str);

    // //��audio���
    virtual int EnableAudioOutput();
    // //�ر�audio���
    virtual int DisableAudioOutput();
    //��video����
    virtual int StartVideo();
    //�ر�video����
    virtual int StopVideo();
    // //��audio����
     virtual int StartAudio();
    // //�ر�audio����
    virtual int StopAudio();
    //��subtitle����
    virtual int StartSubtitle();
    //�ر�subtitle����
    virtual int StopSubtitle();
	virtual int ClosedCaptionsSelect(int id);
	virtual int ClosedCaptionsEnable();
    virtual void GetDecodeFrameInfo(void *DecoderInfo, TIF_HAL_PlaybackType_t eType);
	virtual int SetEcmCallback(tSetEcmCallback);
    virtual void SetSubtitleCallback(SUBTITLELIS sub);
    virtual int GetRatio();
	virtual int GetChannelConfiguration(int numChannels, int acmod);
	virtual int GetHDMIState();
    virtual int ClosedCaptionsAdded();
    virtual int ClosedCaptionsRemoved();
    virtual void SetAudioDelay(char* mode);
    /*end add*/
    bool mIsOmxPlayer;
	virtual int createWindowSurface(int x,int y,int width,int height);
protected:
	int		m_bLeaveChannel;
	
private:
	bool mIsEsVideo;
    bool mIsEsAudio;
    bool mHaveVideo;
    bool mHaveAudio;
    bool mIsTsStream;
    int audio_output_type;
    int v_err_reason;
    int a_err_reason;
    int s_err_reason;
    codec_para_t *m_vpcodec;
    codec_para_t *m_apcodec;
    codec_para_t m_vcodec_para;
    codec_para_t m_acodec_para;
	AUDIO_PARA_T a_aPara[MAX_AUDIO_PARAM_SIZE];
	SUBTITLE_PARA_T sPara[MAX_SUBTITLE_PARAM_SIZE];
	VIDEO_PARA_T vPara;
	AUDIO_PARA_T mAudioPara;
	VIDEO_PARA_T mVideoPara;
	codec_para_t m_Tscodec_para;
	codec_para_t *m_Tspcodec;
	int player_pid;
	codec_para_t codec;
	codec_para_t *pcodec;
	int adatalen, vdatalen, sdatalen;
	
	bool		m_bIsPlay;
	int			m_nOsdBpp;
	int			m_nAudioBalance;
	int			m_nVolume;
	int	m_nEPGWidth;
	int 	m_nEPGHeight;
	bool	m_bFast;
	bool    m_bStop;
	bool m_firstStart;
	bool 	m_bSetEPGSize;
    bool    m_bWrFirstPkg;
	int	m_nMode;
    IPTV_PLAYER_EVT_CB pfunc_player_evt;
    void *player_evt_hander;
	unsigned int writecount ;
	int64_t  m_StartPlayTimePoint;
	bool    m_isSoftFit;
	bool    m_isSurfaceWindow;
	int v_overflows;
	int a_overflows;
	int v_underflows;
	int a_underflows;
	int last_anumdecoderErrors;
	int last_vnumdecoderErrors;
    int last_anumdecoder;
	int last_vnumdecoder;
	int sub_enable_output;
	int sub_index;
	FILE*	m_fp;
    lock_t mutex;
    
	pthread_t mThread[2];
	pthread_cond_t m_pthread_cond;

	
	pthread_t mSubThread;
    pthread_t mCheckResetThread;
    virtual void checkAbend();
    virtual int checkBuffLevel();
    virtual void checkBuffstate();
    virtual void checkVideostate();
    virtual void checkAudiostate();
    virtual void checkEncryptAudiostate();
    virtual void restartAudio();
    static void *threadCheckAbend(void *pthis);
	static void *threadSub(void *pthis);
    static void *threadCheckReset(void *pthis);
    bool    m_isBlackoutPolicy;
    bool m_bchangeH264to4k;
    lock_t mutex_lp;
    bool iReset();
    void checkVdecstate();
    bool        m_bIsPause;
    virtual bool  iStartPlay( );
	//for contax
	virtual int DscOpenAndSetSource();
	virtual int iAllocateChannel(int dev_no, int *chan_id);
	virtual int iSetChannelPID(int dev_no, int chan_id, unsigned int pid);
	virtual int iFreeChannelAndDscClose();
	virtual int SetKeyforHwDescrambling(E_EncryptAlgorithm ea, E_EncryptKeyBits ekb, const uint8_t *key, 
											DSC_KeyType_t  key_type, unsigned int pid);
	//virtual int GetEncryptAlgorithm(E_EncryptAlgorithm *ea);
	//virtual int GetEncryptKeyBits(E_EncryptAlgorithm ea, E_EncryptKeyBits *ekb);

	void * stop_thread(void );
    static void * init_thread(void *pthis);

    virtual bool  iStop( );
	virtual int iCheckDecoderOccupied();
	virtual void iGetPlayerAttribute();
	virtual void iStartDrmPlayer();
	virtual void iPrepareAndOpenSubtitle();
	virtual void iMallocLpbuffer();
    int64_t m_PreviousOverflowTime;
    android::sp<android::SurfaceComposerClient> mSoftComposerClient;
    android::sp<android::SurfaceControl> mSoftControl;
    android::sp<android::Surface> mSoftSurface;
};

#endif
