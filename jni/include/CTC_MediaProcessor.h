/*
 * Copyright (C) 2019 Amlogic, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef _CTC_MEDIAPROCESSOR_H_
#define _CTC_MEDIAPROCESSOR_H_

#include "CTC_Common.h"

namespace android {
class Surface;
}
struct ANativeWindow;

namespace aml {
class ITsPlayer;
class CTC_MediaProcessor;

/**
 * \brief GetMediaProcessor
 *
 * \param channelNo the channel number, start from zero
 * \param initialParam initialize parameter
 *
 * \return a pointer of CTC_MediaProcessor object, need delete by user
 */
CTC_MediaProcessor* GetMediaProcessor(CTC_InitialParameter* initialParam = nullptr);

/**
 * \brief GetMediaProcessorVersion
 *
 * \param versionString version string
 *
 * \return CTC_MediaProcessor version number
 */
int GetMediaProcessorVersion(const char** versionString = nullptr);


class CTC_MediaProcessor
{
public:
    virtual ~CTC_MediaProcessor();

public:
    int GetPlayMode();

    /**
     * \brief SetVideoWindow
     *
     * \param x
     * \param y
     * \param width
     * \param height
     *
     * \return
     */
    int SetVideoWindow(int x,int y,int width,int height);

    /**
     * \brief show video
     *
     * \return
     */
    int VideoShow(void);

    /**
     * \brief hide video
     *
     * \return
     */
    int VideoHide(void);

    /**
     * \brief EnableAudioOutput
     *
     * \return
     */
    int EnableAudioOutput();

    /**
     * \brief DisableAudioOUtput
     *
     * \return
     */
    int DisableAudioOutput();

    /**
     * \brief initialize video parameter
     *
     * \param pVideoPara
     */
    void InitVideo(PVIDEO_PARA_T pVideoPara);

    /**
     * \brief initialize audio parameter
     *
     * \param pAudioPara
     */
    void InitAudio(PAUDIO_PARA_T pAudioPara);

    /**
     * \brief initialize subtitle parameter
     *
     * \param sParam
     */
    void InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara);

    /**
     * @brief initialize dvb cas parameter
     *
     * @param pCASInitPara
     */
    int InitCAS(DVB_CAS_INIT_PARA* pCASInitPara);

    /**
     * \brief initSyncSource
     *
     * \param syncSource
     *
     * \return 0 if success, -1 otherwise
     */
    int initSyncSource(CTC_SyncSource syncSource);

    /**
     * \brief StartPlay
     *
     * \return true if success, otherwise false.
     */
    bool StartPlay();

    /**
     * \brief Pause
     *
     * \return
     */
    bool Pause();

    /**
     * \brief resume from paused state
     *
     * \return
     */
    bool Resume();

    /**
     * \brief request decoder enter into fast forward or fast backward state
     *
     * \return
     */
    bool Fast();

    /**
     * \brief return to normal playing state
     *
     * \return
     */
    bool StopFast();

    /**
     * \brief Stop
     *
     * \return
     */
    bool Stop();

    /**
     * \brief flush all buffers and kepp last frame
     *
     * \return
     */
    bool Seek();

    /**
     * \brief SetVolume
     *
     * \param volume 0~100
     *
     * \return
     */
    int SetVolume( float volume);

    /**
     * \brief GetVolume
     *
     * \return
     */
    float GetVolume();

    /**
     * \brief SetRatio
     *
     * \param contentMode
     *
     * \return
     */
    int SetRatio(CONTENTMODE_E contentMode);

    /**
     * \brief GetAudioBalance
     *
     * \return
     */
    int GetAudioBalance();

    /**
     * \brief SetAudioBalance
     *
     * \param nAudioBalance
     *
     * \return
     */
    bool SetAudioBalance(ABALANCE_E nAudioBalance);

    /**
     * \brief GetVideoPixels
     *
     * \param width
     * \param height
     */
    void GetVideoPixels(int& width, int& height);

    /**
     * \brief IsSoftFit
     *
     * \return
     */
    bool IsSoftFit();

    /**
     * \brief SetEPGSize
     *
     * \param w
     * \param h
     */
    void SetEPGSize(int w, int h);

    /**
     * \brief SetSurface
     *
     * \param pSurface pointer to Surface
     */
    void SetSurface(::android::Surface *pSurface);

    /**
     * \brief SetSurface_ANativeWindow
     *
     * \param pSurface
     */
    void SetSurface_ANativeWindow(ANativeWindow* pSurface);

    /**
     * \brief GetWriteBuffer get nSize buffer
     *
     * \param type
     * \param pBuffer
     * \param nSize
     *
     * \return
     */
    int GetWriteBuffer(PLAYER_STREAMTYPE_E type, uint8_t **pBuffer, uint32_t *nSize);

    /**
     * \brief WriteData
     *
     * \param pBuffer
     * \param nSize
     *
     * \return >=0: bytes written
     *        -1: failure
     */
    int WriteData(uint8_t* pBuffer, uint32_t nSize);

    /**
     * \brief WriteData
     *
     * \param type
     * \param pBuffer
     * \param nSize
     * \param pts 90KHz
     *
     * \return
     */
    int WriteData(PLAYER_STREAMTYPE_E type, uint8_t* pBuffer, uint32_t nSize, int64_t pts);

    /**
     * \brief SwitchVideoTrack
     *
     * \param pid
     * \param pVideoPara
     */
    void SwitchVideoTrack(uint16_t pid, PVIDEO_PARA_T pVideoPara);

    /**
     * \brief SwitchAudioTrack
     *
     * \param pid
     * \param pAudioPara
     */
    void SwitchAudioTrack(uint16_t pid, PAUDIO_PARA_T pAudioPara);

    /**
     * \brief SwitchSubtitle
     *
     * \param pid
     * \param pSubtitlePara
     */
    void SwitchSubtitle(uint16_t pid, PSUBTITLE_PARA_T pSubtitlePara);

    /**
     * \brief SubtitleShowHide
     *
     * \param bShow
     *
     * \return
     */
    bool SubtitleShowHide(bool bShow);

    /**
     * \brief GetCurrentPts
     *
     * \param type
     *
     * \return  raw pts in stream, 90k
     */
    int64_t GetCurrentPts(PLAYER_STREAMTYPE_E type);

    /**
     * \brief GetCurrentPlayTime
     *
     * \return us, start from zero.
     */
    int64_t GetCurrentPlayTime();

    /**
     * \brief SetStopMode
     *
     * \param bHoldLastPic
     */
    void SetStopMode(bool bHoldLastPic);

    /**
     * \brief GetBufferStatus
     *
     * \param pstatus
     *
     * \return
     */
    int GetBufferStatus(PAVBUF_STATUS pstatus);

    /**
     * \brief GetStreamInfo
     *
     * \param pVideoInfo
     * \param pAudioInfo
     * \param pSubtitleInfo
     *
     * \return
     */
    int GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo);

    /**
     * \brief for mediaplayer::SetParameter
     *
     * \param key
     * \param request
     */
    void SetParameter(int32_t key, void* request);

    /**
     * \brief GetParameter
     *
     * \param key
     * \param reply
     */
    void GetParameter(int32_t key, void* reply);

    /**
     * \brief playerback_register_evt_cb
     *
     * \param pfunc
     * \param handler
     */
    void playerback_register_evt_cb(IPTV_PLAYER_EVT_CB pfunc, void* handler);

    /**
     * \brief AML_DMX_CreateChannel
     *
     * \param pid
     */
    AML_HANDLE DMX_CreateChannel(int pid);

    /**
     * \brief AML_DMX_DestroyChannel
     *
     * \param channel
     */
    int DMX_DestroyChannel(AML_HANDLE channel);

    /**
     * \brief AML_DMX_OpenChannel
     *
     * \param channel
     * \return
     */
    int DMX_OpenChannel(AML_HANDLE channel);

    /**
     * \brief AML_DMX_CloseChannel
     *
     * \param channel
     * \return
     */
    int DMX_CloseChannel(AML_HANDLE channel);

    /**
     * \brief AML_DMX_CreateFilter
     *
     * \param cb
     * \param pUserData
     * \param id
     */
    AML_HANDLE DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id);

    /**
     * \brief AML_DMX_DestroyFilter
     *
     * \param filter
     * \return
     */
    int DMX_DestroyFilter(AML_HANDLE filter);

    /**
     * \brief AML_DMX_AttachFilter
     *
     * \param filter
     * \param channel
     * \return
     */
    int DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel);

    /**
     * \brief AML_DMX_DetachFilter
     *
     * \param filter
     * \param channel
     * \return
     */
    int DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel);

public:
    IPTV_PLAYER_EVT_CB ctc_func_player_evt = nullptr;
    void *ctc_player_evt_hander = nullptr;

protected:
    friend CTC_MediaProcessor* GetMediaProcessor(CTC_InitialParameter* initialParam);
	CTC_MediaProcessor(CTC_InitialParameter* initialParam);

    ITsPlayer* mTsPlayer = nullptr;
    void* opaque = nullptr;

private:
    CTC_MediaProcessor(const CTC_MediaProcessor&);
    CTC_MediaProcessor& operator=(const CTC_MediaProcessor&);
};

}

///////////////////////////////////////////////////////////////////////////////
//
// The Following is just for backward compatibility
//
///////////////////////////////////////////////////////////////////////////////
#ifdef USE_LEGACY_CTC
#include "legacy_CTC_MediaProcessor.h"
#endif


#endif  // _CTC_MEDIAPROCESSOR_H_
