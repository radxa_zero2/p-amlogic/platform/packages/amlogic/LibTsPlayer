#ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 19))

LOCAL_PATH:= $(call my-dir)

######################################################
include $(CLEAR_VARS)
_value_vformat := $(TELECOM_VFORMAT_SUPPORT)
_value_qos := $(TELECOM_QOS_SUPPORT)
TELECOM_VFORMAT_SUPPORT := false
TELECOM_QOS_SUPPORT := false
include $(LOCAL_PATH)/CTsHwOmxPlayer.mk
TELECOM_VFORMAT_SUPPORT := $(_value_vformat)
TELECOM_QOS_SUPPORT := $(_value_qos)

LOCAL_MODULE    := libctshwomxplayer
include $(BUILD_STATIC_LIBRARY)

######################################################
include $(CLEAR_VARS)
_value_vformat := $(TELECOM_VFORMAT_SUPPORT)
_value_qos := $(TELECOM_QOS_SUPPORT)
TELECOM_VFORMAT_SUPPORT := true
TELECOM_QOS_SUPPORT := true
include $(LOCAL_PATH)/CTsHwOmxPlayer.mk
TELECOM_VFORMAT_SUPPORT := $(_value_vformat)
TELECOM_QOS_SUPPORT := $(_value_qos)

LOCAL_MODULE    := libctshwomxplayer_telecom
#include $(BUILD_STATIC_LIBRARY)

######################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := CTsHwOmxPlayer.cpp
LOCAL_CFLAGS := -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION) \
	-Wno-unused-parameter -Wno-unused-variable

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../../include \
	$(TOP)/hardware/amlogic/media/amcodec/include \
	$(TOP)/hardware/amlogic/media/amavutils/include \
	$(TOP)/vendor/amlogic/common/frameworks/av/mediaextconfig/include

LOCAL_SHARED_LIBRARIES :=libutils libz libbinder liblog libcutils  \
	libstagefright_foundation libnativewindow

LOCAL_HEADER_LIBRARIES := libnativebase_headers

LOCAL_STATIC_LIBRARIES := libarect

ifeq ($(USE_VENDOR_GUI), true)
LOCAL_CFLAGS += \
		-DNO_INPUT \
		-DHAS_VENDOR_GUI
LOCAL_SHARED_LIBRARIES += \
		 libgui_vendor
endif

LOCAL_MODULE    := libctshwomxplayer.vendor
LOCAL_VENDOR_MODULE := true
include $(BUILD_STATIC_LIBRARY)


#endif

