#ifndef __CTC_HWOMXPLAYER_H__
#define __CTC_HWOMXPLAYER_H__

#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/AString.h>
#include <utils/Log.h>
#include <utils/Mutex.h>
#include <media/stagefright/MetaData.h>
#include "../ITsPlayer.h"
#include <cutils/native_handle.h>
#include <AM_MPP/AmlHalPlayer.h>
#include <AM_MPP/AmlHalUtils.h>

namespace aml {

class CTsHwOmxPlayer : public ITsPlayer {
public:
    explicit CTsHwOmxPlayer(const CTC_InitialParameter* initialParam);
    virtual ~CTsHwOmxPlayer();

    virtual int  GetPlayMode() override;
    virtual int  SetVideoWindow(int x,int y,int width,int height) override;
    virtual int  VideoShow(void) override;
    virtual int  VideoHide(void) override;
    virtual int EnableAudioOutput() override;
    virtual int DisableAudioOutput() override;
    virtual void InitVideo(PVIDEO_PARA_T pVideoPara) override;
    virtual void InitAudio(PAUDIO_PARA_T pAudioPara) override;
    virtual void InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara) override;
    virtual int InitCAS(DVB_CAS_INIT_PARA* pCASPara) override;
    virtual int initSyncSource(CTC_SyncSource syncSource) override;
    virtual bool StartPlay() override;
    virtual bool Pause() override;
    virtual bool Resume() override;
    virtual bool Fast() override;
    virtual bool StopFast() override;
    virtual bool Stop() override;
    virtual bool Seek() override;
    virtual bool SetVolume(int volume) override;
    virtual int GetVolume() override;
    virtual bool SetRatio(int nRatio) override;
    virtual int GetAudioBalance() override;
    virtual bool SetAudioBalance(int nAudioBalance) override;
    virtual void GetVideoPixels(int& width, int& height) override;
    virtual bool IsSoftFit() override;
    virtual void SetEPGSize(int w, int h) override;
    virtual void SetSurface(::android::Surface* pSurface) override;
    virtual void SetSurface_ANativeWindow(ANativeWindow* pSurface) override;
    virtual int WriteData(uint8_t* pBuffer, uint32_t nSize) override;
    virtual int WriteData(PLAYER_STREAMTYPE_E type, uint8_t *pBuffer, uint32_t nSize, int64_t timestamp) override;
    virtual void SwitchVideoTrack(PVIDEO_PARA_T pVideoPara) override;
    virtual void SwitchAudioTrack(int pid, PAUDIO_PARA_T pAudioPara) override;
    virtual void SwitchSubtitle(int pid, PSUBTITLE_PARA_T pSubtitlePara) override;
    virtual bool SubtitleShowHide(bool bShow) override;
    virtual int64_t GetCurrentPts(PLAYER_STREAMTYPE_E type) override;
    virtual int64_t GetCurrentPlayTime() override;
    virtual void SetStopMode(bool bHoldLastPic) override;
    virtual int GetBufferStatus(PAVBUF_STATUS pstatus) override;
    virtual int GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo) override;
    virtual int SetParameter(void *hander, int type, void * ptr) override;
    virtual int GetParameter(void *hander, int type, void * ptr) override;
    virtual void ctc_register_evt_cb(CTC_PLAYER_EVT_CB probe_func_player_evt, void *hander) override;
    virtual AML_HANDLE DMX_CreateChannel(int pid);
    virtual int DMX_DestroyChannel(AML_HANDLE channel);
    virtual int DMX_OpenChannel(AML_HANDLE channel);
    virtual int DMX_CloseChannel(AML_HANDLE channel);
    virtual AML_HANDLE DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id);
    virtual int DMX_DestroyFilter(AML_HANDLE filter);
    virtual int DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel);
    virtual int DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel);

protected:
    bool isValidVideoFormat(const aml::AML_UNF_VCODEC_ATTR_S* vdeoFormat);
    bool isValidAudioFormat(const aml::AML_UNF_ACODEC_ATTR_S* audioFormat);
    int initParas();
    int destroyParas();
    int addAndSetCurAudio(PAUDIO_PARA_T pAudioPara);

private:
    void halPlayerEvevntCb(aml::AML_AVPLAY_EVENT_E enEvent, unsigned long param1 = 0, int param2 = 0);

    aml::AML_HANDLE mPlayer = AML_INVALID_HANDLE;
    aml::AML_HANDLE mDemux = AML_INVALID_HANDLE;
    AML_U32 mPcrPid = 0x1FFF;
    aml::AML_AVPLAY_SYNC_MODE_E mAvSyncMode = aml::AML_UNF_SYNC_REF_AUDIO;

    aml::AML_UNF_VCODEC_ATTR_S mVideoFormat;
    aml::AML_PLAY_MULTIAUD_ATTR_S mAudioFormats;
    aml::AML_PLAYER_STOP_OPT_U mVideoStopOption;
    bool mRequestKeepLastFrame;

    CTC_PLAYER_EVT_CB mCTCEventCb = NULL;
    void* mCTCEventHandler = NULL;

    ::android::sp<::ANativeWindow> mSurface;
    bool mStarted = false;
    int mZorder = 0;
    float mVolume = 100.0f;
    //uint64_t mTotalDataReceived = 0;
    int mApkUiWidth = 1920;
    int mApkUiHeight = 1080;
};

}

#endif  //  _CTC_HWOMXPLAYER_H_

