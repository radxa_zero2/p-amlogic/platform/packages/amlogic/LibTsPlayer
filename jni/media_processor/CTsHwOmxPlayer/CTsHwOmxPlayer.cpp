//#define LOG_NDEBUG 0
#define LOG_TAG "CTsHwOmxPlayer"

#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/system_properties.h>
#include <cutils/properties.h>
#include <binder/ProcessState.h>
#include <utils/CallStack.h>
#include <sys/select.h>
#include <amports/amstream.h>
#include <media/ammediaplayerext.h>
#include <system/window.h>
#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
#include <gui/Surface.h>
#endif
#include <Amsysfsutils.h>
#include "CTsHwOmxPlayer.h"
#include "../CTC_Utils.h"
#include "../CTC_Log.h"

namespace aml {

using namespace android;

///////////////////////////////////////////////////////////////////////////////
CTsHwOmxPlayer::CTsHwOmxPlayer(const CTC_InitialParameter* initialParam)
: ITsPlayer(initialParam)
{
    ALOGD("CTsHwOmxPlayer Ctor");

    if (mUserId >= 0) {
        mZorder = mUserId;
    } else {
        mZorder = mPlayerId;
    }

    initParas();
}

CTsHwOmxPlayer::~CTsHwOmxPlayer()
{
    ALOGD("~CTsHwOmxPlayer");

    Stop();

    destroyParas();
}

int CTsHwOmxPlayer::GetPlayMode()
{
    int prop_omxDebug = CTC_getConfig("media.ctcplayer.omxdebug", 0);
    if (prop_omxDebug) {
        return 1;
    }

    return 0;
}

int CTsHwOmxPlayer::SetVideoWindow(int x, int y, int width, int height)
{
    MLOG("x:%d, y:%d, width:%d, height:%d", x, y, width, height);

    return 0;
}

int CTsHwOmxPlayer::VideoShow(void)
{
    MLOG();

    return 0;
}

int CTsHwOmxPlayer::VideoHide(void)
{
    MLOG();

    return 0;
}

int CTsHwOmxPlayer::EnableAudioOutput()
{
    MLOG();

    AML_PLAYER_START_OPT_U startOptions{};
    AML_PLAYER_Start(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, &startOptions);

    AML_PLAYER_CONFIG_OPT_U configOptions{};
    configOptions.configAudioMute.mute = false;
    AML_PLAYER_SetConfig(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, AML_PLAYER_CONFIG_AUDIO_MUTE, &configOptions);

    return 0;
}

int CTsHwOmxPlayer::DisableAudioOutput()
{
    MLOG();

    AML_PLAYER_STOP_OPT_U stopOptions{};
    AML_PLAYER_Stop(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, &stopOptions);

    AML_PLAYER_CONFIG_OPT_U configOptions{};
    configOptions.configAudioMute.mute = true;
    AML_PLAYER_SetConfig(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, AML_PLAYER_CONFIG_AUDIO_MUTE, &configOptions);

    return 0;
}

void CTsHwOmxPlayer::InitVideo(PVIDEO_PARA_T pVideoPara)
{
    MLOG();
    ALOGD("nVideoWidth =%d, nVideoHeight=%d", pVideoPara->nVideoWidth, pVideoPara->nVideoHeight);

    if (mVideoFormat.extraData) {
        free(mVideoFormat.extraData);
        mVideoFormat.extraData = nullptr;
        mVideoFormat.extraDataSize = 0;
    }

    mVideoFormat.enType = getAmlVideoCodec(pVideoPara->vFmt);
    mVideoFormat.u32Pid = pVideoPara->pid;
    mVideoFormat.zorder = mZorder;
    mVideoFormat.width = pVideoPara->nVideoWidth;
    mVideoFormat.height = pVideoPara->nVideoHeight;
    if (pVideoPara->nExtraSize < 1024) { //sanity check
        mVideoFormat.extraDataSize = pVideoPara->nExtraSize;
        mVideoFormat.extraData = calloc(pVideoPara->nExtraSize, 1);
        if (mVideoFormat.extraData) {
            memcpy(mVideoFormat.extraData, pVideoPara->pExtraData, pVideoPara->nExtraSize);
        }
    }
}

void CTsHwOmxPlayer::InitAudio(PAUDIO_PARA_T pAudioPara)
{
    MLOG();

    PAUDIO_PARA_T pAP = pAudioPara;
    int count = 0;
    AML_UNF_ACODEC_ATTR_S* aAttr = nullptr;

    while ((pAP->pid != 0 || pAP->nSampleRate != 0) && (count < MAX_AUDIO_PARAM_SIZE)) {
        aAttr = &mAudioFormats.pstAcodecAttr[count];
        ALOGI("%d, aAttr extraData:%p, extraDataSize:%d", count, aAttr->extraData, aAttr->extraDataSize);
        if (aAttr->extraData) {
            free(aAttr->extraData);
            aAttr->extraData = NULL;
            aAttr->extraDataSize = 0;
        }

        aAttr->enType = getAmlAudioCodec(pAP->aFmt);
        aAttr->u32CurPid = pAP->pid;
        aAttr->nChannels = pAP->nChannels;
        aAttr->nSampleRate = pAP->nSampleRate;
        if (pAP->nExtraSize < 1024) {  //sanity check
            aAttr->extraDataSize = pAP->nExtraSize;
            aAttr->extraData = calloc(aAttr->extraDataSize, 1);
            if (aAttr->extraData) {
                memcpy(aAttr->extraData, pAP->pExtraData, pAP->nExtraSize);
            }
        }

        mAudioFormats.pu32AudPid[count] = pAP->pid;

        pAP++;
        count++;
    }

    mAudioFormats.u32PidNum = count;
    if (count > 0) {
        mAudioFormats.u32CurPid = mAudioFormats.pu32AudPid[0];
    }

    ALOGI("audio para count:%d", count);
}

void CTsHwOmxPlayer::InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara)
{
    MLOG();
}

int CTsHwOmxPlayer::InitCAS(DVB_CAS_INIT_PARA* pCASPara)
{
    MLOG();

    return 0;
}

int CTsHwOmxPlayer::initSyncSource(CTC_SyncSource syncSource)
{
    MLOG("syncSource:%d", syncSource);

    if (syncSource == CTC_SYNCSOURCE_PCR) {
        mAvSyncMode = aml::AML_UNF_SYNC_REF_PCR;
    } else {
        mAvSyncMode = aml::AML_UNF_SYNC_REF_AUDIO;
    }

    return 0;
}

bool CTsHwOmxPlayer::StartPlay()
{
    MLOG();

    //init demux
    if (mAvSyncMode == AML_UNF_SYNC_REF_PCR) {
        ALOGI("pcr sync mode, create demux");
        if (mDemux == AML_INVALID_HANDLE)
            mDemux = AML_DMX_OpenDemux(mZorder);
    }

    //init player
    AML_AVPLAY_Create(nullptr, &mPlayer, mZorder);

    AML_AVPLAY_STREAM_TYPE_E streamType = AML_AVPLAY_STREAM_TYPE_NONE;
    if (mStreamType == PLAYER_STREAMTYPE_ES) {
        streamType = AML_AVPLAY_STREAM_TYPE_ES;
    } else if (mStreamType == PLAYER_STREAMTYPE_TS) {
        streamType = AML_AVPLAY_STREAM_TYPE_TS;
    }

    AML_AVPLAY_SetStreamType(mPlayer, streamType);

    //set stc
    AML_AVPLAY_ATTR_U attr;
    attr.syncAttr.mDemux = mDemux;
    attr.syncAttr.pcrPid = mPcrPid;
    attr.syncAttr.enSyncRef = mAvSyncMode;
    AML_AVPLAY_SetAttr(mPlayer, AML_AVPLAY_ATTR_ID_SYNC, &attr);
    MLOG("mPcrPid:%d", mPcrPid);

    AML_AVPLAY_SetAudioOutputType(mPlayer, AUDIO_OUTPUT_TYPE::NORMAL);

    AML_AVPLAY_SetNetworkJitter(mPlayer, mDemux, 300);

    AML_AVPLAY_SetSurface(mPlayer, mSurface.get());

    AML_AVPLAY_RegisterEvent64(mPlayer, [](void* handler, AML_AVPLAY_EVENT_E event, unsigned long param1, int param2) {
            CTsHwOmxPlayer* player = (CTsHwOmxPlayer*)handler;
            player->halPlayerEvevntCb(event, param1, param2);
        }, this);

    AML_AVPLAY_SetVolume(mPlayer, mVolume);

    //init audio channel
    AML_PLAYER_OPEN_OPT_U openOptions;
    openOptions.audioOptions.audioOnly = mVideoFormat.u32Pid == 0x1FFF;
    AML_PLAYER_ChnOpen(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, &openOptions);

    AML_PLAYER_ATTR_U* pAttr = nullptr;

    pAttr = (AML_PLAYER_ATTR_U*)&mAudioFormats;
    AML_PLAYER_SetAttr(mPlayer, AML_PLAYER_ATTR_ID_MULTIAUD, pAttr);

    //default play the first audio
    AML_UNF_ACODEC_ATTR_S* audioAttr = &mAudioFormats.pstAcodecAttr[0];

    if (isValidAudioFormat(audioAttr)) {
        pAttr = (AML_PLAYER_ATTR_U*)audioAttr;
        AML_PLAYER_SetAttr(mPlayer, AML_PLAYER_ATTR_ID_AUD_PID, pAttr);
    }

    //init video channel
    openOptions.videoOptions.enMode = mRequestKeepLastFrame ? AML_PLAYER_LAST_FRAME_MODE_STILL : AML_PLAYER_LAST_FRAME_MODE_BLACK;
    openOptions.videoOptions.screenMode = AML_PLAYER_SCREEN_MODE_STRETCH;
    AML_PLAYER_ChnOpen(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, &openOptions);

    if (isValidVideoFormat(&mVideoFormat)) {
        pAttr = (AML_PLAYER_ATTR_U*)&mVideoFormat;
        AML_PLAYER_SetAttr(mPlayer, AML_PLAYER_ATTR_ID_VDEC, pAttr);
    }

    int videoPid = mVideoFormat.u32Pid;
    pAttr = (AML_PLAYER_ATTR_U*)&videoPid;
    AML_PLAYER_SetAttr(mPlayer, AML_PLAYER_ATTR_ID_VID_PID, pAttr);

    //start demux
    if (mDemux != AML_INVALID_HANDLE) {
        AML_DMX_StartDemux(mDemux);
    }

    //start audio channel
    AML_PLAYER_START_OPT_U startOptions;
    AML_PLAYER_Start(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, &startOptions);

    //start video channel
    AML_PLAYER_Start(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, &startOptions);

    //start player
    if (mPlayer != AML_INVALID_HANDLE) {
        AML_AVPLAY_Start(mPlayer, nullptr);
    }

    mStarted = true;

    MLOG();
    return true;
}

bool CTsHwOmxPlayer::Pause()
{
    AML_AVPLAY_PAUSE_OPT_S options;
    options.u32Reserved = 0;

    AML_AVPLAY_Pause(mPlayer, &options);

    return true;
}

bool CTsHwOmxPlayer::Resume()
{
    AML_AVPLAY_Resume(mPlayer, NULL);

    return true;
}

bool CTsHwOmxPlayer::Fast()
{
    AML_AVPLAY_TPLAY_OPT_S options;
    options.enTplayDirect = AML_AVPLAY_TPLAY_DIRECT_NONE;
    options.u32SpeedInteger = 2;
    options.u32SpeedDecimal = 0;

    AML_AVPLAY_Tplay(mPlayer, &options);

    return true;
}

bool CTsHwOmxPlayer::StopFast()
{
    AML_AVPLAY_TPLAY_OPT_S options;
    options.enTplayDirect = AML_AVPLAY_TPLAY_DIRECT_NONE;
    options.u32SpeedInteger = 1;
    options.u32SpeedDecimal = 0;

    AML_AVPLAY_Tplay(mPlayer, &options);

    return true;
}

bool CTsHwOmxPlayer::Stop()
{
    MLOG();

    if (!mStarted)
        return true;

#if 0
    //stop video channel
    mVideoStopOption.videoOptions.enMode = mRequestKeepLastFrame ? AML_PLAYER_LAST_FRAME_MODE_STILL : AML_PLAYER_LAST_FRAME_MODE_BLACK;
    AML_PLAYER_Stop(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, &mVideoStopOption);

    //stop audio channel
    AML_PLAYER_STOP_OPT_U stopOptions;
    memset(&stopOptions, 0, sizeof(stopOptions));
    AML_PLAYER_Stop(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, &stopOptions);
#endif

    //close video channel
    AML_PLAYER_CLOSE_OPT_U closeOptions;
    closeOptions.videoOptions.enMode = mRequestKeepLastFrame ? AML_PLAYER_LAST_FRAME_MODE_STILL : AML_PLAYER_LAST_FRAME_MODE_BLACK;
    AML_PLAYER_ChnClose(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, &closeOptions);

    //close audio channel
    memset(&closeOptions, 0, sizeof(closeOptions));
    AML_PLAYER_ChnClose(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, &closeOptions);

    //stop demux
    if (mDemux != AML_INVALID_HANDLE) {
        AML_DMX_StopDemux(mDemux);
    }

    //stop player
    if (mPlayer != AML_INVALID_HANDLE) {
        AML_AVPLAY_Stop(mPlayer, nullptr);
    }

    //close demux
    if (mPlayer != AML_INVALID_HANDLE) {
        AML_DMX_CloseDemux(mDemux);
        mDemux = AML_INVALID_HANDLE;
    }

    //destroy player
    if (mPlayer != AML_INVALID_HANDLE) {
        AML_AVPLAY_Destroy(mPlayer, mZorder);
        mPlayer = AML_INVALID_HANDLE;
    }

    destroyParas();

    initParas();

    mStarted = false;

    return true;
}

bool CTsHwOmxPlayer::Seek()
{
    MLOG();

    AML_AVPLAY_FlushStream(mPlayer, NULL);

    return true;
}

bool CTsHwOmxPlayer::SetVolume(int volume)
{
    ALOGD("SetVolume volume = %d", volume);

    mVolume = volume;

    if (mPlayer != AML_INVALID_HANDLE) {
        AML_AVPLAY_SetVolume(mPlayer, volume);
    }

    return true;
}

int CTsHwOmxPlayer::GetVolume()
{
    return mVolume;

    return 0;
}

bool CTsHwOmxPlayer::SetRatio(int nRatio)
{
    MLOG("nRatio:%d", nRatio);

    AML_AVPLAY_SetScreenMode(mPlayer, nRatio);

    return true;
}

int CTsHwOmxPlayer::GetAudioBalance()
{
    MLOG();

    return 0;
}

bool CTsHwOmxPlayer::SetAudioBalance(int nAudioBalance)
{
    MLOG();

    return true;
}

void CTsHwOmxPlayer::GetVideoPixels(int& width, int& height)
{
    MLOG();

    char value[PROPERTY_VALUE_MAX] = {0};
    if (property_get("ubootenv.var.uimode", value, NULL) > 0) {
        LOGI("ubootenv.var.uimode=%s", value);
        if (!strcmp(value,"1080p")) {
            width = 1920;
            height = 1080;
        }

        else if (!strcmp(value,"720p")) {
            width = 1280;
            height = 720;
        }
        else {
            width = 1280;
            height = 720;
        }
    } else {
        width = 1280;
        height = 720;
        LOGW("Can not read property ubootenv.var.uimode, using width=%d, height=%d\n", width, height);
    }
}

bool CTsHwOmxPlayer::IsSoftFit()
{
    return true;
}

void CTsHwOmxPlayer::SetEPGSize(int w, int h)
{
    ALOGV("SetEPGSize: w=%d, h=%d\n", w, h);
    mApkUiWidth = w;
    mApkUiHeight = h;
}

void CTsHwOmxPlayer::SetSurface(Surface* pSurface)
{
#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
    MLOG("pSurface: %p, refCount:%d", pSurface, pSurface->getStrongCount());

    mSurface = pSurface;
#endif
}

void CTsHwOmxPlayer::SetSurface_ANativeWindow(ANativeWindow* pSurface)
{
    MLOG("pSurface:%p", pSurface);

    mSurface = pSurface;
}

int CTsHwOmxPlayer::WriteData(uint8_t* pBuffer, uint32_t nSize)
{
    status_t ret = 0;

    ret = AML_DMX_PushTsBuffer(mPlayer, mDemux, pBuffer, nSize);

    return ret;
}

int CTsHwOmxPlayer::WriteData(PLAYER_STREAMTYPE_E type, uint8_t *pBuffer, uint32_t nSize, int64_t pts)
{
    status_t ret = 0;

    AML_PLAYER_MEDIA_CHAN_E channel = AML_PLAYER_MEDIA_CHAN_NONE;
    if (type == PLAYER_STREAMTYPE_AUDIO) {
        channel = AML_PLAYER_MEDIA_CHAN_AUD;
    } else if (type == PLAYER_STREAMTYPE_VIDEO) {
        channel = AML_PLAYER_MEDIA_CHAN_VID;
    } else if (type == PLAYER_STREAMTYPE_SUBTITLE) {
        channel = AML_PLAYER_MEDIA_CHAN_SUB;
    } else if (type == PLAYER_STREAMTYPE_TS) {
        return WriteData(pBuffer, nSize);
    } else {
        MLOGE("streamType = %d", type);
        TRESPASS();
    }

    ret = AML_AVPLAY_PushEsBuffer90K(mPlayer, channel, pBuffer, nSize, pts);

    return ret;
}

void CTsHwOmxPlayer::SwitchVideoTrack(PVIDEO_PARA_T pVideoPara)
{
    AML_UNF_VCODEC_ATTR_S videoFormat{};

    videoFormat.enType = getAmlVideoCodec(pVideoPara->vFmt);
    videoFormat.u32Pid = pVideoPara->pid;
    videoFormat.zorder = mZorder;
    videoFormat.width = pVideoPara->nVideoWidth;
    videoFormat.height = pVideoPara->nVideoHeight;
    if (pVideoPara->nExtraSize < 1024) {
        videoFormat.extraDataSize = pVideoPara->nExtraSize;
        videoFormat.extraData = calloc(pVideoPara->nExtraSize, 1);
        if (videoFormat.extraData) {
            memcpy(videoFormat.extraData, pVideoPara->pExtraData, pVideoPara->nExtraSize);
        }
    }

    AML_PLAYER_Stop(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, NULL);

    AML_PLAYER_ATTR_U* pAttr = nullptr;
    pAttr = (AML_PLAYER_ATTR_U*)&videoFormat;
    AML_PLAYER_SetAttr(mPlayer, AML_PLAYER_ATTR_ID_VDEC, pAttr);

    AML_PLAYER_Start(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, NULL);

    if (videoFormat.extraData) {
        free(videoFormat.extraData);
        videoFormat.extraData = nullptr;
    }
}

void CTsHwOmxPlayer::SwitchAudioTrack(int pid, PAUDIO_PARA_T pAudioPara)
{
    AML_UNF_ACODEC_ATTR_S* aAttr = nullptr;
    for (size_t i = 0; i < mAudioFormats.u32PidNum; ++i) {
        if (mAudioFormats.pu32AudPid[i] == (uint32_t)pid) {
            aAttr = &mAudioFormats.pstAcodecAttr[i];
            break;
        }
    }

    if (aAttr == nullptr) {
        ALOGW("can't find pid:%d", pid);
        aAttr = &mAudioFormats.pstAcodecAttr[addAndSetCurAudio(pAudioPara)];
    }

    AML_PLAYER_Stop(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, NULL);

    AML_PLAYER_ATTR_U options;
    options.audioAttr = *aAttr;
    AML_PLAYER_SetAttr(mPlayer, AML_PLAYER_ATTR_ID_AUD_PID, &options);

    AML_PLAYER_Start(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, NULL);
}

void CTsHwOmxPlayer::SwitchSubtitle(int pid, PSUBTITLE_PARA_T pSubtitlePara)
{
    MLOG();
}

bool CTsHwOmxPlayer::SubtitleShowHide(bool bShow)
{
    MLOG();

    return true;
}

int64_t CTsHwOmxPlayer::GetCurrentPts(PLAYER_STREAMTYPE_E type)
{
    AML_AVPLAY_STATUS_INFO_S status = {};

    int64_t currentPts = INT64_MIN;

    AML_AVPLAY_GetStatusInfo(mPlayer, &status);

    if (type == PLAYER_STREAMTYPE_VIDEO) {
        currentPts = status.stSyncStatus.u32LastVidPts;
    } else {
        currentPts = status.stSyncStatus.u32LastAudPts;
    }

    return currentPts;
}

int64_t CTsHwOmxPlayer::GetCurrentPlayTime()
{
    MLOG();

    return 0;
}

void CTsHwOmxPlayer::SetStopMode(bool bHoldLastPic)
{
    MLOG("bHoldLastPic:%d", bHoldLastPic);

    mRequestKeepLastFrame = bHoldLastPic;
}

int CTsHwOmxPlayer::GetBufferStatus(PAVBUF_STATUS pstatus)
{
    AML_AVPLAY_STATUS_INFO_S status;
    memset(&status, 0, sizeof(status));

    int ret = AML_AVPLAY_GetStatusInfo(mPlayer, &status);
    if (AML_SUCCESS == ret) {
        pstatus->abuf_data_len = status.audioBufferStatus.usedSize;
        pstatus->abuf_size = status.audioBufferStatus.totalSize;
        pstatus->abuf_ms = status.audioBufferStatus.bufferedUs/1000;

        pstatus->vbuf_data_len = status.videoBufferStatus.usedSize;
        pstatus->vbuf_size = status.videoBufferStatus.totalSize;
        pstatus->vbuf_ms = status.videoBufferStatus.bufferedUs/1000;
    }

    return 0;
}

int CTsHwOmxPlayer::GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo)
{
    if (pVideoInfo) {
        memset(pVideoInfo, 0, sizeof(*pVideoInfo));
    }

    if (pAudioInfo) {
        memset(pAudioInfo, 0, sizeof(*pAudioInfo));
    }

    if (pSubtitleInfo) {
        memset(pSubtitleInfo, 0, sizeof(*pSubtitleInfo));
    }

    return 0;
}

int CTsHwOmxPlayer::SetParameter(void *hander, int type, void * ptr)
{
    switch (type) {
        case KEY_PARAMETER_AML_PLAYER_SET_TS_OR_ES:
        {
            int isTSdata = *(int *)ptr;
            ALOGI("CTsHwOmxPlayer SetParameter mIsTSdata = %d\n", isTSdata);
            if (!isTSdata)
                mStreamType = aml::PLAYER_STREAMTYPE_ES;
            else
                mStreamType = aml::PLAYER_STREAMTYPE_TS;
        }
        break;

        case CTC_KEY_PARAMETER_PLAYSPEED:
        {
            aml::CTC_PLAYSPEED_OPT_S* mpOptions = reinterpret_cast<aml::CTC_PLAYSPEED_OPT_S *>(ptr);

            AML_AVPLAY_TPLAY_OPT_S options;
            options.enTplayDirect   = (AML_AVPLAY_TPLAY_DIRECT_E)mpOptions->enPlaySpeedDirect;
            options.u32SpeedInteger = mpOptions->u32SpeedInteger;
            options.u32SpeedDecimal = mpOptions->u32SpeedDecimal;

            AML_AVPLAY_Tplay(mPlayer, &options);
            break;
        }
        case CTC_KEY_PARAMETER_VIDEO_DELAY:
        {
            aml::CTC_AVOFFEST_OPT_S* mpOptions = reinterpret_cast<aml::CTC_AVOFFEST_OPT_S *>(ptr);

            AML_AVPLAY_SetVideoPtsOffset(mPlayer, (int)mpOptions->u64offest);
            break;
        }
        case CTC_KEY_PARAMETER_AUDIO_DELAY:
        {
            aml::CTC_AVOFFEST_OPT_S* mpOptions = reinterpret_cast<aml::CTC_AVOFFEST_OPT_S *>(ptr);

            AML_AVPLAY_SetAudioPtsOffset(mPlayer, (int)mpOptions->u64offest);
            break;
        }

        case CTC_KEY_PARAMETER_SET_AUDIOMUTE:
        {
            aml::CTC_MEDIACONFIG_AUDIOMUTE_S* mpOptions = reinterpret_cast<aml::CTC_MEDIACONFIG_AUDIOMUTE_S*>(ptr);

            AML_PLAYER_CONFIG_OPT_U configOptions{};
            configOptions.configAudioMute.mute = mpOptions->mute;
            AML_PLAYER_SetConfig(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, AML_PLAYER_CONFIG_AUDIO_MUTE, &configOptions);
            break;
        }

        case CTC_KEY_PARAMETER_DEMUX_PCRPID:
        {
            aml::CTC_DEMUX_PCRPID_OPT_S* mpOptions = reinterpret_cast<aml::CTC_DEMUX_PCRPID_OPT_S *>(ptr);
            mPcrPid = mpOptions->pcrpid;
            break;
        }

        case CTC_KEY_PARAMETER_START_VIDEO:
        {
            ALOGI("start video");
            AML_PLAYER_Start(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, NULL);
        }
        break;

        case CTC_KEY_PARAMETER_STOP_VIDEO:
        {
            ALOGI("stop video");
            AML_PLAYER_Stop(mPlayer, AML_PLAYER_MEDIA_CHAN_VID, NULL);
        }
        break;

        default:
            ALOGW("CTsHwOmxPlayer SetParameter unknow type = %d\n", type);
            break;
    }

    return 0;
}

int CTsHwOmxPlayer::GetParameter(void *hander, int type, void * ptr)
{
    MLOG("key:%#x", type);
    if (ptr == NULL) {
        return -1;
    }

    switch (type) {
        case CTC_KEY_PARAMETER_GET_AUDIOMUTE:
        {
            AML_PLAYER_CONFIG_OPT_U options;
            AML_PLAYER_GetConfig(mPlayer, AML_PLAYER_MEDIA_CHAN_AUD, AML_PLAYER_CONFIG_AUDIO_MUTE, &options);

            aml::CTC_MEDIACONFIG_AUDIOMUTE_S* mpOptions = reinterpret_cast<aml::CTC_MEDIACONFIG_AUDIOMUTE_S*>(ptr);
            mpOptions->mute = options.configAudioMute.mute;
            break;
        }
        default:
            ALOGD("CTsHwOmxPlayer SetParameter unknow type = %d\n", type);
            break;
    }

    return 0;
}

void CTsHwOmxPlayer::ctc_register_evt_cb(CTC_PLAYER_EVT_CB ctc_func_player_evt, void *handler)
{
    MLOG();

    mCTCEventCb = ctc_func_player_evt;
    mCTCEventHandler = handler;
}

AML_HANDLE CTsHwOmxPlayer::DMX_CreateChannel(int pid)
{
    MLOG();

    if (mDemux == AML_INVALID_HANDLE) {
        mDemux = AML_DMX_OpenDemux(mZorder);
    }

    return AML_DMX_CreateChannel(mDemux, pid);
}

int CTsHwOmxPlayer::DMX_DestroyChannel(AML_HANDLE channel)
{
    MLOG();

    return AML_DMX_DestroyChannel(mDemux, channel);
}

int CTsHwOmxPlayer::DMX_OpenChannel(AML_HANDLE channel)
{
    MLOG();

    return AML_DMX_OpenChannel(mDemux, channel);
}

int CTsHwOmxPlayer::DMX_CloseChannel(AML_HANDLE channel)
{
    MLOG();

    return AML_DMX_CloseChannel(mDemux, channel);
}

AML_HANDLE CTsHwOmxPlayer::DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id)
{
    MLOG();

    return AML_DMX_CreateFilter(mDemux, cb, pUserData, id);
}

int CTsHwOmxPlayer::DMX_DestroyFilter(AML_HANDLE filter)
{
    MLOG();

    return AML_DMX_DestroyFilter(mDemux, filter);
}

int CTsHwOmxPlayer::DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel)
{
    MLOG();

    return AML_DMX_AttachFilter(mDemux, filter, channel);
}

int CTsHwOmxPlayer::DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel)
{
    MLOG();

    return AML_DMX_DetachFilter(mDemux, filter, channel);
}

bool CTsHwOmxPlayer::isValidVideoFormat(const aml::AML_UNF_VCODEC_ATTR_S* vdeoFormat)
{
    return (vdeoFormat->u32Pid != 0x1FFF && vdeoFormat->enType != AMLOGIC_VCODEC_TYPE_UNKNOWN) ? true : false;
}

bool CTsHwOmxPlayer::isValidAudioFormat(const aml::AML_UNF_ACODEC_ATTR_S* audioFormat)
{
    return (audioFormat->u32CurPid != 0x1FFF && audioFormat->enType != AMLOGIC_ACODEC_TYPE_UNKNOWN) ? true : false;
}

int CTsHwOmxPlayer::destroyParas()
{
    MLOG();

    if (mVideoFormat.extraData) {
        free(mVideoFormat.extraData);
        mVideoFormat.extraData = nullptr;
        mVideoFormat.extraDataSize = 0;
    }

    for (size_t i = 0; i < mAudioFormats.u32PidNum; ++i) {
        AML_UNF_ACODEC_ATTR_S* aAttr = &mAudioFormats.pstAcodecAttr[i];
        if (aAttr->extraData) {
            free(aAttr->extraData);
            aAttr->extraData = nullptr;
            aAttr->extraDataSize = 0;
        }
    }

    return OK;
}

int CTsHwOmxPlayer::initParas()
{
    MLOG();

    memset(&mVideoFormat, 0, sizeof(mVideoFormat));
    memset(&mAudioFormats, 0, sizeof(mAudioFormats));

    mVideoFormat.u32Pid = 0x1FFF;
    mVideoStopOption.videoOptions.enMode = AML_PLAYER_LAST_FRAME_MODE_STILL;
    mRequestKeepLastFrame = true;

    return OK;
}

void CTsHwOmxPlayer::halPlayerEvevntCb(AML_AVPLAY_EVENT_E event, unsigned long param1, int param2)
{
    ALOGI("event:%d", event);

    CTC_PLAYER_EVT_e ctcEvent = (CTC_PLAYER_EVT_e)-1;
    switch (event) {
    case AML_AVPLAY_EVENT_E::EVENT_FIRST_IFRAME_DISPLAYED:
        ctcEvent = CTC_PLAYER_EVT_e::PROBE_PLAYER_EVT_FIRST_PTS;
        break;

    case AML_AVPLAY_EVENT_E::EVENT_AUDIO_FIRST_DECODED:
        ctcEvent = CTC_PLAYER_EVT_e::PROBE_PLAYER_EVT_AUDIO_FIRST_DECODED;
        break;

    case AML_AVPLAY_EVENT_E::EVENT_USERDATA_READY:
        ctcEvent = CTC_PLAYER_EVT_e::PROBE_PLAYER_EVT_USERDATA_READY;
        break;

    default:
        break;
    }

    if (mCTCEventCb) {
        mCTCEventCb(mCTCEventHandler, ctcEvent, param1, param2);
    }
}

int CTsHwOmxPlayer::addAndSetCurAudio(PAUDIO_PARA_T pAudioPara)
{
    MLOG();

    PAUDIO_PARA_T pAP = pAudioPara;
    int count = mAudioFormats.u32PidNum;
    AML_UNF_ACODEC_ATTR_S* aAttr = nullptr;

    if (pAP && (pAP->pid != 0 || pAP->nSampleRate != 0) && (count < MAX_AUDIO_PARAM_SIZE)) {
        aAttr = &mAudioFormats.pstAcodecAttr[count];
        ALOGI("%d, aAttr extraData:%p, extraDataSize:%d, pid:%d", count, aAttr->extraData, aAttr->extraDataSize, pAP->pid);
        if (aAttr->extraData) {
            free(aAttr->extraData);
            aAttr->extraData = NULL;
            aAttr->extraDataSize = 0;
        }

        aAttr->enType = getAmlAudioCodec(pAP->aFmt);
        aAttr->u32CurPid = pAP->pid;
        aAttr->nChannels = pAP->nChannels;
        aAttr->nSampleRate = pAP->nSampleRate;
        if (pAP->nExtraSize < 1024) {  //sanity check
            aAttr->extraDataSize = pAP->nExtraSize;
            aAttr->extraData = calloc(aAttr->extraDataSize, 1);
            if (aAttr->extraData) {
                memcpy(aAttr->extraData, pAP->pExtraData, pAP->nExtraSize);
            }
        }

        mAudioFormats.pu32AudPid[count] = pAP->pid;
        mAudioFormats.u32CurPid = mAudioFormats.pu32AudPid[count];

        count++;
        mAudioFormats.u32PidNum = count;
    }

    ALOGI("audio para count:%d, CurPid:%d", count, mAudioFormats.u32CurPid);

    return count > 0 ? --count : 0;
}

}
