ifeq ($(TELECOM_VFORMAT_SUPPORT),true)
LOCAL_CFLAGS += -DTELECOM_VFORMAT_SUPPORT
endif
ifeq ($(TELECOM_QOS_SUPPORT),true)
LOCAL_CFLAGS += -DTELECOM_QOS_SUPPORT
endif
ifeq ($(IPTV_ZTE_SUPPORT),true)
LOCAL_CFLAGS += -DIPTV_ZTE_SUPPORT
endif
LOCAL_ARM_MODE := arm
#LOCAL_MODULE    := libctshwomxplayer

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := \
	CTsHwOmxPlayer.cpp

OS_MAJOR_VER	:= $(shell echo $(PLATFORM_VERSION) | cut -d. -f1)
#$(warning $(OS_MAJOR_VER))

#$(warning $(PLATFORM_SDK_VERSION))
LOCAL_CFLAGS += -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

ifeq ($(shell test $(PLATFORM_SDK_VERSION) -gt 24; echo $$?),0)
	include $(TOP)/hardware/amlogic/media/media_base_config.mk
	MESON_GRALLOC_DIR?=hardware/amlogic/gralloc
endif

ifeq ($(OS_MAJOR_VER),6)
$(warning MM)
LOCAL_CFLAGS	+= -DANDROID6
endif
ifeq ($(OS_MAJOR_VER),5)
$(warning Lollipop)
LOCAL_CFLAGS	+= -DANDROID5
LOCAL_C_INCLUDES += $(TOP)/external/stlport/stlport
LOCAL_SHARED_LIBRARIES += libstlport
endif
ifeq ($(OS_MAJOR_VER),4)
$(warning Kitkat)
LOCAL_CFLAGS	+= -DANDROID4
LOCAL_C_INCLUDES += $(TOP)/external/stlport/stlport
LOCAL_SHARED_LIBRARIES += libstlport
endif
ifneq (,$(wildcard vendor/amlogic/frameworks/av/LibPlayer))
LIBPLAYER_PATH:=$(TOP)/vendor/amlogic/frameworks/av/LibPlayer
SUBTITLE_SERVICE_PATH:=$(TOP)/vendor/amlogic/apps/SubTitle
else
LIBPLAYER_PATH := $(TOP)/packages/amlogic/LibPlayer
SUBTITLE_SERVICE_PATH:=$(TOP)/packages/amlogic/SubTitle
endif

CTsHwOmxPlayer_INCS_19 := \
	$(LIBPLAYER_PATH)/amplayer/player/include \
	$(LIBPLAYER_PATH)/amplayer/control/include \
	$(LIBPLAYER_PATH)/amffmpeg \
	$(LIBPLAYER_PATH)/amcodec/include \
	$(LIBPLAYER_PATH)/amcodec/amsub_ctl \
	$(LIBPLAYER_PATH)/amadec/include \
	$(LIBPLAYER_PATH)/amavutils/include \
	$(LIBPLAYER_PATH)/amsubdec \
	$(SUBTITLE_SERVICE_PATH)/service \
	$(TOP)/packages/amlogic/LibTsPlayer/jni/include \
	$(TOP)/device/amlogic/common/cmcc \
	$(TOP)/device/amlogic/common/cmcc/include \


HAREWARE_PATH := $(TOP)/hardware/amlogic
CTsHwOmxPlayer_INCS_28 := \
	$(HAREWARE_PATH)/media/amcodec/include \
	$(HAREWARE_PATH)/LibAudio/amadec/include \
	$(HAREWARE_PATH)/media/amavutils/include \
	$(TOP)/vendor/amlogic/common/frameworks/services \
	$(TOP)/system/media/audio/include \
	$(LOCAL_PATH)/../../../jni/include \
	$(TOP)/vendor/amlogic/common/frameworks/av/mediaextconfig/include \

CTsHwOmxPlayer_INCS_29 := $(CTsHwOmxPlayer_INCS_28)

LOCAL_C_INCLUDES := \
	$(CTsHwOmxPlayer_INCS_$(PLATFORM_SDK_VERSION)) \
	$(AMCODEC_NEED_INCLUDE)\
	$(JNI_H_INCLUDE)/ \
	$(LOCAL_PATH)/../include \
	$(LOCAL_PATH)/../include/cmcc \
	$(LOCAL_PATH)/../AmFFmpegAdapter/include \
	$(LOCAL_PATH)/../../LivePlayer \
	$(LOCAL_PATH)/../../miniframeworks \
	$(TOP)/frameworks/av/ \
	$(TOP)/frameworks/av/media/libstagefright/include \
	$(TOP)/frameworks/native/include/media/openmax \
	$(TOP)/vendor/amlogic/frameworks/services \
	$(AMAVUTILS_PATH)/include/      \
	$(TOP)/$(MESON_GRALLOC_DIR) \

LOCAL_SHARED_LIBRARIES +=libutils libmedia libz libbinder
LOCAL_SHARED_LIBRARIES +=liblog libcutils libdl
LOCAL_SHARED_LIBRARIES +=libgui libui
LOCAL_SHARED_LIBRARIES +=libstagefright libstagefright_foundation
LOCAL_SHARED_LIBRARIES +=libamffmpeg

ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 19))
LOCAL_SHARED_LIBRARIES +=libamavutils
endif


ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 28))
LOCAL_SHARED_LIBRARIES +=libamavutils_sys
LOCAL_SHARED_LIBRARIES +=libaudioclient
LOCAL_SHARED_LIBRARIES +=libmedia_omx
LOCAL_SHARED_LIBRARIES +=libmediaextractor
LOCAL_SHARED_LIBRARIES +=libammediaext
endif

#MODULE_GIT_VERSION="$(shell cd $(LOCAL_PATH);git log | grep commit -m 1 | cut -d' ' -f 2)"
#MODULE_GIT_UNCOMMIT_FILE_NUM=$(shell cd $(LOCAL_PATH);git diff | grep +++ -c)
#MODULE_LAST_CHANGED="$(shell cd $(LOCAL_PATH);git log | grep Date -m 1)"
#MODULE_BUILD_TIME="$(shell date | xargs)"
MODULE_BUILD_NAME="$(shell echo ${LOGNAME})"
#MODULE_BRANCH_NAME="$(shell cd $(LOCAL_PATH);git branch -a | sed -n '/'*'/p')"
MODULE_BUILD_MODE=$(shell echo ${TARGET_BUILD_VARIANT})
#MODULE_HOSTNAME="$(shell hostname | xargs)"
#MODULE_PATH="$(shell echo ${ANDROID_BUILD_TOP})"
#MODULE_CHANGE_ID="$(shell cd $(LOCAL_PATH);git log | grep 'Change-Id:' -m 1 | cut -d':' -f 2| xargs)"

LOCAL_CFLAGS += -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

LOCAL_CFLAGS+=-DHAVE_VERSION_INFO
LOCAL_CFLAGS+=-DMODULE_GIT_VERSION=\"${MODULE_GIT_VERSION}${MODULE_GIT_DIRTY}\"
LOCAL_CFLAGS+=-DMODULE_BRANCH_NAME=\"${MODULE_BRANCH_NAME}\"
LOCAL_CFLAGS+=-DMODULE_LAST_CHANGED=\"${MODULE_LAST_CHANGED}\"
LOCAL_CFLAGS+=-DMODULE_BUILD_TIME=\"${MODULE_BUILD_TIME}\"
LOCAL_CFLAGS+=-DMODULE_BUILD_NAME=\"${MODULE_BUILD_NAME}\"
LOCAL_CFLAGS+=-DMODULE_GIT_UNCOMMIT_FILE_NUM=${MODULE_GIT_UNCOMMIT_FILE_NUM}
LOCAL_CFLAGS+=-DMODULE_HOSTNAME=\"${MODULE_HOSTNAME}\"
#LOCAL_CFLAGS+=-DMODULE_PATH=\"${MODULE_PATH}\"
LOCAL_CFLAGS+=-DMODULE_CHANGE_ID=\"${MODULE_CHANGE_ID}\"
LOCAL_CFLAGS+=-Wno-unused-parameter -Wno-unused-variable
#$(warning LOCAL_SHARED_LIBRARIES=$(LOCAL_SHARED_LIBRARIES))
ifeq ($(BUILD_WITH_VIEWRIGHT_STB), true)
  LOCAL_SHARED_LIBRARIES += libCTC_vmx_iptv_ad
  LOCAL_CFLAGS += -DBUILD_WITH_VIEWRIGHT_STB
endif

ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 19))
	LOCAL_CFLAGS += -std=c++11
endif

#include $(BUILD_SHARED_LIBRARY)
#include $(BUILD_STATIC_LIBRARY)

