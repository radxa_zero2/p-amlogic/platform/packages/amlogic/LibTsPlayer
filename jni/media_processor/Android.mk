LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

##############################################################
USE_SUBTITLE := 1
USE_AVINFO := 1


ifeq ($(BUILD_IN_ZTE_SOURCE), true)
USE_AVINFO := 0
endif


ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 19))
ifneq (,$(wildcard vendor/amlogic/frameworks/av/LibPlayer))
LIBPLAYER_PATH:=$(TOP)/vendor/amlogic/frameworks/av/LibPlayer
SUBTITLE_SERVICE_PATH:=$(TOP)/vendor/amlogic/apps/SubTitle
else
LIBPLAYER_PATH := $(TOP)/packages/amlogic/LibPlayer
SUBTITLE_SERVICE_PATH:=$(TOP)/packages/amlogic/SubTitle
endif
else ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 23))
HARDWARE_PATH := $(TOP)/hardware/amlogic
endif

ifeq ($(shell test -x $(LOCAL_PATH)/update_version.sh && echo 1),1)
    $(shell (cd $(LOCAL_PATH) && ./update_version.sh $(LOGNAME) >/dev/null 2>&1))
else
    $(error update_version.sh not found!)
endif

ifneq ($(BUILD_IN_ZTE_SOURCE), true)
DVB_SHARED_LIB_NAME := libam_adp_adec
else
DVB_SHARED_LIB_NAME := libam_adp_sys
endif


##############################################################
CTC_SRC_FILES :=	\
	CTC_MediaProcessor.cpp \
	CTC_Utils.cpp \
	CTsPlayerImpl.cpp \
	ITsPlayer.cpp \
	LegacyITsPlayerWrapper.cpp \
	AmlProbe/AmlProbe.cpp \
	CTsPlayerInfoCollection.cpp \
	legacy/legacy_CTC_MediaProcessor.cpp \
	legacy/legacy_CTC_MediaControl.cpp

CTC_SRC_FILES_legacy_CTsPlayer := \
	legacy/CTsPlayer.cpp \
	legacy/subtitleservice.cpp

CTC_SRC_FILES_19 := \
	CTsOmxPlayer.cpp \

CTC_SRC_FILES_28 := \
	MemoryLeakTrackUtilTmp.cpp \
	AmlSubtitle/AmlSubtitle.cpp \
	AmlSubtitle/SubtitleSource.cpp \
	AmlSubtitle/HWSubtitleSource.cpp \
    AmlSubtitle/CC_HWSubtitleSource.cpp \
    AmlSubtitle/SWSubtitleSource.cpp \
	$(CTC_SRC_FILES_legacy_CTsPlayer)

CTC_SRC_FILES_29 := \
	MemoryLeakTrackUtilTmp.cpp \
	AmlSubtitle/AmlSubtitle.cpp \
	AmlSubtitle/SubtitleSource.cpp \
	AmlSubtitle/HWSubtitleSource.cpp \
    AmlSubtitle/CC_HWSubtitleSource.cpp \
    AmlSubtitle/SWSubtitleSource.cpp

CTC_VENDOR_SRC_FILES :=	\
	CTC_MediaProcessor.cpp \
	CTC_Utils.cpp \
	ITsPlayer.cpp \

CTC_C_INCLUDES := \
	$(JNI_H_INCLUDE)/ \
	$(LOCAL_PATH)/../include \
	$(LOCAL_PATH)/../compat \
	$(LOCAL_PATH)/AmlProbe \
	$(TOP)/frameworks/av/ \
	$(TOP)/frameworks/av/media/libstagefright/include \
	$(TOP)/frameworks/native/include/media/openmax \
	$(TOP)/vendor/amlogic/common/frameworks/services/subtiltleserver/client/subtitleServerHidlClient


CTC_C_INCLUDES_19 := $(TOP)/external/stlport/stlport \
	$(LIBPLAYER_PATH)/amplayer/player/include \
	$(LIBPLAYER_PATH)/amplayer/control/include \
	$(LIBPLAYER_PATH)/amffmpeg \
	$(LIBPLAYER_PATH)/amcodec/include \
	$(LIBPLAYER_PATH)/amcodec/amsub_ctl \
	$(LIBPLAYER_PATH)/amadec/include \
	$(LIBPLAYER_PATH)/amavutils/include \
	$(LIBPLAYER_PATH)/amsubdec \
	$(SUBTITLE_SERVICE_PATH)/service \

CTC_C_INCLUDES_28 := \
	$(HARDWARE_PATH)/LibAudio/amadec/include \
	$(HARDWARE_PATH)/media/amcodec/include \
	$(HARDWARE_PATH)/media/amvdec/include \
	$(HARDWARE_PATH)/media/amavutils/include \
	$(TOP)/frameworks/native/libs/nativewindow/include \
	$(HARDWARE_PATH)/gralloc/ \
	$(TOP)/vendor/amlogic/common/external/dvb/include/am_adp \
	$(TOP)/vendor/amlogic/common/external/dvb/android/ndk/include \
	$(TOP)/vendor/amlogic/common/frameworks/av/mediaextconfig/include \
	$(TOP)/vendor/verimatrix/iptvclient/include \
	$(TOP)/vendor/amlogic/common/external/ffmpeg \
	$(TOP)/vendor/amlogic/common/frameworks/services/subtiltleserver/client \

CTC_C_INCLUDES_29 := $(CTC_C_INCLUDES_28)

CTC_CFLAGS := -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION) \
	-Wno-unused-variable -Wno-unused-parameter -Wno-unused-function \
	-Wno-deprecated-declarations -Wno-unused-private-field \
	-Wno-unused-value

ifeq ($(USE_SUBTITLE), 1)
CTC_CFLAGS_$(PLATFORM_SDK_VERSION) += -DUSESUBTITLE
endif

ifeq ($(USE_AVINFO), 1)
CTC_CFLAGS_$(PLATFORM_SDK_VERSION) += -DUSEAVINFO
endif


ifeq ($(BUILD_IN_ZTE_SOURCE),true)
CTC_CFLAGS_$(PLATFORM_SDK_VERSION) += -DBUILD_IN_ZTE_SOURCE
#CTC_CFLAGS_$(PLATFORM_SDK_VERSION) += -DBUILD_WITH_VIEWRIGHT_STB
endif

CTC_SHARED_LIBRARIES := libutils libmedia libz libbinder \
	liblog libcutils libdl libgui libui \
	libstagefright libstagefright_foundation

CTC_VENDOR_SHARED_LIBRARIES := libutils libz libbinder \
	liblog libcutils libdl libstagefright_foundation \
	libamavutils libamffmpeg.vendor libliveplayer.vendor

CTC_SHARED_LIBRARIES_19 := libstlport libamcodec libamadec \
	libamsubdec libamavutils libamFFExtractor libFFExtractor \
	libamplayer libsubtitleservice

CTC_SHARED_LIBRARIES_28 := libamavutils libamcodec libaudioclient \
	libhidltransport \
	libbase \
	libamffmpeg \
	libmediaplayerservice libmediametrics \
    libliveplayer \
	libamgralloc_ext@2 \
	vendor.amlogic.hardware.subtitleserver@1.0 libsubtitlebinder \
	$(DVB_SHARED_LIB_NAME)

CTC_SHARED_LIBRARIES_29 := libamavutils libamcodec libaudioclient \
	libhidltransport \
	libbase \
	libamffmpeg \
	libmediaplayerservice libmediametrics \
	libliveplayer \
	libamgralloc_ext@2


CTC_SUBTITLE_SHARED_LIBRARIES := \
	libSubtitleClient

ifeq ($(USE_SUBTITLE), 1)
CTC_SHARED_LIBRARIES_$(PLATFORM_SDK_VERSION) += $(CTC_SUBTITLE_SHARED_LIBRARIES)
endif

CTC_STATIC_LIBRARIES :=



LOCAL_SRC_FILES := $(CTC_SRC_FILES) $(CTC_SRC_FILES_$(PLATFORM_SDK_VERSION))
LOCAL_C_INCLUDES := $(CTC_C_INCLUDES) $(CTC_C_INCLUDES_$(PLATFORM_SDK_VERSION))
LOCAL_CFLAGS := $(CTC_CFLAGS) $(CTC_CFLAGS_$(PLATFORM_SDK_VERSION))
LOCAL_SHARED_LIBRARIES := $(CTC_SHARED_LIBRARIES) $(CTC_SHARED_LIBRARIES_$(PLATFORM_SDK_VERSION))

LOCAL_ARM_MODE := arm
LOCAL_MODULE    := libCTC_MediaProcessor_common
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
include $(BUILD_STATIC_LIBRARY)
##############################################################
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(CTC_VENDOR_SRC_FILES)
LOCAL_C_INCLUDES := $(CTC_C_INCLUDES) $(CTC_C_INCLUDES_$(PLATFORM_SDK_VERSION))
LOCAL_CFLAGS := $(CTC_CFLAGS) $(CTC_CFLAGS_$(PLATFORM_SDK_VERSION))
LOCAL_SHARED_LIBRARIES := $(CTC_SHARED_LIBRARIES) $(CTC_SHARED_LIBRARIES_$(PLATFORM_SDK_VERSION))

LOCAL_ARM_MODE := arm
LOCAL_MODULE    := libCTC_MediaProcessor_common.vendor
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
LOCAL_VENDOR_MODULE := true
include $(BUILD_STATIC_LIBRARY)

##############################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES :=
LOCAL_C_INCLUDES := $(CTC_C_INCLUDES) $(CTC_C_INCLUDES_$(PLATFORM_SDK_VERSION))
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/../include
LOCAL_CFLAGS := $(CTC_CFLAGS) $(CTC_CFLAGS_$(PLATFORM_SDK_VERSION))
LOCAL_SHARED_LIBRARIES := $(CTC_SHARED_LIBRARIES) $(CTC_SHARED_LIBRARIES_$(PLATFORM_SDK_VERSION))
LOCAL_STATIC_LIBRARIES := $(CTC_STATIC_LIBRARIES)
#LOCAL_SANITIZE := address
LOCAL_WHOLE_STATIC_LIBRARIES := libCTC_MediaProcessor_common libctcmediaplayer libctshwomxplayer
LOCAL_MODULE := libCTC_MediaProcessor
LOCAL_ARM_MODE := arm
include $(BUILD_SHARED_LIBRARY)

##############################################################
ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 29))
include $(CLEAR_VARS)
LOCAL_SRC_FILES :=
LOCAL_C_INCLUDES := $(CTC_C_INCLUDES) $(CTC_C_INCLUDES_$(PLATFORM_SDK_VERSION))
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/../include \
	$(TOP)/$(HARDWARE_PATH)/media/amcodec/include
LOCAL_CFLAGS := $(CTC_CFLAGS) $(CTC_CFLAGS_$(PLATFORM_SDK_VERSION))
LOCAL_SHARED_LIBRARIES := $(CTC_VENDOR_SHARED_LIBRARIES)
LOCAL_STATIC_LIBRARIES :=
LOCAL_WHOLE_STATIC_LIBRARIES := libCTC_MediaProcessor_common.vendor libctcmediaplayer.vendor libctshwomxplayer.vendor
LOCAL_MODULE := libCTC_MediaProcessor.vendor
LOCAL_ARM_MODE := arm
LOCAL_VENDOR_MODULE := true
include $(BUILD_SHARED_LIBRARY)
endif

##############################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES :=
LOCAL_C_INCLUDES := $(CTC_C_INCLUDES) $(CTC_C_INCLUDES_$(PLATFORM_SDK_VERSION))
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/../include
LOCAL_CFLAGS := $(CTC_CFLAGS) $(CTC_CFLAGS_$(PLATFORM_SDK_VERSION))
LOCAL_SHARED_LIBRARIES := $(CTC_SHARED_LIBRARIES) $(CTC_SHARED_LIBRARIES_$(PLATFORM_SDK_VERSION))
LOCAL_STATIC_LIBRARIES := $(CTC_STATIC_LIBRARIES)
LOCAL_WHOLE_STATIC_LIBRARIES := libCTC_MediaProcessor_common libctcmediaplayer libctshwomxplayer
LOCAL_MODULE := libZTE_CTC.ZTE
LOCAL_ARM_MODE := arm
LOCAL_PRODUCT_MODULE := true
include $(BUILD_SHARED_LIBRARY)

##############################################################
include $(call all-makefiles-under,$(LOCAL_PATH))

