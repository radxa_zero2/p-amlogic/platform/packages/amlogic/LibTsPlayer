#define LOG_TAG "CTsPlayerImpl-info"
#include "CTsPlayerInfoCollection.h"
#include "CTC_Log.h"

#define VIDTYPE_INTERLACE_TOP           0x1
#define VIDTYPE_INTERLACE_BOTTOM        0x3

CTsPlayerInfoCollection::CTsPlayerInfoCollection() {
    LOGI("CTsPlayerInfoCollection Ctor, this");
    memset(&pqualityinfo, 0, sizeof(codec_quality_info));
    unload_start_underflow = 0;
    unload_start_time = 0;
    first_picture_comming = 0;
    stream_bitrate = 0;
    unload_times = 0;
    unload_time = 0;
    pcodec = NULL;
    vcodec = NULL;
    acodec = NULL;
    scodec = NULL;
    playerprobeinfo = NULL;
}

CTsPlayerInfoCollection::~CTsPlayerInfoCollection() {
    LOGI("~CTsPlayerInfoCollection");
    pcodec = NULL;
    vcodec = NULL;
    acodec = NULL;
    playerprobeinfo = NULL;
}

int64_t CTsPlayerInfoCollection::GetCurTime(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_usec;
}

int CTsPlayerInfoCollection::UpdateCurVdecInfo(VIDEO_DEC_INFO_T& pVdecInfo) {
    LOGV("UpdateCurVdecInfo");
    if (pcodec == NULL || pcodec->has_video == 0) {
        LOGI("UpdateCurVdecInfo: no video and return failed!\n");
        return -1;
    }
    int ratio_list[8][2] = {{720,480},{720,576},{1280,720},{1920,960},{1920,1080},{1920,1080},{3840,2160},{7680,4320}};
    float videoWH = 0;
    memset(&pVdecInfo, 0, sizeof(VIDEO_DEC_INFO_T));
    struct av_param_info_t av_param_info;
    memset(&av_param_info , 0 ,sizeof(av_param_info));

#ifdef USEAVINFO
    struct av_param_mvdec_t av_decoder_info;
    memset(&av_decoder_info.minfo, 0, 8*sizeof(struct vframe_counter_s));
    av_decoder_info.struct_size = sizeof(struct av_param_mvdec_t);
    int max_slots = sizeof(av_decoder_info.minfo) / sizeof(av_decoder_info.minfo[0]);
    int vdec_id = 0;
    av_decoder_info.vdec_id = vdec_id;
    av_decoder_info.struct_size = sizeof(struct av_param_mvdec_t);

    if (pcodec->has_video && !vcodec) {
        codec_get_av_param_info(pcodec, &av_param_info);
        codec_get_av_decoder_info(pcodec, &av_decoder_info);
    } else if (vcodec) {
        codec_get_av_param_info(vcodec, &av_param_info);
        codec_get_av_decoder_info(vcodec, &av_decoder_info);
    }
    if (av_param_info.av_info.first_pic_coming == 0)
        return 0;
    pVdecInfo.vdec_underflow = playerprobeinfo->vdec_underflow;
    pVdecInfo.vdec_overflow = playerprobeinfo->vdec_overflow;
    pVdecInfo.m_bIsPlay = playerprobeinfo->m_bIsPlay;
    pVdecInfo.video_hide = playerprobeinfo->video_hide;
    pVdecInfo.first_picture_comming = av_param_info.av_info.first_pic_coming;
    pVdecInfo.vpts = (int64_t)av_param_info.av_info.vpts;
    pVdecInfo.vpts_error = av_param_info.av_info.vpts_err;
    pVdecInfo.stream_bps = av_param_info.av_info.dec_video_bps;
    pVdecInfo.has_video = pcodec->has_video;
    pVdecInfo.video_pid = pcodec->video_pid;
    LOGV("[%s:%d]slots:%d\n", __FUNCTION__, __LINE__, av_decoder_info.slots);
    if (av_decoder_info.slots <= 0 || av_decoder_info.slots > max_slots) {
        LOGI("[%s:%d]reading too fast and return, slots:%d\n", __FUNCTION__, __LINE__, av_decoder_info.slots);
        return 0;
    }
    if (av_decoder_info.minfo[av_decoder_info.slots - 1].frame_width == 0) {
        LOGI("UpdateCurVdecInfo:get first picture, width not get\n");
        return 0;
    }
    pVdecInfo.vdec_error = av_decoder_info.minfo[av_decoder_info.slots - 1].error_count;
    pVdecInfo.vdec_drop = av_decoder_info.minfo[av_decoder_info.slots - 1].drop_frame_count;
    pVdecInfo.frame_rate = av_decoder_info.minfo[av_decoder_info.slots - 1].frame_rate;
    //LOGI("[%s:%d]frame_rate:%d\n", __FUNCTION__, __LINE__, av_decoder_info.minfo[av_decoder_info.slots - 1].frame_rate);
    //LOGI("[%s:%d]error_count:%d\n", __FUNCTION__, __LINE__, av_decoder_info.minfo[av_decoder_info.slots - 1].error_count);
    //LOGI("[%s:%d]vdec_drop:%d\n", __FUNCTION__, __LINE__, av_decoder_info.minfo[av_decoder_info.slots - 1].drop_frame_count);
    pVdecInfo.video_width = av_decoder_info.minfo[av_decoder_info.slots - 1].frame_width;
    pVdecInfo.video_height = av_decoder_info.minfo[av_decoder_info.slots - 1].frame_height;
    //LOGI("[%s:%d]frame_width:%d\n", __FUNCTION__, __LINE__, av_decoder_info.minfo[av_decoder_info.slots - 1].frame_width);
    //LOGI("[%s:%d]frame_height:%d\n", __FUNCTION__, __LINE__, av_decoder_info.minfo[av_decoder_info.slots - 1].frame_height);
    //pVdecInfo.Video_frame_format = av_param_info.av_info.frame_format == FRAME_FORMAT_PROGRESS ? 1:0;
    pVdecInfo.Video_frame_format =
        (av_decoder_info.minfo[av_decoder_info.slots - 1].vf_type == VIDTYPE_INTERLACE_TOP
        || av_decoder_info.minfo[av_decoder_info.slots - 1].vf_type == VIDTYPE_INTERLACE_BOTTOM) ? 0:1;
    //LOGI("[%s:%d]Video_frame_format:%d vf_type:0x%x\n",
        //__FUNCTION__, __LINE__, pVdecInfo.Video_frame_format, av_decoder_info.minfo[av_decoder_info.slots - 1].vf_type);
    playerprobeinfo->frame_rate_ctc = pVdecInfo.frame_rate;
    playerprobeinfo->vdec_total = av_decoder_info.minfo[av_decoder_info.slots - 1].frame_count;
    pVdecInfo.frame_count = av_decoder_info.minfo[av_decoder_info.slots - 1].frame_count;
    playerprobeinfo->vdec_drop = pVdecInfo.vdec_drop;
    //LOGI("[%s:%d]vdec_total:%d\n", __FUNCTION__, __LINE__, av_decoder_info.minfo[av_decoder_info.slots - 1].frame_count);
    //LOGI("[%s:%d]current_fps:%d\n", __FUNCTION__, __LINE__, playerprobeinfo->current_fps);
    UpdateBlurScreenInfo(&av_param_info.av_info, &av_decoder_info.minfo[av_decoder_info.slots - 1], &pVdecInfo.blurredscreen_flag);
    //LOGI("[%s:%d]blurredscreen_flag:%d\n", __FUNCTION__, __LINE__, pVdecInfo.blurredscreen_flag);
#endif
    if (pVdecInfo.video_width  > 0 && pVdecInfo.video_height > 0)
        videoWH = (float)pVdecInfo.video_width  / (float)pVdecInfo.video_height;
    if (videoWH < 1.34) {
        pVdecInfo.video_rWH = 0;
    } else if(videoWH > 1.7) {
        pVdecInfo.video_rWH = 1;
    }
    if (pVdecInfo.video_width > 0 && pVdecInfo.video_height > 0) {
        if ((pVdecInfo.video_width == 720) && (pVdecInfo.video_height == 480)) {
            pVdecInfo.video_ratio = 1;
        } else if ((pVdecInfo.video_width == 720) && (pVdecInfo.video_height == 576)) {
            pVdecInfo.video_ratio = 2;
        } else if ((pVdecInfo.video_width == 1280) &&
            (pVdecInfo.video_height == 720)) {
            pVdecInfo.video_ratio = 3;
        } else if ((pVdecInfo.video_width == 1920) && (pVdecInfo.video_height == 960)) {
            pVdecInfo.video_ratio = 4;
        } else if ((pVdecInfo.video_width == 1920) && (pVdecInfo.video_height == 1080)) {
            pVdecInfo.video_ratio = 5;
        } else if ((pVdecInfo.video_width == 1920) && (pVdecInfo.video_height == 1080)) {
            pVdecInfo.video_ratio = 6;
        } else if ((pVdecInfo.video_width == 3840) && (pVdecInfo.video_height == 2160)) {
            pVdecInfo.video_ratio = 7;
        } else if ((pVdecInfo.video_width == 7680) && (pVdecInfo.video_height == 4320)) {
            pVdecInfo.video_ratio = 8;
        } else {
            for (int j = 0;j < 8;j++) {
                for (int k = 0;k < 2;k++) {
                    if ((pVdecInfo.video_width == ratio_list[j][k]) ||
                        (pVdecInfo.video_height == ratio_list[j][k])) {
                        pVdecInfo.video_ratio = j + 1;
                    } else if ((pVdecInfo.video_width != ratio_list[j][k]) &&
                        (pVdecInfo.video_height != ratio_list[j][k])) {
                        LOGV("video_ratio is not a standard resolution, pVideoParaInfo.video_width = %d,pVideoParaInfo.video_height = %d\n",
                           pVdecInfo.video_width, pVdecInfo.video_height);
                        int min_ratio = abs(ratio_list[0][1] - pVdecInfo.video_height);
                        if ((abs(ratio_list[j][1] - pVdecInfo.video_height)) <	min_ratio) {
                            min_ratio = abs(ratio_list[j][1] - pVdecInfo.video_height);
                            pVdecInfo.video_ratio = j + 1;
                            LOGV("abnormal pVideoParaInfo.video_ratio = %d\n", pVdecInfo.video_ratio);
                        }
                    }
                }
            }
        }
    }
    return 0;
}


int CTsPlayerInfoCollection::UpdateCurAdecInfo(AUDIO_DEC_INFO_T& pAdecInfo) {
    LOGV("UpdateCurAdecInfo");
#ifdef USEAVINFO
    if (pcodec == NULL || pcodec->has_audio == 0) {
        LOGI("UpdateCurAdecInfo: no audio and return failed!\n");
        return -1;
    }
    struct av_param_info_t av_param_info;
    memset(&av_param_info , 0 ,sizeof(av_param_info));
    adec_status_ex ainfostatus;
    memset(&ainfostatus, 0, sizeof(ainfostatus));
    if (pcodec->has_video && !vcodec) {
        codec_get_av_param_info(pcodec, &av_param_info);
    } else if (vcodec) {
        codec_get_av_param_info(vcodec, &av_param_info);
    }
    if (pcodec->has_audio) {
        codec_get_audio_basic_info(pcodec);
        codec_get_adec_state_ex(pcodec, &ainfostatus);
    }
    pAdecInfo.apts = (int64_t)av_param_info.av_info.apts;
    pAdecInfo.apts_error = av_param_info.av_info.apts_err;
    pAdecInfo.adec_error = pcodec->audio_info.error_num;
    pAdecInfo.adec_underflow = playerprobeinfo->adec_underflow;
    pAdecInfo.adec_overflow = playerprobeinfo->adec_overflow;
    pAdecInfo.audio_hide = playerprobeinfo->audio_hide;
    pAdecInfo.numDecodedSamples = ainfostatus.numDecodedSamples;
    pAdecInfo.numOutputSamples = ainfostatus.numOutputSamples;
    pAdecInfo.numDecoderErrors = ainfostatus.numDecoderErrors;
    pAdecInfo.numSamplesDiscarded = ainfostatus.numSamplesDiscarded;
    pAdecInfo.acmod = ainfostatus.acmod;
#endif
    return 0;
}


void CTsPlayerInfoCollection::UpdateAVbufInfo(AVBUF_INFO_T& pBbufInfo) {
    LOGV("UpdateAVbufInfo");
    buf_status video_buf;
    buf_status audio_buf;
    memset(&video_buf , 0 ,sizeof(buf_status));
    memset(&audio_buf , 0 ,sizeof(buf_status));
    if (pcodec->has_video && !vcodec) {
        codec_get_vbuf_state(pcodec,&video_buf);
    } else if (vcodec) {
        codec_get_vbuf_state(vcodec,&video_buf);
    }
    if (pcodec->has_audio && !acodec) {
        codec_get_abuf_state(pcodec,&audio_buf);
    } else if (acodec) {
        codec_get_abuf_state(acodec,&audio_buf);
    }
    pBbufInfo.vbuf_data_len = video_buf.data_len;
    pBbufInfo.vbuf_size = video_buf.size;
    pBbufInfo.vbuf_used = video_buf.data_len;
    pBbufInfo.abuf_data_len = audio_buf.data_len;
    pBbufInfo.abuf_size = audio_buf.size;
    pBbufInfo.abuf_used = audio_buf.data_len;
}


void CTsPlayerInfoCollection::UpdateCurAudioParaInfo(AUDIO_PARA_INFO_T& pAudioParaInfo) {
    LOGV("UpdateCurAudioParaInfo");
#ifdef USEAVINFO
    if (pcodec->has_audio) {
        codec_get_audio_basic_info(pcodec);
        codec_get_audio_cur_bitrate(pcodec, &pcodec->audio_info.bitrate);
        pAudioParaInfo.has_audio = pcodec->has_audio;
        pAudioParaInfo.audio_pid = pcodec->audio_pid;
        pAudioParaInfo.samplerate = pcodec->audio_info.sample_rate;
        pAudioParaInfo.channel= pcodec->audio_info.channels;
        pAudioParaInfo.bitrate= pcodec->audio_info.bitrate;
    }
#endif
}


void CTsPlayerInfoCollection::UpdateStreamBitrate() {
    LOGV("UpdateStreamBitrate");
    int64_t curtime_ms = 0;
    curtime_ms = (int64_t)(GetCurTime() / 1000);
    LOGV("UpdateStreamBitrate time diff:%lld\n", (curtime_ms - playerprobeinfo->bytes_record_starttime_ms));
    if (curtime_ms != playerprobeinfo->bytes_record_starttime_ms) {
        int64_t size = (playerprobeinfo->bytes_record_cur - playerprobeinfo->bytes_record_start) * 8;
        int64_t diff = curtime_ms - playerprobeinfo->bytes_record_starttime_ms;
        stream_bitrate = size *1000 / diff;
        LOGI("stream_bitrate: %dKB/s", stream_bitrate>>13);
        playerprobeinfo->bytes_record_start = playerprobeinfo->bytes_record_cur;
        playerprobeinfo->bytes_record_starttime_ms = curtime_ms;
    }
}


int CTsPlayerInfoCollection::SetCodecPara(codec_para_t* pCodec, player_probeinfo* player_info, int codec_type) {
    LOGI("SetCodecPara codec_type:%d\n", codec_type);
    if (pCodec == NULL) {
        LOGI("SetCodecPara failed!\n");
        return -1;
    }
    playerprobeinfo = player_info;
    if (codec_type == 0) {
        pcodec = pCodec;
    }
    if (codec_type == STREAM_ES_V) {
        vcodec = pCodec;
    }
    if (codec_type == STREAM_ES_A) {
        acodec = pCodec;
    }
    return 0;
}



void CTsPlayerInfoCollection::UpdateUnloadInfo(int& pUnload_flag) {
    LOGV("UpdateUnloadInfo");
#ifdef USEAVINFO
    struct av_param_info_t av_param_info;
    memset(&av_param_info , 0 ,sizeof(av_param_info));
    if (pcodec->has_video && !vcodec) {
        codec_get_av_param_info(pcodec, &av_param_info);
    } else if (vcodec) {
        codec_get_av_param_info(vcodec, &av_param_info);
    }
    if (codec_get_upload(&av_param_info.av_info, &pqualityinfo)) {
        //start check underflow
        if (pqualityinfo.unload_flag == 1 && unload_start_underflow == 0) {
            LOGI("unload start : underflow(%d -> %d)\n",
                unload_start_underflow, pqualityinfo.unload_flag);

            unload_times += 1;
            unload_start_underflow = pqualityinfo.unload_flag;
            unload_start_time = (int64_t)(GetCurTime() / 1000);
            pUnload_flag = 1;
        } else if (pqualityinfo.unload_flag == 0) {
            //already underflow unload
            //unload end
            LOGI("unload end : underflow(%d -> %d)\n",
                unload_start_underflow, pqualityinfo.unload_flag);
            unload_time +=
                (int64_t)(GetCurTime() / 1000) - unload_start_time;
            unload_start_underflow = 0;
            LOGI("unload_times:%d, unload_time:%d\n", unload_times, unload_time);
            pUnload_flag = 0;
        }
    }
#endif
}


void CTsPlayerInfoCollection::UpdateBlurScreenInfo(struct av_info_t *info1, struct vframe_counter_s *info2, int* pBlurscreen_flag) {
    LOGV("UpdateBlurScreenInfo");
#ifdef USEAVINFO
    if (codec_get_blurred_screen(info1, info2, &pqualityinfo)) {
        if (pqualityinfo.blurred_flag) {
            LOGI("blurred screen start\n");
            *pBlurscreen_flag = 1;
        } else {
            LOGI("blurred screen end\n");
            *pBlurscreen_flag = 0;
        }
    }
#endif
}


void CTsPlayerInfoCollection::UpdateOtherInfo(OTHER_INFO_T& pOtherInfo) {
    LOGV("UpdateOtherInfo");
    pOtherInfo.stream_bitrate = stream_bitrate;
    pOtherInfo.unload_times = unload_times;
    pOtherInfo.unload_time = unload_time;
    pOtherInfo.v_overflows = playerprobeinfo->v_overflows;
    pOtherInfo.v_underflows = playerprobeinfo->v_underflows;
    pOtherInfo.a_overflows = playerprobeinfo->a_overflows;
    pOtherInfo.a_underflows = playerprobeinfo->a_underflows;
}



