#ifndef _IINFOCOLLECTION_H_
#define _IINFOCOLLECTION_H_

#include <utils/RefBase.h>
#include <utils/Errors.h>
#include <utils/KeyedVector.h>
#include <utils/String8.h>
#include <android/log.h>

using android::RefBase;

class IInfoCollection : public RefBase
{
public:

    typedef struct VIDEO_DEC_INFO {
        int has_video;
        int video_pid;
        int video_hide;
        bool m_bIsPlay;
        int64_t vpts;
        uint32_t frame_count;
        int vdec_error;                     //IPTV_PLAYER_EVT_VID_FRAME_ERROR
        int vdec_drop;                      //IPTV_PLAYER_EVT_VID_DISCARD_FRAME
        int vpts_error;                     //IPTV_PLAYER_EVT_VID_PTS_ERROR
        int vdec_underflow;                 //IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW
        int vdec_overflow;                  //IPTV_PLAYER_EVT_VID_DEC_OVERFLOW,
        int first_picture_comming;          //IPTV_PLAYER_EVT_FIRST_PTS
        int stream_bps;
        int frame_rate;                     //IPTV_PLAYER_ATTR_VID_FRAMERATE
        int video_width;                    //IPTV_PLAYER_ATTR_V_WIDTH
        int video_height;                   //IPTV_PLAYER_ATTR_V_HEIGHT
        int video_ratio;                    //IPTV_PLAYER_ATTR_VID_ASPECT
        int video_rWH = 0;                  //IPTV_PLAYER_ATTR_VID_RATIO
        int Video_frame_format = 0;         //IPTV_PLAYER_ATTR_VID_SAMPLETYPE
        int blurredscreen_flag;
    }VIDEO_DEC_INFO_T;


    typedef struct AUDIO_DEC_INFO {
        int64_t apts;
        int apts_error;                     //IPTV_PLAYER_EVT_AUD_PTS_ERROR
        int adec_error;                     //IPTV_PLAYER_EVT_AUD_FRAME_ERROR and IPTV_PLAYER_EVT_AUD_DISCARD_FRAME
        int adec_underflow;                 //IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW
        int adec_overflow;                  //IPTV_PLAYER_EVT_AUD_DEC_OVERFLOW
        int audio_hide;
        unsigned int numDecodedSamples;
        unsigned int numOutputSamples;
        unsigned int numDecoderErrors;
        unsigned int numSamplesDiscarded;
        int acmod;
    }AUDIO_DEC_INFO_T;


    typedef struct AVBUF_INFO {
        int vbuf_data_len;
        int vbuf_size;                      //IPTV_PLAYER_ATTR_VID_BUF_SIZE
        int vbuf_used;                      //IPTV_PLAYER_ATTR_VID_USED_SIZE
        int abuf_data_len;
        int abuf_size;                      //IPTV_PLAYER_ATTR_AUD_BUF_SIZE
        int abuf_used;                      //IPTV_PLAYER_ATTR_AUD_USED_SIZE
    }AVBUF_INFO_T;


    typedef struct VIDEO_PARA_INFO {
        int video_width;                    //IPTV_PLAYER_ATTR_V_WIDTH
        int video_height;                   //IPTV_PLAYER_ATTR_V_HEIGHT
        int video_ratio;                    //IPTV_PLAYER_ATTR_VID_ASPECT
        int video_rWH = 0;                  //IPTV_PLAYER_ATTR_VID_RATIO
        int Video_frame_format = 0;         //IPTV_PLAYER_ATTR_VID_SAMPLETYPE
    }VIDEO_PARA_INFO_T;

    typedef struct AUDIO_PARA_INFO {
        int has_audio;
        int audio_pid;
        int samplerate;                       //IPTV_PLAYER_ATTR_AUD_SAMPLERATE
        int channel;                        //IPTV_PLAYER_ATTR_AUD_CHANNEL_NUM
        int bitrate;                        //IPTV_PLAYER_ATTR_AUD_BITRATE
    }AUDIO_PARA_INFO_T;

    typedef struct OTHER_INFO {
        int stream_bitrate;                 //IPTV_PLAYER_ATTR_STREAM_BITRATE
        int unload_times;                   //IPTV_PLAYER_ATTR_CATON_TIMES
        int unload_time;                    //IPTV_PLAYER_ATTR_CATON_TIME
        int v_overflows;
        int v_underflows;
        int a_overflows;
        int a_underflows;
    }OTHER_INFO_T;

    IInfoCollection() {}
    virtual ~IInfoCollection() {}

    /**
    * Update video decoder infomation.
    * need update in time.
    * @param pVdecInfo  video decoder info.
    * @return 0 in case of success
    */
    virtual int UpdateCurVdecInfo(VIDEO_DEC_INFO_T& pVdecInfo) = 0;

    /**
    * Update audio decoder infomation.
    * need update in time.
    * @param pAdecInfo  audio decoder info.
    * @return 0 in case of success
    */
    virtual int UpdateCurAdecInfo(AUDIO_DEC_INFO_T& pAdecInfo) = 0;

    /**
    * Update video/audio buffer infomation.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pBbufInfo  buffer info.
    */
    virtual void UpdateAVbufInfo(AVBUF_INFO_T& pBbufInfo) = 0;

    /**
    * Update video para infomation.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pVideoParaInfo  video para info.
    */
    //virtual void UpdateCurVideoParaInfo(VIDEO_PARA_INFO_T& pVideoParaInfo) = 0;

    /**
    * Update audio para infomation.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pAudioParaInfo  audio para info.
    */
    virtual void UpdateCurAudioParaInfo(AUDIO_PARA_INFO_T& pAudioParaInfo) = 0;

    /**
    * Update some player para infomation such as stream bitrate and so on.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pOtherInfo  some player para info.
    */
    virtual void UpdateOtherInfo(OTHER_INFO_T& pOtherInfo) = 0;

    /**
    * Update stream bitrate.
    * need update in time.
    */
    virtual void UpdateStreamBitrate() = 0;

    /**
    * Update unload infomation.
    * need update in time.
    * @param pUnload_flag  if pUnload_flag equals 0 it represents player unload end.
    *                      if pUnload_flag equals 1 it represents player unload start.
    */
    virtual void UpdateUnloadInfo(int& pUnload_flag) = 0;

    /**
    * Update blurred screen infomation.
    * need update in time.
    * @param pBlurscreen_flag  if pBlurscreen_flag equals 0 it represents player blurred screen end.
    *                          if pBlurscreen_flag equals 1 it represents player blurred screen start.
    */
    virtual void UpdateBlurScreenInfo(struct av_info_t *info1, struct vframe_counter_s *info2, int* pBlurscreen_flag) = 0;//need update in time
    virtual int64_t GetCurTime(void) = 0;
};

#endif

