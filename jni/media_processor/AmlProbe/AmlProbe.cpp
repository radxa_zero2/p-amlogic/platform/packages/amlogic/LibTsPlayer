#define LOG_TAG "AmlProbe"
#include "AmlProbe.h"
#include "CTC_Log.h"
#include <android/log.h>
#include <sys/times.h>
#include <time.h>
#include "Amsysfsutils.h"
#include <cutils/properties.h>

using namespace android;

static const int64_t kCollecInfoInterval = 1000000ll;

void test_probe_evt_func(void* handler, aml::CTC_PLAYER_EVT_e evt, unsigned long, unsigned long)
{
    LOGI("test_probe_evt_func\n");
    switch (evt) {
        case aml::PROBE_PLAYER_EVT_STREAM_VALID:
        case aml::PROBE_PLAYER_EVT_FIRST_PTS:
        case aml::PROBE_PLAYER_EVT_VOD_EOS:
        case aml::PROBE_PLAYER_EVT_ABEND:
        case aml::PROBE_PLAYER_EVT_PLAYBACK_ERROR:
        case aml::PROBE_PLAYER_EVT_VID_FRAME_ERROR:
        case aml::PROBE_PLAYER_EVT_VID_DISCARD_FRAME:
        case aml::PROBE_PLAYER_EVT_VID_DEC_UNDERFLOW:
        case aml::PROBE_PLAYER_EVT_VID_PTS_ERROR:
        case aml::PROBE_PLAYER_EVT_AUD_FRAME_ERROR:
        case aml::PROBE_PLAYER_EVT_AUD_DISCARD_FRAME:
        case aml::PROBE_PLAYER_EVT_AUD_DEC_UNDERFLOW:
        case aml::PROBE_PLAYER_EVT_AUD_PTS_ERROR:
        case aml::PROBE_PLAYER_EVT_BUTT:
    default :
        LOGI("evt : %d\n", evt);
        break;
    }
}

AmlProbe::AmlProbe() {
    LOGI("AmlProbe Ctor, this");
    m_StopProbeThread = false;
    m_ProbeCount = 0;
    memset(&m_ProbePara, 0, sizeof(PROBE_PARA_T));
    pfunc_player_evt = test_probe_evt_func;
    last_unload_flag = 0;
    last_blurredscreen_flag = 0;
    DisableVideoCount = 0;
    DisableAudioCount = 0;
    FrozenVideoNum = 0;
    ForzenAudioNum = 0;
    CurVideoNum = 0;
    CurAudioNum = 0;
    last_anumdecoderErrors = 0;
    last_anumdecoder = 0;
    last_vnumdecoderErrors = 0;
    last_vnumdecoder = 0;
    video_length_change = 3;
    video_frame_change = 10;
    audio_length_change = 3;
    audio_frame_change = 10;
    //pthread_mutexattr_t mutexattr;
    //pthread_mutexattr_init(&mutexattr);
    //pthread_mutexattr_settype(&mutexattr,PTHREAD_MUTEX_RECURSIVE);
    //lp_lock_init(&mutex_session, &mutexattr);
    //pthread_cond_init(&s_pthread_cond, NULL);
    //pthread_mutexattr_destroy(&mutexattr);
}


AmlProbe::~AmlProbe() {
    LOGI("~AmlProbe");
    m_StopProbeThread = true;

    stopLooper();
    //thread_wake_up();
    //pthread_join(mInfoThread, NULL);
    //lp_lock_deinit(&mutex_session);
    //pthread_cond_destroy(&s_pthread_cond);
}


int AmlProbe::SetInfoCollection(const sp<IInfoCollection>& info_collection) {
    LOGI("SetInfoCollection");
    if (info_collection == nullptr) {
        LOGI("SetInfoCollection failed!\n");
        return -1;
    }
    pInfoCollection = info_collection;
    return 0;
}

void AmlProbe::probe_register_evt_cb(aml::CTC_PLAYER_EVT_CB pfunc, void *hander) {
    LOGI("probe_register_evt_cb");
    pfunc_player_evt = pfunc;
    player_evt_hander = hander;
}

/*void AmlProbe::notifyListener(PROBE_PLAYER_EVT_e msg, int ext1 = 0, int ext2 = 0)
{
    if (player_evt_hander) {
        pfunc_player_evt(player_evt_hander, msg, ext1, ext2);
    }
}*/

void AmlProbe::start() {
    LOGI("AmlProbe start");
    //pthread_attr_t attr;
    //pthread_attr_init(&attr);
    //pthread_create(&mInfoThread, &attr, threadProbeInfo, this);
    //pthread_attr_destroy(&attr);
    //LOGI("create mInfoThread end\n");
    {
        Mutex::Autolock _l(mLock);
        if (mLooper == NULL) {
            mLooper = new ALooper;
            AString looperName = AStringPrintf("AmlProbe");
            mLooper->setName(looperName.c_str());
            mLooper->start();

            mLooper->registerHandler(this);
        }
    }
    mStarted = true;
    sp<AMessage> msg = new AMessage(kWhatCollecInfo, this);
    msg->setInt32("generation", mCollecInfoGeneration);
    msg->post(kCollecInfoInterval);
}

void AmlProbe::stopLooper()
{
    if (mLooper) {
        mLooper->unregisterHandler(id());
        mLooper->stop();
    }
}

void AmlProbe::stop() {
    LOGI("AmlProbe::stop\n");
    sp<AMessage> msg = new AMessage(kWhatStop, this);

    sp<AMessage> response;
    msg->postAndAwaitResponse(&response);
}

void AmlProbe::onStop()
{
    mStarted = false;

    ++mCollecInfoGeneration;
}


void AmlProbe::onMessageReceived(const sp<AMessage>& msg)
{
    Mutex::Autolock _l(mLock);

    switch (msg->what()) {

    case kWhatCollecInfo:
    {
        LOGV("msg:kWhatCollecInfo\n");
        int generation;
        CHECK(msg->findInt32("generation", &generation));
        if (generation != mCollecInfoGeneration) {
            break;
        }
        onCollectInfo();
        msg->post(kCollecInfoInterval);
    }
    break;

    case kWhatStop:
    {
        LOGI("msg:kWhatStop\n");
        onStop();

        sp<AReplyToken> replyID;
        if (msg->senderAwaitsResponse(&replyID)) {
            (new AMessage)->postReply(replyID);
        }
    }
    break;
    default:
        TRESPASS();
        break;
    }
}

/*void AmlProbe::thread_wait_timeUs(int microseconds)
{
    int ret = -1;
    struct timespec outtime;
    if (m_StopProbeThread) {
        LOGI("receive close flag,direct return\n");
        return;
    }
    if (microseconds > 0) {
        int64_t time = pInfoCollection->GetCurTime() + microseconds;
        ret =  lp_trylock(&mutex_session);
        if (ret != 0) {
            LOGI("can't get lock, use usleep,microseconds = %d\n", microseconds);
            usleep(microseconds);
            return;
        }
        outtime.tv_sec = time/1000000;
        outtime.tv_nsec = (time % 1000000) * 1000;
        ret = pthread_cond_timedwait(&s_pthread_cond,  &mutex_session, &outtime);
        if (ret != ETIMEDOUT) {
            LOGI("break by untimeout ret=%d\n", ret);
        }
        lp_unlock(&mutex_session);
    }
}*/

/*void AmlProbe::thread_wake_up()
{
    lp_lock(&mutex_session);
    pthread_cond_broadcast(&s_pthread_cond);
    lp_unlock(&mutex_session);
}*/

void AmlProbe::update_probe_reportinfo() {
    LOGV("update_probe_reportinfo enter\n");
    IInfoCollection::VIDEO_DEC_INFO_T pVdecInfo;
    IInfoCollection::AUDIO_DEC_INFO_T pAdecInfo;
    IInfoCollection::AVBUF_INFO_T pAVbufInfo;
    IInfoCollection::OTHER_INFO_T pOtherInfo;

    pInfoCollection->UpdateStreamBitrate();
    pInfoCollection->UpdateCurVdecInfo(pVdecInfo);

    if (pVdecInfo.first_picture_comming > 0) {
        if (m_ProbePara.first_picture_comming == 0) {
            IInfoCollection::AUDIO_PARA_INFO_T pAparaInfo;
            pInfoCollection->UpdateCurAudioParaInfo(pAparaInfo);

            m_ProbePara.has_video = pVdecInfo.has_video;
            m_ProbePara.video_pid = pVdecInfo.video_pid;
            m_ProbePara.video_width = pVdecInfo.video_width;
            m_ProbePara.video_height = pVdecInfo.video_height;
            m_ProbePara.video_ratio = pVdecInfo.video_ratio;
            m_ProbePara.video_rWH = pVdecInfo.video_rWH;
            m_ProbePara.Video_frame_format = pVdecInfo.Video_frame_format;

            m_ProbePara.has_audio = pAparaInfo.has_audio;
            m_ProbePara.audio_pid = pAparaInfo.audio_pid;
            m_ProbePara.samplerate = pAparaInfo.samplerate;
            m_ProbePara.channel = pAparaInfo.channel;
            m_ProbePara.bitrate = pAparaInfo.bitrate;
            LOGI("samplate:%d channel:%d bitrate:%d\n", m_ProbePara.samplerate, m_ProbePara.channel, m_ProbePara.bitrate);

            LOGI("send PROBE_PLAYER_EVT_FIRST_PTS event\n");
            pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_FIRST_PTS, 0, 0);
            m_ProbePara.first_picture_comming = 1;
        }
    }else
        return;

    last_unload_flag = m_ProbePara.unload_flag;
    last_blurredscreen_flag = m_ProbePara.blurredscreen_flag;

    pInfoCollection->UpdateUnloadInfo(m_ProbePara.unload_flag);
    pInfoCollection->UpdateCurAdecInfo(pAdecInfo);
    pInfoCollection->UpdateAVbufInfo(pAVbufInfo);

    m_ProbePara.blurredscreen_flag = pVdecInfo.blurredscreen_flag;
    m_ProbePara.vpts = pVdecInfo.vpts;
    m_ProbePara.m_bIsPlay = pVdecInfo.m_bIsPlay;
    m_ProbePara.video_hide = pVdecInfo.video_hide;
    m_ProbePara.frame_count = pVdecInfo.frame_count;
    m_ProbePara.frame_rate = pVdecInfo.frame_rate;
    m_ProbePara.stream_bps = pVdecInfo.stream_bps;
    m_ProbePara.audio_hide = pAdecInfo.audio_hide;
    m_ProbePara.apts = pAdecInfo.apts;
    m_ProbePara.numDecodedSamples = pAdecInfo.numDecodedSamples;
    m_ProbePara.numOutputSamples = pAdecInfo.numOutputSamples;
    m_ProbePara.numDecoderErrors = pAdecInfo.numDecoderErrors;
    m_ProbePara.numSamplesDiscarded = pAdecInfo.numSamplesDiscarded;
    m_ProbePara.acmod = pAdecInfo.acmod;
    m_ProbePara.avdiff = llabs(m_ProbePara.apts - m_ProbePara.vpts);
    //LOGI("avdiff:%lld(%lld---%lld)\n", m_ProbePara.avdiff, m_ProbePara.apts, m_ProbePara.vpts);
    m_ProbePara.vbuf_size = pAVbufInfo.vbuf_size;
    m_ProbePara.vbuf_used = pAVbufInfo.vbuf_used;
    m_ProbePara.abuf_size = pAVbufInfo.abuf_size;
    m_ProbePara.abuf_used = pAVbufInfo.abuf_used;
    m_ProbePara.vdatalen = pAVbufInfo.vbuf_data_len;
    m_ProbePara.adatalen = pAVbufInfo.abuf_data_len;

    pInfoCollection->UpdateOtherInfo(pOtherInfo);
    m_ProbePara.stream_bitrate = pOtherInfo.stream_bitrate;
    //LOGI("vbuf_size:%d vbuf_used:%d stream_bitrate:%d\n", m_ProbePara.vbuf_size, m_ProbePara.vbuf_used, m_ProbePara.stream_bitrate);
    m_ProbePara.unload_times = pOtherInfo.unload_times;
    m_ProbePara.unload_time = pOtherInfo.unload_time;
    m_ProbePara.v_overflows = pOtherInfo.v_overflows;
    m_ProbePara.v_underflows = pOtherInfo.v_underflows;
    m_ProbePara.a_overflows = pOtherInfo.a_overflows;
    m_ProbePara.a_underflows = pOtherInfo.a_underflows;
    if (m_ProbePara.unload_flag != last_unload_flag) {
        if (m_ProbePara.unload_flag) {
            LOGI("send PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_START event\n");
            pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_START, 0, 0);
        } else {
            LOGI("send PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_END event\n");
            pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_END, 0, 0);
        }
    }

    //LOGI("[%s:%d]blurredscreen_flag:%d last_blurredscreen_flag:%d\n",
        //__FUNCTION__, __LINE__, m_ProbePara.blurredscreen_flag, last_blurredscreen_flag);
    if (m_ProbePara.blurredscreen_flag != last_blurredscreen_flag) {
        if (m_ProbePara.blurredscreen_flag) {
            LOGI("send PROBE_PLAYER_EVT_VID_MOSAIC_START event\n");
            pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_MOSAIC_START, 0, 0);
        } else {
            LOGI("send PROBE_PLAYER_EVT_VID_MOSAIC_END event\n");
            pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_MOSAIC_END, 0, 0);
        }
    }
    if (pVdecInfo.vdec_error != m_ProbePara.vdec_error) {
        LOGI("send PROBE_PLAYER_EVT_VID_FRAME_ERROR event\n");
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_FRAME_ERROR, 0, 0);
        m_ProbePara.vdec_error = pVdecInfo.vdec_error;
    }
    if (pVdecInfo.vdec_drop != m_ProbePara.vdec_drop) {
        LOGI("send PROBE_PLAYER_EVT_VID_DISCARD_FRAME event\n");
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_DISCARD_FRAME, 0, 0);
        m_ProbePara.vdec_drop = pVdecInfo.vdec_drop;
    }
    if (pVdecInfo.vdec_underflow != m_ProbePara.vdec_underflow) {
        LOGI("send PROBE_PLAYER_EVT_VID_DEC_UNDERFLOW event\n");
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_DEC_UNDERFLOW, 0, 0);
        m_ProbePara.vdec_underflow = pVdecInfo.vdec_underflow;
    }
    if (pVdecInfo.vdec_overflow != m_ProbePara.vdec_overflow) {
        LOGI("send PROBE_PLAYER_EVT_VID_DEC_OVERFLOW event\n");
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_DEC_OVERFLOW, 0, 0);
        m_ProbePara.vdec_overflow = pVdecInfo.vdec_overflow;
    }
    if (pVdecInfo.vpts_error != m_ProbePara.vpts_error) {   //it is ambiguous
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_PTS_ERROR, 0, 0);
        m_ProbePara.vpts_error = pVdecInfo.vpts_error;
    }
    if (pAdecInfo.adec_error != m_ProbePara.adec_error) {
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_FRAME_ERROR, 0, 0);
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_DISCARD_FRAME, 0, 0);
        m_ProbePara.adec_error = pAdecInfo.adec_error;
    }
    if (pAdecInfo.apts_error != m_ProbePara.apts_error) {
        LOGI("send PROBE_PLAYER_EVT_AUD_PTS_ERROR event\n");
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_PTS_ERROR, 0, 0);
        m_ProbePara.apts_error = pAdecInfo.apts_error;
    }
    if (pAdecInfo.adec_underflow != m_ProbePara.adec_underflow) {
        LOGI("send PROBE_PLAYER_EVT_AUD_DEC_UNDERFLOW event\n");
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_DEC_UNDERFLOW, 0, 0);
        m_ProbePara.adec_underflow = pAdecInfo.adec_underflow;
    }
    if (pAdecInfo.adec_overflow != m_ProbePara.adec_overflow) {
        LOGI("send PROBE_PLAYER_EVT_AUD_DEC_OVERFLOW event\n");
        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_DEC_OVERFLOW, 0, 0);
        m_ProbePara.adec_overflow = pAdecInfo.adec_overflow;
    }
}

int AmlProbe::playerback_getStatusInfo(int enAttrType, int *value) {
    LOGI("AmlProbe::playerback_getStatusInfo\n");
    if (value == NULL) {
        LOGI("[%s],point of value is NULL\n", __func__);
        return 0;
    }
    switch (enAttrType) {
        case PROBE_PLAYER_ATTR_VID_ASPECT:
            *value = m_ProbePara.video_ratio;
            break;
        case PROBE_PLAYER_ATTR_VID_RATIO :
            *value = m_ProbePara.video_rWH;
            break;
        case PROBE_PLAYER_ATTR_VID_SAMPLETYPE:
            *value = m_ProbePara.Video_frame_format;
            break;
        case PROBE_PLAYER_ATTR_VIDAUDDIFF:
            *value = m_ProbePara.avdiff;
            break;
        case PROBE_PLAYER_ATTR_VID_BUF_SIZE:
            *value = m_ProbePara.vbuf_size;
            break;
        case PROBE_PLAYER_ATTR_VID_USED_SIZE:
            *value = m_ProbePara.vbuf_used;
            break;
        case PROBE_PLAYER_ATTR_AUD_BUF_SIZE:
            *value = m_ProbePara.abuf_size;
            break;
        case PROBE_PLAYER_ATTR_AUD_USED_SIZE:
            *value = m_ProbePara.abuf_used;
            break;
        case PROBE_PLAYER_ATTR_AUD_SAMPLERATE:
            *value = m_ProbePara.samplerate;
            break;
        case PROBE_PLAYER_ATTR_AUD_BITRATE:
            *value = m_ProbePara.bitrate;
            break;
        case PROBE_PLAYER_ATTR_AUD_CHANNEL_NUM:
            *value = m_ProbePara.channel;
            break;
        case PROBE_PLAYER_ATTR_BUTT :
            LOGI("--IPTV_PLAYER_ATTR_BUTT\n");
            break;
        case PROBE_PLAYER_ATTR_VID_FRAMERATE:
            *value = m_ProbePara.frame_rate;
            break;
        case PROBE_PLAYER_ATTR_V_HEIGHT:
            *value = m_ProbePara.video_height;
            break;
        case PROBE_PLAYER_ATTR_V_WIDTH:
            *value = m_ProbePara.video_width;
            break;
        case PROBE_PLAYER_ATTR_STREAM_BITRATE:
            /* +[SE] [BUG][IPTV-3760][yinli.xia]
               added: get different bps for accuracy*/
            if (m_ProbePara.stream_bitrate > m_ProbePara.stream_bps)
              *value = m_ProbePara.stream_bitrate;
            else
              *value = m_ProbePara.stream_bps;
            break;
        case PROBE_PLAYER_ATTR_CATON_TIMES:
            *value = m_ProbePara.unload_times;
            break;
        case PROBE_PLAYER_ATTR_CATON_TIME:
            *value = m_ProbePara.unload_time;
            break;
        default:
            LOGI("unknow type");
            break;
    }

    //LOGI("---playerback_getStatusInfo, enAttrType=%d,value=%d\n", enAttrType, *value);

    return 0;
}

int AmlProbe::GetChannelConfiguration(int numChannels, int acmod) {
    int channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO;
    if (numChannels == 1)
        channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_MONO;
    else if (numChannels == 2)
        channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO;
    else if (numChannels == 3) {
        if (acmod == 3)
            channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R;
        else if (acmod == 4)
            channelConfiguration =	aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_S;
        else
            channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO;
    } else if (numChannels == 4){
        if (acmod == 5)
            channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_S;
        else if (acmod == 6)
            channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_S;
        else
            channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_SL_RS;
    } else if (numChannels == 5)
        channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR;
    else if (numChannels == 6)
        channelConfiguration =	aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_5_1;
    else if (numChannels == 8)
        channelConfiguration = aml::TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_7_1;
    return channelConfiguration;
}

void AmlProbe::GetDecodeFrameInfo(void *DecoderInfo, aml::TIF_HAL_PlaybackType_t eType) {
    static int64_t nowTime = 0;
    static int64_t lastTime = ::android::ALooper::GetNowUs();
    static int64_t video_invalid_lastTime = ::android::ALooper::GetNowUs();
    static int64_t audio_invalid_lastTime = ::android::ALooper::GetNowUs();
    static int lastvdatalen = 0;
    static int lastadatalen = 0;
    static int firstin = 1;
    nowTime = ::android::ALooper::GetNowUs();

    if (aml::TIF_HAL_PLAYBACK_TYPE_ALL == eType)
    {
        aml::TIF_HAL_PlaybackStatus_t* info = (aml::TIF_HAL_PlaybackStatus_t*)DecoderInfo;

        //video
        LOGV("GetDecodeFrameInfo , pcodec->has_video =%d, video_disable=%d\n", pcodec->has_video, video_disable);
        if (m_ProbePara.has_video) {
            info->video.statistics.numDecodedFrames = m_ProbePara.frame_count;

            // LOGE("info->video.statistics.numDecodedFrames=%d, videoDecoderIsRun=%d\n",info->video.statistics.numDecodedFrames, videoDecoderIsRun);

            /*if((info->video.statistics.numDecodedFrames > 0) && (videoDecoderIsRun == 1))
            {
                info->video.enabled = m_bIsPlay? 1:0;
            }*/

            info->video.enabled = m_ProbePara.m_bIsPlay ? 1:0;

            info->video.info.height = m_ProbePara.video_height;
            info->video.info.width = m_ProbePara.video_width;

            int framerate = 0;
            #ifdef  BUILD_WITH_VIEWRIGHT_STB
            if (prop_media_vmx_iptv == 1) {
                framerate = VmxGetVideoFrameRate();
            } else
            #endif
            {
                framerate = m_ProbePara.frame_rate;
            }
            // LOGE("framerate=%d", framerate);
            if (framerate == 23) {
                info->video.info.frameRate = 23.97;
            } else if (framerate == 29){
                info->video.info.frameRate = 29.97;
            } else if(framerate == 59){
                info->video.info.frameRate = 59.97;
            } else {
                info->video.info.frameRate = (float)framerate;
            }

            char frame_aspect_ratio[64] = {0};
            amsysfs_get_sysfs_str("/sys/class/video/frame_aspect_ratio", frame_aspect_ratio, 64);
            //LOGE("GetDecodeFrameInfo , frame_aspect_ratio=%s\n", frame_aspect_ratio);

            if (!strncmp(frame_aspect_ratio, "0x90", 4) || !strncmp(frame_aspect_ratio, "0x8c", 4)) { // 16:9
                if (info->video.info.height <= 480 &&
                    info->video.info.width <= 720)
                    info->video.info.pixelAspectRatio = (float)(16.0*(info->video.info.height)) / (9.0*(info->video.info.width));
                else
                    info->video.info.pixelAspectRatio = 1.0;
            } else { //4:3 0xc0
                if (info->video.info.height <= 480 &&
                    info->video.info.width <= 720)
                    info->video.info.pixelAspectRatio = (float)(4.0*(info->video.info.height))/(3.0*(info->video.info.width));
                else
                    info->video.info.pixelAspectRatio = 1.0;
            }

            char afdValue[1024] = {0};
            int afd = 0;
            memset(afdValue, 0, 1024);
            property_get("vendor.sys.subtitleservice.afdValue", afdValue, "0");
            afd = atoi(afdValue);
            // LOGE("-------afd------=%d\n", afd);
            info->video.info.activeFormatDescription = afd;

            if (0 == m_ProbePara.video_hide)
            {
                DisableVideoCount += (CurVideoNum - FrozenVideoNum);
                int first_frame_toggled = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/first_frame_toggled");
                int new_frame_count = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/new_frame_count");
                info->video.statistics.numDisplayedFrames = new_frame_count + first_frame_toggled - DisableVideoCount;
                FrozenVideoNum = info->video.statistics.numDisplayedFrames;
                CurVideoNum = info->video.statistics.numDisplayedFrames;

                LOGV("DisableVideoCount = %d, CurVideoNum = %d, FrozenVideoNum = %d, first_frame_toggled = %d, new_frame_count = %d, numDisplayedFrames = %d\n",
                    DisableVideoCount, CurVideoNum, FrozenVideoNum, first_frame_toggled, new_frame_count, info->video.statistics.numDisplayedFrames);
            }
            else
            {
                int first_frame_toggled = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/first_frame_toggled");
                int new_frame_count = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/new_frame_count");
                info->video.statistics.numDisplayedFrames = FrozenVideoNum;
                CurVideoNum = new_frame_count + first_frame_toggled - DisableVideoCount;
            }

            LOGV("DisableVideoCount = %d, CurVideoNum = %d, FrozenVideoNum = %d\n", DisableVideoCount, CurVideoNum, FrozenVideoNum);

            info->video.statistics.numDecoderErrors = m_ProbePara.vdec_error;
            info->video.statistics.numDecoderOverflows = m_ProbePara.v_overflows;
            info->video.statistics.numDecoderUnderflows = m_ProbePara.v_underflows;
            info->video.statistics.numFramesDropped = m_ProbePara.vdec_drop;

            if (pfunc_player_evt != NULL && info->video.enabled) {
                //LOGI("video nowTime:%lld, lastTime:%lld\n", nowTime/1000, lastTime/1000);
                if (info->video.statistics.numDecodedFrames == 0 && ((nowTime - lastTime) /1000 >= 5000) && m_ProbePara.vdatalen != 0) {
                    //vpid set to apid or video pid is right but codec type is error, we need report invalid data
                    pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_INVALID_DATA, 0, 0);
                    LOGI("####report video invalid data evt by 1 case\n");
                    lastTime = nowTime;
                }
                /*LOGI("last_vnumdecoder:%d numDecodedFrames:%d nowTime:%lld video_invalid_lastTime:%lld vdatalen:%d lastvdatalen:%d",
                    last_vnumdecoder, info->video.statistics.numDecodedFrames, nowTime, video_invalid_lastTime, vdatalen, lastvdatalen);*/

                if (info->video.statistics.numDecodedFrames != 0  &&
                    (info->video.statistics.numDecodedFrames == last_vnumdecoder) &&
                    ((nowTime - video_invalid_lastTime) /1000 >= 8000) &&
                    (video_length_change <= 0) && (video_frame_change <= 0)) {
                        /*if (firstin){
                            lastvdatalen = vdatalen;
                            firstin = 0;
                            LOGI("###for save lastvdatalen= %d\n", lastvdatalen);
                        }*/

                        pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_INVALID_DATA, 0, 0);
                        LOGI("report video invalid data evt by 2 case, vdatalen =%d, lastvdatalen =%d\n", m_ProbePara.vdatalen, lastvdatalen);

                        video_invalid_lastTime = nowTime;
                }

                if (info->video.statistics.numDecoderErrors > last_vnumdecoderErrors) {
                    pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_VID_INVALID_DATA, 0, 0);
                    LOGI("report video invalid data evt by 3 case\n");
                }
            }
            last_vnumdecoderErrors = info->video.statistics.numDecoderErrors;
            if (last_vnumdecoder == info->video.statistics.numDecodedFrames)
                video_frame_change--;
            else {
                video_frame_change = 10;
                last_vnumdecoder = info->video.statistics.numDecodedFrames;
            }

            if (lastvdatalen == m_ProbePara.vdatalen) {
                video_length_change = 1;
            } else {
                video_length_change--;
                lastvdatalen = m_ProbePara.vdatalen;
            }

            LOGV("GetDecodeFrameInfo , video, enabled=%d\n", info->video.enabled);
            LOGV("GetDecodeFrameInfo , video, numDecodedFrames=%d\n", info->video.statistics.numDecodedFrames);
            LOGV("GetDecodeFrameInfo , video, numDisplayedFrames=%d\n", info->video.statistics.numDisplayedFrames);
            LOGV("GetDecodeFrameInfo , video, numDecoderErrors=%d\n", info->video.statistics.numDecoderErrors);
            LOGV("GetDecodeFrameInfo , video, numDecoderOverflows=%d\n", info->video.statistics.numDecoderOverflows);
            LOGV("GetDecodeFrameInfo , video, numDecoderUnderflows=%d\n", info->video.statistics.numDecoderUnderflows);
            LOGV("GetDecodeFrameInfo , video, numFramesDropped=%d\n", info->video.statistics.numFramesDropped);
            LOGV("GetDecodeFrameInfo , video, height=%d\n", info->video.info.height);
            LOGV("GetDecodeFrameInfo , video, width=%d\n", info->video.info.width);
            LOGV("GetDecodeFrameInfo , video, frameRate=%f\n", info->video.info.frameRate);
            LOGV("GetDecodeFrameInfo , video, pixelAspectRatio=%f\n", info->video.info.pixelAspectRatio);
            LOGV("GetDecodeFrameInfo , video, activeFormatDescription=%d\n", info->video.info.activeFormatDescription);
        }

        //audio
        LOGV("GetDecodeFrameInfo , pcodec->has_audio =%d, audio_disable=%d\n", m_ProbePara.has_audio, audio_disable);
        if (m_ProbePara.has_audio) {
            #ifdef  BUILD_WITH_VIEWRIGHT_STB
            if (prop_media_vmx_iptv == 1) {
                AM_AV_AudioStatus_t aStat;
                memset(&aStat, 0, sizeof(AM_AV_AudioStatus_t));
                AM_AV_GetAudioStatus(AV_DEV_NO, &aStat);
                info->audio.statistics.numDecodedSamples = aStat.numDecodedSamples;
                // LOGE("info->audio.statistics.numDecodedSamples=%d, encryptAudioDecoderIsRun=%d\n", info->audio.statistics.numDecodedSamples, encryptAudioDecoderIsRun);

                /*if((info->audio.statistics.numDecodedSamples > 0) &&
                    (encryptAudioDecoderIsRun == 1))
                {
                    info->audio.enabled = m_bIsPlay? 1:0;
                }*/

                info->audio.enabled = m_ProbePara.m_bIsPlay? 1:0;

                info->audio.info.numChannels = aStat.originchannels;
                info->audio.info.channelConfiguration = (aml::TIF_HAL_Playback_AudioSourceChannelConfiguration_t)GetChannelConfiguration(info->audio.info.numChannels, m_ProbePara.acmod);
                info->audio.info.sampleRate = aStat.sample_rate;
                //LOGE("for vmx decodernum %d\n", aStat.numDecodedSamples);
                if (0 == m_ProbePara.audio_hide)
                {
                    DisableAudioCount += (CurAudioNum - ForzenAudioNum);
                    info->audio.statistics.numOutputSamples = aStat.numOutputSamples - DisableAudioCount;
                    ForzenAudioNum = info->audio.statistics.numOutputSamples;
                    CurAudioNum = info->audio.statistics.numOutputSamples;

                    LOGE("Vmx DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d, aStat.numOutputSamples = %d, numOutputSamples = %d\n",
                        DisableAudioCount, CurAudioNum, ForzenAudioNum, aStat.numOutputSamples, info->audio.statistics.numOutputSamples);
                }
                else
                {
                    info->audio.statistics.numOutputSamples = ForzenAudioNum;
                    CurAudioNum = aStat.numOutputSamples - DisableAudioCount;
                }

                LOGV("Vmx DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d\n", DisableAudioCount, CurAudioNum, ForzenAudioNum);

                if ((info->audio.info.sampleRate == 0) && (info->audio.statistics.numOutputSamples > 0))
                {
                    if (m_ProbePara.samplerate != 0)
                        info->audio.info.sampleRate = m_ProbePara.samplerate;
                    else
                        info->audio.statistics.numOutputSamples = 0;
                }

                info->audio.statistics.numDecoderErrors = aStat.numDecoderErrors;
                info->audio.statistics.numDecoderOverflows = m_ProbePara.a_overflows;
                info->audio.statistics.numDecoderUnderflows = m_ProbePara.a_underflows;
                info->audio.statistics.numSamplesDiscarded = aStat.numSamplesDiscarded;
            }else
            #endif
            {
                info->audio.statistics.numDecodedSamples = m_ProbePara.numDecodedSamples;
                // LOGE("info->audio.statistics.numDecodedSamples=%d, audioDecoderIsRun=%d", info->audio.statistics.numDecodedSamples, audioDecoderIsRun);

                /*
                if ((info->audio.statistics.numDecodedSamples > 0) && (audioDecoderIsRun == 1))
                {
                    info->audio.enabled = m_bIsPlay? 1:0;
                }
                */

                info->audio.enabled = m_ProbePara.m_bIsPlay ? 1:0;

                info->audio.info.numChannels = m_ProbePara.channel;

                info->audio.info.channelConfiguration = (aml::TIF_HAL_Playback_AudioSourceChannelConfiguration_t)GetChannelConfiguration(info->audio.info.numChannels, m_ProbePara.acmod);

                info->audio.info.sampleRate = m_ProbePara.samplerate;

                if (0 == m_ProbePara.audio_hide)
                {
                    DisableAudioCount += (CurAudioNum - ForzenAudioNum);
                    info->audio.statistics.numOutputSamples = m_ProbePara.numOutputSamples - DisableAudioCount;
                    ForzenAudioNum = info->audio.statistics.numOutputSamples;
                    CurAudioNum = info->audio.statistics.numOutputSamples;

                    LOGV("DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d, ainfostatus.numOutputSamples = %d, numOutputSamples = %d\n",
                        DisableAudioCount, CurAudioNum, ForzenAudioNum, m_ProbePara.numOutputSamples, info->audio.statistics.numOutputSamples);

                }
                else
                {
                    info->audio.statistics.numOutputSamples = ForzenAudioNum;
                    CurAudioNum = m_ProbePara.numOutputSamples - DisableAudioCount;
                }

                LOGV("DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d\n", DisableAudioCount, CurAudioNum, ForzenAudioNum);


                if ((info->audio.info.sampleRate == 0) && (info->audio.statistics.numOutputSamples > 0))
                {
                    // LOGI("#####audio numOutputSamples:%d, audio sampleRate:%d\n", info->audio.statistics.numOutputSamples, mAudioPara.nSampleRate);
                    if (m_ProbePara.samplerate != 0)
                        info->audio.info.sampleRate = m_ProbePara.samplerate;
                    else
                        info->audio.statistics.numOutputSamples = 0;
                    // LOGI("#####audio sampleRate:%d\n", info->audio.info.sampleRate);
                }

                info->audio.statistics.numDecoderErrors = m_ProbePara.numDecoderErrors;
                info->audio.statistics.numDecoderOverflows = m_ProbePara.a_overflows;
                info->audio.statistics.numDecoderUnderflows = m_ProbePara.a_underflows;
                info->audio.statistics.numSamplesDiscarded = m_ProbePara.numSamplesDiscarded;
            }

            if (pfunc_player_evt != NULL && info->audio.enabled) {
                if (info->audio.statistics.numDecodedSamples == 0 &&
                    (/*(adatalen != 0) || */(m_ProbePara.audio_pid == m_ProbePara.video_pid))){
                    //apid set to vpid or apid is right but codec type is error, we need report invalid data
                    pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_INVALID_DATA, 0, 0);
                    LOGI("report audio invalid data evt by 1 case\n");
                } else if(info->audio.statistics.numDecoderErrors > last_anumdecoderErrors) {
                    pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_INVALID_DATA, 0, 0);
                    LOGI("report audio invalid data evt by 2 case\n");
                }
                if (info->audio.statistics.numDecodedSamples != 0  &&
                    (info->audio.statistics.numDecodedSamples == last_anumdecoder) &&
                    ((nowTime - audio_invalid_lastTime) / 1000 >= 8000) &&
                    (audio_length_change <= 0) && (audio_frame_change <= 0)) {

                    pfunc_player_evt(player_evt_hander, aml::PROBE_PLAYER_EVT_AUD_INVALID_DATA, 0, 0);
                    LOGI("report audio invalid data evt by 3 case, adatalen =%d, lastadatalen =%d\n", m_ProbePara.adatalen, lastadatalen);

                    video_invalid_lastTime = nowTime;
                }
            }
            last_anumdecoderErrors = info->audio.statistics.numDecoderErrors;

            if (last_anumdecoder == info->audio.statistics.numDecodedSamples)
                audio_frame_change--;
            else {
                audio_frame_change = 10;
                last_anumdecoder = info->audio.statistics.numDecodedSamples;
            }

            if (lastadatalen == m_ProbePara.adatalen) {
                audio_length_change = 1;
            } else {
                audio_length_change--;
                lastadatalen = m_ProbePara.adatalen;
            }

            LOGV("GetDecodeFrameInfo , audio, enabled=%d\n", info->audio.enabled);
            LOGV("GetDecodeFrameInfo , audio, numDecodedSamples=%d\n", info->audio.statistics.numDecodedSamples);
            LOGV("GetDecodeFrameInfo , audio, numOutputSamples=%d\n", info->audio.statistics.numOutputSamples);
            LOGV("GetDecodeFrameInfo , audio, numDecoderErrors=%d\n", info->audio.statistics.numDecoderErrors);
            LOGV("GetDecodeFrameInfo , audio numDecoderOverflows=%d\n", info->audio.statistics.numDecoderOverflows);
            LOGV("GetDecodeFrameInfo , audio, numDecoderUnderflows=%d\n", info->audio.statistics.numDecoderUnderflows);
            LOGV("GetDecodeFrameInfo , audio, numSamplesDiscarded=%d\n", info->audio.statistics.numSamplesDiscarded);
            LOGV("GetDecodeFrameInfo , audio channelConfiguration=%d\n", info->audio.info.channelConfiguration);
            LOGV("GetDecodeFrameInfo , audio, numChannels=%d\n", info->audio.info.numChannels);
            LOGV("GetDecodeFrameInfo , audio, sampleRate=%d\n", info->audio.info.sampleRate);
        }

        //subtitle
        /*if (pcodec->has_sub == 1 || hasSctesub)
        {
            // LOGE("GetDecodeFrameInfo , getSubState is ok");
            if (sub_enable_output)
            {
                info->subtitles.enabled = 1;
            }

            info->subtitles.info.height = amsysfs_get_sysfs_int("/sys/class/subtitle/height");
            info->subtitles.info.width = amsysfs_get_sysfs_int("/sys/class/subtitle/width");
            if ((int)sPara[0].pid != 0 && (int)sPara[0].sub_type == CODEC_ID_SCTE27) {
                //info->subtitles.info.height = (unsigned int)subtitleGetSubHeight();
                //info->subtitles.info.width = (unsigned int)subtitleGetSubWidth;
                memcpy(info->subtitles.info.iso639Code, subtitleGetLanguage(), 3);
                if (!strcmp(info->subtitles.info.iso639Code, "\0"))
                {
                    info->subtitles.info.height = 0;
                    info->subtitles.info.width = 0;
                }
            }

            if (info->subtitles.info.height > 0 && sub_count <= 2)
                sub_count++;
            else
                sub_count = 0;

            if (getSubState() && sub_count == 1)
            {
                if (sub_handle_ctc)
                {
                    sub_handle_ctc(SUBTITLE_AVAIABLE, 0);
                    LOGI("%s, ###MIRADA_IZZI Subtitle_CTsplayer Callback State = %d\n", __func__, 0);
                }
            }

            //memcpy(info->subtitles.info.iso639Code, subtitleGetLanguage(), 3);
            info->subtitles.statistics.numDecoderErrors = amsysfs_get_sysfs_int("/sys/class/subtitle/errnums");

            LOGV("GetDecodeFrameInfo , subtitle, enabled=%d\n", info->audio.statistics.numSamplesDiscarded);
            LOGV("GetDecodeFrameInfo , subtitle, numDecoderErrors=%d\n", info->subtitles.statistics.numDecoderErrors);
            LOGV("GetDecodeFrameInfo , subtitle, height=%d\n", info->subtitles.info.height);
            LOGV("GetDecodeFrameInfo , subtitle, width=%d\n", info->subtitles.info.width);
            LOGV("GetDecodeFrameInfo , subtitle, iso639Code=%s\n", info->subtitles.info.iso639Code);
        }*/
    }
    return;
}


/*void *AmlProbe::threadProbeInfo(void *pthis) {
    LOGI("threadProbeInfo start pthis: %p\n", pthis);
    AmlProbe *amlprobe = static_cast<AmlProbe *>(pthis);
    int ret = 0;
    int max_count = 0;
    do {
        if (amlprobe->m_ProbePara.first_picture_comming == 0) {
            LOGI("first picture not comming!\n");
            max_count = 1;
        } else {
            amlprobe->pInfoCollection->UpdateStreamBitrate();
            if (amlprobe->m_ProbePara.frame_rate > 0)
                max_count = amlprobe->m_ProbePara.frame_rate;
            else
                max_count = 25;
        }
        amlprobe->m_ProbeCount++;
		LOGI("threadProbeInfo m_ProbeCount:%d max_count:%d\n", amlprobe->m_ProbeCount, max_count);
        if (amlprobe->m_ProbeCount >= max_count) {
            amlprobe->update_probe_reportinfo();
            if (max_count > 1) {
                //LOGI("TsPlayer update writedata bitrate:%dbps\n", tsplayer->m_sCtsplayerState.stream_bitrate);
            }
            amlprobe->m_ProbeCount = 0;
        }
        if (max_count > 1)
            amlprobe->thread_wait_timeUs(1000 / max_count * 1000);
        else
            amlprobe->thread_wait_timeUs(40 * 1000);
    } while(!amlprobe->m_StopProbeThread);
    LOGI("threadProbeInfo end\n");
    return NULL;
}*/

void AmlProbe::onCollectInfo() {
    LOGV("onCollectInfo\n");
    if (m_ProbePara.first_picture_comming == 0) {
        LOGI("first picture not comming!\n");
    } else {
        //pInfoCollection->UpdateStreamBitrate();
    }
    update_probe_reportinfo();
}



