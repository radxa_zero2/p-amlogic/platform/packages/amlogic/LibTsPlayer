#ifndef _AML_PROBE_H
#define _AML_PROBE_H

#include <android/log.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/socket.h>
#include <utils/RefBase.h>
#include <utils/Errors.h>
#include "IInfoCollection.h"
#include <CTC_Common.h>
#include <utils/Mutex.h>
#include <utils/String16.h>
#include <media/stagefright/foundation/AString.h>
#include <media/stagefright/foundation/ABase.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/AHandler.h>
#include <media/stagefright/foundation/ADebug.h>
#include <CTC_Internal.h>


namespace android {

#define lock_t          pthread_mutex_t
#define lp_lock_init(x,v)   pthread_mutex_init(x,v)
#define lp_lock_deinit(x)   pthread_mutex_destroy(x)
#define lp_lock(x)      pthread_mutex_lock(x)
#define lp_trylock(x)   pthread_mutex_trylock(x)
#define lp_unlock(x)    pthread_mutex_unlock(x)


typedef struct PROBE_PARA {
        // player info
        int has_video;
        int has_audio;
        int first_picture_comming;
        int64_t avdiff;
        int ts_error;
        bool m_bIsPlay;
        int video_hide;
        int audio_hide;
        int video_pid;
        int audio_pid;

        // video info
        int64_t vpts;
        uint32_t frame_count;
        int video_ratio;
        int video_rWH = 0;
        int Video_frame_format = 0;
        int video_width;
        int video_height;
        int vbuf_size;
        int vbuf_used;
        int vdec_error;
        int vdec_drop;
        int vdec_underflow;
        int vdec_overflow;
        int vpts_error;
        int frame_rate;
        int current_fps;

        // audio info
        int64_t apts;
        int abuf_size;
        int abuf_used;
        int adec_error;
        int adec_drop;
        int adec_underflow;
        int adec_overflow;
        int apts_error;
        unsigned int numDecodedSamples;
        unsigned int numOutputSamples;
        unsigned int numDecoderErrors;
        unsigned int numSamplesDiscarded;
        int acmod;

        //audio decode info
        int samplerate;
        int channel;
        int bitrate;
        int audio_bps;
        int audio_type;
        //other info
        int stream_bitrate;  //avg from writedata, duration 1 sec
        int unload_times;    //the num of carton
        int unload_time;
        int stream_bps;      //avg dev video bitrate
        int v_overflows;
        int v_underflows;
        int a_overflows;
        int a_underflows;
        int vdatalen;
        int adatalen;

        int unload_flag;
        int blurredscreen_flag;
    }PROBE_PARA_T;

    typedef enum {
        PROBE_PLAYER_ATTR_VID_ASPECT=0,       /* Unicom:video resolution 0--640*480£¬1--720*576£¬2--1280*720£¬3--1920*1080,4--3840*2160,5--others*/
                                              /* Telecom:video resolution 1: 480P 2: 576P 3: 720P 4: 960P 5:1080i 6:1080P 7:4k 8:8K*/
        PROBE_PLAYER_ATTR_VID_RATIO,          //video width height ratio, 0 represent 4£º3£¬1 represent 16£º9
        PROBE_PLAYER_ATTR_VID_SAMPLETYPE,     //frame field mode, 1 represent Progressive£¬0 represent Interlaced
        PROBE_PLAYER_ATTR_VIDAUDDIFF,         //av pts diff
        PROBE_PLAYER_ATTR_VID_BUF_SIZE,       //video buffer size
        PROBE_PLAYER_ATTR_VID_USED_SIZE,      //video buffer used size
        PROBE_PLAYER_ATTR_AUD_BUF_SIZE,       //audio buffer size
        PROBE_PLAYER_ATTR_AUD_USED_SIZE,      //audio buffer used size
        PROBE_PLAYER_ATTR_AUD_SAMPLERATE,     //audio sample rate
        PROBE_PLAYER_ATTR_AUD_BITRATE,        //audio bitrate
        PROBE_PLAYER_ATTR_AUD_CHANNEL_NUM,    //audio channel num
        PROBE_PLAYER_ATTR_VID_FRAMERATE = 18, //video frame rate
        PROBE_PLAYER_ATTR_BUTT,
        PROBE_PLAYER_ATTR_V_HEIGHT,           //video height
        PROBE_PLAYER_ATTR_V_WIDTH,            //video width
        PROBE_PLAYER_ATTR_STREAM_BITRATE,     //stream bitrate
        PROBE_PLAYER_ATTR_CATON_TIMES,        //the num of caton
        PROBE_PLAYER_ATTR_CATON_TIME,         //the time of caton total time
    }PROBE_ATTR_TYPE_e;



//typedef void (*PROBE_PLAYER_EVT_CB)(PROBE_PLAYER_EVT_e evt, void *handler, uint32_t, uint32_t);

class AmlProbe : public AHandler
{
public:
    AmlProbe();
    virtual ~AmlProbe();

    /**
    * Register event callback.
    *
    * @param pfunc  player event.
    * @param hander callback handler.
    */
    virtual void probe_register_evt_cb(aml::CTC_PLAYER_EVT_CB pfunc, void *hander);
    virtual void start();
    void stopLooper();
    virtual void stop();

    /**
    * Set InfoCollection to AmlProbe.
    */
    virtual int SetInfoCollection(const sp<IInfoCollection>& info_collection);
    virtual void onMessageReceived(const sp<AMessage> &msg);

    /**
    * Collect infomation.
    */
    void onCollectInfo();

    /**
    * Get player status infomation.
    *
    * @param enAttrType   player event.
    * @param[out] value   the infomation value.
    */
    virtual int playerback_getStatusInfo(int enAttrType, int *value);

    /**
    * Get decoder frame infomation.
    *
    * @param DecoderInfo   struct for decoder info.
    * @parame Type         playback type such as all,
    *                      only video, only audo, only subtitles.
    */
    void GetDecodeFrameInfo(void *DecoderInfo, aml::TIF_HAL_PlaybackType_t eType);
private:
    enum {
        kWhatStart,
        kWhatStop,
        kWhatCollecInfo,
    };
    sp <IInfoCollection> pInfoCollection;
    //pthread_t mInfoThread;
    bool m_StopProbeThread;
    int m_ProbeCount;
    PROBE_PARA_T m_ProbePara;
    aml::CTC_PLAYER_EVT_CB pfunc_player_evt = nullptr;
    void *player_evt_hander = nullptr;
    int last_unload_flag;
    int last_blurredscreen_flag;

    int DisableVideoCount;
    int DisableAudioCount;
    int FrozenVideoNum;
    int ForzenAudioNum;
    int CurVideoNum;
    int CurAudioNum;

    int last_anumdecoderErrors;
    int last_vnumdecoderErrors;
    int last_anumdecoder;
    int last_vnumdecoder;

    int video_length_change;
    int video_frame_change;
    int audio_length_change;
    int audio_frame_change;
    //pthread_cond_t s_pthread_cond;
    //lock_t  mutex_session;
    sp<ALooper> mLooper;
    mutable Mutex mLock;
    bool mStarted = false;
    int32_t mCollecInfoGeneration = 0;
    //static void *threadProbeInfo(void *pthis);
    void update_probe_reportinfo();
    //void thread_wait_timeUs(int microseconds);
    //void thread_wake_up();
    void onStop();
    int GetChannelConfiguration(int numChannels, int acmod);
};
}
#endif
