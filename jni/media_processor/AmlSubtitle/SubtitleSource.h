#ifndef _SUBTITLE_SOURCE_
#define _SUBTITLE_SOURCE_

#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/AHandler.h>
#include <utils/List.h>
#include <CTC_Common.h>

//#define SOURCE_LOOP_BUFFER_SIZE (64*1024)
//#define LEAST_LOOP_BUFFER_READ_SIZE (200)
//#define MAST_LOOP_BUFFER_READ_SIZE (1024)


typedef struct {
    int64_t         vers;
    int64_t         videoId;
    int             subtitleCount;
    aml::SubtitleType    type;
    int64_t         pts;
    int             duration;
    int             headerSize;
    uint8_t*        headerData;
    int             rawDataSize;
    uint8_t*        rawData;
}SourceSocketPkt, *PSourceSocketPkt;


namespace android {

class SubtitleSource : public AHandler
{
public:
    enum {
        kWhatRun
    };

public:
    SubtitleSource();
    virtual ~SubtitleSource();

//    void loopBufCopy(char* src, int size);
//    void loopBufRead(char* des, int size);
//    int loopBufSize();
//    bool isGetLoopBuf();
//    int isGetLoopBufSize();

    virtual int sourceInit(const aml::SUBTITLE_PARA_T& param) = 0;
    virtual int sourceStart() = 0;

    virtual int isGetLoopBuf() = 0;
    virtual int readData(SourceSocketPkt** pkt) = 0;
    virtual int writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp) = 0;
    virtual int flush() = 0;

protected:
    virtual void onMessageReceived(const sp<AMessage> &msg);

private:

//    mutable Mutex mLoopBufLock;
//    char *mLoopBuf;
//    char *mRPtr;
//    char *mWPtr;

};

}

//loop buff
class SubtitleSourceLBuf
{
public:
    SubtitleSourceLBuf(int bufsize = (64*1024));
    virtual ~SubtitleSourceLBuf();

    int     init(int len);
    int     reinit(int len);
    int     deinit();
    int     flush();
    int     empty();
    int     free();
    int     avail();
    void    read(unsigned char *buf, int len);
    int     write(const unsigned char *buf, int len);

private:
	unsigned char *mData;
	int     mSize;
	int     mRead;
	int     mWrite;
	int     mError;
    bool    mInit;

};
#endif

