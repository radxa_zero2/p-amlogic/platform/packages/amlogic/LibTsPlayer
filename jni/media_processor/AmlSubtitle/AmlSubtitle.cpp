#define LOG_NDEBUG  0
#define LOG_TAG "AmlSubtitle"
#include "AmlSubtitle.h"
#include <media/stagefright/foundation/ALooper.h>
#include "HWSubtitleSource.h"
#include "CC_HWSubtitleSource.h"
#include "SWSubtitleSource.h"
#include <SubtitleClient.h>

#include <CTC_Log.h>
#include <utils/Mutex.h>
#include <utils/String16.h>
#include <cutils/properties.h>
#include "Amsysfsutils.h"

#define MSG_SUBTITLEVISIBLE "SubtitleVisible"
using namespace android;

extern sp<CC_HWSubtitleSource> getCC_HWSubtitleSource();

aml::CTC_PLAYER_EVT_CB pfunc_player_evt = NULL;
void *player_evt_hander = NULL;
aml::CTC_PLAYER_CLOCK_CB pfunc_player_clock = NULL;
void *player_clock_hander = NULL;

#define RETURN_NONE_IF(mode)                                                         \
    do {                                                                             \
        if (mode) {                                                                  \
            LOGI("%s:%d return <- %s", __FUNCTION__, __LINE__, #mode);              \
            return;                                                                  \
        }                                                                            \
    } while (0)

#define RETURN_RET_IF(mode, ret)                                                     \
    do {                                                                             \
        if (mode) {                                                                  \
            LOGI("%s:%d return <- %s", __FUNCTION__, __LINE__, #mode);              \
            return ret;                                                              \
        }                                                                            \
    } while (0)

#ifdef USESUBTITLE
static void convertToSubtitleClientParam(const aml::SUBTITLE_PARA_T& param, subtitle::Subtitle_Param *subParam)
{
    memset(subParam, 0, sizeof(subtitle::Subtitle_Param));

    switch (param.sub_type) {
    case aml::SubtitleType::CC:
        subParam->sub_type             = subtitle::CC;
        subParam->cc_param.Channel_ID  = param.cc_param.Channel_ID;
        subParam->cc_param.vfmt        = param.cc_param.vfmt;
        break;
    case aml::SCTE27:
        subParam->sub_type                = subtitle::SCTE27;
        subParam->scte27_param.SCTE27_PID = param.scte27_param.SCTE27_PID;
        break;
    case aml::DVB_SUBTITLE:
        subParam->sub_type                       = subtitle::DVB_SUBTITLE;
        subParam->dvb_sub_param.Sub_PID          = param.dvb_sub_param.Sub_PID;
        subParam->dvb_sub_param.Composition_Page = param.dvb_sub_param.Composition_Page;
        subParam->dvb_sub_param.Ancillary_Page   = param.dvb_sub_param.Ancillary_Page;
        LOGI("[%s:%d] DVB Composition_Page:%d, Ancillary_Page:%d", __FUNCTION__, __LINE__, subParam->dvb_sub_param.Composition_Page, subParam->dvb_sub_param.Ancillary_Page);
        break;
    case aml::TELETEXT:
        subParam->sub_type              = subtitle::TELETEXT;
        subParam->tt_param.Teletext_PID = param.tt_param.Teletext_PID;
        subParam->tt_param.Magzine_No   = param.tt_param.Magzine_No;
        subParam->tt_param.Page_No      = param.tt_param.Page_No;
        subParam->tt_param.SubPage_No   = param.tt_param.SubPage_No;
        break;
    default:
        LOGI("[%s:%d] unknow subtitle type.", __FUNCTION__, __LINE__);
        break;
    }
}
#endif

AmlSubtitle::AmlSubtitle()
{
    LOGI("AmlSubtitle");

    mDumpEnable = false;
    if (mDumpEnable) {
        mDumpHandle = fopen("/data/AmlSubtitle.dump", "wb");
        if (mDumpHandle) {
            LOGI("[%s:%d] enable dump.", __FUNCTION__, __LINE__);
        }
    }
    mpLooper = NULL;
    mSubtitleSource = NULL;
    mDetectCC = false;

    memset(&mSubtitleParam, 0, sizeof(mSubtitleParam));
    mDemuxType = HW_DEMUXTYPE;
    mCallbackParam = {NULL, NULL};

    //cc status
    mCCremoved = false;
    mCCenabled = false;
}

AmlSubtitle::~AmlSubtitle()
{
    mDemuxType = NONE_DEMUXTYPE;

    if (mDumpEnable) {
        fclose(mDumpHandle);
    }

    //stopSourceLooper();
    stopMsgLooper();
    LOGI("~AmlSubtitle");
}

int AmlSubtitle::startMsgLooper()
{
    {
        Mutex::Autolock _l(mLock);
        if (mpLooper == NULL) {
            mpLooper = new ALooper;
            AString looperName = AStringPrintf("AmlSubtitle");
            mpLooper->setName(looperName.c_str());
            mpLooper->registerHandler(this);
            //mpLooper->registerHandler(mSubtitleSource);
            mpLooper->start();
            LOGI("[%s:%d] startMsgLooper", __FUNCTION__, __LINE__);
        }
    }

    return 0;
}

int AmlSubtitle::stopMsgLooper()
{
    Mutex::Autolock _l(mLock);
    if (mpLooper) {
        mpLooper->unregisterHandler(id());
        if (mSubtitleSource) {
            mpSourceLooper->unregisterHandler(mSubtitleSource->id());
        }
        if (mCCSubtitleSource) {
            mpSourceLooper->unregisterHandler(mCCSubtitleSource->id());
        }
        mpLooper->stop();
        LOGI("[%s:%d] stopMsgLooper", __FUNCTION__, __LINE__);
    }

    return 0;
}

int AmlSubtitle::registerMsgHandler(const sp<AHandler> &handler)
{
    int ret = -1;
    Mutex::Autolock _l(mLock);
    if ((mpLooper == NULL) || (handler == NULL)) {
        ret = -1;
        goto ERROR;
    }

    mpLooper->registerHandler(handler);

ERROR:
    return ret;
}

int AmlSubtitle::startSourceLooper()
{
    Mutex::Autolock _l(mSourceLock);
    if (mpSourceLooper == NULL) {
        mpSourceLooper = new ALooper;
        AString looperName = AStringPrintf("AmlSubtitleSource");
        mpSourceLooper->setName(looperName.c_str());
        mpSourceLooper->start();
    }

    return 0;
}

int AmlSubtitle::stopSourceLooper()
{
    Mutex::Autolock _l(mSourceLock);
    if (mpSourceLooper) {
        if (mSubtitleSource) {
            mpSourceLooper->unregisterHandler(mSubtitleSource->id());
        }
        if (mCCSubtitleSource) {
            mpSourceLooper->unregisterHandler(mCCSubtitleSource->id());
        }
        mpSourceLooper->stop();
        LOGI("[%s:%d] stopSourceLooper", __FUNCTION__, __LINE__);
    }

    return 0;
}

int AmlSubtitle::registerSourceHandler(const sp<AHandler> &handler)
{
    int ret = -1;
    Mutex::Autolock _l(mSourceLock);
    if ((mpSourceLooper == NULL) || (handler == NULL)) {
        ret = -1;
        goto ERROR;
    }

    mpSourceLooper->registerHandler(handler);

ERROR:
    return ret;
}


int AmlSubtitle::prepare(const aml::SUBTITLE_PARA_T& param)
{
    LOGI("[%s:%d] prepare, sub_type:%d", __FUNCTION__, __LINE__, param.sub_type);

    memset(&mSubtitleParam, 0, sizeof(aml::SUBTITLE_PARA_T));
    mSubtitleParam = param;

    sp<AMessage> msg = new AMessage(kWhatInit, this);
    //msg->setPointer("SubtitleParam",(void *)&mSubtitleParam);
    msg->post();

    return 0;
}

int AmlSubtitle::connect()
{
    LOGI("[%s:%d] connect", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(kWhatConnect, this);
    msg->post();

    return 0;
}

int AmlSubtitle::init()
{
    LOGI("[%s:%d] ", __FUNCTION__, __LINE__);

    //.init and start msg looper
    startMsgLooper();
#ifdef USESUBTITLE
    if (mSubtitleClient == NULL) {
        mSubtitleClient = new SubtitleClient();
    }
#endif

    //.callback
    mCallbackParam.eventCb = subtitle_callback;
    mCallbackParam.clockCb = clock_callback;
    registerCallback(mCallbackParam);

    //.detect signel mode and multi mode
    setInputSource(AmlSubtitle::INTERNAL_SOURCE);
#ifdef USESUBTITLE
    RETURN_RET_IF((mSubtitleClient->getInputSource() && (SW_DEMUXTYPE != getDefaultDemuxType())), 0);
    //.open trans channels
    openTransferChannel();
#endif

    //.start source looper
    //startSourceLooper();

    //.start cc source
    if (mDetectCC) {
        if (mCCSubtitleSource == NULL) {
            mCCSubtitleSource = getCC_HWSubtitleSource();
            registerMsgHandler(mCCSubtitleSource);
            mCCSubtitleSource->sourceInit(mSubtitleParam);
            mCCSubtitleSource->sourceStart();
            LOGI("[%s:%d] CC_HW_DEMUXTYPE getStrogCount=%d", __FUNCTION__, __LINE__,mCCSubtitleSource->getStrongCount());
        }
    }

    //.start SW source or HW source
    if (mSubtitleSource == NULL) {
        AmlSubtitle::DemuxType type = getDefaultDemuxType();
        if (HW_DEMUXTYPE == type) {
            mSubtitleSource = new HWSubtitleSource();
            registerMsgHandler(mSubtitleSource);
            mSubtitleSource->sourceInit(mSubtitleParam);
            mSubtitleSource->sourceStart();
            LOGI("[%s:%d] HW_DEMUXTYPE ", __FUNCTION__, __LINE__);
        } else if (SW_DEMUXTYPE == type) {
            LOGI("[%s:%d] SW_DEMUXTYPE //TODO", __FUNCTION__, __LINE__);
            mSubtitleSource = new SWSubtitleSource();
            registerMsgHandler(mSubtitleSource);
            mSubtitleSource->sourceInit(mSubtitleParam);
            mSubtitleSource->sourceStart();
        }

        //. start get subtitle data
        isGet();
        sendPTS();
    }

    return 0;
}

int AmlSubtitle::start()
{
    LOGI("[%s:%d] start", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(kWhatStart, this);
    msg->post();

    return 0;
}

int AmlSubtitle::stop()
{
    LOGI("[%s:%d] stop", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(kWhatStop, this);

    sp<AMessage> response;
    msg->postAndAwaitResponse(&response);

    return 0;
}

int AmlSubtitle::setVisible(bool visible)
{
    LOGI("[%s:%d] stop, visible:%d", __FUNCTION__, __LINE__, (true == visible ? 1 : 0));

    sp<AMessage> msg = new AMessage(kWhatVisible, this);
    msg->setInt32(MSG_SUBTITLEVISIBLE,(int32_t)visible);

    sp<AMessage> response;
    msg->postAndAwaitResponse(&response);

    return 0;
}

int AmlSubtitle::disconnect()
{
    LOGI("[%s:%d] stop", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(kWhatDisconnect, this);

    sp<AMessage> response;
    msg->postAndAwaitResponse(&response);

    return 0;
}

status_t AmlSubtitle::setViewWindow(int x, int y, int width, int height)
{
    RETURN_RET_IF(mSubtitleClient==NULL, 0);
    LOGE("[%s:%d]x=%d y=%d width=%d height=%d ", __FUNCTION__, __LINE__,x, y, width, height);

    mSubtitleClient->setViewWindow(x, y, width, height);

    return 0;
}

status_t AmlSubtitle::setViewAttribute(const AmlSubtitle::ViewAttribute& attr)
{
    return 0;
}

status_t AmlSubtitle::getSourceAttribute(aml::SUBTITLE_PARA_T* param)
{
    return 0;
}

void AmlSubtitle::subtitle_register_evt_cb(aml::CTC_PLAYER_EVT_CB pfunc, void *hander)
{
    pfunc_player_evt = pfunc;
    player_evt_hander = hander;
}

void AmlSubtitle::subtitle_callback(SUB_Event evt, int32_t param1, int32_t param2)
{
    RETURN_NONE_IF(pfunc_player_evt==NULL);
    RETURN_NONE_IF(pfunc_player_evt==NULL);

    LOGE("[%s:%d]subtitle_callback evt=%d param1=%d param2=%d", __FUNCTION__, __LINE__,evt, param1, param2);

    switch (evt) {
    case SUB_Event::Subtitle_Available:
        pfunc_player_evt(player_evt_hander, aml::CTC_PLAYER_EVT_Available, param1, param2);
        break;
    case SUB_Event::Subtitle_Unavailable:
        pfunc_player_evt(player_evt_hander, aml::CTC_PLAYER_EVT_Unavailable, param1, param2);
        break;
    case SUB_Event::CC_Removed:
        pfunc_player_evt(player_evt_hander, aml::CTC_PLAYER_EVT_CC_Removed, param1, param2);
        break;
    case SUB_Event::CC_Added:
        pfunc_player_evt(player_evt_hander, aml::CTC_PLAYER_EVT_CC_Added, param1, param2);
        break;
    default:
        LOGE("[%s:%d]unknow type", __FUNCTION__, __LINE__);
        break;
    }
}

status_t AmlSubtitle::clock_callback(int64_t* timeUs) {
    RETURN_RET_IF(player_clock_hander==NULL, -1);
    RETURN_RET_IF(pfunc_player_clock==NULL, -1);

   *timeUs = pfunc_player_clock(player_clock_hander);
    ALOGD("[%s:%d] timeUs=%lld", __FUNCTION__, __LINE__, *timeUs);

    return 0;
}

void AmlSubtitle::registerCallback(const AmlSubtitle::CallbackParam& cbParam)
{
#ifdef USESUBTITLE
    mCallbackParam = cbParam;
    SubtitleClient::CallbackParam clientCbParam;

    clientCbParam.eventCb = [this](SubtitleClient::Event event, int32_t param1, int32_t param2) {
        //need to convert SubtitleClient::Event to event type defined by ourself
        SUB_Event sEvt = static_cast<SUB_Event>(event);

        switch (sEvt) {
        case SUB_Event::CC_Removed:
            mCCremoved = true;
            mCCenabled = false;
            break;
        case SUB_Event::CC_Added:
            mCCenabled = true;
            break;
        default:
            LOGE("[%s:%d]unknow type", __FUNCTION__, __LINE__);
            break;
        }

        mCallbackParam.eventCb(sEvt, param1, param2);
    };

    clientCbParam.clockCb = [this](int64_t* timeUs) {
        mCallbackParam.clockCb(timeUs);
        return 0;
    };

    mSubtitleClient->registerCallback(clientCbParam);
#endif

    return ;
}

int AmlSubtitle::flush()
{
    sp<AMessage> msg = new AMessage(kWhatFlush, this);

    sp<AMessage> response;
    msg->postAndAwaitResponse(&response);

    return 0;
}

int AmlSubtitle::isVisible()
{
    return 0;
}

void AmlSubtitle::subtitle_setCCDataChecktime(int check_time)
{
    return ;
}

void AmlSubtitle::subtitle_setSubDataChecktime(int check_time)
{
    return;
}

aml::SubtitleType AmlSubtitle::getCurrentType()
{
    return aml::SubtitleType::UNKNOWN_SUBTYPE;
}

int AmlSubtitle::getCurrentPageInf(int* Page_no, int* Sub_Page_No)
{
    return 0;
}

int AmlSubtitle::setInputSource(AmlSubtitle::InputSource input)
{
    char vaule[PROPERTY_VALUE_MAX] = {0};
    int subType1 = 0;
    memset(vaule, 0, PROPERTY_VALUE_MAX);
    property_get("ott.ctcsubtitle.NoSocket", vaule, "1");
    subType1 = atoi(vaule);
    LOGI("[%s:%d]  inputsource:%d", __FUNCTION__, __LINE__,subType1);
#ifdef USESUBTITLE
    mSubtitleClient->setInputSource((SubtitleClient::InputSource)subType1);
#endif
    return 0;
}

int AmlSubtitle::isGet()
{
    LOGI("[%s:%d] isGet", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(kWhatIsGet, this);
    msg->post();

    return 0;
}

void AmlSubtitle::openTransferChannel()
{
    LOGI("[%s:%d] openTransferChannel", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(kWhatOpenTransfer, this);
    msg->post();
}

int AmlSubtitle::onConnect(const sp<AMessage> &msg)
{
#ifdef USESUBTITLE
    bool attachMode = false;
    bool isMovieplayer = true;

    char value[PROPERTY_VALUE_MAX] = {0};
    if (property_get("ctc.subtitle.movieplayer", value, "true") > 0) {
        if (!strcmp(value, "true")) {
            isMovieplayer = true;
        }
    }

    if (mDemuxType == SW_DEMUXTYPE) {
        if (isMovieplayer) {
            attachMode = true;
         }
    }

    LOGI("[%s:%d] isMovieplayer=%d attachMode=%d",__FUNCTION__, __LINE__, isMovieplayer, attachMode);
    mSubtitleClient->connect(attachMode);
#endif
    return 0;
}

int AmlSubtitle::onInit(const sp<AMessage> &msg)
{
#ifdef USESUBTITLE
    subtitle::Subtitle_Param subClinetParam;
    aml::SUBTITLE_PARA_T amSubParam = mSubtitleParam;
    convertToSubtitleClientParam(amSubParam, &subClinetParam);
    mSubtitleClient->init(subClinetParam);
#endif
    return 0;
}

int AmlSubtitle::onStart(const sp<AMessage> &msg)
{
    if (mStarted)
        return 0;

#ifdef USESUBTITLE
    mSubtitleClient->start();
#endif

    mStarted = true;

    return 0;
}

int AmlSubtitle::onStop(const sp<AMessage> &msg)
{
    MLOG();
    if (!mStarted)
        return 0;

#ifdef USESUBTITLE
    mSubtitleClient->stop();
#endif

    mStarted = false;

    return 0;
}

int AmlSubtitle::onSetVisible(const sp<AMessage> &msg)
{
    bool visible = true;
    int32_t value = 0;
    if (msg->findInt32(MSG_SUBTITLEVISIBLE, &value)) {
        if (0 == value) {
            visible = false;
        }
    }
    LOGI("[%s:%d] kWhatVisible value:%d", __FUNCTION__, __LINE__, value);
#ifdef USESUBTITLE
    mSubtitleClient->setVisible(visible);
#endif
    return 0;
}

int AmlSubtitle::onDisconnect(const sp<AMessage> &msg)
{
#ifdef USESUBTITLE
    mSubtitleClient->disconnect();
#endif
    return 0;
}

static subtitle::SubtitleType convertSubtitleType(aml::SubtitleType type)
{
    switch (type) {
    case aml::CC:
        return subtitle::CC;

    case aml::SCTE27:
        return subtitle::SCTE27;

    case aml::DVB_SUBTITLE:
        return subtitle::DVB_SUBTITLE;

    case aml::TELETEXT:
        return subtitle::TELETEXT;

    case aml::UNKNOWN_SUBTYPE:
        return subtitle::UNKNOWN;
    }
}

int AmlSubtitle::onGet(const sp<AMessage> &msg)
{
    RETURN_RET_IF(mSubtitleClient==NULL, -1);
    RETURN_RET_IF(mSubtitleSource==NULL, -1);
    RETURN_RET_IF((mSubtitleClient->isTransferChannelOpened() != true), -1);

    int size = mSubtitleSource->isGetLoopBuf();
    uint64_t time = 0;
    int i =0;

    if (size) {
        PSourceSocketPkt pkt = NULL;
        mSubtitleSource->readData(&pkt);

        if (pkt != NULL) {
            pkt->type           = mSubtitleParam.sub_type;
            pkt->subtitleCount  = mSubCount;

            char *sub_data = NULL;
            int sub_datasize = 0;

            setSubStartPts((void *)pkt);

            pkt->headerSize = mSubtitleClient->getHeaderSize();
            pkt->headerData = (uint8_t *)malloc(pkt->headerSize);
            memset(pkt->headerData, 0, pkt->headerSize);

            SubtitleClient::PackHeaderAttribute attr = {pkt->subtitleCount, convertSubtitleType(pkt->type), static_cast<size_t>(pkt->rawDataSize),(pkt->pts),pkt->duration};
            mSubtitleClient->constructPacketHeader((void *)pkt->headerData, pkt->headerSize, attr);

            sub_datasize = pkt->headerSize + pkt->rawDataSize;
            sub_data = (char *)malloc(sub_datasize);

            memcpy(sub_data, pkt->headerData, pkt->headerSize);
            memcpy(sub_data + pkt->headerSize, pkt->rawData, pkt->rawDataSize);
#ifdef USESUBTITLE
            mSubtitleClient->send((void*)sub_data, sub_datasize);
#endif

            ALOGD("[%s:%d] socket pkt=%p size:%d time:%llu sub_headsize:%d sub_datasize:%d", __FUNCTION__, __LINE__,pkt, size, pkt->pts, pkt->headerSize, sub_datasize);

            free(pkt->rawData);
            free(pkt->headerData);
            free(pkt);
            free(sub_data);
        }
    }


    msg->post(10000);// 10ms

    return 0;
}

void AmlSubtitle::onMessageReceived(const sp<AMessage> &msg)
{
    Mutex::Autolock _l(mLock);

    switch (msg->what()) {
    case kWhatConnect:
        onConnect(msg);
        break;
    case kWhatInit:
        onInit(msg);
        break;
    case kWhatStart:
        onStart(msg);
        break;
    case kWhatStop:
    {
        onStop(msg);

        sp<AReplyToken> replyID;
        CHECK(msg->senderAwaitsResponse(&replyID));

        sp<AMessage> response = new AMessage;
        response->postReply(replyID);
    }
    break;

    case kWhatVisible:
    {
        onSetVisible(msg);

        sp<AReplyToken> replyID;
        CHECK(msg->senderAwaitsResponse(&replyID));

        sp<AMessage> response = new AMessage;
        response->postReply(replyID);
    }
    break;

    case kWhatDisconnect:
    {
        onDisconnect(msg);

        sp<AReplyToken> replyID;
        CHECK(msg->senderAwaitsResponse(&replyID));

        sp<AMessage> response = new AMessage;
        response->postReply(replyID);
    }
    break;

    case kWhatIsGet:
        onGet(msg);
        break;
    case kWhatSendVPTS:
        onSendPTS(msg);
        break;
    case kWhatOpenTransfer:
        onOpenTransferChannel();
        break;
    case kWhatFlush:
    {
        onFlush();

        sp<AReplyToken> replyID;
        CHECK(msg->senderAwaitsResponse(&replyID));

        sp<AMessage> response = new AMessage;
        response->postReply(replyID);
        break;
    }
    default:
        LOGI("[%s:%d] unknow kWhat =%d", __FUNCTION__, __LINE__, msg->what());
        break;
    }
}

AmlSubtitle::DemuxType AmlSubtitle::getDefaultDemuxType()
{
    return mDemuxType;
}

int AmlSubtitle::setDefaultDemuxType(AmlSubtitle::DemuxType type)
{
    mDemuxType = type;
    return 0;
}

int AmlSubtitle::writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp)
{
    RETURN_RET_IF(mSubtitleSource==NULL, 0);

    mSubtitleSource->writeData(pBuffer, nSize, timestamp);
    return 0;
}

void AmlSubtitle::onOpenTransferChannel()
{
    SubtitleClient::TransferType type = mSubtitleClient->getDefaultTransferType();
    if (type == SubtitleClient::TransferType::TRANSFER_BY_SOCKET) {
        mSubtitleClient->openTransferChannel(type);
        setSubInfo();
    }
}

int AmlSubtitle::onFlush()
{
    LOGE("[%s:%d] flush", __FUNCTION__, __LINE__);
    RETURN_RET_IF(mSubtitleSource==NULL, 0);

    mSubtitleSource->flush();

    return 0;
}

void AmlSubtitle::setSubInfo() {
    char subTypeStr[20] = "none,";
    char subLanStr[20] = "none,";

    int total = mSubCount;
    LOGI("[setSubInfo]total:%d,subTypeStr:%s,subLanStr:%s\n", total,
        subTypeStr, subLanStr);

    char buf[8] = {0x53, 0x54, 0x4F, 0x54};//STOT
    buf[4] = (total >> 24) & 0xff;
    buf[5] = (total >> 16) & 0xff;
    buf[6] = (total >> 8) & 0xff;
    buf[7] = total & 0xff;

    int size = 8 + strlen(subTypeStr) + 1 + strlen(subLanStr);
    char * data = (char *)malloc(size + 1);
    if (!data)
    {
        ALOGE("setSubInfo malloc Failed!\n");
        return;
    }

    memset(data, 0, size + 1);
    memcpy(data, buf, 8);
    memcpy(data + 8, subTypeStr, strlen(subTypeStr));
    memcpy(data + 8 + strlen(subTypeStr), "/", 1);
    memcpy(data + 9 + strlen(subTypeStr), subLanStr, strlen(subLanStr) );
    data[size] = '\0';
#ifdef USESUBTITLE
    if (mSubtitleClient != NULL) {
        mSubtitleClient->send((void*)data, size);
    } else {
        LOGI("[setSubTotal]total:%d , mClient == NULL\n", total);
    }
#endif

    if (data != NULL) {
        free(data);
        data = NULL;
    }
}


int AmlSubtitle::setSubCount(int total)
{
    mSubCount = total;
    LOGI("[%s:%d] mSubCount:%d", __FUNCTION__, __LINE__, mSubCount);

    return 0;
}

void AmlSubtitle::detectClosedCaption(bool enable)
{
    mDetectCC = enable;
    LOGI("[%s:%d] detectClosedCaption:%d", __FUNCTION__, __LINE__, mDetectCC);
}

int AmlSubtitle::sendPTS()
{
    LOGI("[%s:%d] sendPTS", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(kWhatSendVPTS, this);
    msg->post();

    return 0;
}

int AmlSubtitle::onSendPTS(const sp<AMessage> &msg)
{
    if (mStartPts > 0) {
        int64_t timeUs;
        clock_callback(&timeUs);
        timeUs -= mStartPts;
        sendTime(timeUs);
    }

    msg->post(100000);

    return 0;
}

void AmlSubtitle::sendTime(int64_t timeUs)
{
    ALOGD("[%s:%d] timeUs=%lld", __FUNCTION__, __LINE__, timeUs);
    RETURN_NONE_IF(timeUs<0);

    char buf[8] = {0x53, 0x52, 0x44, 0x54};//SRDT //subtitle render time
    buf[4] = (timeUs >> 24) & 0xff;
    buf[5] = (timeUs >> 16) & 0xff;
    buf[6] = (timeUs >> 8) & 0xff;
    buf[7] = timeUs & 0xff;
#ifdef USESUBTITLE
    if (mSubtitleClient != NULL) {
        mSubtitleClient->send((void*)buf, 8);
    }
#endif
}

void AmlSubtitle::setSubStartPts(void* pkt)
{
    RETURN_NONE_IF(pkt==NULL);

    PSourceSocketPkt pack = (PSourceSocketPkt)pkt;

    if (!mStartPtsUpdate) {
        if (pack->pts >= 0) {

            LOGI("[%s:%d] setSubStartPts %lld", __FUNCTION__, __LINE__, pack->pts);

            mStartPtsUpdate = true;
            mStartPts = pack->pts;

            char buf[8] = {0x53, 0x50, 0x54, 0x53};//SPTS
            buf[4] = ((pack->pts) >> 24) & 0xff;
            buf[5] = ((pack->pts) >> 16) & 0xff;
            buf[6] = ((pack->pts) >> 8) & 0xff;
            buf[7] = (pack->pts) & 0xff;
#ifdef USESUBTITLE
            if (mSubtitleClient != NULL) {
                mSubtitleClient->send((void*)buf, 8);
            } else {
                ALOGE("[setSubStartPts]  mClient == NULL\n");
            }
#endif
        }
    }
}

int AmlSubtitle::getClosedCaptionsRemoved()
{
    return (mCCremoved==true) ? 1 : 0;
}

int AmlSubtitle::getClosedCaptionsEnable()
{
    return (mCCenabled==true) ? 1 : 0;
}

void AmlSubtitle::subtitle_register_clock_cb(aml::CTC_PLAYER_CLOCK_CB pfunc, void *hander)
{
    pfunc_player_clock  = pfunc;
    player_clock_hander = hander;
}


