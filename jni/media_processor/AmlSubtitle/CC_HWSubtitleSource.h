/***************************************************************************
 * Copyright (C) 2017 Amlogic, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * Description:
 */
/**\file
 * \brief USER DATA module
 *
 * \author Xia Lei Peng <leipeng.xia@amlogic.com>
 * \date 2013-3-13: create the document
 ***************************************************************************/

#ifndef _CCHWSUBTITLE_SOURCE_
#define _CCHWSUBTITLE_SOURCE_


#include "SubtitleSource.h"
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/AHandler.h>
#include <media/stagefright/foundation/ALooper.h>
#include <utils/Looper.h>
#include <stdint.h>


#include <map>


//typedef unsigned char uint8_t;
//typedef int int32_t;
//typedef unsigned int uint32_t;

using  namespace android;



typedef struct
{
    int channel_id;
    int vfmt;
    int format;
    SubtitleSourceLBuf mLoopBuf;
}MY_CC;

/****************************************************************************
 * Type definitions
 ***************************************************************************/

typedef enum {
    INVALID_TYPE    = 0,
    MPEG_CC_TYPE    = 1,
    H264_CC_TYPE    = 2,
    DIRECTV_CC_TYPE = 3,
    AVS_CC_TYPE     = 4,
    SCTE_CC_TYPE = 5
} userdata_type;


typedef enum {
    /* 0 forbidden */
    I_TYPE = 1,
    P_TYPE = 2,
    B_TYPE = 3,
    D_TYPE = 4,
    /* 5 ... 7 reserved */
} picture_coding_type;

struct userdata_meta_info_t {
    uint32_t poc_number;
    /************ flags bit defination ***********
    bit 0:      //used for mpeg2
        1, group start
        0, not group start
    bit 1-2:    //used for mpeg2
        0, extension_and_user_data( 0 )
        1, extension_and_user_data( 1 )
        2, extension_and_user_data( 2 )
    bit 3-6:    //video format
        0,  VFORMAT_MPEG12
        1,  VFORMAT_MPEG4
        2,  VFORMAT_H264
        3,  VFORMAT_MJPEG
        4,  VFORMAT_REAL
        5,  VFORMAT_JPEG
        6,  VFORMAT_VC1
        7,  VFORMAT_AVS
        8,  VFORMAT_SW
        9,  VFORMAT_H264MVC
        10, VFORMAT_H264_4K2K
        11, VFORMAT_HEVC
        12, VFORMAT_H264_ENC
        13, VFORMAT_JPEG_ENC
        14, VFORMAT_VP9
    bit 7-9:    //frame type
        0, Unknown Frame Type
        1, I Frame
        2, B Frame
        3, P Frame
        4, D_Type_MPEG2
    bit 10:  //top_field_first_flag valid
        0: top_field_first_flag is not valid
        1: top_field_first_flag is valid
    bit 11: //top_field_first bit val
    **********************************************/
    uint32_t flags;
    uint32_t vpts;          /*video frame pts*/
    /******************************************
    0: pts is invalid, please use duration to calcuate
    1: pts is valid
    ******************************************/
    uint32_t vpts_valid;
    /*duration for frame*/
    uint32_t duration;
    /* how many records left in queue waiting to be read*/
    uint32_t records_in_que;
    unsigned long long priv_data;
    uint32_t padding_data[4];
};


struct userdata_param_t {
    uint32_t version;
    uint32_t instance_id; /*input, 0~9*/
    uint32_t buf_len; /*input*/
    uint32_t data_size; /*output*/
    void *pbuf_addr; /*input*/
    struct userdata_meta_info_t meta_info; /*output*/
};
#if 0
typedef struct {
     unsigned int poc_info;
     unsigned int poc_number;
} userdata_poc_info_t;
#endif

typedef struct AM_CCData_s AM_CCData;
struct AM_CCData_s {
    AM_CCData *next;
    uint8_t   *buf;
    int     size;
    int     cap;
    int     poc;
};

typedef struct {
    uint32_t picture_structure:16;
    uint32_t temporal_reference:10;
    uint32_t picture_coding_type:3;
    uint32_t reserved:3;
    uint32_t index:16;
    uint32_t offset:16;
    uint8_t  atsc_flag[5];
    uint8_t  cc_data_start[4];
} aml_ud_header_t;

typedef struct {
//    pthread_t      th;
    int          fd;
    int          vdec_id;
    int vfmt;
    bool      running;
    AM_CCData     *cc_list;
    AM_CCData     *free_list;
    int          cc_num;
    userdata_type   format;
    int          curr_poc;
    int scte_enable;
} AM_UDDrvData;

/**\brief MPEG user data device open parameters*/
typedef struct
{
//	int    foo;
	int    vfmt;
    int    cur_vdec_id;
} AM_USERDATA_OpenPara_t;



class CC_HWSubtitleSource : public SubtitleSource
{
public:
    enum {
        kWhatRun
    };
public:
    CC_HWSubtitleSource();
    virtual ~CC_HWSubtitleSource();

    virtual int sourceInit(const aml::SUBTITLE_PARA_T& param);
    virtual int sourceStart();
    virtual int isGetLoopBuf();
    virtual int readData(SourceSocketPkt** pkt);
    virtual int writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp);
    virtual int flush();

    static int callbackFunc(int fd, int events, void* data);


protected:
    virtual void onMessageReceived(const sp<AMessage> &msg);
    virtual void onFirstRef();

    void read_vdec_user_data(int vdec_ids);
    void aml_add_cc_data(int vdec_ids, int poc, int type, uint8_t *p, int len);
    void aml_flush_cc_data(int vdec_ids);
    void aml_mpeg_userdata_package(int vdec_ids, int poc, int type, uint8_t *p, int len);
    int  aml_process_h264_userdata(int vdec_ids, uint8_t *data, int len, int poc, uint32_t pts);
    int  aml_process_scte_userdata(int vdec_ids, uint8_t *data, int len, int flags, uint32_t pts);
    int  aml_process_mpeg_userdata(int vdec_ids, uint8_t *data, int flag, uint32_t pts, int len);

    int workThread(const sp<AMessage> &msg);
    int getSubData();
//    int startSourceLooper();
    int init_param(const aml::SUBTITLE_PARA_T& param);

    int aml_open();
//  int aml_open(const AM_USERDATA_OpenPara_t *para);
    int aml_close();

private:
    AM_UDDrvData *ud = nullptr;
    std::map<int, MY_CC> myCC;
    bool mFlush;// flush data at first

    mutable Mutex mSourceLock;
//    sp<ALooper> mpSourceLooper;

    sp<Looper> mpLooper;
    //mutable Mutex mLock;

    //test
    bool mDumpEnable;
    FILE* mDumpHandle = nullptr;
};

#endif


