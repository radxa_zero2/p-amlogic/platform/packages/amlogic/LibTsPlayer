#define LOG_TAG "HWSubtitleSource"
#include "HWSubtitleSource.h"
#include <media/stagefright/foundation/ALooper.h>
#include <CTC_Log.h>
#include "cutils/properties.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <sys/ioctl.h>
#include <amports/amstream.h>
#include <utils/Mutex.h>
#include <utils/String16.h>


#define SUBTITLE_READ_DEVICE "/dev/amstream_sub_read"

#define AMSTREAM_IOC_UD_AVAILABLE_VDEC      _IOR(AMSTREAM_IOC_MAGIC, 0x5c, unsigned int)
#define AMSTREAM_IOC_GET_VDEC_ID			_IOR(AMSTREAM_IOC_MAGIC, 0x5d, int)
#define MAX_CC_DATA_LEN  (1024*5 + 4)

#define LEAST_LOOP_BUFFER_READ_SIZE (200)
#define MAST_LOOP_BUFFER_READ_SIZE (1024)

#define LEAST_AMSTREAM_SUBTITLE_READ (200)
static int SPU_RD_HOLD_SIZE = 0x20;

using namespace android;


HWSubtitleSource::HWSubtitleSource()
{
    LOGI("HWSubtitleSource");

    mDumpEnable = false;
    char value[PROPERTY_VALUE_MAX] = {0};
    if (property_get("ott.HWSubtitleSource.dump", value, "false") > 0) {
        if (!strcmp(value, "true")) {
            mDumpEnable = true;
        }
    }

    if (mDumpEnable) {
        mDumpHandle = fopen("/data/HWSubtitleSource.dump", "wb");
        if (mDumpHandle) {
            LOGI("[%s:%d] enable HWSubtitleSource dump", __FUNCTION__, __LINE__);
        } else {
            LOGI("[%s:%d] SWSubtitleSource fopen failed", __FUNCTION__, __LINE__);
        }
    }

}

HWSubtitleSource::~HWSubtitleSource()
{
    LOGI("~HWSubtitleSource");
    if (mfd >= 0) {
        LOGI("close fd");
        close(mfd);
        mfd = -1;
    }


    if (mDumpEnable) {
        fclose(mDumpHandle);
    }

}

void HWSubtitleSource::onMessageReceived(const sp<AMessage> &msg)
{
    Mutex::Autolock _l(mSourceLock);

    switch (msg->what()) {

    case kWhatRun:
        workThread(msg);
        break;

    default:
        LOGI("[%s:%d] unknow kWhat", __FUNCTION__, __LINE__);
        break;
    }
}

int HWSubtitleSource::sourceInit(const aml::SUBTITLE_PARA_T& param)
{
    int ret = -1;

    //startSourceLooper();

    if (mpLooper == NULL) {
        mpLooper = new Looper(false);
    }
    mfd = open(SUBTITLE_READ_DEVICE, O_RDONLY);
    if (mfd < 0) {
        LOGI("[%s:%d] open error", __FUNCTION__, __LINE__);
    } else {
        LOGI("[%s:%d] open ok,mfd=%d", __FUNCTION__, __LINE__, mfd);
    }

    ret = mpLooper->addFd(mfd, 0, Looper::EVENT_OUTPUT | Looper::EVENT_ERROR, callbackFunc, (void *)this);
    LOGI("[%s:%d] addFd = %d", __FUNCTION__, __LINE__, ret);

    return ret;
}

int HWSubtitleSource::workThread(const sp<AMessage> &msg)
{
    int ret = mpLooper->pollOnce(10);

    LOGI("[%s:%d] workThread ret %d", __FUNCTION__, __LINE__, ret);// POLL_CALLBACK = -2,
    msg->post(83000);//this delay refer to sub_jni.c
    return ret;
}

int HWSubtitleSource::callbackFunc(int fd, int events, void* data)
{
    LOGI("[%s:%d] callbackFunc run", __FUNCTION__, __LINE__);
    sp<HWSubtitleSource> mSource = static_cast<HWSubtitleSource *>(data);
    mSource->getSubData();

    return 1;
}

int HWSubtitleSource::getSubData()
{
    LOGI("[%s:%d] getSubData ", __FUNCTION__, __LINE__);

//    {
//        char tmpbuf[256];
//        int64_t packet_header = 0;
//
//        LOGI("enter get_dvb_spu\n");
//        while (1)
//        {
//            packet_header = 0;
//            if (subtitle_get_sub_size_fd(mfd) < SPU_RD_HOLD_SIZE)
//            {
//                LOGI("current dvb sub buffer size %d\n", subtitle_get_sub_size_fd(mfd));
//                usleep(10000);
//                continue;
//                //break;
//            }
//            while (subtitle_read_sub_data_fd(mfd, tmpbuf, 1) == 1)
//            {
//                packet_header = (packet_header << 8) | tmpbuf[0];
//                LOGI("## get_dvb_spu %x, %llx,-------------\n",tmpbuf[0], packet_header);
//                if ((packet_header & 0xffffffff) == 0x000001bd)
//                {
//                    LOGI("## 222  get_dvb_spu hardware demux dvb %x,%llx,-----------\n", tmpbuf[0], packet_header & 0xffffffffff);
//                    break;
//                }
//                else if ((packet_header & 0xffffffffff) == 0x414d4c5577 || (packet_header & 0xffffffffff) == 0x414d4c55aa)
//                {
//                    LOGI("## 222  get_dvb_spu soft demux dvb %x,%llx,-----------\n", tmpbuf[0], packet_header & 0xffffffffff);
//                    break;
//                }
//            }
//            if ((packet_header & 0xffffffff) == 0x000001bd)
//            {
//                LOGI("find header 0x000001bd\n");
//                //TODO
//                break;
//            }
//            else
//            {
//                LOGI("header is not 0x000001bd\n");
//                break;
//            }
//        }
//    }

//    {
//        int packet_header = 0;
//        int read_done = 0;
//        char tmp[1024];
//        do {
//            packet_header = subtitle_read_sub_data_fd(mfd, tmp, 1024);
//            LOGI(" read_done=%d ", read_done);
//            read_done += packet_header;
//        } while(packet_header);
//        LOGI("[%s:%d] read_done=%d ", __FUNCTION__, __LINE__,read_done);
//    }

    {
        int has_size=0;
        int read_need = 0;
        int read_size = 0;
        int read_all = 0;
        unsigned char tmp[1024];
        do {
            has_size = subtitle_get_sub_size_fd(mfd);
            if (has_size < SPU_RD_HOLD_SIZE) {
                LOGI("[%s:%d] has_size Underflow:%d ", __FUNCTION__, __LINE__,has_size);
                break;
            }
            read_need = has_size > LEAST_AMSTREAM_SUBTITLE_READ ? LEAST_AMSTREAM_SUBTITLE_READ : has_size;
            if ((read_size = subtitle_read_sub_data_fd(mfd, tmp, read_need)) == read_need) {
                read_all += read_size;
                //loopBufCopy(tmp, read_size);
                mLoopBuf.write(tmp, read_size);

                if (mDumpHandle && mDumpEnable) {
                    int dump_done = 0;
                    dump_done = fwrite(tmp, read_size, 1, mDumpHandle);
                    LOGI("[%s:%d] dump_size:%d dump_done:%d", __FUNCTION__, __LINE__, read_size, dump_done);
                }

            }

            LOGI("has_size=%d  read_need=%d read_size=%d read_all=%d",has_size, read_need, read_size, read_all);

        } while(1);
    }

    return 0;
}

int HWSubtitleSource::subtitle_get_sub_size_fd(int sub_fd)
{
    unsigned long sub_size;
    int r;
    r = ioctl(sub_fd, AMSTREAM_IOC_SUB_LENGTH, &sub_size);
    if (r < 0)
        return 0;
    else
        return sub_size;
}

int HWSubtitleSource::subtitle_read_sub_data_fd(int sub_fd, unsigned char *buf, int length)
{
    int data_size = length, r, read_done = 0;
    while (data_size)
    {
        r = read(sub_fd, buf + read_done, data_size);
        if (r < 0)
            return read_done;
        else
        {
            data_size -= r;
            read_done += r;
        }
    }
    return read_done;
}

int HWSubtitleSource::subtitle_poll_sub_fd(int sub_fd, int timeout)
{
    struct pollfd sub_poll_fd[1];
    if (sub_fd <= 0)
    {
        return 0;
    }
    sub_poll_fd[0].fd = sub_fd;
    sub_poll_fd[0].events = POLLOUT;
    return poll(sub_poll_fd, 1, timeout);
}


//int HWSubtitleSource::startSourceLooper()
//{
//    Mutex::Autolock _l(mSourceLock);
//    if (mpSourceLooper == NULL) {
//        mpSourceLooper = new ALooper;
//        AString looperName = AStringPrintf("AmlSubtitleSource");
//        mpSourceLooper->setName(looperName.c_str());
//        mpSourceLooper->registerHandler(this);
//        mpSourceLooper->start();
//    }
//
//    return 0;
//}

int HWSubtitleSource::isGetLoopBuf()
{
    return 0;//do nothing now

#if 0
    int size = mLoopBuf.avail();
    if (size > LEAST_LOOP_BUFFER_READ_SIZE) {
        return (size < MAST_LOOP_BUFFER_READ_SIZE ? size : MAST_LOOP_BUFFER_READ_SIZE);
    }

    return 0;
#endif
}

int HWSubtitleSource::readData(SourceSocketPkt** pkt)
{
//    mLoopBuf.read(buf, len);
//    return len;
    return 0;
}

int HWSubtitleSource::sourceStart()
{
    LOGI("[%s:%d] sourceStart", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(HWSubtitleSource::kWhatRun, this);
    msg->post();

    return 0;
}

int HWSubtitleSource::writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp)
{
    return 0;
}

int HWSubtitleSource::flush()
{
    return 0;
}

