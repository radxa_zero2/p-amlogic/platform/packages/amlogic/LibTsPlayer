#ifdef _FORTIFY_SOURCE
#undef _FORTIFY_SOURCE
#endif

/***************************************************************************
 * Copyright (C) 2017 Amlogic, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * Description:
 */
/**\file
 * \brief aml user data driver
 *
 * \author Xia Lei Peng <leipeng.xia@amlogic.com>
 * \date 2013-3-13: create the document
 ***************************************************************************/
/*#define LOG_NDEBUG 0*/
#define LOG_TAG "CC_HWSubtitleSource"

#include "CC_HWSubtitleSource.h"
#include <CTC_Log.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <math.h>
#include <signal.h>

#define USERDATA_POLL_TIMEOUT 10
//#define MAX_CC_NUM          64
#define MAX_CC_NUM          10

#define MAX_CC_DATA_LEN  (1024*5 + 4)

#define IS_H264(p)  ((p[0] == 0xb5 && p[3] == 0x47 && p[4] == 0x41 && p[5] == 0x39 && p[6] == 0x34))
#define IS_DIRECTV(p) ((p[0] == 0xb5 && p[1] == 0x00 && p[2] == 0x2f))
#define IS_AVS(p)    ((p[0] == 0x47) && (p[1] == 0x41) && (p[2] == 0x39) && (p[3] == 0x34))
#define IS_ATSC(p)  ((p[0] == 0x47) && (p[1] == 0x41) && (p[2] == 0x39) && (p[3] == 0x34) && (p[4] == 0x3))
#define IS_SCTE(p)  ((p[0]==0x3) && ((p[1]&0x7f) == 1))

#define AMSTREAM_IOC_MAGIC  'S'
#define AMSTREAM_IOC_UD_LENGTH _IOR(AMSTREAM_IOC_MAGIC, 0x54, unsigned long)
#define AMSTREAM_IOC_UD_POC _IOR(AMSTREAM_IOC_MAGIC, 0x55, int)
#define AMSTREAM_IOC_UD_FLUSH_USERDATA _IOR(AMSTREAM_IOC_MAGIC, 0x56, int)
#define AMSTREAM_IOC_UD_BUF_READ _IOR(AMSTREAM_IOC_MAGIC, 0x57, unsigned int)
#define AMSTREAM_IOC_UD_AVAILABLE_VDEC      _IOR(AMSTREAM_IOC_MAGIC, 0x5c, unsigned int)
#define AMSTREAM_IOC_GET_VDEC_ID			_IOR(AMSTREAM_IOC_MAGIC, 0x5d, int)

//////////////////////////////////////////////////////////////

static void dump_cc_data(char *who, int poc, uint8_t *buff, int size)
{
    int i;
    char buf[4096];

    if (size > 1024)
        size = 1024;
    for (i=0; i<size; i++)
    {
        sprintf(buf+i*3, "%02x ", buff[i]);
    }

    ALOGI("CC DUMP:who:%s poc: %d :%s", who, poc, buf);
}

static void aml_free_cc_data (AM_CCData *cc)
{
    if (cc->buf)
        free(cc->buf);
    free(cc);
}

static void aml_swap_data(uint8_t *user_data, int ud_size)
{
    int swap_blocks, i, j, k, m;
    unsigned char c_temp;

    /* swap byte order */
    swap_blocks = ud_size >> 3;
    for (i = 0; i < swap_blocks; i ++) {
        j = i << 3;
        k = j + 7;
        for (m=0; m<4; m++) {
            c_temp = user_data[j];
            user_data[j++] = user_data[k];
            user_data[k--] = c_temp;
        }
    }
}

static uint8_t scte20_char_map[256];

static uint8_t
scte20_get_char (uint8_t c)
{
    if (scte20_char_map[1] == 0) {
        int i;

        for (i = 0; i < 256; i ++) {
            uint8_t v1, v2;
            int     b;

            v1 = i;
            v2 = 0;

            for (b = 0; b < 8; b ++) {
                if (v1 & (1 << b))
                    v2 |= (1 << (7 - b));
            }

            scte20_char_map[i] = v2;
        }
    }

    return scte20_char_map[c];
}

static userdata_type aml_check_userdata_format (uint8_t *buf, int vfmt, int len)
{
    if (len < 8)
        return INVALID_TYPE;
    //vfmt:
    //0 for MPEG
    //2 for H264
    //7 for AVS
    if (vfmt == 2)
    {
        if (IS_H264(buf))
        {
            //ALOGI("CC format is h264_cc_type");
            return H264_CC_TYPE;
        }
        else if (IS_DIRECTV(buf))
        {
            ALOGI("CC format is directv_cc_type");
            return DIRECTV_CC_TYPE;
        }
    }
    else if (vfmt == 7)
    {
        if (IS_AVS(buf)) {
            ALOGI("CC format is avs_cc_type");
            return AVS_CC_TYPE;
        }
    }
    else if (vfmt == 0)
    {
        if (len >= (int)sizeof(aml_ud_header_t))
        {
            aml_ud_header_t *hdr = (aml_ud_header_t*)buf;

            if (IS_ATSC(hdr->atsc_flag))
            {
                ALOGI("CC format is mpeg_cc_type");
                return MPEG_CC_TYPE;
            }
            else if (IS_SCTE(hdr->atsc_flag))
            {
                ALOGI("CC format is scte_cc_type");
                return SCTE_CC_TYPE;
            }
        }
    }

    return INVALID_TYPE;
}

void CC_HWSubtitleSource::aml_flush_cc_data(int vdec_ids)
{
    AM_CCData *cc, *ncc;
    char  buf[256];
    char *pp = buf;
    int   left = sizeof(buf), i, pr;

    std::map<int, MY_CC>::iterator iter;
    iter = myCC.find(vdec_ids);

    for (cc = ud->cc_list; cc; cc = ncc) {
        ncc = cc->next;

        for (i = 0; i < cc->size; i ++) {
            pr = snprintf(pp, left, "%02x ", cc->buf[i]);
            if (pr < left) {
                pp   += pr;
                left -= pr;
            }
        }

        LOGI("cc_write_package decode:%s", buf);

        if (iter != myCC.end()) {
            (iter->second).mLoopBuf.write(cc->buf, cc->size);

            if (mDumpHandle && mDumpEnable) {
                int dump_done = 0;
                dump_done = fwrite(cc->buf, cc->size, 1, mDumpHandle);
                LOGI("[%s:%d] cc->size=%d dump_done:%d", __FUNCTION__, __LINE__,cc->size, dump_done);
            }
        }

        cc->next = ud->free_list;
        ud->free_list = cc;
    }

    ud->cc_list  = NULL;
    ud->cc_num   = 0;
    ud->curr_poc = -1;
}

void CC_HWSubtitleSource::aml_add_cc_data(int vdec_ids, int poc, int type, uint8_t *p, int len)
{
    AM_CCData **pcc, *cc;
    char  buf[256];
    char *pp = buf;
    int   left = sizeof(buf), i, pr;

    LOGI("[%s:%d] ud->cc_num =%d ", __FUNCTION__, __LINE__, ud->cc_num );

    if (ud->cc_num >= MAX_CC_NUM) {
#if 0
        aml_flush_cc_data(vdec_ids);
#else
        cc = ud->cc_list;
        //dump_cc_data("add cc ",cc->poc,cc->buf, cc->size);
        //dev->write_package(dev, cc->buf, cc->size);
        std::map<int, MY_CC>::iterator iter;
        iter = myCC.find(vdec_ids);
        if (iter != myCC.end()) {
            LOGI("[%s:%d] aml_add_cc_data cc->size=%d vdec_ids=%d", __FUNCTION__, __LINE__, cc->size, vdec_ids);


            (iter->second).mLoopBuf.write(cc->buf, cc->size);

            if (mDumpHandle && mDumpEnable) {
                int dump_done = 0;
                dump_done = fwrite(cc->buf, cc->size, 1, mDumpHandle);
                LOGI("[%s:%d] cc->size=%d dump_done:%d", __FUNCTION__, __LINE__,cc->size, dump_done);
            }

            ud->cc_list = cc->next;
            cc->next = ud->free_list;
            ud->free_list = cc;
            ud->cc_num --;
        }
#endif
    }

    for (i = 0; i < len; i ++) {
        pr = snprintf(pp, left, "%02x ", p[i]);
        if (pr < left) {
            pp   += pr;
            left -= pr;
        }
    }

    //ALOGI("CC poc:%d ptype:%d data:%s", poc, type, buf);

    pcc = &ud->cc_list;
    if (*pcc && poc < ((*pcc)->poc - 30))
        aml_flush_cc_data(vdec_ids);

    while ((cc = *pcc)) {
        /*if (cc->poc == poc) {
            aml_flush_cc_data(vdec_ids);
            pcc = &ud->cc_list;
            break;
        }*/

        if (cc->poc > poc) {
            break;
        }

        pcc = &cc->next;
    }

    if (ud->free_list) {
        cc = ud->free_list;
        ud->free_list = cc->next;
    } else {
        cc = (AM_CCData *)malloc(sizeof(AM_CCData));
        cc->buf  = NULL;
        cc->size = 0;
        cc->cap  = 0;
        cc->poc  = 0;
    }

    if (cc->cap < len) {
        cc->buf = (unsigned char *)realloc(cc->buf, len);
        cc->cap = len;
    }

    memcpy(cc->buf, p, len);

    cc->size = len;
    cc->poc  = poc;
    cc->next = *pcc;
    *pcc = cc;

    ud->cc_num ++;
}

void CC_HWSubtitleSource::aml_mpeg_userdata_package(int vdec_ids, int poc, int type, uint8_t *p, int len)
{
#if 0
    int i;
    char display_buffer[10240];
    for (i=0;i<len; i++)
        sprintf(&display_buffer[i*3], " %02x", p[i]);
    ALOGI(0, "mpeg_process len:%d data:%s", len, display_buffer);
#endif

    if (len < 5)
        return;

    if (p[4+4] != 3)
        return;

    if (type == I_TYPE)
        aml_flush_cc_data(vdec_ids);

    if (poc == ud->curr_poc + 1) {
        AM_CCData *cc, **pcc;

        std::map<int, MY_CC>::iterator iter;
        iter = myCC.find(vdec_ids);
        if (iter != myCC.end()) {
            (iter->second).mLoopBuf.write(p, len);
        }
        ud->curr_poc ++;

        pcc = &ud->cc_list;
        while ((cc = *pcc)) {
            if (ud->curr_poc + 1 != cc->poc)
                break;

            if (iter != myCC.end()) {
                (iter->second).mLoopBuf.write(cc->buf, cc->size);
            }
            *pcc = cc->next;
            ud->curr_poc ++;

            cc->next = ud->free_list;
            ud->free_list = cc;
        }

        return;
    }

    aml_add_cc_data(vdec_ids, poc, type, p, len);
}

int CC_HWSubtitleSource::aml_process_scte_userdata(int vdec_ids, uint8_t *data, int len, int flags, uint32_t pts)
{
    int cc_count = 0, cnt, i;
    int field_num;
    uint8_t* cc_section;
    uint8_t cc_data[64] = {0};
    uint8_t* scte_head;
    uint8_t* scte_head_search_position;
    int head_posi = 0;
    int prio, field, line, cc1, cc2, mark, size, ptype, ref;
    int write_position, bits = 0, array_position = 0;
    uint8_t *p;
    int left = len;
    int left_bits;
    uint32_t v;
    uint32_t *pts_in_buffer;
    uint8_t userdata_with_pts[MAX_CC_DATA_LEN];
#if 0
    char display_buffer[10240];
#endif
    int top_bit_value, top_bit_valid;


    if (ud->scte_enable != 1)
        return len;

    v = (data[4] << 24) | (data[5] << 16) | (data[6] << 8) | data[7];
    top_bit_valid = flags & (1<<10);
    top_bit_value = flags & (1<<11);
    if (top_bit_valid == 0)
        top_bit_value = 1;

    ref = (v >> 16) & 0x3ff;
    ptype = (v >> 26) & 7;

#if 0
    for (i=0; i<len; i++)
        sprintf(display_buffer+3*i, " %02x", data[i]);
    ALOGI(0, "scte buffer type %d ref %d top_bit %d %s", ptype, ref, top_bit_value, display_buffer);
#endif

    if (ptype == I_TYPE)
        aml_flush_cc_data(vdec_ids);

    scte_head = data;
    while (head_posi < len)
    {
        scte_head_search_position = &scte_head[head_posi];
        if (IS_SCTE(scte_head_search_position))
            break;
        head_posi += 8;
    }

    if ((len - head_posi) < 8)
        return len;

    p = &data[head_posi + 2];
    cc_data[0] = 0x47;
    cc_data[1] = 0x41;
    cc_data[2] = 0x39;
    cc_data[3] = 0x34;
    cc_data[4] = 0x3;

    left_bits = (len - head_posi) << 3;

#define NST_BITS(v, b, l) (((v) >> (b)) & (0xff >> (8 - (l))))
#define GET(n, b)\
    do \
    {\
        int off, bs;\
        if (bits + b > left_bits) goto error;\
        off = bits >> 3;\
        bs  = bits & 7;\
        if (8 - bs >= b) {\
          n = NST_BITS(p[off], 8 - bs - b, b);\
        } else {\
          int n1, n2, b1 = 8 - bs, b2 = b- b1;\
          n1 = NST_BITS(p[off], 0, b1);\
          n2 = NST_BITS(p[off + 1], 8 - b + b1, b - b1);\
          n = (n1 << b2) | n2;\
        }\
        bits += b;\
    } while(0)

    GET(cnt, 5);
    array_position = 7;
    for (i = 0; i < cnt; i ++) {
        GET(prio, 2);
        GET(field, 2);
        GET(line, 5);
        GET(cc1, 8);
        GET(cc2, 8);
        GET(mark, 1);
        if (field == 3)
            field = 1;
        ALOGI("loop %d field %d line %d cc1 %x cc2 %x",
            i, field, line, cc1, cc2);
        if (field == 1)
            line = (top_bit_value)?line+10:line+273;
        else if (field == 2)
            line = (top_bit_value)?line+273:line+10;
        else
            continue;

        if (line == 21)
        {
            cc_data[array_position] = 4;
            cc_data[array_position + 1] = scte20_get_char(cc1);
            cc_data[array_position + 2] = scte20_get_char(cc2);
            array_position += 3;
            cc_count++;
        }
        else if (line == 284)
        {
            cc_data[array_position] = 4|1;
            cc_data[array_position + 1] = scte20_get_char(cc1);
            cc_data[array_position + 2] = scte20_get_char(cc2);
            array_position += 3;
            cc_count++;
        }
        else
            continue;
    }
    cc_data[5] = 0x40 |cc_count;
    size = 7 + cc_count*3;

    pts_in_buffer = reinterpret_cast<unsigned int *>(userdata_with_pts);
    *pts_in_buffer = pts;
    memcpy(userdata_with_pts+4, cc_data, size);
#if 0
    for (i=0; i<size; i++)
            sprintf(display_buffer+3*i, " %02x", cc_data[i]);
        //ALOGI(0, "scte_write_buffer len: %d data: %s", size, display_buffer);
#endif
    if (cc_count > 0)
        aml_add_cc_data(vdec_ids, ref, ptype, userdata_with_pts, size+4);
error:
    return len;
}

int CC_HWSubtitleSource::aml_process_mpeg_userdata(int vdec_ids, uint8_t *data, int flag, uint32_t pts, int len)
{
    uint8_t *pd = data;
    int left = len;
    int r = 0;
    int i;
    int package_count = 0;
    uint32_t *pts_in_buffer;
    int userdata_length;
    uint8_t userdata_with_pts[MAX_CC_DATA_LEN];

    while (left >= (int)sizeof(aml_ud_header_t)) {
        aml_ud_header_t *hdr = (aml_ud_header_t*)pd;
        int ref, ptype;

        if (IS_ATSC(hdr->atsc_flag) ) {
            aml_ud_header_t *nhdr;
            uint8_t *pp, t;
            uint32_t v;
            int pl;

            pp = (uint8_t*)&hdr->atsc_flag;
            pl = 8;

            v = (pd[4] << 24) | (pd[5] << 16) | (pd[6] << 8) | pd[7];
            ref   = (v >> 16) & 0x3ff;
            ptype = (v >> 26) & 7;

            /* We read one packet in one time, so treat entire buffer */
            if (!(flag & (1<<2)))
                return len;

            userdata_length = len-r-8;
            pts_in_buffer = reinterpret_cast<unsigned int *>(userdata_with_pts);
            *pts_in_buffer = pts;
            memcpy(userdata_with_pts+4, pp, userdata_length);
            //ALOGI(0, "CC header len: %d pts %x flag %d digi: %02x %02x %02x %02x %02x %02x %02x %02x", userdata_length, pts, flag,
            //  pd[0], pd[1], pd[2], pd[3], pd[4], pd[5], pd[6], pd[7]);
            aml_mpeg_userdata_package(vdec_ids, ref, ptype, userdata_with_pts, userdata_length+4);
            r = len;

            break;
        } else {
            pd   += 8;
            left -= 8;
            r   += 8;
        }
    }

    return r;
}

int CC_HWSubtitleSource::aml_process_h264_userdata(int vdec_ids, uint8_t *data, int len, int poc, uint32_t pts)
{
    int fd = ud->fd;
    uint8_t *pd = data;
    int left = len;
    int r = 0;
    uint32_t *pts_in_buffer;
    uint8_t userdata_with_pts[MAX_CC_DATA_LEN];

    //dump_cc_data("wcs-123:", poc, pd, len);

    while (left >= 8) {
        if (IS_H264(pd) || IS_DIRECTV(pd) || IS_AVS(pd)) {
            int hdr = (ud->format == H264_CC_TYPE) ? 3 : 0;
            int pl;

            pd += hdr;

            pl = 8 + (pd[5] & 0x1f) * 3;

            //ALOGI("aml_process_h264_userdata: pd[5]:0x%x, pl=%d", pd[5], pl);

            if (pl + hdr > left) {
                break;
            }

            //ALOGI("CC poc_number:%x hdr:%d pl:%d", poc, hdr, pl);
            if (poc == 0) {
                aml_flush_cc_data(vdec_ids);
            }
            //dump_cc_data("process h264:", poc, pd, pl);
            pts_in_buffer = reinterpret_cast<unsigned int *>(userdata_with_pts);
            *pts_in_buffer = pts;
            memcpy(userdata_with_pts+4, pd, pl);
            aml_add_cc_data(vdec_ids, poc, I_TYPE, userdata_with_pts, pl+4);

            pd   += pl;
            left -= pl + hdr;
            r   += pl + hdr;
        } else {
            pd   += 8;
            left -= 8;
            r   += 8;
        }
    }

    return r;
}

void CC_HWSubtitleSource::read_vdec_user_data(int vdec_ids)
{
    int r, i;
    uint8_t data[MAX_CC_DATA_LEN];
    uint8_t *pd = data;
    int left = 0;
    int index = 0;
    int fd = ud->fd;
    int cur_vdec_id = vdec_ids;
    struct userdata_param_t user_para_info;

    //char display_buffer[10*1024];

    LOGI("read_vdec_user_data, vdec_ids=%d, cur_vdec_id=%d", vdec_ids, cur_vdec_id);

    std::map<int, MY_CC>::iterator iter;
    iter = myCC.find(vdec_ids);

    do {
        memset(&user_para_info, 0, sizeof(struct userdata_param_t));
        user_para_info.pbuf_addr = (void*)data;
        user_para_info.buf_len = sizeof(data);
        user_para_info.instance_id = vdec_ids;

        if (-1 == ioctl(fd, AMSTREAM_IOC_UD_BUF_READ, &user_para_info))
            ALOGI("call AMSTREAM_IOC_UD_BUF_READ failed, errno=%d, %s\n", errno, strerror(errno));

        //ALOGI("ioctl left data: %d, user_para_info.data_size=%u, ud->vfmt=%d, vdec-%d",
        //   user_para_info.meta_info.records_in_que, user_para_info.data_size, ud->vfmt, vdec_ids);
        index++;

        //Old userdata was read from node. now use ioctl to get data.
        // memset(&poc_block, 0, sizeof(userdata_poc_info_t));
        // r = read(fd, data + left, sizeof(data) - left);
/*
        if (flush) {
            ioctl(fd, AMSTREAM_IOC_UD_FLUSH_USERDATA, NULL);
            flush = 0;
            continue;
        }
*/
        r = user_para_info.data_size;
        r = (r > MAX_CC_DATA_LEN) ? MAX_CC_DATA_LEN : r;

        if (r <= 0)
            continue;
#if 0
        memset(display_buffer, 0, 10*1024);
        for (i=0; i<r; i++)
            sprintf(&display_buffer[i*3], " %02x", data[i]);
        ALOGI("[%s,%d] vpts: 0x%x ,ud_aml_buffer: %s", __FUNCTION__, __LINE__, user_para_info.meta_info.vpts, display_buffer);
#endif

        aml_swap_data(data + left, r);
        left += r;
        pd = data;
#if 0
        memset(display_buffer, 0, 10*1024);
        for (i=0; i<left; i++)
            sprintf(&display_buffer[i*3], " %02x", data[i]);
        ALOGI("[%s,%d] vpts: 0x%x ud_aml_buffer: %s", __FUNCTION__, __LINE__,user_para_info.meta_info.vpts, display_buffer);
#endif
        ud->format = INVALID_TYPE;
        if (iter != myCC.end()) {
            ud->vfmt= (iter->second).vfmt;
        }

        while (ud->format == INVALID_TYPE) {
            if (left < 8)
                break;

            ud->format = aml_check_userdata_format(pd, ud->vfmt, left);
            if (ud->format == INVALID_TYPE) {
                pd   += 8;
                left -= 8;
            }
        }

        ALOGI("----ud->vfmt=%d,ud->format=%d, left=%d--\n",ud->vfmt, ud->format, left);

        if (ud->format == MPEG_CC_TYPE) {
            ud->scte_enable = 0;
            r = aml_process_mpeg_userdata(vdec_ids, pd, user_para_info.meta_info.flags,user_para_info.meta_info.vpts, left);
        } else if (ud->format == SCTE_CC_TYPE) {
            if (ud->scte_enable == 1)
                r = aml_process_scte_userdata(vdec_ids, pd, left, user_para_info.meta_info.flags, user_para_info.meta_info.vpts);
            else
                r = left;
        } else if (ud->format != INVALID_TYPE) {
            r = aml_process_h264_userdata(vdec_ids, pd, left, user_para_info.meta_info.poc_number , user_para_info.meta_info.vpts);
        } else {
            r = left;
        }

        if ((data != pd + r) && (r < left)) {
            memmove(data, pd + r, left - r);
        }

        left -= r;
    }while(user_para_info.meta_info.records_in_que > 1);

}


//static void* aml_userdata_thread (void *arg)
//{
//    int fd = ud->fd;
//    int cur_vdec_id = ud->vdec_id;
//    int ret, i;
//    int flush[32];
//    struct pollfd pfd;
//    uint8_t data[MAX_CC_DATA_LEN];
//    uint8_t *pd = data;
//
//    int left = 0;
//
//    for (i = 0; i < 32; i++)
//        flush[i] = 1;
//
//
//    pfd.events = POLLIN | POLLERR;
//    pfd.fd   = fd;
//
//    while (ud->running) {
//        //If scte and mpeg both exist, we need to ignore scte cc data,
//        //so we need to check cc type every time.
//        left = 0;
//
//        ret = poll(&pfd, 1, USERDATA_POLL_TIMEOUT);
//        //ALOGI("ret=%d, ud->running=%d--\n", ret, ud->running);
//        if (!ud->running)
//            break;
//        if (ret != 1) {
//            ALOGV("poll failed, ret=%d",  ret);
//            continue;
//        }
//        if (!(pfd.revents & POLLIN)) {
//            ALOGI("poll failed, pfd.revents=0x%x, ud->running:%d, ud:%p",  pfd.revents, ud->running, ud);
//            continue;
//        }
//
//        int vdec_ids = 0;
//        if (-1 == ioctl(fd, AMSTREAM_IOC_UD_AVAILABLE_VDEC, &vdec_ids)) {
//            ALOGV("get avaible vdec failed\n");
//        } else {
//            ALOGV("get avaible vdec OK: 0x%x\n", vdec_ids);
//        }
//
//        for (i = 0; i < 32; i++) {
//            if (vdec_ids & (1<<i)) {
//                //Old userdata was read from node. now use ioctl to get data.
//                // memset(&poc_block, 0, sizeof(userdata_poc_info_t));
//                // r = read(fd, data + left, sizeof(data) - left);
//                if (flush[i]) {
//                    ioctl(fd, AMSTREAM_IOC_UD_FLUSH_USERDATA, i);
//                    flush[i] = 0;
//                    continue;
//                }
//                if (i == cur_vdec_id) {
//                    read_vdec_user_data(dev, i);
//                }
//            }
//        }
//
//
//    }
//    ALOGI("aml userdata thread exit ud:%p", ud);
//    return NULL;
//}

int CC_HWSubtitleSource::aml_open()
{
    ud = (AM_UDDrvData *)calloc(1, sizeof(AM_UDDrvData));
    int r;

    if (!ud) {
        ALOGI("not enough memory");
        return -1;
    }

    ud->fd = open("/dev/amstream_userdata", O_RDONLY);
    if (ud->fd == -1) {
        ALOGI("cannot open userdata device");
        free(ud);
        return -1;
    }

    ud->vfmt = INVALID_TYPE;
    ud->format  = INVALID_TYPE;
    ud->cc_list   = NULL;
    ud->free_list = NULL;
    ud->running   = true;
    ud->cc_num  = 0;
    ud->curr_poc  = -1;
    ud->scte_enable = 1;

//    LOGI("[%s:%d] para->vfmt:%d, para->cur_vdec_id=%d", __FUNCTION__, __LINE__, para->vfmt, para->cur_vdec_id);

    return 0;
}

int CC_HWSubtitleSource::aml_close()
{
    AM_CCData *cc, *cc_next;

    ALOGI("---%s---\n", __FUNCTION__);

    ud->running = false;
    ALOGI("set ud->running %d, ud:%p", ud->running, ud);

    close(ud->fd);

    for (cc = ud->cc_list; cc; cc = cc_next) {
        cc_next = cc->next;
        aml_free_cc_data(cc);
    }

    for (cc = ud->free_list; cc; cc = cc_next) {
        cc_next = cc->next;
        aml_free_cc_data(cc);
    }


    free (ud);

    return 0;
}

CC_HWSubtitleSource::CC_HWSubtitleSource()
{
    mpLooper = NULL;
    mFlush = true;
    myCC.clear();

    mDumpEnable = false;
    if (mDumpEnable) {
        mDumpHandle = fopen("/data/CC_HWSubtitleSource.dump", "wb");
        if (mDumpHandle) {
            LOGI("[%s:%d] enable HWSubtitleSource dump", __FUNCTION__, __LINE__);
        }
    }

}

CC_HWSubtitleSource::~CC_HWSubtitleSource()
{
    aml_close();

    if (mDumpEnable) {
        fclose(mDumpHandle);
    }
}

void CC_HWSubtitleSource::onMessageReceived(const sp<AMessage> &msg)
{
    Mutex::Autolock _l(mSourceLock);

    switch (msg->what()) {

    case kWhatRun:
        workThread(msg);
        break;

    default:
        LOGI("[%s:%d] unknow kWhat", __FUNCTION__, __LINE__);
        break;
    }

}

int CC_HWSubtitleSource::init_param(const aml::SUBTITLE_PARA_T& param)
{
    MY_CC cc;
    cc.channel_id = param.cc_param.Channel_ID;
    cc.vfmt = param.cc_param.vfmt;
    cc.format = INVALID_TYPE;

    myCC.insert(std::pair<int, MY_CC>(cc.channel_id,cc));

    LOGI("[%s:%d] init_param vfmt:%d, cur_vdec_id=%d", __FUNCTION__, __LINE__, param.cc_param.vfmt, param.cc_param.Channel_ID);

    return 0;
}

/*
int CC_HWSubtitleSource::sourceInit(const SUBTITLE_PARA_T& param)
{
    int ret = -1;

    startSourceLooper();

    if (mpLooper == NULL) {
        mpLooper = new Looper(false);
    }

    AM_USERDATA_OpenPara_t para = {0,0};
    if (param.sub_type == SubtitleType::CC) {
        para.vfmt = param.cc_param.vfmt;
        para.cur_vdec_id = param.cc_param.Channel_ID;
    }

    ret = aml_open(&para);
    if (ret < 0) {
        LOGI("[%s:%d] open error", __FUNCTION__, __LINE__);
        return -1;
    } else {
        LOGI("[%s:%d] open ok,ud->fd=%d", __FUNCTION__, __LINE__, ud->fd);
    }
    ret = mpLooper->addFd(ud->fd, 0, Looper::EVENT_INPUT | Looper::EVENT_ERROR, callbackFunc, (void *)this);
    LOGI("[%s:%d] addFd = %d", __FUNCTION__, __LINE__, ret);

    return 0;
}
*/

int CC_HWSubtitleSource::sourceInit(const aml::SUBTITLE_PARA_T& param)
{
    LOGI("[%s:%d] sourceInit", __FUNCTION__, __LINE__);

    int ret = -1;

    //startSourceLooper();
    init_param(param);

    return 0;
}

void CC_HWSubtitleSource::onFirstRef()
{
    int ret;
    LOGI("[%s:%d] onFirstRef", __FUNCTION__, __LINE__);
    if (mpLooper == NULL) {
        mpLooper = new Looper(false);
    }

    ret = aml_open();
    if (ret < 0) {
        LOGI("[%s:%d] open error", __FUNCTION__, __LINE__);
    } else {
        LOGI("[%s:%d] open ok,ud->fd=%d", __FUNCTION__, __LINE__, ud->fd);
    }
    ret = mpLooper->addFd(ud->fd, 0, Looper::EVENT_INPUT | Looper::EVENT_ERROR, callbackFunc, (void *)this);
    LOGI("[%s:%d] addFd = %d", __FUNCTION__, __LINE__, ret);
}

int CC_HWSubtitleSource::sourceStart()
{
    LOGI("[%s:%d] sourceStart", __FUNCTION__, __LINE__);

    sp<AMessage> msg = new AMessage(CC_HWSubtitleSource::kWhatRun, this);
    msg->post();

    return 0;
}

int CC_HWSubtitleSource::isGetLoopBuf()
{
//TODO:
    int size = 0;
    std::map<int, MY_CC>::iterator iter;
    iter = myCC.find(0);

    if (iter != myCC.end()) {
        size = (iter->second).mLoopBuf.avail();
        LOGI("[%s:%d] size=%d", __FUNCTION__, __LINE__, size);
        if (size > 1024) {
            return 1024;
        } else if (size > 100 ) {
            return size;
        }
    }

    return 0;
}

int CC_HWSubtitleSource::readData(SourceSocketPkt** pkt)
{
//TODO:
/*
    std::map<int, MY_CC>::iterator iter;
    iter = myCC.find(0);


    if (iter != myCC.end()) {
        (iter->second).mLoopBuf.read(buf, len);
    }

    return len;
*/
    return 0;
}

int CC_HWSubtitleSource::workThread(const sp<AMessage> &msg)
{
    int ret = mpLooper->pollOnce(10);

    LOGI("[%s:%d] workThread ret %d", __FUNCTION__, __LINE__, ret);// POLL_CALLBACK = -2,
    msg->post(10);//this delay refer to sub_jni.c
    return ret;
}

int CC_HWSubtitleSource::callbackFunc(int fd, int events, void* data)
{
    LOGI("[%s:%d] cc_callbackFunc run", __FUNCTION__, __LINE__);
    sp<CC_HWSubtitleSource> mSource = static_cast<CC_HWSubtitleSource *>(data);
    mSource->getSubData();

    return 1;
}

int CC_HWSubtitleSource::getSubData()
{
    LOGI("[%s:%d] getSubData ", __FUNCTION__, __LINE__);
    int i = 0;
    int vdec_ids = 0;
    int cur_vdec_id = 0;
    int fd = ud->fd;

    if (-1 == ioctl(fd, AMSTREAM_IOC_UD_AVAILABLE_VDEC, &vdec_ids)) {
        LOGI("get avaible vdec failed\n");
    } else {
        LOGI("get avaible vdec OK: 0x%x\n", vdec_ids);
    }

    for (i = 0; i < 32; i++) {
        if (vdec_ids & (1<<i)) {
            if (mFlush) {
                ioctl(fd, AMSTREAM_IOC_UD_FLUSH_USERDATA, i);
                mFlush = false;
                LOGI("[%s:%d] flush at first i=%d", __FUNCTION__, __LINE__, vdec_ids);
                continue;
            }
            if (i == cur_vdec_id) {
                read_vdec_user_data(i);
            }
        }
    }

    return 0;
}

//int CC_HWSubtitleSource::startSourceLooper()
//{
//    Mutex::Autolock _l(mSourceLock);
//    if (mpSourceLooper == NULL) {
//        mpSourceLooper = new ALooper;
//        AString looperName = AStringPrintf("CC_HWSubtitleSource");
//        mpSourceLooper->setName(looperName.c_str());
//        mpSourceLooper->registerHandler(this);
//        mpSourceLooper->start();
//    }
//
//    return 0;
//}

Mutex gCC_HWSubtitleSourceLock;
sp<CC_HWSubtitleSource> gCC_HWSubtitleSource;

sp<CC_HWSubtitleSource> getCC_HWSubtitleSource()
{
    if (gCC_HWSubtitleSource != NULL) return gCC_HWSubtitleSource;

    {
        Mutex::Autolock _l(gCC_HWSubtitleSourceLock);
        while (gCC_HWSubtitleSource == NULL) {
            gCC_HWSubtitleSource = new CC_HWSubtitleSource();

            if (gCC_HWSubtitleSource == NULL)
                LOGI("[%s:%d] NUll ", __FUNCTION__, __LINE__);
        }
    }

    LOGI("[%s:%d] getCC_HWSubtitleSource ", __FUNCTION__, __LINE__);

    return gCC_HWSubtitleSource;
}

int CC_HWSubtitleSource::writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp)
{
    return 0;
}

int CC_HWSubtitleSource::flush()
{
    return 0;
}


