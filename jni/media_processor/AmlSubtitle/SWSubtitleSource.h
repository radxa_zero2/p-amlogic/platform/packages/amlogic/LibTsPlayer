#ifndef _SWSUBTITLE_SOURCE_
#define _SWSUBTITLE_SOURCE_

#include "SubtitleSource.h"
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/AHandler.h>
#include <utils/Looper.h>

#include <list>

namespace android {


class SWSubtitleSource : public SubtitleSource
{
public:
    SWSubtitleSource();
    virtual ~SWSubtitleSource();

    virtual int sourceInit(const aml::SUBTITLE_PARA_T& param);
    virtual int sourceStart();

    virtual int isGetLoopBuf();
    virtual int readData(SourceSocketPkt** pkt);
    virtual int writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp);
    virtual int flush();

    static int callbackFunc(int fd, int events, void* data);

protected:
    virtual void onMessageReceived(const sp<AMessage> &msg);

    int workThread(const sp<AMessage> &msg);
    int getSubData();

private:
    bool mDebug;
    mutable Mutex mSourceLock;

    sp<Looper> mpLooper;
    //mutable Mutex mLock;

    //dumptest
    bool mDumpEnable;
    FILE* mDumpHandle = nullptr;

    //loop buf
    std::list<PSourceSocketPkt> mLoopBuf;
    mutable Mutex mSWSourceLock;
};


}

#endif
