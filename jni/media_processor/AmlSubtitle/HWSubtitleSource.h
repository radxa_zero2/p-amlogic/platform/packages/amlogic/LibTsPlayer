#ifndef _HWSUBTITLE_SOURCE_
#define _HWSUBTITLE_SOURCE_

#include "SubtitleSource.h"
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/AHandler.h>
#include <utils/Looper.h>

namespace android {

class HWSubtitleSource : public SubtitleSource
{
public:
    HWSubtitleSource();
    virtual ~HWSubtitleSource();

    virtual int sourceInit(const aml::SUBTITLE_PARA_T& param);
    virtual int sourceStart();

    virtual int isGetLoopBuf();
    virtual int readData(SourceSocketPkt** pkt);

    virtual int writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp);
    virtual int flush();

    static int callbackFunc(int fd, int events, void* data);

protected:
    virtual void onMessageReceived(const sp<AMessage> &msg);

    int workThread(const sp<AMessage> &msg);
    int getSubData();

    int subtitle_get_sub_size_fd(int sub_fd);
    int subtitle_read_sub_data_fd(int sub_fd, unsigned char *buf, int length);
    int subtitle_poll_sub_fd(int sub_fd, int timeout);

private:
    mutable Mutex mSourceLock;
//    sp<ALooper> mpSourceLooper;

    sp<Looper> mpLooper;
    //mutable Mutex mLock;
    int mfd = -1;


    //dumptest
    bool mDumpEnable;
    FILE* mDumpHandle = nullptr;

    //loop buf
    SubtitleSourceLBuf mLoopBuf;

};

}
#endif
