#define LOG_TAG "SWSubtitleSource"
#include <CTC_Log.h>
#include "SWSubtitleSource.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <sys/ioctl.h>
#include <amports/amstream.h>
#include <utils/Mutex.h>
#include <utils/String16.h>
#include <media/stagefright/foundation/ALooper.h>


#define LEAST_LOOP_BUFFER_READ_SIZE (0)
#define MAST_LOOP_BUFFER_READ_SIZE (10240)

#define LEAST_AMSTREAM_SUBTITLE_READ (200)


using namespace android;


SWSubtitleSource::SWSubtitleSource()
{
    LOGI("SWSubtitleSource");
    mDebug = true;

    mDumpEnable = false;
    if (mDumpEnable) {
        mDumpHandle = fopen("/data/SWSubtitleSource.dump", "wb");
        if (mDumpHandle) {
            LOGI("[%s:%d] enable SWSubtitleSource dump", __FUNCTION__, __LINE__);
        } else {
            LOGI("[%s:%d] SWSubtitleSource fopen failed", __FUNCTION__, __LINE__);
        }
    }

    mLoopBuf.clear();
}

SWSubtitleSource::~SWSubtitleSource()
{
    LOGI("~SWSubtitleSource");

    if (mDumpEnable) {
        fclose(mDumpHandle);
    }

    flush();
}

void SWSubtitleSource::onMessageReceived(const sp<AMessage> &msg)
{
    Mutex::Autolock _l(mSourceLock);

    switch (msg->what()) {

    case kWhatRun:
        workThread(msg);
        break;

    default:
        LOGI("[%s:%d] unknow kWhat", __FUNCTION__, __LINE__);
        break;
    }
}

int SWSubtitleSource::sourceInit(const aml::SUBTITLE_PARA_T& param)
{
    int ret = -1;
    LOGI("[%s:%d] sourceInit", __FUNCTION__, __LINE__);

    return ret;
}

int SWSubtitleSource::workThread(const sp<AMessage> &msg)
{
    int ret = -1;
    return ret;
}

int SWSubtitleSource::callbackFunc(int fd, int events, void* data)
{
    LOGI("[%s:%d] callbackFunc run", __FUNCTION__, __LINE__);
    sp<SWSubtitleSource> mSource = static_cast<SWSubtitleSource *>(data);
    mSource->getSubData();

    return 1;
}

int SWSubtitleSource::getSubData()
{
    LOGI("[%s:%d] getSubData ", __FUNCTION__, __LINE__);

    return 0;
}

int SWSubtitleSource::isGetLoopBuf()
{
    return mLoopBuf.size();
}

int SWSubtitleSource::readData(SourceSocketPkt** pkt)
{
    ALOGD("[%s:%d] readData enter=%p,size=%d", __FUNCTION__, __LINE__, *pkt, mLoopBuf.size());

    {
        Mutex::Autolock _l(mSWSourceLock);
        *pkt = mLoopBuf.front();
        mLoopBuf.pop_front();
    }

    ALOGD("[%s:%d] readData leave=%p,size=%d", __FUNCTION__, __LINE__, *pkt, mLoopBuf.size());

    return 0;
}

int SWSubtitleSource::sourceStart()
{
    LOGI("[%s:%d] sourceStart", __FUNCTION__, __LINE__);

    return 0;
}

int SWSubtitleSource::writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp)
{
    PSourceSocketPkt pkt = (PSourceSocketPkt)malloc(sizeof(SourceSocketPkt));
    if (pkt == NULL) {
        LOGI("[%s:%d] malloc", __FUNCTION__, __LINE__);
        return -1;
    }
    memset(pkt, 0, sizeof(SourceSocketPkt));
    pkt->rawData = (uint8_t *)malloc(nSize);
    if (pkt->rawData == NULL) {
        LOGI("[%s:%d] malloc", __FUNCTION__, __LINE__);
        free(pkt);
        return -1;
    }
    pkt->vers           = 0;
    pkt->videoId        = 0;
    pkt->subtitleCount  = 0;
    pkt->type           = aml::UNKNOWN_SUBTYPE;
    pkt->pts            = timestamp & 0xFFFFFFFF;
    pkt->duration       = 0;
    pkt->headerSize     = -1;
    pkt->headerData     = NULL;
    pkt->rawDataSize    = nSize;
    memcpy(pkt->rawData, pBuffer, nSize);

    ALOGD("[%s:%d] writeData enter ", __FUNCTION__, __LINE__);

    {
        Mutex::Autolock _l(mSWSourceLock);
        mLoopBuf.push_back(pkt);
    }

    ALOGD("[%s:%d] writeData enter leave buf=%p nSize=%d timestamp=%llu size=%d", __FUNCTION__, __LINE__, pkt, nSize, timestamp, mLoopBuf.size());

    return 0;
}

int SWSubtitleSource::flush()
{
    ALOGD("[%s:%d] flush enter size=%d", __FUNCTION__, __LINE__, mLoopBuf.size());

    {
        Mutex::Autolock _l(mSWSourceLock);
        for (std::list<PSourceSocketPkt>::iterator it = mLoopBuf.begin(); it != mLoopBuf.end(); ) {
            free((*it)->rawData);
            free((*it));
            mLoopBuf.erase(it++);
        }
        mLoopBuf.clear();
    }

    ALOGD("[%s:%d] flush exit size=%d", __FUNCTION__, __LINE__, mLoopBuf.size());

    return 0;
}


