#ifndef _AMSUBTITLE_H_
#define _AMSUBTITLE_H_


#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/AHandler.h>
#include <CTC_Common.h>
#include <utils/Errors.h>
#include <functional>
#include <CTC_Internal.h>


class SubtitleClient;


namespace android {
class SubtitleSource;


class AmlSubtitle : public AHandler
{
public:
    enum DemuxType {
        HW_DEMUXTYPE,
        SW_DEMUXTYPE,
        NONE_DEMUXTYPE
    };

    enum SUB_Event : uint32_t{
        INFO,
        Subtitle_Available,
        Subtitle_Unavailable,
        CC_Removed,
        CC_Added,
        ERROR,
    };

    using ClockCallback = std::function<status_t(int64_t* timeUs)>;
    using EventCallback = std::function<void(SUB_Event, int32_t, int32_t)>;

    struct CallbackParam {
        ClockCallback clockCb;
        EventCallback eventCb;
    };

    struct ViewAttribute {
        bool        opaque;
    };

    enum InputSource : uint32_t{
        EXTERNAL_SOURCE,
        INTERNAL_SOURCE,
    };


public:
    AmlSubtitle();
    virtual ~AmlSubtitle();

    int init();
    int connect();
    int prepare(const aml::SUBTITLE_PARA_T& param);
    int start();
    int stop();
    int setVisible(bool visible);
    int isVisible();
    int disconnect();
    int flush();
    void openTransferChannel();

    status_t setViewWindow(int x, int y, int width, int height);
    status_t setViewAttribute(const AmlSubtitle::ViewAttribute& attr);
    status_t getSourceAttribute(aml::SUBTITLE_PARA_T* param);

    void subtitle_setCCDataChecktime(int check_time);
    void subtitle_setSubDataChecktime(int check_time);
    aml::SubtitleType getCurrentType();
    int getCurrentPageInf(int* Page_no, int* Sub_Page_No);
    void subtitle_register_evt_cb(aml::CTC_PLAYER_EVT_CB pfunc, void *hander);
    void subtitle_register_clock_cb(aml::CTC_PLAYER_CLOCK_CB pfunc, void *hander);


    AmlSubtitle::DemuxType getDefaultDemuxType();
    int setDefaultDemuxType(AmlSubtitle::DemuxType type);

    int writeData(uint8_t *pBuffer, uint32_t nSize, uint64_t timestamp);
    int setSubCount(int total);
    void detectClosedCaption(bool enable);
    int ClosedCaptionsRemoved();
    int getClosedCaptionsRemoved();
    int getClosedCaptionsEnable();

protected:
    virtual void onMessageReceived(const sp<AMessage> &msg);
    int onConnect(const sp<AMessage> &msg);
    int onInit(const sp<AMessage> &msg);
    int onStart(const sp<AMessage> &msg);
    int onStop(const sp<AMessage> &msg);
    int onSetVisible(const sp<AMessage> &msg);
    int onDisconnect(const sp<AMessage> &msg);
    int onGet(const sp<AMessage> &msg);
    int onFlush();

    int startMsgLooper();
    int stopMsgLooper();
    int registerMsgHandler(const sp<AHandler> &handler);
    int startSourceLooper();
    int stopSourceLooper();
    int isGet();
    int registerSourceHandler(const sp<AHandler> &handler);
    int setInputSource(AmlSubtitle::InputSource input);
    static void subtitle_callback(SUB_Event evt, int32_t param1, int32_t param2);
    void registerCallback(const AmlSubtitle::CallbackParam& cbParam);

    void onOpenTransferChannel();
    void setSubInfo();
    static status_t  clock_callback(int64_t* timeUs);
    int sendPTS();
    int onSendPTS(const sp<AMessage> &msg);
    void sendTime(int64_t timeUs);
    void setSubStartPts(void* pkt);

private:
    enum {
        kWhatConnect,
        kWhatInit,
        kWhatStart,
        kWhatStop,
        kWhatVisible,
        kWhatDisconnect,
        kWhatIsGet,
        kWhatSendVPTS,
        kWhatCompleteConnection,
        kWhatOpenTransfer,
        kWhatFlush
    };
    mutable Mutex mLock;
    sp<ALooper> mpLooper;
    mutable Mutex mSourceLock;
    sp<ALooper> mpSourceLooper;
    bool mDetectCC = false;

    aml::SUBTITLE_PARA_T    mSubtitleParam;
    sp<SubtitleClient> mSubtitleClient;
    sp<SubtitleSource> mSubtitleSource;
    sp<SubtitleSource> mCCSubtitleSource;

    DemuxType mDemuxType;

    //dumptest
    bool mDumpEnable;
    FILE* mDumpHandle = nullptr;
    bool mAmlSubDebug = false;

    //subtitle params
    AmlSubtitle::CallbackParam mCallbackParam;
    int mSubCount = 0;
    bool mStartPtsUpdate = false;
    int64_t mStartPts = -1;

    //cc status
    bool mCCremoved = false;
    bool mCCenabled = false;

    bool mStarted = false;
};

}

#endif
