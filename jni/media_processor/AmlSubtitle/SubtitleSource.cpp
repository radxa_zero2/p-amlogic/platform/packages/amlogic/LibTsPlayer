#define LOG_TAG "SubtitleSource"
#include "SubtitleSource.h"
#include <CTC_Log.h>
#include <CTC_Common.h>



using namespace android;

SubtitleSource::SubtitleSource()
{
//    mLoopBuf = NULL;
//    mRPtr = NULL;
//    mWPtr = NULL;
//    if (mLoopBuf == NULL) {
//        mLoopBuf = (char *)malloc(SOURCE_LOOP_BUFFER_SIZE);
//        if (NULL == mLoopBuf) {
//            LOGI("[%s:%d] malloc buf failed.", __FUNCTION__, __LINE__);
//        }
//        memset(mLoopBuf, 0, SOURCE_LOOP_BUFFER_SIZE);
//
//        mRPtr = mLoopBuf;
//        mWPtr = mLoopBuf;
//        LOGI("[%s:%d] sPtr:0x%x, mWPtr:0x%x, mRPtr:0x%x, \n", __FUNCTION__, __LINE__, mLoopBuf, mWPtr, mRPtr);
//    }
}

SubtitleSource::~SubtitleSource()
{
//    mRPtr = mLoopBuf;
//    mWPtr = mLoopBuf;
//    if (mLoopBuf != NULL) {
//        ::free(mLoopBuf);
//        mLoopBuf = NULL;
//        mRPtr = NULL;
//        mWPtr = NULL;
//    }
}

void SubtitleSource::onMessageReceived(const sp<AMessage> &msg)
{
    ;
}


/**
 * Safe copy for loop buffer, check edge before copy
 * @param
 * sPtr : loop buffer start pointer
 * mWPtr : current write pointer
 * mRPtr : current read pointer
 * src : source data for copy
 * size : source data size
 */
 /*
void SubtitleSource::loopBufCopy(char* src, int size) {
    char* ptrStart = mLoopBuf;
    char* ptrEnd = mLoopBuf + SOURCE_LOOP_BUFFER_SIZE;
    int leftReg = 0;//from nearest wptr to ptrEnd

     if (size <= 0) return;
    // a simple workaround to avoid overflow. do not care if retry still fail
    int retry_time = 10000;
    int receivedSize = 0;
    while (retry_time-- > 0) {
        receivedSize = (mWPtr >= mRPtr) ? mWPtr - mRPtr : (ptrEnd - mRPtr) + (mWPtr - ptrStart);
        if ((SOURCE_LOOP_BUFFER_SIZE-receivedSize) > size)
            break;
        usleep(1000);
    }
    if (retry_time <= 0) {
        ALOGE("[%s:%d][safeRead] DATA_CORRUCTION MAY HAPPENED! Error!!!!", __FUNCTION__, __LINE__);
        // the buffer always pulled, no need for handle this now.
    }

    //skip case for data recover, which means write ptr is 256*1024 bigger than read ptr
    leftReg = ptrEnd - mWPtr;
    LOGI("[%s:%d] sPtr:0x%x, mWPtr:0x%x, mRPtr:0x%x, size:%d, leftReg:%d\n", __FUNCTION__, __LINE__, mLoopBuf, mWPtr, mRPtr, size, leftReg);
    if (mWPtr != 0) {
        Mutex::Autolock _l(mLoopBufLock);
        if (leftReg >= size) {
            memcpy(mWPtr, src, size);
            mWPtr += size;
        }
        else {
            memcpy(mWPtr, src, leftReg);
            mWPtr = mLoopBuf;
            memcpy(mWPtr, (src + leftReg), (size - leftReg));
            mWPtr += (size - leftReg);
        }
    }
}

void SubtitleSource::loopBufRead(char* des, int size) {
    char* ptrStart = mLoopBuf;
    char* ptrEnd = mLoopBuf + SOURCE_LOOP_BUFFER_SIZE;
    int leftReg = 0;//from nearest rptr to ptrEnd

    if (size <= 0) return;

    // a simple workaround to avoid underflow. do not care if retry still fail
    int retry_time = 10000;
    int receivedSize = 0;
    while (retry_time-- > 0) {
        receivedSize = (mWPtr >= mRPtr) ? mWPtr - mRPtr : (ptrEnd - mRPtr) + (mWPtr - ptrStart);
        if (receivedSize >= size)
            break;
        usleep(1000);
    }
    if (retry_time <= 0) {
        ALOGE("[%s:%d][safeRead] DATA_CORRUCTION MAY HAPPENED! Error!!!!", __FUNCTION__, __LINE__);
        // if this happens, rescure the buffer.
        memset(des, 0, size);
        size = receivedSize; // read all the data, maybe can use, maybe still crash.
    }

    leftReg = ptrEnd - mRPtr;
    LOGI("[%s:%d]sPtr:0x%x,mWPtr:0x%x, mRPtr:0x%x, size:%d, leftReg:%d\n", __FUNCTION__, __LINE__, mLoopBuf, mWPtr, mRPtr, size, leftReg);
    if (mRPtr != 0) {
        Mutex::Autolock _l(mLoopBufLock);
        if (leftReg >= size) {
            memcpy(des, mRPtr, size);
            mRPtr += size;
        }
        else {
            memcpy(des, mRPtr, leftReg);
            mRPtr = mLoopBuf;
            memcpy((des + leftReg), mRPtr, (size - leftReg));
            mRPtr += (size - leftReg);
        }
    }
}

int SubtitleSource::loopBufSize() {
    Mutex::Autolock _l(mLoopBufLock);

    char* ptrStart = mLoopBuf;
    char* ptrEnd = mLoopBuf + SOURCE_LOOP_BUFFER_SIZE;
    int size = 0;

    if (mWPtr >= mRPtr) {
        size = mWPtr - mRPtr;
    }
    else {
        size = (ptrEnd - mRPtr) + (mWPtr - ptrStart);
    }
    LOGI("[%s:%d] mWPtr:0x%x, mRPtr:0x%x, size:%d\n", __FUNCTION__, __LINE__, mWPtr, mRPtr, size);

    return size;
}

bool SubtitleSource::isGetLoopBuf() {
    return (loopBufSize() > LEAST_LOOP_BUFFER_READ_SIZE ? true : false);
}

int SubtitleSource::isGetLoopBufSize() {
    int size = loopBufSize();

    return (size >= MAST_LOOP_BUFFER_READ_SIZE ? MAST_LOOP_BUFFER_READ_SIZE : size);
}
*/




//looper buff
/*
int userdata_ring_buf_init(AM_USERDATA_RingBuffer_t *ringbuf, size_t len)
{
	ringbuf->pread=ringbuf->pwrite=0;
	ringbuf->data=(uint8_t*)malloc(len);
	if (ringbuf->data == NULL)
		return -1;
	ringbuf->size=len;
	ringbuf->error=0;

	pthread_cond_init(&ringbuf->cond, NULL);

	return 0;
}

int userdata_ring_buf_deinit(AM_USERDATA_RingBuffer_t *ringbuf)
{
	ringbuf->pread=ringbuf->pwrite=0;
	if (ringbuf->data != NULL)
		free(ringbuf->data);
	ringbuf->size=0;
	ringbuf->error=0;

	pthread_cond_destroy(&ringbuf->cond);

	return 0;
}

int userdata_ring_buf_empty(AM_USERDATA_RingBuffer_t *ringbuf)
{
	return (ringbuf->pread==ringbuf->pwrite);
}

ssize_t userdata_ring_buf_free(AM_USERDATA_RingBuffer_t *ringbuf)
{
	ssize_t free;

	free = ringbuf->pread - ringbuf->pwrite;
	if (free <= 0)
		free += ringbuf->size;

	return free-1;
}

ssize_t userdata_ring_buf_avail(AM_USERDATA_RingBuffer_t *ringbuf)
{
	ssize_t avail;

	avail = ringbuf->pwrite - ringbuf->pread;
	if (avail < 0)
		avail += ringbuf->size;

	return avail;
}

void userdata_ring_buf_read(AM_USERDATA_RingBuffer_t *ringbuf, uint8_t *buf, size_t len)
{
	size_t todo = len;
	size_t split;

	if (ringbuf->data == NULL)
		return;

	split = ((ssize_t)(ringbuf->pread + len) > ringbuf->size) ? ringbuf->size - ringbuf->pread : 0;
	if (split > 0) {
		memcpy(buf, ringbuf->data+ringbuf->pread, split);
		buf += split;
		todo -= split;
		ringbuf->pread = 0;
	}

	memcpy(buf, ringbuf->data+ringbuf->pread, todo);

	ringbuf->pread = (ringbuf->pread + todo) % ringbuf->size;
}

ssize_t userdata_ring_buf_write(AM_USERDATA_RingBuffer_t *ringbuf, const uint8_t *buf, size_t len)
{
	size_t todo = len;
	size_t split;

	if (ringbuf->data == NULL)
		return 0;

	split = ((ssize_t)(ringbuf->pwrite + len) > ringbuf->size) ? ringbuf->size - ringbuf->pwrite : 0;

	if (split > 0) {
		memcpy(ringbuf->data+ringbuf->pwrite, buf, split);
		buf += split;
		todo -= split;
		ringbuf->pwrite = 0;
	}

	memcpy(ringbuf->data+ringbuf->pwrite, buf, todo);

	ringbuf->pwrite = (ringbuf->pwrite + todo) % ringbuf->size;

	if (len > 0)
		pthread_cond_signal(&ringbuf->cond);

	return len;
}
*/


SubtitleSourceLBuf::SubtitleSourceLBuf(int bufsize):mInit(false)
{
    LOGI("[%s:%d] SubtitleSourceLBuf", __FUNCTION__, __LINE__);
    init(bufsize);
}

SubtitleSourceLBuf::~SubtitleSourceLBuf()
{
    deinit();
}

int SubtitleSourceLBuf::init(int len)
{
    if (mInit) {
        LOGI("[%s:%d] mInit:%d ", __FUNCTION__, __LINE__, mInit);
        return 0;
    }

	mRead = mWrite = 0;
	mData = (unsigned char*)malloc(len);
	if (mData == NULL)
		return -1;
    memset(mData, 0, len);
	mSize = len;
	mError = 0;
    mInit = true;

	return 0;
}

int SubtitleSourceLBuf::reinit(int len)
{
    deinit();

	return init(len);
}


int SubtitleSourceLBuf::deinit()
{
	mRead = mWrite = 0;
	if (mData != NULL)
		::free(mData);
	mSize = 0;
	mError = 0;
    mInit = false;

	return 0;
}

int SubtitleSourceLBuf::flush()
{
	mRead = mWrite = 0;
	if (mData == NULL)
		return -1;
    memset(mData, 0, mSize);
	mError = 0;

	return 0;
}


int SubtitleSourceLBuf::empty()
{
	return (mRead == mWrite);
}

int SubtitleSourceLBuf::free()
{
	int fre;

	fre = mRead - mWrite;
	if (fre <= 0)
		fre += mSize;

	return (fre - 1);
}

int SubtitleSourceLBuf::avail()
{
	int avail;

	avail = mWrite - mRead;
	if (avail < 0)
		avail += mSize;

    LOGI("[%s:%d]avail:%d \n", __FUNCTION__, __LINE__,avail);

	return avail;
}

void SubtitleSourceLBuf::read(unsigned char *buf, int len)
{
	int todo = len;
	int split;

	if (mData == NULL)
		return;

	split = ((int)(mRead + len) > mSize) ? mSize - mRead : 0;
	if (split > 0) {
		memcpy(buf, mData + mRead, split);
		buf += split;
		todo -= split;
		mRead = 0;
	}

	memcpy(buf, mData + mRead, todo);

	mRead = (mRead + todo) % mSize;

    LOGI("[%s:%d]mWrite:%d mRead:%d mSize:%d \n", __FUNCTION__, __LINE__, mWrite, mRead, mSize);
}

int SubtitleSourceLBuf::write(const unsigned char *buf, int len)
{
	int todo = len;
	int split;

	if (mData == NULL)
		return 0;

	split = ((int)(mWrite + len) > mSize) ? mSize - mWrite : 0;

	if (split > 0) {
		memcpy(mData + mWrite, buf, split);
		buf += split;
		todo -= split;
		mWrite = 0;
	}

	memcpy(mData + mWrite, buf, todo);

	mWrite = (mWrite + todo) % mSize;

    LOGI("[%s:%d]mWrite:%d mRead:%d mSize:%d \n", __FUNCTION__, __LINE__, mWrite, mRead, mSize);


	return len;
}



