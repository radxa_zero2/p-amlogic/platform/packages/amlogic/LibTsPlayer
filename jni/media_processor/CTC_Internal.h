#ifndef _CTC_INTERNAL_H_
#define _CTC_INTERNAL_H_

#include <stdlib.h>
#include <stdint.h>
#include <amports/amstream.h>

namespace aml {

typedef enum {
    PROBE_PLAYER_EVT_STREAM_VALID=0,
    PROBE_PLAYER_EVT_FIRST_PTS,             //first frame decoded event
    PROBE_PLAYER_EVT_AUDIO_FIRST_DECODED,   //audio first frame decoded event
    PROBE_PLAYER_EVT_USERDATA_READY,
    PROBE_PLAYER_EVT_VOD_EOS,               //VOD EOS event
    PROBE_PLAYER_EVT_ABEND,                 //under flow event
    PROBE_PLAYER_EVT_PLAYBACK_ERROR,        // playback error event
    PROBE_PLAYER_EVT_VID_FRAME_ERROR =0x200,//video decoding error event
    PROBE_PLAYER_EVT_VID_DISCARD_FRAME,     //video decoding drop frame event
    PROBE_PLAYER_EVT_VID_DEC_OVERFLOW,
    PROBE_PLAYER_EVT_VID_DEC_UNDERFLOW,     //video decoding underflow event
    PROBE_PLAYER_EVT_VID_PTS_ERROR,         //video decoding vpts error event
    PROBE_PLAYER_EVT_VID_INVALID_DATA,
    PROBE_PLAYER_EVT_AUD_FRAME_ERROR,       //audio decoding error event
    PROBE_PLAYER_EVT_AUD_DISCARD_FRAME,     //audio decoding drop frame event
    PROBE_PLAYER_EVT_AUD_DEC_OVERFLOW,
    PROBE_PLAYER_EVT_AUD_DEC_UNDERFLOW,     //audio decoding underflow event
    PROBE_PLAYER_EVT_AUD_PTS_ERROR,         //audio decoding apts error event
    PROBE_PLAYER_EVT_AUD_INVALID_DATA,
    PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_START=0x307,  //unload start event
    PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_END=0x308,    //unload end event
    PROBE_PLAYER_EVT_VID_MOSAIC_START=0x309,       //blurred screen start event
    PROBE_PLAYER_EVT_VID_MOSAIC_END=0x30a,         //blurred screen end event
    PROBE_PLAYER_EVT_BUTT,

    //subtitle
    CTC_PLAYER_EVT_Available = 0x400,
    CTC_PLAYER_EVT_Unavailable,
    CTC_PLAYER_EVT_CC_Removed,
    CTC_PLAYER_EVT_CC_Added,
} CTC_PLAYER_EVT_e;

typedef void (*CTC_PLAYER_EVT_CB)(void* handler, CTC_PLAYER_EVT_e evt, unsigned long, unsigned long);

typedef int64_t (*CTC_PLAYER_CLOCK_CB)(void* handler);


typedef enum {
    IPTV_PLAYER_ATTR_VID_ASPECT=0,  /* 视频宽高比 0--640*480，1--720*576，2--1280*720，3--1920*1080,4--3840*2160,5--others等标识指定分辨率*/
    IPTV_PLAYER_ATTR_VID_RATIO,     //视频宽高比, 0代表4：3，1代表16：9
    IPTV_PLAYER_ATTR_VID_SAMPLETYPE,     //帧场模式, 1代表逐行源，0代表隔行源
    IPTV_PLAYER_ATTR_VIDAUDDIFF,     //音视频播放diff
    IPTV_PLAYER_ATTR_VID_BUF_SIZE,     //视频缓冲区大小
    IPTV_PLAYER_ATTR_VID_USED_SIZE,     //视频缓冲区使用大小
    IPTV_PLAYER_ATTR_AUD_BUF_SIZE,     //音频缓冲区大小
    IPTV_PLAYER_ATTR_AUD_USED_SIZE,     //音频缓冲区已使用大小
    IPTV_PLAYER_ATTR_AUD_SAMPLERATE,     //音频缓冲区已使用大小
    IPTV_PLAYER_ATTR_AUD_BITRATE,     //音频缓冲区已使用大小
    IPTV_PLAYER_ATTR_AUD_CHANNEL_NUM,     //音频缓冲区已使用大小
    IPTV_PLAYER_ATTR_VID_FRAMERATE = 18, //video frame rate
    IPTV_PLAYER_ATTR_BUTT,
    IPTV_PLAYER_ATTR_V_HEIGHT, //video height
    IPTV_PLAYER_ATTR_V_WIDTH,  //video width
    IPTV_PLAYER_ATTR_STREAM_BITRATE,//stream bitrate
    IPTV_PLAYER_ATTR_CATON_TIMES,  //the num of caton
    IPTV_PLAYER_ATTR_CATON_TIME,    //the time of caton total time
} IPTV_ATTR_TYPE_e;


typedef enum {
    IPTV_PLAYER_PARAM_EVT_VIDFRM_STATUS_REPORT = 0,
    IPTV_PLAYER_PARAM_EVT_FIRSTFRM_REPORT,
    IPTV_PLAYER_PARAM_EVT_BUTT
} IPTV_PLAYER_PARAM_Evt_e;

typedef enum {
    TIF_HAL_PLAYBACK_TYPE_ALL,
    TIF_HAL_PLAYBACK_TYPE_VIDEO,
    TIF_HAL_PLAYBACK_TYPE_AUDIO,
    TIF_HAL_PLAYBACK_TYPE_SUBTITLES,
    TIF_HAL_PLAYBACK_TYPE_RESERVED
} TIF_HAL_PlaybackType_t;

typedef enum {
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_UNKNOWN = 0,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_C, /**< Center */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_MONO = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_C,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R,  /**< Left and right speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R, /**< Left, center and right speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_S, /**< Left, right and surround speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_S, /**< Left, center right and surround speakers */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_SL_RS, /**< Left, right, surround left and surround right */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR, /**< Left, center, right, surround left and surround right */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_LFE, /**< Left, center, right, surround left, surround right and lfe*/
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_5_1 = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_LFE,
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_RL_RR_LFE, /**< Left, center, right, surround left, surround right, rear left, rear right and lfe */
    TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_7_1 = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR_RL_RR_LFE,
} TIF_HAL_Playback_AudioSourceChannelConfiguration_t;

typedef struct {
    uint32_t height; /**< Height of the video source in pixels units */
    uint32_t width; /**< Width of the video source in pixels units */
    float frameRate; /**< Frame rate of the video source in frame per second (or in hz) units, float value to support rational frame rates (i.e 29,97 or 59,94) */
    float pixelAspectRatio; /**< Ratio of a pixel's width to its height. 1.0 indicates square pixel. For some those video formats such as 720x480 4:3 and 720x480 16:9
                                 where pixels are not square floating point value must be set. Examples
                                 - SD 4:3 source:  (4.0f * height) / (3.0f * width) must be returned
                                 - SD 16:9 source: (16.0f * height) / (9.0f * width) must be returned
                                 - HD sources 1920x1080 (16:9) or 1440x1080 (4:3) where pixel aspect is square, 1.0f must be returned.
                             */
    uint8_t  activeFormatDescription; /**< Video Active Format description as defined in ETSI TS 101 154 V1.7.1 Annex B, ATSC A/53 Part 4 and SMPTE 2016-1-2007. */
} TIF_HAL_PlaybackVideoSourceInformation_t;

typedef struct {
    uint32_t sampleRate; /**< Sample rate of the audio source in sample per seconds (or in hz) units */
    uint32_t numChannels; /**< Number of channels of the audio source (i.e 2 for stereo, 6 for 5.1 surround).  See channelConfiguration to more detailed information about speaker layout */
    TIF_HAL_Playback_AudioSourceChannelConfiguration_t channelConfiguration; /**< Configuration or speaker layout of the audio source channels */
} TIF_HAL_PlaybackAudioSourceInformation_t;

typedef struct {
    struct
    {
        uint8_t enabled; /**< Video decoding is enabled (running) */
        TIF_HAL_PlaybackVideoSourceInformation_t info;
        struct {
            uint32_t numDecodedFrames;
            uint32_t numDisplayedFrames;
            uint32_t numDecoderErrors;
            uint32_t numDecoderOverflows;
            uint32_t numDecoderUnderflows;
            uint32_t numFramesDropped;
        } statistics;
    } video;
    struct
    {
        uint8_t enabled; /**< Audio decoding is enabled (running) */
        TIF_HAL_PlaybackAudioSourceInformation_t info;
        struct {
            uint32_t numDecodedSamples;
            uint32_t numOutputSamples;
            uint32_t numDecoderErrors;
            uint32_t numDecoderOverflows;
            uint32_t numDecoderUnderflows;
            uint32_t numSamplesDiscarded;
        } statistics;
    } audio;
    /*struct
    {
        //uint8_t enabled; < Subtitles decoding is enabled (running)
        TIF_HAL_PlaybackSubtitlesSourceInformation_t info;
        struct {
            uint32_t numDecoderErrors;
        } statistics;
    } subtitles;*/
}TIF_HAL_PlaybackStatus_t,*p_DecodeFrameInfo;


typedef void (*IPTV_PLAYER_PARAM_EVENT_CB)( void *hander, IPTV_PLAYER_PARAM_Evt_e enEvt, void *pParam);


typedef struct {
    int instanceNo;
    int video_width;
    int video_height;
    int64_t mFirstVideoPTSUs;
}VIDEO_FRM_INFO_T;

struct CTCWriteBuffer {
    uint8_t* mWriteBuffer = nullptr;
    uint32_t mWriteBufferSize = 0;

    void reset() {
        if (mWriteBuffer) {
            free(mWriteBuffer);
            mWriteBuffer = nullptr;
            mWriteBufferSize = 0;
        }
    }
};

}
#endif
