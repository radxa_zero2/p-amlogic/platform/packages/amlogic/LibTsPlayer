#ifndef _CTSPLAYER_IMPL_H_
#define _CTSPLAYER_IMPL_H_

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <gui/Surface.h>
#include <utils/KeyedVector.h>
#include <utils/Mutex.h>

extern "C" {
#include <amports/vformat.h>
#include <amports/aformat.h>
#include <amports/amstream.h>
#include <codec.h>
#include <codec_info.h>
#include <list.h>
#include <codec_videoinfo.h>
}
#include <string.h>
#include <utils/Timers.h>
#include <ITsPlayer.h>

using android::sp;
using android::Surface;
class SubtitleClient;
namespace android {
    class AmlSubtitle;
    class AmlProbe;
}
class CTsPlayerInfoCollection;


typedef struct _dvb_ca dvb_ca_t;
typedef struct _aml_dvb_init_para aml_dvb_init_para_t;


#define lock_t          pthread_mutex_t
#define lp_lock_init(x,v)   pthread_mutex_init(x,v)
#define lp_lock_deinit(x)   pthread_mutex_destroy(x)
#define lp_lock(x)      pthread_mutex_lock(x)
#define lp_trylock(x)   pthread_mutex_trylock(x)
#define lp_unlock(x)    pthread_mutex_unlock(x)

#define TS_PACKET_SIZE 188
#define PROCESS_NAME 64


#define TRICKMODE_NONE       0x00
#define TRICKMODE_I          0x01
#define TRICKMODE_I_HEVC     0x07
#define TRICKMODE_FFFB       0x02

namespace aml {
struct CTsParameter {
    int mMultiSupport;
};

typedef struct {
    struct list_head list;
    unsigned char* tmpbuffer;
    unsigned int size;
}V_HEADER_T, *PV_HEADER_T;

typedef struct ST_LPbuffer{
    unsigned char *rp;
    unsigned char *wp;
    unsigned char *buffer;
    unsigned char *bufferend;
    int valid_can_read;
    bool enlpflag;
}LPBUFFER_T;

typedef enum {
    VIDEO_FRAME_TYPE_UNKNOWN = 0,
    VIDEO_FRAME_TYPE_I,
    VIDEO_FRAME_TYPE_P,
    VIDEO_FRAME_TYPE_B,
    VIDEO_FRAME_TYPE_IDR,
    VIDEO_FRAME_TYPE_BUTT,
}VID_FRAME_TYPE_e;

typedef struct VIDEO_FRM_STATUS_INFO {
    VID_FRAME_TYPE_e enVidFrmType;
    int  nVidFrmSize;
    int  nMinQP;
    int  nMaxQP;
    int  nAvgQP;
    int  nMaxMV;
    int  nMinMV;
    int  nAvgMV;
    int  SkipRatio;
    int  nUnderflow;
} VIDEO_FRM_STATUS_INFO_T;

class CTsPlayer : public ITsPlayer
{
public:
    CTsPlayer();
    explicit CTsPlayer(CTsParameter p);
	explicit CTsPlayer(const CTC_InitialParameter* initialParam);
    virtual ~CTsPlayer();

public:
    virtual int  GetPlayMode() override;
    virtual int  SetVideoWindow(int x,int y,int width,int height) override;
    virtual int  VideoShow(void) override;
    virtual int  VideoHide(void) override;
    virtual int EnableAudioOutput() override;
    virtual int DisableAudioOutput() override;
    virtual void InitVideo(PVIDEO_PARA_T pVideoPara) override;
    virtual void InitAudio(PAUDIO_PARA_T pAudioPara) override;
    virtual void InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara) override;
    virtual int InitCAS(DVB_CAS_INIT_PARA* pCASInitPara) override;
    virtual int initSyncSource(CTC_SyncSource syncSource) override;
    virtual bool StartPlay() override;
    virtual bool Pause() override;
    virtual bool Resume() override;
    virtual bool Fast() override;
    virtual bool StopFast() override;
    virtual bool Stop() override;
    virtual bool Seek() override;
    virtual bool SetVolume(int volume) override;
    virtual int GetVolume() override;
    virtual bool SetRatio(int nRatio) override;
    virtual int GetAudioBalance() override;
    virtual bool SetAudioBalance(int nAudioBalance) override;
    virtual void GetVideoPixels(int& width, int& height) override;
    virtual bool IsSoftFit() override;
    virtual void SetEPGSize(int w, int h) override;
    virtual void SetSurface(Surface* pSurface) override;
    virtual void SetSurface_ANativeWindow(ANativeWindow* pSurface) override;
    virtual int WriteData(uint8_t* pBuffer, uint32_t nSize) override;
    virtual int WriteData(PLAYER_STREAMTYPE_E type, uint8_t *pBuffer, uint32_t nSize, int64_t pts) override;
    virtual void SwitchVideoTrack(PVIDEO_PARA_T pVideoPara) override;
    virtual void SwitchAudioTrack(int pid, PAUDIO_PARA_T pAudioPara) override;
    virtual void SwitchSubtitle(int pid, PSUBTITLE_PARA_T pSubtitlePara) override;
    virtual bool SubtitleShowHide(bool bShow) override;
    virtual int64_t GetCurrentPts(PLAYER_STREAMTYPE_E type) override;
    virtual int64_t GetCurrentPlayTime() override;
    virtual void SetStopMode(bool bHoldLastPic) override;
    virtual int GetBufferStatus(PAVBUF_STATUS pstatus) override;
    virtual int GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo) override;
    virtual int SetParameter(void* handler, int key, void * request) override;
    virtual int GetParameter(void* handler, int key, void * reply) override;
    virtual void ctc_register_evt_cb(CTC_PLAYER_EVT_CB ctc_func_player_evt, void *hander) override;
    virtual AML_HANDLE DMX_CreateChannel(int pid);
    virtual int DMX_DestroyChannel(AML_HANDLE channel);
    virtual int DMX_OpenChannel(AML_HANDLE channel);
    virtual int DMX_CloseChannel(AML_HANDLE channel);
    virtual AML_HANDLE DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id);
    virtual int DMX_DestroyFilter(AML_HANDLE filter);
    virtual int DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel);
    virtual int DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel);

private:
    virtual bool iStartPlay();
    virtual bool iStop();
    void updateinfo_to_middleware(struct av_param_info_t av_param_info,struct av_param_qosinfo_t av_param_qosinfo);
    void update_nativewindow_wrapper();
    void update_nativewindow();
    int updateCTCInfo();
    void ShowFrameInfo(struct vid_frame_info* frameinfo);
    static void eventNotifier(void* handler, CTC_PLAYER_EVT_e evt, unsigned long, unsigned long);
    void sendEvent(CTC_PLAYER_EVT_e evt, uint32_t, uint32_t);
    virtual void checkAbend();
    virtual void checkBuffLevel();
    virtual void checkBuffstate();
    static void *threadCheckAbend(void *pthis);
    static void *threadReadPacket(void *pthis);
    static void *Get_TsHeader_thread(void *pthis);
    void checkVdecstate();
    bool iReset();

    //static void * threadReportInfo(void *pthis);
    void update_caton_info(struct av_param_info_t * info);
    //void update_stream_bitrate();
    int SetVideoWindowImpl(int x,int y,int width,int height);
    bool CheckMultiSupported(int video_type);
	void * stop_thread(void );
    static void * init_thread(void *pthis);
    void thread_wait_timeUs(int microseconds);
    void thread_wake_up();
    virtual void init_params();
    int is_use_double_write();
    void check_use_double_write();
    void stop_double_write();
    int parser_header(unsigned char* pBuffer, unsigned int size, unsigned char *buffer);
    int get_hevc_csd_packet(unsigned char* buf, int size, unsigned char *buffer);
    int Add_Packet_ToList(unsigned char* pBuffer, unsigned int nSize);

    void init_amlprobe();
    void stop_amlprobe();
    void setSubtitlePara(PSUBTITLE_PARA_T para);
    void init_amlsubtitle();
    void stop_amlsubtitle();
    int reinit_amstream_subtitle(int pid);
    bool checkAmstreamOverDuration(PLAYER_STREAMTYPE_E type, codec_para_t* mpcode);
    bool checkAmstreamTSDataWriteable(codec_para_t* mVpcode, codec_para_t* mApcode);
    bool checkAmstreamESDataWriteable(codec_para_t* mVpcode, codec_para_t* mApcode, PLAYER_STREAMTYPE_E type, uint32_t nSize);

    void iGetPlayerAttribute();

    void initDVBCasParaByProp();
    int initAmlDVBCasPara(DVB_CAS_INIT_PARA* para);
    int initAmlDVBCas();
    void stopAmlDVBCas();
    int amCasLibInit();

    void ClearLastFrame();
    void BlackOut(int EarseLastFrame);
    bool SetErrorRecovery(int mode);

    void openDumpFile();
    void closeDumpIfle();

private:
    int		m_bLeaveChannel;

    AUDIO_PARA_T a_aPara[MAX_AUDIO_PARAM_SIZE];
    SUBTITLE_PARA_T sPara[MAX_SUBTITLE_PARAM_SIZE];
    VIDEO_PARA_T vPara;
    PV_HEADER_T tsheader;
    int player_pid;
    codec_para_t  codec;
    codec_para_t  *pcodec;
    codec_para_t  *vcodec;
    codec_para_t  *acodec;
    codec_para_t  *scodec;
    bool		  m_bIsPlay;
    bool          m_bIsSeek;
    int			  m_nOsdBpp;
    int			  m_nAudioBalance;
    int			  m_nVolume;
    int           m_nEPGWidth;
    int           m_nEPGHeight;
    bool          m_bFast;
    bool          m_bSetEPGSize;
    bool          m_bWrFirstPkg;
    int	          m_nMode;
    int mWinAis[4];
    sp<ANativeWindow> mNativeWindow;
    int width_old,width_new;
    int height_old,height_new;

    int threshold_value;
    int threshold_ctl_flag;
    int underflow_ctc;
    int underflow_kernel;
    int underflow_tmp;
    int underflow_count;
    int qos_count;
    int prev_vread_buffer;
    int vrp_is_buffer_changed;
    int last_data_len;
    int last_data_len_statistics;
    int mIsTSdata;

    IPTV_PLAYER_EVT_CB pfunc_player_evt;
    void *player_evt_hander;

    //send event would be protected by ctc_evt_lock
    ::android::Mutex ctc_evt_lock;
    CTC_PLAYER_EVT_CB ctc_pfunc_evt = nullptr;
    void *ctc_evt_hander = nullptr;

    void *m_player_evt_hander_regitstercallback;
    IPTV_PLAYER_PARAM_EVENT_CB  m_pfunc_player_param_evt_registercallback;

//#ifdef TELECOM_QOS_SUPPORT
    IPTV_PLAYER_PARAM_EVENT_CB  pfunc_player_param_evt;
    void *player_evt_param_handler;
//#endif
    unsigned int writecount ;
    int64_t m_StartPlayTimePoint;
    /*+[SE] [BUG][BUG-170677][yinli.xia] added:2s later
        to statistics video frame when start to play*/
    int m_Frame_StartTime_Ctl;
    int64_t m_Frame_StartPlayTimePoint;
    bool    m_isSoftFit;
    FILE*	  m_fp;
    lock_t  mutex;
    pthread_t mThread[2];
    pthread_cond_t m_pthread_cond;
    pthread_cond_t s_pthread_cond;

    pthread_t readThread;
    pthread_t tsheaderThread;
    unsigned char header_buffer[TS_PACKET_SIZE];

    bool    m_isBlackoutPolicy;
    bool    m_bchangeH264to4k;
    lock_t  mutex_lp;
    lock_t  mutex_session;
    lock_t  mutex_header;
    
    bool    m_bIsPause;
    int64_t m_PreviousOverflowTime;
    size_t  mInputQueueSize;
    //ctsplayer_state m_sCtsplayerState;
    //pthread_t mInfoThread;
    int mLastVdecInfoNum;
    char CallingPidName[PROCESS_NAME];
    int mSubCount = 0;

    sp<android::AmlProbe> mAmlProbe;
    sp<CTsPlayerInfoCollection> mInfoCollection;

    sp<::android::AmlSubtitle> mAmlSubtitle;
    SUBTITLE_PARA_T mCurPara;

    DVB_CAS_INIT_PARA mDVBCasInitPara;
    dvb_ca_t* m_dvb_ca = nullptr;
    aml_dvb_init_para_t* mInitPara = nullptr;
};


}
#endif

