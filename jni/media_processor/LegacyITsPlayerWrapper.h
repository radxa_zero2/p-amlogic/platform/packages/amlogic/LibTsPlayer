#ifndef _LEGACY_ITSPLAYER_WRAPPER_H_
#define _LEGACY_ITSPLAYER_WRAPPER_H_

#include "ITsPlayer.h"
#include <CTsPlayer.h>

class ITsPlayer;

namespace aml {

class LegacyITsPlayerWrapper : public ITsPlayer
{
public:
    LegacyITsPlayerWrapper(const android::sp<::ITsPlayer>& legacyITsPlayer);
    virtual ~LegacyITsPlayerWrapper();
    virtual int GetPlayMode() override;
    virtual int SetVideoWindow(int x, int y, int width, int height) override;
    virtual int VideoShow() override;
    virtual int VideoHide(void) override;
    virtual int EnableAudioOutput() override;
    virtual int DisableAudioOutput() override;
    virtual void InitVideo(PVIDEO_PARA_T pVideoPara) override;
    virtual void InitAudio(PAUDIO_PARA_T pAudioPara) override;
    virtual void InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara) override;
    virtual int InitCAS(DVB_CAS_INIT_PARA* pCASInitPara) override;
    virtual int initSyncSource(CTC_SyncSource syncSource) override;
    virtual bool StartPlay() override;
    virtual bool Pause() override;
    virtual bool Resume() override;
    virtual bool Fast() override;
    virtual bool StopFast() override;
    virtual bool Stop() override;
    virtual bool Seek() override;
    virtual bool SetVolume(int volume) override;
    virtual int GetVolume() override;
    virtual bool SetRatio(int nRatio) override;
    virtual int GetAudioBalance() override;
    virtual bool SetAudioBalance(int nAudioBalance) override;
    virtual void GetVideoPixels(int& width, int& height) override;
    virtual bool IsSoftFit() override;
    virtual void SetEPGSize(int w, int h) override;
    virtual void SetSurface(::android::Surface* pSurface) override;
    virtual void SetSurface_ANativeWindow(ANativeWindow* pSurface) override;
    virtual int WriteData(uint8_t* pBuffer, uint32_t nSize) override;
    virtual int WriteData(PLAYER_STREAMTYPE_E type, uint8_t *pBuffer, uint32_t nSize, int64_t timestamp) override;
    virtual void SwitchVideoTrack(PVIDEO_PARA_T pVideoPara) override;
    virtual void SwitchAudioTrack(int pid, PAUDIO_PARA_T pAudioPara) override;
    virtual void SwitchSubtitle(int pid, PSUBTITLE_PARA_T pSubtitlePara) override;
    virtual bool SubtitleShowHide(bool bShow) override;
    virtual int64_t GetCurrentPts(PLAYER_STREAMTYPE_E type) override;
    virtual int64_t GetCurrentPlayTime() override;
    virtual void SetStopMode(bool bHoldLastPic) override;
    virtual int GetBufferStatus(PAVBUF_STATUS pstatus) override;
    virtual int GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo) override;
    virtual int SetParameter(void* handler, int key, void * request) override;
    virtual int GetParameter(void* handler, int key, void * reply) override;
    virtual void ctc_register_evt_cb(CTC_PLAYER_EVT_CB ctc_func_player_evt, void *hander) override;
    virtual AML_HANDLE DMX_CreateChannel(int pid);
    virtual int DMX_DestroyChannel(AML_HANDLE channel);
    virtual int DMX_OpenChannel(AML_HANDLE channel);
    virtual int DMX_CloseChannel(AML_HANDLE channel);
    virtual AML_HANDLE DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id);
    virtual int DMX_DestroyFilter(AML_HANDLE filter);
    virtual int DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel);
    virtual int DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel);

private:
    ::PLAYER_STREAMTYPE_E toLegacyStreamType(aml::PLAYER_STREAMTYPE_E type);

private:
    android::sp<::ITsPlayer> mLegacyITsPlayer;

    LegacyITsPlayerWrapper(const LegacyITsPlayerWrapper&) = delete;
    LegacyITsPlayerWrapper& operator=(const LegacyITsPlayerWrapper&) = delete;

};

}

#endif
