#ifndef _CTSPLAYERINFOCOLLECTION_H_
#define _CTSPLAYERINFOCOLLECTION_H_

#include <utils/RefBase.h>
#include <utils/Errors.h>
#include <utils/KeyedVector.h>
#include <utils/String8.h>
#include "IInfoCollection.h"

extern "C" {
#include <amports/vformat.h>
#include <amports/aformat.h>
#include <amports/amstream.h>
#include <codec.h>
#include <codec_info.h>
#include <list.h>
#include <codec_videoinfo.h>
}

#define STREAM_TS 0
#define STREAM_ES_V 1
#define STREAM_ES_A 2

struct player_probeinfo {
    bool m_bIsPlay;
    int video_hide;
    int audio_hide;
    int v_overflows;
    int v_underflows;
    int a_overflows;
    int a_underflows;
    /*for calc avg stream bitrate*/
    int64_t bytes_record_starttime_ms;
    int64_t bytes_record_start;
    int64_t bytes_record_cur;
    int vdec_underflow;
    int adec_underflow;
    int vdec_overflow;
    int adec_overflow;
    int vdec_total;
    int vdec_drop;
    int frame_rate_ctc;
    /*for calc realtime frame rate*/
    int current_fps;
    int64_t last_record_time;
    int last_record_toggled_num;
};

class CTsPlayerInfoCollection : public IInfoCollection
{
public:

    CTsPlayerInfoCollection();
    virtual ~CTsPlayerInfoCollection();

    /**
    * Update video decoder infomation.
    * need update in time.
    * @param pVdecInfo  video decoder info.
    * @return 0 in case of success
    */
    virtual int UpdateCurVdecInfo(VIDEO_DEC_INFO_T& pVdecInfo);

    /**
    * Update audio decoder infomation.
    * need update in time.
    * @param pAdecInfo  audio decoder info.
    */
    virtual int UpdateCurAdecInfo(AUDIO_DEC_INFO_T& pAdecInfo);

    /**
    * Update video/audio buffer infomation.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pBbufInfo  buffer info.
    */
    virtual void UpdateAVbufInfo(AVBUF_INFO_T& pBbufInfo);

    /**
    * Update video para infomation.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pVideoParaInfo  video para info.
    */
    //virtual void UpdateCurVideoParaInfo(VIDEO_PARA_INFO_T& pVideoParaInfo);

    /**
    * Update audio para infomation.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pAudioParaInfo  audio para info.
    */
    virtual void UpdateCurAudioParaInfo(AUDIO_PARA_INFO_T& pAudioParaInfo);

    /**
    * Update some player para infomation such as stream bitrate and so on.
    * Middleware takes the initiative to query no need to be updated in time.
    * @param pOtherInfo  some player para info.
    */
    virtual void UpdateOtherInfo(OTHER_INFO_T& pOtherInfo);

    /**
    * Update stream bitrate.
    * need update in time.
    */
    virtual void UpdateStreamBitrate();

    /**
    * Update unload infomation.
    * need update in time.
    * @param pUnload_flag  if pUnload_flag equals 0 it represents player unload end.
    *                      if pUnload_flag equals 1 it represents player unload start.
    */
    virtual void UpdateUnloadInfo(int& pUnload_flag);

    /**
    * Update blurred screen infomation.
    * need update in time.
    * @param pBlurscreen_flag  if pBlurscreen_flag equals 0 it represents player blurred screen end.
    *                          if pBlurscreen_flag equals 1 it represents player blurred screen start.
    */
    virtual void UpdateBlurScreenInfo(struct av_info_t *info1, struct vframe_counter_s *info2, int* pBlurscreen_flag);

    /**
    * Set codec para to CTsPlayerInfocollection.
    * @param pCodec  codec para.
    * @return 0 in case of success, -1 in case of failure
    */
    virtual int SetCodecPara(codec_para_t* pCodec, player_probeinfo* player_info, int codec_type);
    virtual int64_t GetCurTime(void);
private:
    codec_para_t  *pcodec;
    codec_para_t  *vcodec;
    codec_para_t  *acodec;
    codec_para_t  *scodec;
    player_probeinfo* playerprobeinfo;
    codec_quality_info pqualityinfo;
    int unload_start_underflow;
    int64_t unload_start_time;
    int first_picture_comming;
    int stream_bitrate;
    int unload_times;
    int unload_time;
};

#endif

