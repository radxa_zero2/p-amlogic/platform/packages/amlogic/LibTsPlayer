/* 
 * Copyright (C) 2019 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#define LOG_NDEBUG 0
#define LOG_TAG "CTC_MediaProcessor"
#include "CTC_Log.h"
#include <cutils/properties.h>

#include <CTC_MediaProcessor.h>
#if ANDROID_PLATFORM_SDK_VERSION <= 27
#include "CTsOmxPlayer.h"
#endif
//#include "Amsysfsutils.h"
#include "CTC_Utils.h"
#include "ITsPlayer.h"
#include <gui/Surface.h>
#include <CTC_Internal.h>

///////////////////////////////////////////////////////////////////////////////
#define CTC_VERSION_MAJOR   1
#define CTC_VERSION_MINOR   0

#define CTC_VERSION_INT     CTC_VERSION_MAJOR << 16 | CTC_VERSION_MINOR
#define CTC_VERSION         "AML_CTC v" CTC_STRINGIFY(CTC_VERSION_GLUE(CTC_VERSION_MAJOR, CTC_VERSION_MINOR))

#define CTC_VERSION_GLUE(a, b)   CTC_VERSION_DOT(a, b)
#define CTC_VERSION_DOT(a, b)   a ## . ## b
#define CTC_STRINGIFY(x)    CTC_TOSTRING(x)
#define CTC_TOSTRING(x)     #x


namespace aml {

static void CTC_EventCallback(void* handler, CTC_PLAYER_EVT_e evt, unsigned long, unsigned long);

CTC_MediaProcessor* GetMediaProcessor(CTC_InitialParameter* initialParam)
{
    ALOGI(CTC_VERSION);

    return new CTC_MediaProcessor(initialParam);
}

int GetMediaProcessorVersion(const char** versionString)
{
    if (versionString) {
        *versionString = CTC_VERSION;
    }

    return CTC_VERSION_INT;
}

CTC_MediaProcessor::CTC_MediaProcessor(CTC_InitialParameter* initialParam)
{

    mTsPlayer = ITsPlayerFactory::instance().createITsPlayer(initialParam);
    mTsPlayer->incStrong(this);

    MLOG("mTsPlayer:%p this: %p", mTsPlayer, this);
}

CTC_MediaProcessor::~CTC_MediaProcessor()
{
    MLOG("mTsPlayer:%p, ref count:%d", mTsPlayer, mTsPlayer->getStrongCount());
    mTsPlayer->decStrong(this);
}

int CTC_MediaProcessor::GetPlayMode()
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->GetPlayMode();

    return ret;
}

int CTC_MediaProcessor::SetVideoWindow(int x, int y, int width, int height)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->SetVideoWindow(x, y, width, height);

    return ret;
}

int CTC_MediaProcessor::VideoShow()
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->VideoShow();

    return ret;
}

int CTC_MediaProcessor::VideoHide()
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->VideoHide();

    return ret;
}

int CTC_MediaProcessor::EnableAudioOutput()
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->EnableAudioOutput();

    return ret;
}

int CTC_MediaProcessor::DisableAudioOutput()
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->DisableAudioOutput();

    return ret;
}

void CTC_MediaProcessor::InitVideo(PVIDEO_PARA_T pVideoPara)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->InitVideo(pVideoPara);
}

void CTC_MediaProcessor::InitAudio(PAUDIO_PARA_T pAudioPara)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->InitAudio(pAudioPara);
}

void CTC_MediaProcessor::InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->InitSubtitle(pSubtitlePara);
}

int CTC_MediaProcessor::InitCAS(DVB_CAS_INIT_PARA* pCASInitPara)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    return mTsPlayer->InitCAS(pCASInitPara);
}

int CTC_MediaProcessor::initSyncSource(CTC_SyncSource syncSource)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    return mTsPlayer->initSyncSource(syncSource);
}

bool CTC_MediaProcessor::StartPlay()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->StartPlay();

    return ret;
}

bool CTC_MediaProcessor::Pause()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->Pause();

    return ret;
}

bool CTC_MediaProcessor::Resume()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->Resume();

    return ret;
}

bool CTC_MediaProcessor::Fast()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->Fast();

    return ret;
}

bool CTC_MediaProcessor::StopFast()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->StopFast();

    return ret;
}

bool CTC_MediaProcessor::Stop()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->Stop();

    return ret;
}

bool CTC_MediaProcessor::Seek()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->Seek();

    return ret;
}

int CTC_MediaProcessor::SetVolume(float volume)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->SetVolume(volume);

    return ret;
}

float CTC_MediaProcessor::GetVolume()
{
    RETURN_IF(0.0f, mTsPlayer == nullptr);

    float volume = 1.0f;

    volume = mTsPlayer->GetVolume();

    return volume;
}

int CTC_MediaProcessor::SetRatio(CONTENTMODE_E contentMode)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->SetRatio(contentMode);

    return ret;
}

int CTC_MediaProcessor::GetAudioBalance()
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->GetAudioBalance();

    return ret;
}

bool CTC_MediaProcessor::SetAudioBalance(ABALANCE_E nAudioBalance)
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->SetAudioBalance(nAudioBalance);


    return ret;
}

void CTC_MediaProcessor::GetVideoPixels(int& width, int& height)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->GetVideoPixels(width, height);
}

bool CTC_MediaProcessor::IsSoftFit()
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = false;

    ret = mTsPlayer->IsSoftFit();

    return ret;
}

void CTC_MediaProcessor::SetEPGSize(int w, int h)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SetEPGSize(w, h);
}

void CTC_MediaProcessor::SetSurface(android::Surface* pSurface)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SetSurface(pSurface);
}

void CTC_MediaProcessor::SetSurface_ANativeWindow(ANativeWindow* pSurface)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SetSurface_ANativeWindow(pSurface);
}

int CTC_MediaProcessor::GetWriteBuffer(PLAYER_STREAMTYPE_E type, uint8_t** pBuffer, uint32_t *nSize)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->GetWriteBuffer(type, pBuffer, nSize);

    return ret;
}

int CTC_MediaProcessor::WriteData(uint8_t* pBuffer, uint32_t nSize)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->WriteData(pBuffer, nSize);

    return ret;
}

int CTC_MediaProcessor::WriteData(PLAYER_STREAMTYPE_E type, uint8_t* pBuffer, uint32_t nSize, int64_t pts)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->WriteData(type, pBuffer, nSize, pts);

    return ret;
}

void CTC_MediaProcessor::SwitchVideoTrack(uint16_t pid, PVIDEO_PARA_T pVideoPara)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SwitchVideoTrack(pVideoPara);
}

void CTC_MediaProcessor::SwitchAudioTrack(uint16_t pid, PAUDIO_PARA_T pAudioPara)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SwitchAudioTrack(pid, pAudioPara);
}

void CTC_MediaProcessor::SwitchSubtitle(uint16_t pid, PSUBTITLE_PARA_T pSubtitlePara)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SwitchSubtitle(pid, pSubtitlePara);
}

bool CTC_MediaProcessor::SubtitleShowHide(bool bShow)
{
    RETURN_IF(false, mTsPlayer == nullptr);

    bool ret = true;

    ret = mTsPlayer->SubtitleShowHide(bShow);

    return ret;
}

int64_t CTC_MediaProcessor::GetCurrentPts(PLAYER_STREAMTYPE_E type)
{
    RETURN_IF(kUnknownPTS, mTsPlayer == nullptr);

    int64_t pts = kUnknownPTS;

    pts = mTsPlayer->GetCurrentPts(type);

    return pts;
}

int64_t CTC_MediaProcessor::GetCurrentPlayTime()
{
    RETURN_IF(kUnknownPTS, mTsPlayer == nullptr);

    int64_t playTime = kUnknownPTS;

    playTime = mTsPlayer->GetCurrentPlayTime();

    return playTime;
}

void CTC_MediaProcessor::SetStopMode(bool bHoldLastPic)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SetStopMode(bHoldLastPic);
}

int CTC_MediaProcessor::GetBufferStatus(PAVBUF_STATUS pstatus)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    mTsPlayer->GetBufferStatus(pstatus);

    return ret;
}

int CTC_MediaProcessor::GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->GetStreamInfo(pVideoInfo, pAudioInfo, pSubtitleInfo);

    return ret;
}

void CTC_MediaProcessor::SetParameter(int32_t key, void* request)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->SetParameter(nullptr, key, request);
}

void CTC_MediaProcessor::GetParameter(int32_t key, void* reply)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    mTsPlayer->GetParameter(nullptr, key, reply);
}

void CTC_MediaProcessor::playerback_register_evt_cb(IPTV_PLAYER_EVT_CB pfunc, void* handler)
{
    RETURN_VOID_IF(mTsPlayer == nullptr);

    ctc_func_player_evt = pfunc;
    ctc_player_evt_hander = handler;

    mTsPlayer->ctc_register_evt_cb(CTC_EventCallback, this);
}

AML_HANDLE CTC_MediaProcessor::DMX_CreateChannel(int pid)
{
    RETURN_IF(nullptr, mTsPlayer == nullptr);

    return mTsPlayer->DMX_CreateChannel(pid);
}

int CTC_MediaProcessor::DMX_DestroyChannel(AML_HANDLE channel)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->DMX_DestroyChannel(channel);

    return ret;
}

int CTC_MediaProcessor::DMX_OpenChannel(AML_HANDLE channel)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->DMX_OpenChannel(channel);

    return ret;
}

int CTC_MediaProcessor::DMX_CloseChannel(AML_HANDLE channel)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->DMX_CloseChannel(channel);

    return ret;
}

AML_HANDLE CTC_MediaProcessor::DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id)
{
    RETURN_IF(nullptr, mTsPlayer == nullptr);

    return mTsPlayer->DMX_CreateFilter(cb, pUserData, id);
}

int CTC_MediaProcessor::DMX_DestroyFilter(AML_HANDLE filter)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->DMX_DestroyFilter(filter);

    return ret;
}

int CTC_MediaProcessor::DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->DMX_AttachFilter(filter, channel);

    return ret;
}

int CTC_MediaProcessor::DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel)
{
    RETURN_IF(-1, mTsPlayer == nullptr);

    int ret = AML_SUCCESS;

    ret = mTsPlayer->DMX_DetachFilter(filter, channel);

    return ret;
}

///////////////////////////////////////////////////////////////////////////////
static void CTC_EventCallback(void* handler, CTC_PLAYER_EVT_e evt, unsigned long param1, unsigned long param2)
{
    MLOG("CTC_EventCallback evt:%d pthis: %p\n", evt, handler);

    CTC_MediaProcessor *ctc_mediaprocessor = static_cast<CTC_MediaProcessor *>(handler);
    IPTV_PLAYER_EVT_CB ctc_func_player_evt = ctc_mediaprocessor->ctc_func_player_evt;
    void* ctc_player_evt_hander = ctc_mediaprocessor->ctc_player_evt_hander;

    if (ctc_func_player_evt == NULL)
        return;

    switch (evt) {
        case PROBE_PLAYER_EVT_FIRST_PTS:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_FIRST_PTS, 0, 0);
            break;
        case PROBE_PLAYER_EVT_AUDIO_FIRST_DECODED:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_AUDIO_FIRST_DECODED, 0, 0);
            break;
        case PROBE_PLAYER_EVT_USERDATA_READY:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_USERDATA_READY, param1, param2);
            break;
        case PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_START:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_BUFF_UNLOAD_START, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_BUFF_UNLOAD_END:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_BUFF_UNLOAD_END, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_MOSAIC_START:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_MOSAIC_START, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_MOSAIC_END:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_MOSAIC_END, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_FRAME_ERROR:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_FRAME_ERROR, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_DISCARD_FRAME:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_DISCARD_FRAME, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_DEC_UNDERFLOW:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_DEC_OVERFLOW:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_DEC_OVERFLOW, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_PTS_ERROR:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_PTS_ERROR, 0, 0);
            break;
        case PROBE_PLAYER_EVT_VID_INVALID_DATA:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_VID_INVALID_DATA, 0, 0);
            break;
        case PROBE_PLAYER_EVT_AUD_FRAME_ERROR:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_AUD_FRAME_ERROR, 0, 0);
            break;
        case PROBE_PLAYER_EVT_AUD_DISCARD_FRAME:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_AUD_DISCARD_FRAME, 0, 0);
            break;
        case PROBE_PLAYER_EVT_AUD_PTS_ERROR:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_AUD_PTS_ERROR, 0, 0);
            break;
        case PROBE_PLAYER_EVT_AUD_DEC_UNDERFLOW:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW, 0, 0);
            break;
        case PROBE_PLAYER_EVT_AUD_DEC_OVERFLOW:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_AUD_DEC_OVERFLOW, 0, 0);
            break;
        case PROBE_PLAYER_EVT_AUD_INVALID_DATA:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_AUD_INVALID_DATA, 0, 0);
            break;
        case CTC_PLAYER_EVT_Available:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_Subtitle_Available, 0, 0);
            break;
        case CTC_PLAYER_EVT_Unavailable:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_Subtitle_Unavailable, 0, 0);
            break;
        case CTC_PLAYER_EVT_CC_Removed:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_CC_Removed, 0, 0);
            break;
        case CTC_PLAYER_EVT_CC_Added:
            ctc_func_player_evt(ctc_player_evt_hander, IPTV_PLAYER_EVT_CC_Added, 0, 0);
            break;
    default :
        MLOG("evt : %d\n", evt);
        break;
    }
}


}
