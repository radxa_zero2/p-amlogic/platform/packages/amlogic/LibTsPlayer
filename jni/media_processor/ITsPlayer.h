#ifndef _ITSPLAYER_H_
#define _ITSPLAYER_H_

#include <utils/RefBase.h>
#include <utils/Errors.h>
#include <utils/KeyedVector.h>
#include <utils/String8.h>
#include <android/native_window.h>
#include <mutex>

#include <CTC_Common.h>
#include "CTC_Internal.h"

namespace android {
class Surface;
};

namespace aml {

using android::RefBase;
using android::status_t;
using android::KeyedVector;
using android::String8;
//using android::Surface;


class ITsPlayer : public RefBase
{
public:
    typedef uint32_t player_id;
    static constexpr player_id kInvalidId = UINT32_MAX;

    explicit ITsPlayer(const CTC_InitialParameter* initialParam = nullptr);
    virtual ~ITsPlayer();

    virtual int  GetPlayMode()=0;

    /**
     * \brief SetVideoWindow
     *
     * \param x
     * \param y
     * \param width
     * \param height
     *
     * \return
     */
    virtual int  SetVideoWindow(int x,int y,int width,int height)=0;

    /**
     * \brief VideoShow
     *
     * \return
     */
    virtual int  VideoShow(void)=0;

    /**
     * \brief VideoHide
     *
     * \return
     */
    virtual int  VideoHide(void)=0;

    /**
     * \brief enableAudioOutput
     *
     * \return
     */
    virtual int EnableAudioOutput() = 0;

    /**
     * \brief DisableAudioOUtput
     *
     * \return
     */
    virtual int DisableAudioOutput() = 0;

    /**
     * \brief InitVideo
     *
     * \param pVideoPara
     */
    virtual void InitVideo(PVIDEO_PARA_T pVideoPara)=0;

    /**
     * \brief InitAudio
     *
     * \param pAudioPara
     */
    virtual void InitAudio(PAUDIO_PARA_T pAudioPara)=0;

    /**
     * \brief InitSubtitle
     *
     * \param pSubtitlePara
     */
	virtual void InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara)=0;

    /**
     * @brief InitCAS
     *
     * @param pCASInitPara
     */
    virtual int InitCAS(DVB_CAS_INIT_PARA* pCASInitPara)=0;

    /**
     * \brief initSyncSource
     *
     * \param syncSource
     *
     * \return
     */
    virtual int initSyncSource(CTC_SyncSource syncSource) = 0;

    /**
     * \brief StartPlay
     *
     * \return
     */
    virtual bool StartPlay()=0;

    /**
     * \brief Pause
     *
     * \return
     */
    virtual bool Pause()=0;

    /**
     * \brief Resume
     *
     * \return
     */
    virtual bool Resume()=0;

    /**
     * \brief Fast
     *
     * \return
     */
    virtual bool Fast()=0;

    /**
     * \brief StopFast
     *
     * \return
     */
    virtual bool StopFast()=0;

    /**
     * \brief Stop
     *
     * \return
     */
    virtual bool Stop()=0;

    /**
     * \brief Seek
     *
     * \return
     */
    virtual bool Seek()=0;

    /**
     * \brief SetVolume
     *
     * \param volume
     *
     * \return
     */
    virtual bool SetVolume(int volume)=0;

    /**
     * \brief GetVolume
     *
     * \return
     */
    virtual int GetVolume()=0;

    /**
     * \brief SetRatio
     *
     * \param nRatio
     *
     * \return
     */
    virtual bool SetRatio(int nRatio)=0;

    /**
     * \brief GetAudioBalance
     *
     * \return
     */
    virtual int GetAudioBalance()=0;

    /**
     * \brief SetAudioBalance
     *
     * \param nAudioBalance
     *
     * \return
     */
    virtual bool SetAudioBalance(int nAudioBalance)=0;

    /**
     * \brief GetVideoPixels
     *
     * \param width
     * \param height
     */
    virtual void GetVideoPixels(int& width, int& height)=0;

    /**
     * \brief IsSoftFit
     *
     * \return
     */
    virtual bool IsSoftFit()=0;

    /**
     * \brief SetEPGSize
     *
     * \param w
     * \param h
     */
    virtual void SetEPGSize(int w, int h)=0;

    /**
     * \brief SetSurface
     *
     * \param pSurface
     */
    virtual void SetSurface(android::Surface* pSurface)=0;

    /**
     * \brief SetSurface_ANativeWindow
     *
     * \param pSurface
     */
    virtual void SetSurface_ANativeWindow(ANativeWindow* pSurface) = 0;

    /**
     * \brief GetWriteBuffer
     *
     * \param type
     * \param pBuffer
     * \param nSize
     *
     * \return
     */
    virtual int GetWriteBuffer(PLAYER_STREAMTYPE_E type, uint8_t** pBuffer, uint32_t* nSize);

    /**
     * \brief WriteData
     *
     * \param pBuffer
     * \param nSize
     *
     * \return
     */
    virtual int WriteData(uint8_t* pBuffer, uint32_t nSize)=0;

    /**
     * \brief WriteData
     *
     * \param type
     * \param pBuffer
     * \param nSize
     * \param timestamp 90k
     *
     * \return
     */
    virtual int WriteData(PLAYER_STREAMTYPE_E type, uint8_t* pBuffer, uint32_t nSize, int64_t timestamp) = 0;

    /**
     * \brief SwitchVideoTrack
     *
     * \param pVideoPara
     */
    virtual void SwitchVideoTrack(PVIDEO_PARA_T pVideoPara) = 0;

    /**
     * \brief SwitchAudioTrack
     *
     * \param pid
     */
    virtual void SwitchAudioTrack(int pid, PAUDIO_PARA_T pAudioPara) = 0;

    /**
     * \brief SwitchSubtitle
     *
     * \param pid
     * \param pSubtitlePara
     */
    virtual void SwitchSubtitle(int pid, PSUBTITLE_PARA_T pSubtitlePara) = 0;

    /**
     * \brief SubtitleShowHide
     *
     * \param bShow
     *
     * \return
     */
    virtual bool SubtitleShowHide(bool bShow) = 0;

    /**
     * \brief GetCurrentPts
     *
     * \param type
     *
     * \return
     */
    virtual int64_t GetCurrentPts(PLAYER_STREAMTYPE_E type) = 0;

    /**
     * \brief GetCurrentPlayTime
     *
     * \return
     */
    virtual int64_t GetCurrentPlayTime() = 0;

    /**
     * \brief SetStopMode
     *
     * \param bHoldLastPic
     */
    virtual void SetStopMode(bool bHoldLastPic) = 0;

    /**
     * \brief GetBufferStatus
     *
     * \param pstatus
     */
    virtual int GetBufferStatus(PAVBUF_STATUS pstatus) = 0;

    /**
     * \brief GetStreamInfo
     *
     * \param pVideoInfo
     * \param pAudioInfo
     * \param pSubtitleInfo
     *
     * \return
     */
    virtual int GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo) = 0;

    /**
     * \brief SetParameter
     *
     * \param hander
     * \param type
     * \param ptr
     *
     * \return
     */
    virtual int SetParameter(void* handler, int key, void * request) = 0;

    /**
     * \brief GetParameter
     *
     * \param hander
     * \param type
     * \param ptr
     *
     * \return
     */
    virtual int GetParameter(void* handler, int key, void * reply) = 0;

    /**
     * \brief ctc_register_evt_cb
     *
     * \param pfunc
     * \param hander
     */
    virtual void ctc_register_evt_cb(CTC_PLAYER_EVT_CB pfunc, void *hander) = 0;

    /**
     * \brief AML_DMX_CreateChannel
     *
     * \param pid
     */
    virtual AML_HANDLE DMX_CreateChannel(int pid) = 0;

    /**
     * \brief AML_DMX_DestroyChannel
     *
     * \param channel
     */
    virtual int DMX_DestroyChannel(AML_HANDLE channel) = 0;

    /**
     * \brief AML_DMX_OpenChannel
     *
     * \param channel
     * \return
     */
    virtual int DMX_OpenChannel(AML_HANDLE channel) = 0;

    /**
     * \brief AML_DMX_CloseChannel
     *
     * \param channel
     * \return
     */
    virtual int DMX_CloseChannel(AML_HANDLE channel) = 0;

    /**
     * \brief AML_DMX_CreateFilter
     *
     * \param cb
     * \param pUserData
     * \param id
     */
    virtual AML_HANDLE DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id) = 0;

    /**
     * \brief AML_DMX_DestroyFilter
     *
     * \param filter
     * \return
     */
    virtual int DMX_DestroyFilter(AML_HANDLE filter) = 0;

    /**
     * \brief AML_DMX_AttachFilter
     *
     * \param filter
     * \param channel
     * \return
     */
    virtual int DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel) = 0;

    /**
     * \brief AML_DMX_DetachFilter
     *
     * \param filter
     * \param channel
     * \return
     */
    virtual int DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel) = 0;

protected:
    int                 mUserId;
    player_id           mPlayerId;
    PLAYER_STREAMTYPE_E mStreamType;

    CTCWriteBuffer mWriteBuffers[PLAYER_STREAMTYPE_MAX];
private:

    ITsPlayer(const ITsPlayer&) = delete;
    ITsPlayer& operator=(const ITsPlayer&) = delete;
};

///////////////////////////////////////////////////////////////////////////////
#define ITSPLAYER_INSTANCE_MAX      9

class ITsPlayerFactory
{
public:
    typedef ITsPlayer::player_id player_id;

    static ITsPlayerFactory& instance();
    ITsPlayer* createITsPlayer(CTC_InitialParameter* initialParam);
    player_id registerITsPlayer(ITsPlayer* player);
    void unregisterITsPlayer(player_id id);
    void signalCTsPlayerId(int userId, player_id id);
    bool isCTsPlayerExist() const;
    bool awaitCTsPlayerExit();

private:
    ITsPlayerFactory();
    ~ITsPlayerFactory();

private:
    static ITsPlayerFactory* sITsPlayerFactory;

    mutable std::mutex mLock;
    std::condition_variable mCond;
    ITsPlayer* mPlayers[ITSPLAYER_INSTANCE_MAX];
    std::atomic<int> mPlayerNum{0};

    std::atomic<int> mCTsUserId{-1};
    player_id mCTsPlayerId;
    std::atomic<int64_t> mCTsPlayerCreateTime{0};

    ITsPlayerFactory(const ITsPlayerFactory&) = delete;
    ITsPlayerFactory& operator=(const ITsPlayerFactory&) = delete;
};


}

#endif /* ifndef _ITSPLAYER_H_*/

