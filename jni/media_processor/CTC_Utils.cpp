#define LOG_NDEBUG 0
#define LOG_TAG "CTC_Utils"
#include <string.h>
#include <stdlib.h>
#include "CTC_Log.h"
#include "CTC_Utils.h"
#include <Amsysfsutils.h>
#include <media/stagefright/foundation/MediaDefs.h>
#include <media/stagefright/foundation/ADebug.h>
#include <unistd.h>

extern "C" {
#include <libavformat/avformat.h>
#include <Amsysfsutils.h>
}

int prop_shouldshowlog = 0;

namespace aml {

///////////////////////////////////////////////////////////////////////////////
const char* iptvPlayerEvt2Str(IPTV_PLAYER_EVT_E evt)
{
#define ENUM_TO_STR(e) case e: return #e; break

    static char buf[20];
    snprintf(buf, sizeof(buf), "Unknown IPTV_PLAYER_EVT_E:%#x", evt);

    switch (evt) {
        ENUM_TO_STR(IPTV_PLAYER_EVT_STREAM_VALID);
        ENUM_TO_STR(IPTV_PLAYER_EVT_FIRST_PTS);

        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_FRAME_ERROR);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_DISCARD_FRAME);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_DEC_OVERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_PTS_ERROR);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_INVALID_DATA);

        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_BUFF_UNLOAD_START);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_BUFF_UNLOAD_END);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_MOSAIC_START);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_MOSAIC_END);

        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_FRAME_ERROR);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_DISCARD_FRAME);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_DEC_OVERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_PTS_ERROR);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_INVALID_DATA);

        ENUM_TO_STR(IPTV_PLAYER_EVT_Subtitle_Available);
        ENUM_TO_STR(IPTV_PLAYER_EVT_Subtitle_Unavailable);
        ENUM_TO_STR(IPTV_PLAYER_EVT_CC_Removed);
        ENUM_TO_STR(IPTV_PLAYER_EVT_CC_Added);
    };

    return buf;
}

const char* streamType2Str(PLAYER_STREAMTYPE_E type)
{
#define TYPE_TO_STR(t)  {t, #t}
    static struct {
        PLAYER_STREAMTYPE_E name;
        const char* str;
    } t[] {
        TYPE_TO_STR(PLAYER_STREAMTYPE_NULL),
        TYPE_TO_STR(PLAYER_STREAMTYPE_TS),
        TYPE_TO_STR(PLAYER_STREAMTYPE_VIDEO),
        TYPE_TO_STR(PLAYER_STREAMTYPE_AUDIO),
        TYPE_TO_STR(PLAYER_STREAMTYPE_SUBTITLE),
        TYPE_TO_STR(PLAYER_STREAMTYPE_ES),
    };
#undef TYPE_TO_STR

    for (size_t i = 0; i < sizeof(t)/sizeof(*t); ++i) {
        if (type == t[i].name) {
            return t[i].str;
        }
    }

    return "Unknown StreamType";
}

///////////////////////////////////////////////////////////////////////////////
static bool get_vfm_map_info(char *vfm_map)
{
    int fd;
    const char *path = "/sys/class/vfm/map";

    if (!vfm_map) {
        ALOGE("invalid parameter!");
        return false;
    }

    fd = open(path, O_RDONLY);

    if (fd >= 0) {
        memset(vfm_map, 0, 4096);
        int ret = read(fd, vfm_map, 4095);
        vfm_map[4095] = '\0';
        if (ret < 0) {
            ALOGE("read %s failed: %s", path, strerror(errno));
            close(fd);
            return false;
        }

        vfm_map[strlen(vfm_map)] = '\0';
        close(fd);
    } else
        ALOGE("open %s failed: %s", path, strerror(errno));

    return true;
}

OUTPUT_MODE get_display_mode()
{
    int fd;
    char mode[16] = {0};
    char path[64] = {"/sys/class/display/mode"};
    fd = open(path, O_RDONLY);
    if (fd >= 0) {
        memset(mode, 0, 16); // clean buffer and read 15 byte to avoid strlen > 15
        int ret = read(fd, mode, 15);
        if (ret > 0) {
            mode[15] = '\0';
            mode[strlen(mode)] = '\0';
        }
        close(fd);
        if (!strncmp(mode, "480i", 4) || !strncmp(mode, "480cvbs", 7)) {
            return OUTPUT_MODE_480I;
        } else if(!strncmp(mode, "480p", 4)) {
            return OUTPUT_MODE_480P;
        } else if(!strncmp(mode, "576i", 4) || !strncmp(mode, "576cvbs", 7)) {
            return OUTPUT_MODE_576I;
        } else if(!strncmp(mode, "576p", 4)) {
            return OUTPUT_MODE_576P;
        } else if(!strncmp(mode, "720p", 4)) {
            return OUTPUT_MODE_720P;
        } else if(!strncmp(mode, "1080i", 5)) {
            return OUTPUT_MODE_1080I;
        } else if(!strncmp(mode, "1080p", 5)) {
            return OUTPUT_MODE_1080P;
        } else if(!strncmp(mode, "4k2k24hz", 8) ||!strncmp(mode, "2160p24hz", 9)){
            return OUTPUT_MODE_4K2K24HZ;
        } else if(!strncmp(mode, "4k2k25hz", 8) ||!strncmp(mode, "2160p25hz", 9)){
            return OUTPUT_MODE_4K2K25HZ;
        } else if(!strncmp(mode, "4k2k30hz", 8)||!strncmp(mode, "2160p30hz", 9)){
            return OUTPUT_MODE_4K2K30HZ;
        } else if(!strncmp(mode, "4k2ksmpte", 8)) {
            return OUTPUT_MODE_4K2KSMPTE;
        }else if(!strncmp(mode, "2160p60hz", 9)) {
            return OUTPUT_MODE_4K2K60HZ;
        }else if(!strncmp(mode, "2160p50hz", 9)) {
            return OUTPUT_MODE_4K2K50HZ;
        }
    } else {
        ALOGE("get_display_mode open file %s error\n", path);
    }
    return OUTPUT_MODE_720P;
}

void getPosition(OUTPUT_MODE output_mode, int *x, int *y, int *width, int *height)
{
    char vaxis_newx_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_newy_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_width_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_height_str[PROPERTY_VALUE_MAX] = {0};

    switch (output_mode) {
    case OUTPUT_MODE_480I:
        property_get("ubootenv.var.480i_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.480i_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.480i_w", vaxis_width_str, "720");
        property_get("ubootenv.var.480i_h", vaxis_height_str, "480");
        break;
    case OUTPUT_MODE_480P:
        property_get("ubootenv.var.480p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.480p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.480p_w", vaxis_width_str, "720");
        property_get("ubootenv.var.480p_h", vaxis_height_str, "480");
        break;
    case OUTPUT_MODE_576I:
        property_get("ubootenv.var.576i_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.576i_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.576i_w", vaxis_width_str, "720");
        property_get("ubootenv.var.576i_h", vaxis_height_str, "576");
        break;
    case OUTPUT_MODE_576P:
        property_get("ubootenv.var.576p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.576p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.576p_w", vaxis_width_str, "720");
        property_get("ubootenv.var.576p_h", vaxis_height_str, "576");
        break;
    case OUTPUT_MODE_720P:
        property_get("ubootenv.var.720p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.720p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.720p_w", vaxis_width_str, "1280");
        property_get("ubootenv.var.720p_h", vaxis_height_str, "720");
        break;
    case OUTPUT_MODE_1080I:
        property_get("ubootenv.var.1080i_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.1080i_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.1080i_w", vaxis_width_str, "1920");
        property_get("ubootenv.var.1080i_h", vaxis_height_str, "1080");
        break;
    case OUTPUT_MODE_1080P:
        property_get("ubootenv.var.1080p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.1080p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.1080p_w", vaxis_width_str, "1920");
        property_get("ubootenv.var.1080p_h", vaxis_height_str, "1080");
        break;
    case OUTPUT_MODE_4K2K24HZ:
        property_get("ubootenv.var.4k2k24hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k24hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k24hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k24hz_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2K25HZ:
        property_get("ubootenv.var.4k2k25hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k25hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k25hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k25hz_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2K30HZ:
        property_get("ubootenv.var.4k2k30hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k30hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k30hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k30hz_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2KSMPTE:
        property_get("ubootenv.var.4k2ksmpte_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2ksmpte_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2ksmpte_w", vaxis_width_str, "4096");
        property_get("ubootenv.var.4k2ksmpte_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2K60HZ:
        property_get("ubootenv.var.4k2k60hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k60hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k60hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k60hz_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2K50HZ:
        property_get("ubootenv.var.4k2k50hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k50hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k50hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k50hz_h", vaxis_height_str, "2160");
        break;
    default:
          *x = 0;
          *y = 0;
          *width = 1280;
          *height = 720;
        ALOGW("UNKNOW MODE:%d", output_mode);
        return;
    }
    *x = atoi(vaxis_newx_str);
    *y = atoi(vaxis_newy_str);
    *width = atoi(vaxis_width_str);
    *height = atoi(vaxis_height_str);
}

bool checkIfVideoLayerTaken() {
    char vfm_map[4096] = {0};
    const char *delim = "}";
    get_vfm_map_info(vfm_map);
    ALOGD("vfm_map is %s", vfm_map);
    char *stringp = vfm_map;
    char *path;
    bool videoLayerTaken = false;

    while (stringp != NULL) {
        path = strsep(&stringp, delim);
        ALOGD("path=%s", path);

        if (strstr(path, "(1)") && strstr(path, "amvideo") && !strstr(path, "videocomposer(1)"))
            videoLayerTaken = true;
    }

    return videoLayerTaken;
}

void setVideoAxis() {
    char bcmd[256];
    int ret = 0;
    int mode_x = 0, mode_y = 0, mode_width = 0, mode_height = 0;

    OUTPUT_MODE output_mode = get_display_mode();
    getPosition(output_mode, &mode_x, &mode_y, &mode_width, &mode_height);
    ALOGD("setVideoAxis mode_x: %d, mode_y: %d, mode_width: %d, mode_height: %d\n",
            mode_x, mode_y, mode_width, mode_height);
    sprintf(bcmd, "%d %d %d %d", mode_x, mode_y, mode_width + mode_x - 1, mode_height + mode_y -1);
    ret = amsysfs_set_sysfs_str("/sys/class/video/axis", bcmd);
    ALOGD("setVideoAxis ret=%d, errno=%d", ret, errno);
    sprintf(bcmd, "%s", "wtf is /sys/class/video/axis");
    ret = amsysfs_get_sysfs_str("/sys/class/video/axis", bcmd, 256);
    ALOGD("now video axis is %s", bcmd);
}

/* if the pip window show at video layer, it need to convert the axis */
void convert_pip_axis(int x, int y, int w, int h, int *dst_x,
                      int *dst_y, int *dst_w, int *dst_h)
{
#define PATH_AXIS_PIP  "/sys/class/video/axis_pip"
#define PATH_PP_WITH   "/sys/module/amlvideodri/parameters/pp_width"
#define PATH_PP_HEIGHT "/sys/module/amlvideodri/parameters/pp_height"
#define PATH_SCALE_WIDTH "/sys/class/graphics/fb0/scale_width"
#define PATH_SCALE_HEIGHT "/sys/class/graphics/fb0/scale_height"
#define PATH_VIDEO_AXIS  "/sys/class/video/axis"
    OUTPUT_MODE output_mode = get_display_mode();
    int epg_centre_x = 0;
    int epg_centre_y = 0;
    int old_videowindow_certre_x = 0;
    int old_videowindow_certre_y = 0;
    int new_videowindow_certre_x = 0;
    int new_videowindow_certre_y = 0;
    int new_videowindow_width = 0;
    int new_videowindow_height = 0;
    int osd_w = 1280, osd_h =720;
    int x2 = 0, y2 = 0, width2 = 0, height2 = 0;
    int vaxis_newx= -1, vaxis_newy = -1, vaxis_width= -1, vaxis_height= -1;
    int  l = 0,r=0,t=0,b= 0;
    char str_cmd[64] = {0};
    int ret = 0;

    if (access(PATH_AXIS_PIP, F_OK) != 0)
    {
        ALOGI("this version has not the video layer 2 for pip.\n");
        return;
    }
    amsysfs_get_sysfs_str(PATH_SCALE_WIDTH, str_cmd, sizeof(str_cmd));
    osd_w = strtol(str_cmd + strlen("free_scale_width:"), NULL, 10);
    ALOGI("conver_axis: scale_width:%s output_mode:%d\n", str_cmd, output_mode);
    amsysfs_get_sysfs_str(PATH_SCALE_HEIGHT, str_cmd, sizeof(str_cmd));
    osd_h = strtol(str_cmd + strlen("free_scale_height:"), NULL, 10);
    ALOGI("conver_axis: fb w:%d h:%d\n", osd_w, osd_h);
    //getPosition(output_mode, &vaxis_newx, &vaxis_newy, &vaxis_width, &vaxis_height)
    amsysfs_get_sysfs_str(PATH_VIDEO_AXIS, str_cmd, sizeof(str_cmd));
    sscanf(str_cmd,"%d %d %d %d",&l,&t,&r,&b);
    vaxis_newx = l;
    vaxis_newy = t;
    vaxis_width = (r+1)-l;
    vaxis_height = (b+1)-t;
    ALOGI("output_mode: %d, vaxis_newx: %d, vaxis_newy: %d,\
           vaxis_width: %d, vaxis_height: %d\n",
            output_mode, vaxis_newx, vaxis_newy, vaxis_width, vaxis_height);

    if (osd_w != 0) {
        x2 = x*vaxis_width/osd_w;
        width2 = w*vaxis_width/osd_w;
    }

    if (osd_h != 0) {
        y2 = y * vaxis_height / osd_h;
        height2 = h * vaxis_height / osd_h;
    }

    ALOGI("CTsPlayer::new video window x2 = %d, y2 = %d, width2 = %d,\
           height2 = %d \n", x2, y2, width2, height2);
    *dst_x = vaxis_newx +x2;
    *dst_y  =  vaxis_newy +y2;
     *dst_w = *dst_x +width2;
     *dst_h  = *dst_y +height2;
    sprintf(str_cmd, "%d %d %d %d", *dst_x, *dst_y, *dst_w, *dst_h);
    amsysfs_set_sysfs_str(PATH_AXIS_PIP, str_cmd);
    amsysfs_set_sysfs_int(PATH_PP_WITH, width2);
    amsysfs_set_sysfs_int(PATH_PP_HEIGHT, height2);
    ALOGI("setvideo axis_pip: %s\n", str_cmd);

    return;
}

///////////////////////////////////////////////////////////////////////////////
int getMediaInfo(const char* url, CTC_MediaInfo* mediaInfo)
{
    av_register_all();

    memset(mediaInfo, 0, sizeof(*mediaInfo));

    AVFormatContext* ctx = avformat_alloc_context();

    AVInputFormat* inputFormat = av_find_input_format("mpegts");
    int ret = avformat_open_input(&ctx, url, inputFormat, nullptr);
    if (ret != 0) {
        ALOGE("avformat open failed! ret=%d, %s", ret, av_err2str(ret));
        avformat_free_context(ctx);
        return -1;
    }

    ret = avformat_find_stream_info(ctx, nullptr);
    if (ret < 0) {
        ALOGE("find stream info failed! ret:%d, %s", ret, av_err2str(ret));
        avformat_close_input(&ctx);
        return -1;
    }

    mediaInfo->bitrate = ctx->bit_rate;
    printf("bitrate:%lld KB/s, duration:%.2f s\n", mediaInfo->bitrate>>13, ctx->duration*1.0/AV_TIME_BASE);

    av_dump_format(ctx, -1, nullptr, 0);

    for (size_t i = 0; i < ctx->nb_streams; ++i) {
        AVStream* st = ctx->streams[i];
        if (mediaInfo->vPara.pid == 0 && st->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            mediaInfo->vPara.pid = st->id;
            mediaInfo->vPara.vFmt = convertToVFormat(st->codec->codec_id);
            mediaInfo->vPara.nVideoWidth = st->codec->width;
            mediaInfo->vPara.nVideoHeight = st->codec->height;
            mediaInfo->vPara.nFrameRate = av_q2d(st->avg_frame_rate);
            mediaInfo->vPara.pExtraData = (uint8_t*)malloc(st->codec->extradata_size);
            memcpy(mediaInfo->vPara.pExtraData, st->codec->extradata, st->codec->extradata_size);
            mediaInfo->vPara.nExtraSize = st->codec->extradata_size;
            ALOGI("video pid:%d, fmt:%d, frame_rate:%d\n", mediaInfo->vPara.pid, mediaInfo->vPara.vFmt, mediaInfo->vPara.nFrameRate);
        } else if (st->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
            AUDIO_PARA_T *aPara = &mediaInfo->aParas[mediaInfo->aParaCount++];
            aPara->pid = st->id;
            aPara->aFmt = convertToAFormat(st->codec->codec_id);
            aPara->nChannels = st->codec->channels;
            aPara->nSampleRate = st->codec->sample_rate;
            aPara->pExtraData = (uint8_t*)malloc(st->codec->extradata_size);
            memcpy(aPara->pExtraData, st->codec->extradata, st->codec->extradata_size);
            aPara->nExtraSize = st->codec->extradata_size;
            ALOGI("audio pid:%d, fmt:%d\n", aPara->pid, aPara->aFmt);
        } else if (st->codec->codec_type == AVMEDIA_TYPE_SUBTITLE) {
            SUBTITLE_PARA_T* sPara = &mediaInfo->sParas[mediaInfo->sParaCcount++];
            aml::convertToSPara(st, sPara);
            ALOGI("subtitle pid:%d, fmt:%d\n", st->id, sPara->sub_type);
        }
    }

    avformat_close_input(&ctx);
    return 0;
}

void freeMediaInfo(CTC_MediaInfo* mediaInfo)
{
    free(mediaInfo->vPara.pExtraData);

    for (size_t i = 0; i < mediaInfo->aParaCount; ++i) {
        free(mediaInfo->aParas[i].pExtraData);
    }
}


aformat_t convertToAFormat(int _codecId)
{
    AVCodecID codecId = (AVCodecID)_codecId;
    aformat_t format = AFORMAT_UNKNOWN;

    switch (codecId) {
    case AV_CODEC_ID_MP1:
    case AV_CODEC_ID_MP2:
    case AV_CODEC_ID_MP3:
        format = AFORMAT_MPEG;
        break;

    case AV_CODEC_ID_PCM_MULAW:
        //format = AFORMAT_MULAW;
        format = AFORMAT_ADPCM;
        break;

    case AV_CODEC_ID_PCM_ALAW:
        //format = AFORMAT_ALAW;
        format = AFORMAT_ADPCM;
        break;

    case AV_CODEC_ID_AAC_LATM:
        format = AFORMAT_AAC_LATM;
        break;

    case AV_CODEC_ID_AAC:
        format = AFORMAT_AAC;
        break;

    case AV_CODEC_ID_AC3:
        format = AFORMAT_AC3;
        break;
    case AV_CODEC_ID_EAC3:
        format = AFORMAT_EAC3;
        break;
    case AV_CODEC_ID_DTS:
        format = AFORMAT_DTS;
        break;

    case AV_CODEC_ID_PCM_S16BE:
        format = AFORMAT_PCM_S16BE;
        break;

    case AV_CODEC_ID_PCM_S16LE:
        format = AFORMAT_PCM_S16LE;
        format = AFORMAT_PCM_S16LE;
        break;

    case AV_CODEC_ID_PCM_U8:
        format = AFORMAT_PCM_U8;
        break;

    case AV_CODEC_ID_COOK:
        format = AFORMAT_COOK;
        break;

    case AV_CODEC_ID_ADPCM_IMA_WAV:
    case AV_CODEC_ID_ADPCM_MS:
        format = AFORMAT_ADPCM;
        break;
    case AV_CODEC_ID_AMR_NB:
    case AV_CODEC_ID_AMR_WB:
        format =  AFORMAT_AMR;
        break;
    case AV_CODEC_ID_WMAV1:
    case AV_CODEC_ID_WMAV2:
        format =  AFORMAT_WMA;
        break;
    case AV_CODEC_ID_FLAC:
        format = AFORMAT_FLAC;
        break;

    case AV_CODEC_ID_WMAPRO:
        format = AFORMAT_WMAPRO;
        break;

    case AV_CODEC_ID_PCM_BLURAY:
        format = AFORMAT_PCM_BLURAY;
        break;
    case AV_CODEC_ID_ALAC:
        format = AFORMAT_ALAC;
        break;
    case AV_CODEC_ID_VORBIS:
        format =    AFORMAT_VORBIS;
        break;
    case AV_CODEC_ID_APE:
        format =    AFORMAT_APE;
        break;

    default:
        break;
    }

    return format;
}

vformat_t convertToVFormat(int _codecId)
{
    AVCodecID codecId = (AVCodecID)_codecId;
    vformat_t format = VFORMAT_UNKNOWN;

    switch (codecId) {
    case AV_CODEC_ID_MPEG2VIDEO:
        format = VFORMAT_MPEG12;
        break;

    case AV_CODEC_ID_MPEG4:
        format = VFORMAT_MPEG4;
        break;

    case AV_CODEC_ID_H264:
        format = VFORMAT_H264;
        break;

    case AV_CODEC_ID_MJPEG:
        format = VFORMAT_MJPEG;
        break;

    case AV_CODEC_ID_VC1:
        format = VFORMAT_VC1;
        break;

    case AV_CODEC_ID_HEVC:
        format = VFORMAT_HEVC;
        break;

    case AV_CODEC_ID_VP9:
        format = VFORMAT_VP9;
        break;

    case AV_CODEC_ID_CAVS:
        format = VFORMAT_AVS;
        break;

    case AV_CODEC_ID_CAVS2:
        format = VFORMAT_AVS2;
        break;

    default:
        break;
    }

    return format;
}

/*
//refactor CTC to  call Subtitleclient api
SUBTITLE_TYPE convertToSubFormat(int _codecId)
{
    AVCodecID codecId = (AVCodecID)_codecId;
    SUBTITLE_TYPE subType = CTC_CODEC_ID_DVD_SUBTITLE;

    switch (codecId) {
    case AV_CODEC_ID_DVD_SUBTITLE:
        subType = CTC_CODEC_ID_DVD_SUBTITLE;
        break;

    case AV_CODEC_ID_DVB_SUBTITLE:
        subType = CTC_CODEC_ID_DVB_SUBTITLE;
        break;

    case AV_CODEC_ID_TEXT:
        subType = CTC_CODEC_ID_TEXT;
        break;

    case AV_CODEC_ID_XSUB:
        subType = CTC_CODEC_ID_XSUB;
        break;

    case AV_CODEC_ID_SSA:
        subType = CTC_CODEC_ID_SSA;
        break;

    case AV_CODEC_ID_MOV_TEXT:
        subType = CTC_CODEC_ID_MOV_TEXT;
        break;

    case AV_CODEC_ID_HDMV_PGS_SUBTITLE:
        subType = CTC_CODEC_ID_HDMV_PGS_SUBTITLE;
        break;

    case AV_CODEC_ID_DVB_TELETEXT:
        subType = CTC_CODEC_ID_DVB_TELETEXT;
        break;

    case AV_CODEC_ID_SRT:
        subType = CTC_CODEC_ID_SRT;
        break;

    case AV_CODEC_ID_MICRODVD:
        subType = CTC_CODEC_ID_MICRODVD;
        break;

    default:
        break;
    }

    return subType;
}
*/
SubtitleType convertToSubFormat(int _codecId)
{
    SubtitleType subType = UNKNOWN_SUBTYPE;
    AVCodecID codecId = (AVCodecID)_codecId;

    switch (codecId) {
    case AV_CODEC_ID_DVB_SUBTITLE:
        subType = DVB_SUBTITLE;
        break;

    case AV_CODEC_ID_DVB_TELETEXT:
        subType = TELETEXT;
        break;

    default:
        LOGI("[%s:%d] unknow Subtitle codecId: %d", __FUNCTION__, __LINE__, codecId);
        break;
    }

    return subType;
}

void setSubtitleId(SUBTITLE_PARA_T* sPara, SubtitleType subType, size_t pid)
{
    switch (subType) {
    case CC:
        sPara->cc_param.Channel_ID = pid;
        break;

    case SCTE27:
        sPara->scte27_param.SCTE27_PID = pid;
        break;

    case DVB_SUBTITLE:
        sPara->dvb_sub_param.Sub_PID = pid;
        break;

    case TELETEXT:
        sPara->tt_param.Teletext_PID = pid;
        break;

    default:
        LOGI("[%s:%d] unknow Subtitle type: %d", __FUNCTION__, __LINE__, subType);
        break;
    }
}

struct KeyMap {
    const char *mime;
    AVCodecID key;
};

using namespace android;

static const KeyMap kKeyMap[] = {
    { MEDIA_MIMETYPE_VIDEO_AVC, AV_CODEC_ID_H264 },
    { MEDIA_MIMETYPE_VIDEO_HEVC, AV_CODEC_ID_HEVC},
    { MEDIA_MIMETYPE_VIDEO_H263, AV_CODEC_ID_H263 },
    { MEDIA_MIMETYPE_VIDEO_AVC, AV_CODEC_ID_H264 },
    // Setting MPEG2 mime type for MPEG1 video intentionally because
    // All standards-compliant MPEG-2 Video decoders are fully capable of
    // playing back MPEG-1 Video streams conforming to the CPB.
    { MEDIA_MIMETYPE_VIDEO_MPEG2, AV_CODEC_ID_MPEG1VIDEO },
    { MEDIA_MIMETYPE_VIDEO_MPEG2, AV_CODEC_ID_MPEG2VIDEO },
    { MEDIA_MIMETYPE_VIDEO_MPEG4, AV_CODEC_ID_MPEG4 },
    { MEDIA_MIMETYPE_VIDEO_VP9, AV_CODEC_ID_VP9 },

    { MEDIA_MIMETYPE_AUDIO_AAC, AV_CODEC_ID_AAC },
    { MEDIA_MIMETYPE_AUDIO_AC3, AV_CODEC_ID_AC3 },
    // TODO: check if AMR works fine once decoder supports it.
    { MEDIA_MIMETYPE_AUDIO_AMR_NB, AV_CODEC_ID_AMR_NB },
    { MEDIA_MIMETYPE_AUDIO_AMR_WB, AV_CODEC_ID_AMR_WB },
    { MEDIA_MIMETYPE_AUDIO_MPEG_LAYER_I, AV_CODEC_ID_MP1 },
    { MEDIA_MIMETYPE_AUDIO_MPEG_LAYER_II, AV_CODEC_ID_MP2 },
    { MEDIA_MIMETYPE_AUDIO_MPEG, AV_CODEC_ID_MP3 },
    // TODO: add more PCM codecs including A/MuLAW.
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_S16LE },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_S16BE },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_U16LE },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_U16BE },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_S8 },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_U8 },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_S24BE },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_U24LE },
    { MEDIA_MIMETYPE_AUDIO_RAW, AV_CODEC_ID_PCM_U24BE },
    { MEDIA_MIMETYPE_AUDIO_VORBIS, AV_CODEC_ID_VORBIS },
    { MEDIA_MIMETYPE_TEXT_3GPP, AV_CODEC_ID_MOV_TEXT },
    //add for subtitle
    { "subtitle/dvd",           AV_CODEC_ID_DVD_SUBTITLE },
    { "subtitle/dvb",           AV_CODEC_ID_DVB_SUBTITLE },
    { "subtitle/text",          AV_CODEC_ID_TEXT },
    { "subtitle/xsub",          AV_CODEC_ID_XSUB },
    { "subtitle/ssa",           AV_CODEC_ID_SSA },
    { "subtitle/pgs",           AV_CODEC_ID_HDMV_PGS_SUBTITLE },
    { "subtitle/teletext",      AV_CODEC_ID_DVB_TELETEXT },
    { "subtitle/srt",           AV_CODEC_ID_SRT },
    { "subtitle/microdvd",      AV_CODEC_ID_MICRODVD },
    { "subtitle/eia608",        AV_CODEC_ID_EIA_608 },
    { "subtitle/jacosub",       AV_CODEC_ID_JACOSUB },
    { "subtitle/sami",          AV_CODEC_ID_SAMI },
    { "subtitle/realtext",      AV_CODEC_ID_REALTEXT },
    { "subtitle/subviewer",     AV_CODEC_ID_SUBVIEWER },
    { "subtitle/subrip",        AV_CODEC_ID_SUBRIP },
    { "subtitle/webvtt",        AV_CODEC_ID_WEBVTT },
    { "subtitle/ssa",           AV_CODEC_ID_ASS },
    { "subtitle/stl",           AV_CODEC_ID_STL },
    { "subtitle/subviewer1",    AV_CODEC_ID_SUBVIEWER1 },
    { "subtitle/mpl2",          AV_CODEC_ID_MPL2 },
    { "subtitle/vplayer",       AV_CODEC_ID_VPLAYER },
    { "subtitle/pjs",           AV_CODEC_ID_PJS },
    { "subtitle/hdmv_text_subtitle",    AV_CODEC_ID_HDMV_TEXT_SUBTITLE },
};

static const size_t kNumEntries = sizeof(kKeyMap) / sizeof(kKeyMap[0]);

const char *CTC_convertCodecIdToMimeType(AVCodecContext *codec)
{
    CHECK(codec);

    for (size_t i = 0; i < kNumEntries; ++i) {
        if (kKeyMap[i].key == codec->codec_id) {
            return kKeyMap[i].mime;
        }
    }
    ALOGW("Unsupported codec id : %u", codec->codec_id);
    return NULL;
}

int32_t CTC_convertMimeTypetoCodecId(const char *mime)
{
    CHECK(mime);

    for (size_t i = 0; i < kNumEntries; ++i) {
        if (!strcmp(kKeyMap[i].mime, mime)) {
            return kKeyMap[i].key;
        }
    }
    ALOGW("Unsupported mime : %s", mime);
    return 0;
}

void convertToVPara(AVStream* st, VIDEO_PARA_T *vPara)
{
    vPara->pid = st->id;
    vPara->vFmt = aml::convertToVFormat(st->codecpar->codec_id);
    vPara->nVideoWidth = st->codecpar->width;
    vPara->nVideoHeight = st->codecpar->height;
    vPara->nFrameRate = av_q2d(st->avg_frame_rate);
    vPara->pExtraData = st->codecpar->extradata;
    vPara->nExtraSize = st->codecpar->extradata_size;
}

void convertToAPara(AVStream* st, AUDIO_PARA_T* aPara)
{
    aPara->pid = st->id;
    aPara->aFmt = aml::convertToAFormat(st->codecpar->codec_id);
    aPara->nChannels = st->codecpar->channels;
    aPara->nSampleRate = st->codecpar->sample_rate;
    aPara->pExtraData = st->codecpar->extradata;
    aPara->nExtraSize = st->codecpar->extradata_size;
}

void convertToSPara(AVStream* st, SUBTITLE_PARA_T* sPara, bool isMpegts)
{
    SubtitleType sub_type = aml::convertToSubFormat(st->codecpar->codec_id);
    if (sub_type == SubtitleType::UNKNOWN_SUBTYPE) {
        if (isMpegts && st->codecpar->codec_id == AV_CODEC_ID_NONE && st->codecpar->codec_tag == 0x82) {
            sub_type = SubtitleType::SCTE27;
            ALOGW("streamtype 0x82 map to SCTE27");
        }
    }

    sPara->sub_type = sub_type;
    setSubtitleId(sPara, sub_type, st->id);
    switch (sub_type) {
    case CC:
        break;

    case SCTE27:
        break;

    case DVB_SUBTITLE:
    {
        uint8_t *extradata = st->codecpar->extradata;
        if (extradata && (st->codecpar->extradata_size >= 4)) {
            sPara->dvb_sub_param.Composition_Page = (extradata[0] << 8) | (extradata[1]);
            sPara->dvb_sub_param.Ancillary_Page = (extradata[2] << 8) | (extradata[3]);
        }
    }
    break;

    case TELETEXT:
        break;

    default:
        break;
    }
}

}

///////////////////////////////////////////////////////////////////////////////
