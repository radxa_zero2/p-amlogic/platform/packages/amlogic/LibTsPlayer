#define LOG_NDEBUG 0
#define LOG_TAG "ITsPlayer"

#include "ITsPlayer.h"
#ifndef __ANDROID_VNDK__
#include "LegacyITsPlayerWrapper.h"
#include <CTsPlayer.h>
#endif
#include "CTC_Utils.h"
#include "CTC_Log.h"
#include "CTsPlayerImpl.h"
#include "CTsHwOmxPlayer/CTsHwOmxPlayer.h"
#include "CTC_Version.h"
#include <mutex>
#include <media/stagefright/foundation/ADebug.h>

namespace aml {

using namespace android;

///////////////////////////////////////////////////////////////////////////////
std::string ctcVersion()
{
    std::string result;
    char buf[256];

    snprintf(buf, sizeof(buf), "====================================================================\n");
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "version: %s\n", CTC_GIT);
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "changeId: %s\n", CTC_CHANGE_ID);
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "workdir_status: %s\n", CTC_WORKDIR_STATUS);
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "build_date: %s\n", CTC_BUILD_DATE);
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "user: %s\n", CTC_BUILD_USER);
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "android version: %d\n", ANDROID_PLATFORM_SDK_VERSION);
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "arch: %s, %s\n",
#ifdef __arm__
            "ARM"
#elif __aarch64__
            "ARM64"
#else
            "UNKNOWN"
#endif
,
#ifndef __ANDROID_VNDK__
            "platform"
#else
            "vendor"
#endif
        );
    result.append(buf);
    ALOGE("%s", buf);

    snprintf(buf, sizeof(buf), "====================================================================\n");
    result.append(buf);
    ALOGE("%s", buf);

    return result;
}

ITsPlayer::ITsPlayer(const CTC_InitialParameter* initialParam)
: mUserId(-1)
, mPlayerId(kInvalidId)
, mStreamType(PLAYER_STREAMTYPE_NULL)
{
    mPlayerId = ITsPlayerFactory::instance().registerITsPlayer(this);

    if (initialParam) {
        mUserId = initialParam->userId;
        mStreamType = initialParam->isEsSource ? PLAYER_STREAMTYPE_ES: PLAYER_STREAMTYPE_TS;
    }
}

ITsPlayer::~ITsPlayer()
{
    ITsPlayerFactory::instance().unregisterITsPlayer(mPlayerId);

    for (size_t i = 0; i < PLAYER_STREAMTYPE_MAX; ++i) {
        CTCWriteBuffer* b = &mWriteBuffers[i];
        b->reset();
    }
}

int ITsPlayer::GetWriteBuffer(PLAYER_STREAMTYPE_E type, uint8_t** pBuffer, uint32_t* nSize)
{
    if (pBuffer) *pBuffer = nullptr;
    //if (nSize) *nSize = 0;

    AVBUF_STATUS bufStatus;
    int ret = GetBufferStatus(&bufStatus);
    if (ret != OK) {
        ALOGW("%s, GetBufferStatus failed!", __FUNCTION__);
        return ret;
    }

    int level = -1;
    switch (type) {
    case PLAYER_STREAMTYPE_AUDIO:
        if (bufStatus.abuf_size != 0) {
            level = bufStatus.abuf_data_len * 100 / bufStatus.abuf_size;
        } else {
            ALOGD("audio total buffer size is zero!");
        }
        break;

    case PLAYER_STREAMTYPE_VIDEO:
    case PLAYER_STREAMTYPE_TS:
        if (bufStatus.vbuf_size != 0) {
            level = bufStatus.vbuf_data_len * 100 / bufStatus.vbuf_size;
        } else {
            ALOGD("video total buffer size is zero!");
        }
        break;

    default:
        level = 0;
        break;
    }

    if (level < 0) {
        return INVALID_OPERATION;
    } else if (level >= 100) {
        return -EAGAIN;
    }

    CTCWriteBuffer* b = &mWriteBuffers[type];

    if (nSize && *nSize > b->mWriteBufferSize) {
        b->reset();
        b->mWriteBufferSize = (((*nSize) >> 20) + 1) << 20;
        b->mWriteBuffer = (uint8_t*)calloc(b->mWriteBufferSize, 1);
    }

    if (pBuffer) *pBuffer = b->mWriteBuffer;
    if (nSize) *nSize = b->mWriteBufferSize;

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
ITsPlayerFactory::ITsPlayerFactory()
: mPlayers{nullptr}
, mPlayerNum(0)
, mCTsPlayerId(ITsPlayer::kInvalidId) 
{
    ctcVersion();
}

ITsPlayerFactory::~ITsPlayerFactory()
{

}

ITsPlayerFactory* ITsPlayerFactory::sITsPlayerFactory = nullptr;
static std::once_flag g_OnceFlag;

ITsPlayerFactory& ITsPlayerFactory::instance()
{
    if (sITsPlayerFactory == nullptr) {
        std::call_once(g_OnceFlag, [] {
            sITsPlayerFactory = new ITsPlayerFactory();
        });
    }

    return *sITsPlayerFactory;
}


ITsPlayer* ITsPlayerFactory::createITsPlayer(CTC_InitialParameter* initialParam)
{
    if (mPlayerNum >= ITSPLAYER_INSTANCE_MAX) {
        ALOGE("ITsPlayer exceed max instances count: %d", ITSPLAYER_INSTANCE_MAX);
        return nullptr;
    }

    CTC_InitialParameter defaultParam;
    //memset(&defaultParam, 0, sizeof(CTC_InitialParameter));
    CTC_InitialParameter* param = initialParam != nullptr ? initialParam : &defaultParam;

    int prop_omxDebug = CTC_getConfig("media.ctcplayer.omxdebug", 0);
    if (prop_omxDebug) {
        ALOGI("prop_omxDebug is true!");
        param->useOmx = prop_omxDebug;
    } 

    int legacyMode = param->flags & CTC_LEGACY_MODE;
    int prop_legacyMode = CTC_getConfig("media.ctcplayer.legacy-mode", -1);
    if (prop_legacyMode >= 0) {
        legacyMode = prop_legacyMode;
    }

    ALOGI("createITsPlayer begin! userId:%d, omxdebug:%d, legacy mode:%d", param->userId, param->useOmx, legacyMode);

    if (!param->useOmx && isCTsPlayerExist()) {
        int64_t nowUs = ::android::ALooper::GetNowUs();
        int64_t heuristicCheckTime = CTC_getConfig("media.itsplayer.checktime", 800) * 1000ll;
        if (mPlayerNum >= 2 || nowUs - mCTsPlayerCreateTime <= heuristicCheckTime) {
            ALOGI("ctsplayer exist, use omx! mPlayerNum:%d, heuristicCheckTime: %dms", mPlayerNum.load(), (int)heuristicCheckTime/1000);
            param->useOmx = true;
        } else if (param->userId >= 0 && mCTsUserId >= 0 && param->userId != mCTsUserId) {
            ALOGI("pip mode switch channel, use omx!");
            param->useOmx = true;
        }
    }

    ITsPlayer* player = nullptr;
    if (param->useOmx || !awaitCTsPlayerExit()) {
        ALOGI("new CTsHwOmxPlayer!");

        player = new CTsHwOmxPlayer(param);
    } else {
        ALOGI("new CTsPlayer!");
#ifndef __ANDROID_VNDK__
        if (!legacyMode) {
            player = new CTsPlayer(param);
        } else {
#if ANDROID_PLATFORM_SDK_VERSION == 28
            sp<::ITsPlayer> legacyITsPlayer = new ::CTsPlayer();
            player = new LegacyITsPlayerWrapper(legacyITsPlayer);
#else
            player = new CTsPlayer(param);
#endif
        }
#endif
    }

    return player;
}

ITsPlayerFactory::player_id ITsPlayerFactory::registerITsPlayer(ITsPlayer* player)
{
    if (player == nullptr) {
        return ITsPlayer::kInvalidId;
    }

    std::lock_guard<std::mutex> _l(mLock);
    for (size_t i = 0; i < ITSPLAYER_INSTANCE_MAX; ++i) {
        if (mPlayers[i] == nullptr) {
            mPlayers[i] = player;
            (void)++mPlayerNum;

            ALOGI("register player id:%d(%p)", i, player);
            return static_cast<player_id>(i);
        }
    }

    return ITsPlayer::kInvalidId;
}

void ITsPlayerFactory::unregisterITsPlayer(player_id id)
{
    std::lock_guard<std::mutex> _l(mLock);

    CHECK(mPlayers[id]);
    mPlayers[id] = nullptr;
    (void)--mPlayerNum;
    if (id == mCTsPlayerId) {
        ALOGI("CTsPlayer(id:%d) released!", id);
        mCTsUserId = -1;
        mCTsPlayerId = ITsPlayer::kInvalidId;
        mCTsPlayerCreateTime = 0;
        mCond.notify_all();
    } else {
        ALOGI("CTsHwOmxPlayer(id:%d) released!", id);
    }
}

void ITsPlayerFactory::signalCTsPlayerId(int userId, player_id id)
{
    std::lock_guard<std::mutex> _l(mLock);

    mCTsUserId = userId;
    mCTsPlayerId = id;
    mCTsPlayerCreateTime = ::android::ALooper::GetNowUs();

    ALOGI("CTsPlayer id is %d", id);
}

bool ITsPlayerFactory::isCTsPlayerExist() const
{
    std::lock_guard<std::mutex> _l(mLock);
    return mCTsPlayerId != ITsPlayer::kInvalidId;    
}

bool ITsPlayerFactory::awaitCTsPlayerExit()
{
    bool ret = true;

    std::unique_lock<std::mutex> lock(mLock);
    if (mCTsPlayerId != ITsPlayer::kInvalidId) {
        int timeout = 3000;  //ms
        ALOGW("waiting for CTsPlayer to exit...");
        mCond.wait_for(lock, std::chrono::milliseconds(timeout));

        ret = mCTsPlayerId == ITsPlayer::kInvalidId;
        ALOGW("waiting for CTsPlayer to exit %s!", ret ? "success" : "timeout");
    }

    return ret;
}

}
