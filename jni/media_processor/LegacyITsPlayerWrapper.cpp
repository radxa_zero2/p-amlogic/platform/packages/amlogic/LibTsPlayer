#define LOG_NDEBUG 0
#define LOG_TAG "LegacyITsPlayerWrapper"

#include "LegacyITsPlayerWrapper.h"

namespace aml {
LegacyITsPlayerWrapper::LegacyITsPlayerWrapper(const android::sp<::ITsPlayer>& legacyITsPlayer)
: mLegacyITsPlayer(legacyITsPlayer)
{

}

LegacyITsPlayerWrapper::~LegacyITsPlayerWrapper()
{

}

int LegacyITsPlayerWrapper::GetPlayMode()
{
    return mLegacyITsPlayer->GetPlayMode();
}

int LegacyITsPlayerWrapper::SetVideoWindow(int x, int y, int width, int height)
{
    return mLegacyITsPlayer->SetVideoWindow(x, y, width, height);
}

int LegacyITsPlayerWrapper::VideoShow()
{
    return mLegacyITsPlayer->VideoShow();
}

int LegacyITsPlayerWrapper::VideoHide()
{
    return mLegacyITsPlayer->VideoHide();
}

int LegacyITsPlayerWrapper::EnableAudioOutput()
{
    return mLegacyITsPlayer->EnableAudioOutput();
}

int LegacyITsPlayerWrapper::DisableAudioOutput()
{
    return mLegacyITsPlayer->DisableAudioOutput();
}

void LegacyITsPlayerWrapper::InitVideo(PVIDEO_PARA_T pVideoPara)
{
    ::VIDEO_PARA_T para;
    memset(&para, 0, sizeof(para));
    para.pid = pVideoPara->pid;
    para.nVideoWidth = pVideoPara->nVideoWidth;
    para.nVideoHeight = pVideoPara->nVideoHeight;
    para.vFmt = pVideoPara->vFmt;
    para.nExtraSize = pVideoPara->nExtraSize;
    para.pExtraData = pVideoPara->pExtraData;

    mLegacyITsPlayer->InitVideo(&para);
}

void LegacyITsPlayerWrapper::InitAudio(PAUDIO_PARA_T pAudioPara)
{
    ::AUDIO_PARA_T para;
    memset(&para, 0, sizeof(para));
    para.pid = pAudioPara->pid;
    para.aFmt = pAudioPara->aFmt;
    para.nChannels = pAudioPara->nChannels;
    para.nSampleRate = pAudioPara->nSampleRate;
    para.nExtraSize = pAudioPara->nExtraSize;
    para.pExtraData = pAudioPara->pExtraData;

    mLegacyITsPlayer->InitAudio(&para);
}

void LegacyITsPlayerWrapper::InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara)
{
    ::SUBTITLE_PARA_T para;
    memset(&para, 0, sizeof(para));

    switch (pSubtitlePara->sub_type) {
    case aml::SubtitleType::CC:
        para.sub_type = ::CODEC_ID_CLOSEDCAPTION;
        break;

    case aml::SubtitleType::DVB_SUBTITLE:
        para.sub_type = ::CODEC_ID_DVB_SUBTITLE;
        para.pid = pSubtitlePara->dvb_sub_param.Sub_PID;
        break;

    case aml::SubtitleType::SCTE27:
        para.sub_type = ::CODEC_ID_SCTE27;
        para.pid = pSubtitlePara->scte27_param.SCTE27_PID;
        break;

    case aml::SubtitleType::TELETEXT:
        para.sub_type = ::CODEC_ID_DVB_TELETEXT;
        para.pid = pSubtitlePara->tt_param.Teletext_PID;
        break;

    default:
        break;
    }

    mLegacyITsPlayer->InitSubtitle(&para);
}

int LegacyITsPlayerWrapper::InitCAS(DVB_CAS_INIT_PARA* pCASInitPara)
{
    return 0;
}

int LegacyITsPlayerWrapper::initSyncSource(CTC_SyncSource syncSource)
{
    return 0;
}

bool LegacyITsPlayerWrapper::StartPlay()
{
    return mLegacyITsPlayer->StartPlay();
}

bool LegacyITsPlayerWrapper::Pause()
{
    return mLegacyITsPlayer->Pause();
}

bool LegacyITsPlayerWrapper::Resume()
{
    return mLegacyITsPlayer->Resume();
}

bool LegacyITsPlayerWrapper::Fast()
{
    return mLegacyITsPlayer->Fast();
}

bool LegacyITsPlayerWrapper::StopFast()
{
    return mLegacyITsPlayer->StopFast();
}

bool LegacyITsPlayerWrapper::Stop()
{
    return mLegacyITsPlayer->Stop();
}

bool LegacyITsPlayerWrapper::Seek()
{
    return mLegacyITsPlayer->Seek();
}

bool LegacyITsPlayerWrapper::SetVolume(int volume)
{
    return mLegacyITsPlayer->SetVolume(volume);
}

int LegacyITsPlayerWrapper::GetVolume()
{
    return mLegacyITsPlayer->GetVolume();
}

bool LegacyITsPlayerWrapper::SetRatio(int nRatio)
{
    return mLegacyITsPlayer->SetRatio(nRatio);
}

int LegacyITsPlayerWrapper::GetAudioBalance()
{
    return mLegacyITsPlayer->GetAudioBalance();
}

bool LegacyITsPlayerWrapper::SetAudioBalance(int nAudioBalance)
{
    return mLegacyITsPlayer->SetAudioBalance(nAudioBalance);
}

void LegacyITsPlayerWrapper::GetVideoPixels(int& width, int& height)
{
    mLegacyITsPlayer->GetVideoPixels(width, height);
}

bool LegacyITsPlayerWrapper::IsSoftFit()
{
    return mLegacyITsPlayer->IsSoftFit();
}

void LegacyITsPlayerWrapper::SetEPGSize(int w, int h)
{
    mLegacyITsPlayer->SetEPGSize(w, h);
}

void LegacyITsPlayerWrapper::SetSurface(::android::Surface* pSurface)
{
    ALOGI("setSurface:%p", pSurface);
    mLegacyITsPlayer->SetSurface(pSurface);
}

void LegacyITsPlayerWrapper::SetSurface_ANativeWindow(ANativeWindow* pSurface)
{
    return SetSurface(static_cast<::android::Surface*>(pSurface));
}

int LegacyITsPlayerWrapper::WriteData(uint8_t* pBuffer, uint32_t nSize)
{
    return mLegacyITsPlayer->WriteData(pBuffer, nSize);
}

int LegacyITsPlayerWrapper::WriteData(PLAYER_STREAMTYPE_E type, uint8_t* pBuffer, uint32_t nSize, int64_t timestamp)
{
    ::PLAYER_STREAMTYPE_E streamType = toLegacyStreamType(type);

    return mLegacyITsPlayer->WriteData(streamType, pBuffer, nSize, timestamp);
}

void LegacyITsPlayerWrapper::SwitchVideoTrack(PVIDEO_PARA_T pVideoPara)
{

}

void LegacyITsPlayerWrapper::SwitchAudioTrack(int pid, PAUDIO_PARA_T pAudioPara)
{
    mLegacyITsPlayer->SwitchAudioTrack(pid);
}

void LegacyITsPlayerWrapper::SwitchSubtitle(int pid, PSUBTITLE_PARA_T pSubtitlePara)
{
    mLegacyITsPlayer->SwitchSubtitle(pid);
}

bool LegacyITsPlayerWrapper::SubtitleShowHide(bool bShow)
{
    return mLegacyITsPlayer->SubtitleShowHide(bShow);
}

int64_t LegacyITsPlayerWrapper::GetCurrentPts(PLAYER_STREAMTYPE_E type)
{
    unsigned long long pts = kUnknownPTS;

    if (type == PLAYER_STREAMTYPE_VIDEO) {
        mLegacyITsPlayer->GetCurrentVidPTS(&pts);
    }

    return pts;
}

int64_t LegacyITsPlayerWrapper::GetCurrentPlayTime()
{
    return mLegacyITsPlayer->GetCurrentPlayTime();
}

void LegacyITsPlayerWrapper::SetStopMode(bool bHoldLastPic)
{
    if (!bHoldLastPic) {
        mLegacyITsPlayer->ClearLastFrame();
    }
}

int LegacyITsPlayerWrapper::GetBufferStatus(PAVBUF_STATUS pstatus)
{
    memset(pstatus, 0, sizeof(*pstatus));

    return 0;
}

int LegacyITsPlayerWrapper::GetStreamInfo(PVIDEO_INFO_T pVideoInfo, PAUDIO_INFO_T pAudioInfo, PSUBTITLE_INFO_T pSubtitleInfo)
{
    return 0;
}

int LegacyITsPlayerWrapper::SetParameter(void* handler, int key, void* request)
{
    return 0;
}

int LegacyITsPlayerWrapper::GetParameter(void* handler, int key, void* reply)
{
    return 0;
}

void LegacyITsPlayerWrapper::ctc_register_evt_cb(CTC_PLAYER_EVT_CB ctc_func_player_evt, void* hander)
{

}

AML_HANDLE LegacyITsPlayerWrapper::DMX_CreateChannel(int pid)
{
    return nullptr;
}

int LegacyITsPlayerWrapper::DMX_DestroyChannel(AML_HANDLE channel)
{
    return 0;
}

int LegacyITsPlayerWrapper::DMX_OpenChannel(AML_HANDLE channel)
{
    return 0;
}

int LegacyITsPlayerWrapper::DMX_CloseChannel(AML_HANDLE channel)
{
    return 0;
}

AML_HANDLE LegacyITsPlayerWrapper::DMX_CreateFilter(SECTION_FILTER_CB cb, void* pUserData, int id)
{
    return nullptr;
}

int LegacyITsPlayerWrapper::DMX_DestroyFilter(AML_HANDLE filter)
{
    return 0;
}

int LegacyITsPlayerWrapper::DMX_AttachFilter(AML_HANDLE filter, AML_HANDLE channel)
{
    return 0;
}

int LegacyITsPlayerWrapper::DMX_DetachFilter(AML_HANDLE filter, AML_HANDLE channel)
{
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
::PLAYER_STREAMTYPE_E LegacyITsPlayerWrapper::toLegacyStreamType(aml::PLAYER_STREAMTYPE_E type)
{
    ::PLAYER_STREAMTYPE_E streamType;

    switch (type) {
    case aml::PLAYER_STREAMTYPE_TS:
        streamType = ::PLAYER_STREAMTYPE_TS;
        break;

    case aml::PLAYER_STREAMTYPE_AUDIO:
        streamType = ::PLAYER_STREAMTYPE_AUDIO;
        break;

    case aml::PLAYER_STREAMTYPE_VIDEO:
        streamType = ::PLAYER_STREAMTYPE_VIDEO;
        break;

    case aml::PLAYER_STREAMTYPE_SUBTITLE:
        streamType = ::PLAYER_STREAMTYPE_SUBTITLE;
        break;

    case aml::PLAYER_STREAMTYPE_NULL:
    default:
        streamType = ::PLAYER_STREAMTYPE_NULL;
        break;
    }

    return streamType;
}

}
