/*
 * author: wei.liu@amlogic.com
 * date: 2012-07-12
 * wrap original source code for CTC usage
 */

#include "CTC_MediaControl.h"
//#include "am_dsc.h"
//#include "CTsOmxPlayer.h"
#include <cutils/properties.h>

using android::sp;

// need single instance?
sp<ITsPlayer> GetMediaControl(int use_omx_decoder)
{
    //return new CTC_MediaControl();
    char value[PROPERTY_VALUE_MAX] = {0};
    property_get("iptv.decoder.omx", value, "0");
    int prop_use_omxdecoder = atoi(value);

    sp<ITsPlayer> ret;
    switch (use_omx_decoder) {
        case 0: {
            ALOGI("Enter CTsPlayer\n");
#if ANDROID_PLATFORM_SDK_VERSION == 28
            ret = new CTsPlayer();
#endif
            break;
        }
        case 1: /*{
            ALOGI("Enter CTsOmxPlayer\n");
            ret = new CTsOmxPlayer();
            break;
        }*/
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8: {
            ALOGI("Enter CTsHwOmxPlayer\n");
            //ret = new CTsHwOmxPlayer();
            break;
        }
        default: {
#if ANDROID_PLATFORM_SDK_VERSION == 28
            ret = new CTsPlayer();
#endif
        }
    }

    return ret;

#if 0
    if (prop_use_omxdecoder || use_omx_decoder)
        ;//return new CTsHwOmxPlayer();
    else
        return new CTsPlayer();
#endif
}
CTC_MediaControl* GetMediaControlImpl()
{
    //if (NULL == mCTC_MediaControl)
        //mCTC_MediaControl = new CTC_MediaControl();
    //return mCTC_MediaControl;
    return NULL;
}

int Get_MediaControlVersion()
{
    return 1;
}
