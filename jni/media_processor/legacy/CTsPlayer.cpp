#include "am_dsc.h"
#include "CTsPlayer.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/system_properties.h>
#include <android/native_window.h>
#include <cutils/properties.h>
#include <fcntl.h>
//#include "player_set_sys.h"
#include "Amsysfsutils.h"
#include <sys/times.h>
#include <time.h>
/*xuqian 20140905 合入760D 代码*/
#include <sys/ioctl.h>
/*end add*/
#include <media/AudioSystem.h>
#include <system/audio.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>
#include <media/IMediaPlayerService.h>
#include <media/stagefright/foundation/ALooper.h>

// #include "subtitleservice.h"
#include <gui/SurfaceComposerClient.h>
#include <gui/ISurfaceComposer.h>
#include <ui/DisplayInfo.h>
#if ANDROID_PLATFORM_SDK_VERSION >= 24
#include <gralloc_usage_ext.h>
#endif

#ifdef  BUILD_WITH_VIEWRIGHT_STB
#include "../AmlDVB/vmx_ca/hw_decrypt_iptv.h"
#endif

#include <gralloc_usage_ext.h>
#include "amlogic/am_gralloc_ext.h"

//for conax
//#include <linux/dvb/frontend.h>
//#include "am_dsc.h"
#include "am_av.h"
//#include "am_fend.h"
#include "am_dmx.h"
//#include "am_types.h"

//#define PORTING_TEST

using namespace android;
using namespace ctc_subtitle;

#define DEC_CONTROL_FLAG_FORCE_2997_1080P_INTERLACE      0x0001
#define DEC_CONTROL_FLAG_FORCE_2500_576P_INTERLACE       0x0002
#define DEC_CONTROL_FLAG_FORCE_2997_720_480_INTERLACE    0x0004
#define DEC_CONTROL_FLAG_FORCE_2500_528_576_INTERLACE    0x0008
#define DEC_CONTROL_FLAG_FORCE_2500_704_576_INTERLACE    0x0010
#define DEC_CONTROL_FLAG_FORCE_2500_480_576_INTERLACE    0x0020
#define DEC_CONTROL_FLAG_FORCE_3203_1280_1080_INTERLACE  0x0040
#define DEC_CONTROL_FLAG_FORCE_3840_1080P_INTERLACE      0x0080


#define M_LIVE	1
#define M_TVOD	2
#define M_VOD	3
#define RES_VIDEO_SIZE 256
#define RES_AUDIO_SIZE 64
#define MAX_WRITE_COUNT 20

#define MAX_WRITE_ALEVEL 0.97
#define MAX_WRITE_VLEVEL 0.99 

#define UNDERFLOW_ALEVEL 0.0002
#define UNDERFLOW_VLEVEL 0.0002

#define READ_SIZE (64 * 1024)
#define CTC_BUFFER_LOOP_NSIZE 1316

#define CTC_SUCESS 0
#define CTC_FAILURE -1


static bool m_StopThread = false;

//reset while audio device state change
static int last_audio_state = 0;
int prop_audiohal_ctc_reset = 0;

//Force to DI.
static int dec_control =  DEC_CONTROL_FLAG_FORCE_2997_1080P_INTERLACE	|
			   DEC_CONTROL_FLAG_FORCE_2500_576P_INTERLACE			|
			   DEC_CONTROL_FLAG_FORCE_2997_720_480_INTERLACE		|
			   DEC_CONTROL_FLAG_FORCE_2500_528_576_INTERLACE		|
			   DEC_CONTROL_FLAG_FORCE_2500_704_576_INTERLACE		|
			   DEC_CONTROL_FLAG_FORCE_2500_480_576_INTERLACE		|
			   DEC_CONTROL_FLAG_FORCE_3203_1280_1080_INTERLACE		|
			   DEC_CONTROL_FLAG_FORCE_3840_1080P_INTERLACE;

//property control
extern int prop_shouldshowlog;
static int prop_playerwatchdog_support =0;
int prop_dumpfile = 0;
int prop_buffertime = 0;
int prop_singlemode = 0;

int prop_softfit = 0;
bool m_isSurfaceWindow = false;
int prop_blackout_policy = 1; 
float prop_audiobuflevel = 0.0;
float prop_videobuflevel = 0.0;
int prop_audiobuftime = 1000;
int prop_videobuftime = 1000;
int prop_show_first_frame_nosync = 0;

int vmx_write_size_per_sec = 0;

#ifdef  BUILD_WITH_VIEWRIGHT_STB
int prop_media_vmx_iptv =0;
int abuf_data_len = 0;
int abuf_size = 0;
int vbuf_data_len = 0;
int vbuf_size = 0;
int abuf_free_len = 0;
int vbuf_free_len = 0;
#endif

int prop_media_contax_iptv =0;
//

int hasaudio = 0;
int hasvideo = 0;
int hasccsub = 0;
int hasSctesub = 0;
int subOpened = 0;
int ccsubstatus = 1;
int audio_disable = 0;
int video_disable = 0;
int video_hide = 0;
int audio_hide = 0;
int startmode = 1;

int DisableVideoCount = 0;
int DisableAudioCount = 0;
int FrozenVideoNum = 0;
int ForzenAudioNum = 0;
int CurVideoNum = 0;
int CurAudioNum = 0;

int keep_vdec_mem = 0;

int prop_async_stop = 0;//1;
int async_flag = 0;
int m_AStopPending = 0;
int clearlastframe_flag = 0;

int checkcount = 0;
int buffersize = 0;
char old_free_scale_axis[64] = {0};
char old_window_axis[64] = {0};
char old_free_scale[64] = {0};
int s_video_axis[4] = {0};
static LPBUFFER_T lpbuffer_st;
static int H264_error_skip_normal = 0;
static int H264_error_skip_ff = 0;
static int H264_error_skip_reserve = 0;

SUBTITLELIS sub_handle_ctc = NULL;


static FILE* dumpafd = NULL;
static FILE* dumpvfd = NULL;


int numAudioDecodedSamples = 0;
int numPreAudioDecodedSamples = 0;
int audioDecoderIsRun = 0;

int numEncryptAudioDecodedSamples = 0;
int numPreEncryptAudioDecodedSamples = 0;
int encryptAudioDecoderIsRun = 0;

int numVideoDecodedFrames = 0;
int numPreVideoDecodedFrames = 0;
int videoDecoderIsRun = 0;

int checkVideoStateCount = 0;
int checkAudioStateCount = 0;
int checkEncryptAudioStateCount = 0;

#define WAIT_EVENT(value) \
do { \
        LOGE("[%s:%d] WAIT_EVENT in \n",__FUNCTION__, __LINE__); \
        lp_lock(&mutex); \
        while (value) { \
            pthread_cond_wait(&m_pthread_cond, &mutex); \
        } \
        lp_unlock(&mutex); \
        LOGE("[%s:%d] WAIT_EVENT out \n",__FUNCTION__, __LINE__); \
} while(0)


#define CTC_EXIST "/sys/module/amvideo/parameters/ctsplayer_exist"
#define VIDEO 1
#define AUDIO 2

#define LOGV(...) \
    do { \
        if (prop_shouldshowlog == 2) { \
            __android_log_print(ANDROID_LOG_VERBOSE, "TsPlayer", __VA_ARGS__); \
        } \
    } while (0)

#define LOGD(...) \
    do { \
        if (prop_shouldshowlog) { \
            __android_log_print(ANDROID_LOG_DEBUG, "TsPlayer", __VA_ARGS__); \
        } \
    } while (0)

#define LOGI(...) \
    do { \
        if (prop_shouldshowlog) { \
            __android_log_print(ANDROID_LOG_INFO, "TsPlayer", __VA_ARGS__); \
        } \
    } while (0)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN  , "TsPlayer", __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , "TsPlayer", __VA_ARGS__)

//Internal interface

#ifdef TELECOM_VFORMAT_SUPPORT
#define CT_VFORMAT_H265 8
#define CT_VFORMAT_SW 9
#define CT_VFORMAT_UNSUPPORT 10
#define CT_AFORMAT_UNKNOWN2 -2
#define CT_AFORMAT_DDPlUS 19
#define CT_AFORMAT_UNSUPPORT 20

vformat_t changeVformat(vformat_t index){
    LOGI("changeVformat, vfromat: %d\n", index);
    if(index == CT_VFORMAT_H265)
        return VFORMAT_HEVC;
    else if(index == CT_VFORMAT_SW)
        return VFORMAT_SW;

    if(index >= CT_VFORMAT_UNSUPPORT)
        return VFORMAT_UNSUPPORT;
    else
        return index;
}

aformat_t changeAformat(aformat_t index){
    LOGI("changeAformat, afromat: %d\n", index);
    if(index == CT_AFORMAT_UNKNOWN2)
        return AFORMAT_UNKNOWN;
    else if(index == CT_AFORMAT_DDPlUS)
        return AFORMAT_EAC3;
 
    if(index >= CT_AFORMAT_UNSUPPORT)
        return AFORMAT_UNSUPPORT;
    else
        return index;
}
#endif

typedef enum {
    OUTPUT_MODE_480I = 0,
    OUTPUT_MODE_480P,
    OUTPUT_MODE_576I,
    OUTPUT_MODE_576P,
    OUTPUT_MODE_720P,
    OUTPUT_MODE_1080I,
    OUTPUT_MODE_1080P,
    OUTPUT_MODE_4K2K24HZ,
    OUTPUT_MODE_4K2K25HZ,
    OUTPUT_MODE_4K2K30HZ,
    OUTPUT_MODE_4K2KSMPTE,
    OUTPUT_MODE_4K2KFAKE_5G,
    OUTPUT_MODE_4K2K60HZ,
    OUTPUT_MODE_4K2K60HZ_Y420,
    OUTPUT_MODE_4K2K50HZ,
    OUTPUT_MODE_4K2K50HZ_Y420,
}OUTPUT_MODE;

OUTPUT_MODE iGetDisplayMode(){
    int fd;
    char mode[16] = {0};
    char *path = "/sys/class/display/mode";
    fd = open(path, O_RDONLY);
    if (fd >= 0) {
        memset(mode, 0, 16); // clean buffer and read 15 byte to avoid strlen > 15	
        int ret = read(fd, mode, 15);
        mode[15] = '\0';
        if (ret > 0) {
            mode[strlen(mode)] = '\0';
        }
        close(fd);
        if(!strncmp(mode, "480i", 4) || !strncmp(mode, "480cvbs", 7)) {
            return OUTPUT_MODE_480I;
        } else if(!strncmp(mode, "480p", 4)) {
            return OUTPUT_MODE_480P;
        } else if(!strncmp(mode, "576i", 4) || !strncmp(mode, "576cvbs", 7)) {
            return OUTPUT_MODE_576I;
        } else if(!strncmp(mode, "576p", 4)) {
            return OUTPUT_MODE_576P;
        } else if(!strncmp(mode, "720p", 4)) {
            return OUTPUT_MODE_720P;
        } else if(!strncmp(mode, "1080i", 5)) {
            return OUTPUT_MODE_1080I;
        } else if(!strncmp(mode, "1080p", 5)) {
            return OUTPUT_MODE_1080P;
        } else if(!strncmp(mode, "4k2k24hz", 8) ||!strncmp(mode, "2160p24hz", 9)) {
            return OUTPUT_MODE_4K2K24HZ;
        } else if(!strncmp(mode, "4k2k25hz", 8) ||!strncmp(mode, "2160p25hz", 9)) {
            return OUTPUT_MODE_4K2K25HZ;
        } else if(!strncmp(mode, "4k2k30hz", 8)||!strncmp(mode, "2160p30hz", 9)) {
            return OUTPUT_MODE_4K2K30HZ;
        } else if(!strncmp(mode, "4k2ksmpte", 8)) {
            return OUTPUT_MODE_4K2KSMPTE;
        }else if(!strncmp(mode, "2160p60hz", 9)) {
            return OUTPUT_MODE_4K2K60HZ;
        }else if(!strncmp(mode, "2160p50hz", 9)) {
	    return OUTPUT_MODE_4K2K50HZ;
	 }   

    } else {
        LOGE("iGetDisplayMode open file %s error\n", path);
    }
    return OUTPUT_MODE_720P;
}

void iGetPosition(OUTPUT_MODE output_mode, int *x, int *y, int *width, int *height){
    char vaxis_newx_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_newy_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_width_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_height_str[PROPERTY_VALUE_MAX] = {0};

    switch(output_mode) {
    case OUTPUT_MODE_480I:
        property_get("ubootenv.var.480i_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.480i_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.480i_w", vaxis_width_str, "720");
        property_get("ubootenv.var.480i_h", vaxis_height_str, "480");
        break;
    case OUTPUT_MODE_480P:
        property_get("ubootenv.var.480p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.480p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.480p_w", vaxis_width_str, "720");
        property_get("ubootenv.var.480p_h", vaxis_height_str, "480");
        break;
    case OUTPUT_MODE_576I:
        property_get("ubootenv.var.576i_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.576i_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.576i_w", vaxis_width_str, "720");
        property_get("ubootenv.var.576i_h", vaxis_height_str, "576");
        break;
    case OUTPUT_MODE_576P:
        property_get("ubootenv.var.576p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.576p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.576p_w", vaxis_width_str, "720");
        property_get("ubootenv.var.576p_h", vaxis_height_str, "576");
        break;
    case OUTPUT_MODE_720P:
        property_get("ubootenv.var.720p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.720p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.720p_w", vaxis_width_str, "1280");
        property_get("ubootenv.var.720p_h", vaxis_height_str, "720");
        break;
    case OUTPUT_MODE_1080I:
        property_get("ubootenv.var.1080i_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.1080i_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.1080i_w", vaxis_width_str, "1920");
        property_get("ubootenv.var.1080i_h", vaxis_height_str, "1080");
        break;
    case OUTPUT_MODE_1080P:
        property_get("ubootenv.var.1080p_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.1080p_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.1080p_w", vaxis_width_str, "1920");
        property_get("ubootenv.var.1080p_h", vaxis_height_str, "1080");
        break;
    case OUTPUT_MODE_4K2K24HZ:
        property_get("ubootenv.var.4k2k24hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k24hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k24hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k24hz_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2K25HZ:
        property_get("ubootenv.var.4k2k25hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k25hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k25hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k25hz_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2K30HZ:
        property_get("ubootenv.var.4k2k30hz_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2k30hz_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2k30hz_w", vaxis_width_str, "3840");
        property_get("ubootenv.var.4k2k30hz_h", vaxis_height_str, "2160");
        break;
    case OUTPUT_MODE_4K2KSMPTE:
        property_get("ubootenv.var.4k2ksmpte_x", vaxis_newx_str, "0");
        property_get("ubootenv.var.4k2ksmpte_y", vaxis_newy_str, "0");
        property_get("ubootenv.var.4k2ksmpte_w", vaxis_width_str, "4096");
        property_get("ubootenv.var.4k2ksmpte_h", vaxis_height_str, "2160");
        break;
	case OUTPUT_MODE_4K2K60HZ:
		property_get("ubootenv.var.4k2k60hz_x", vaxis_newx_str, "0");
		property_get("ubootenv.var.4k2k60hz_y", vaxis_newy_str, "0");
		property_get("ubootenv.var.4k2k60hz_w", vaxis_width_str, "3840");
		property_get("ubootenv.var.4k2k60hz_h", vaxis_height_str, "2160");
	break;
	case OUTPUT_MODE_4K2K50HZ:
		property_get("ubootenv.var.4k2k50hz_x", vaxis_newx_str, "0");
		property_get("ubootenv.var.4k2k50hz_y", vaxis_newy_str, "0");
		property_get("ubootenv.var.4k2k50hz_w", vaxis_width_str, "3840");
		property_get("ubootenv.var.4k2k50hz_h", vaxis_height_str, "2160");
	break;

    default:
    	  *x = 0;
    	  *y = 0;
    	  *width = 1280;
    	  *height = 720;
        LOGW("UNKNOW MODE:%d", output_mode);
        return;
    }
    *x = atoi(vaxis_newx_str);
    *y = atoi(vaxis_newy_str);
    *width = atoi(vaxis_width_str);
    *height = atoi(vaxis_height_str);
	LOGW("x:%d ,y:%d ,width:%d ,height:%d ", *x,*y,*width,*height);
}

void InitOsdScale(int width, int height){
    LOGI("InitOsdScale, width: %d, height: %d\n", width, height);
    int x = 0, y = 0, w = 0, h = 0;
    char fsa_bcmd[64] = {0};
    char wa_bcmd[64] = {0};
    
    sprintf(fsa_bcmd, "0 0 %d %d", width-1, height-1);
    OUTPUT_MODE output_mode = iGetDisplayMode();
    iGetPosition(output_mode, &x, &y, &w, &h);
    sprintf(wa_bcmd, "%d %d %d %d", x, y, x+w-1, y+h-1);
    LOGI("InitOsdScale, free_scale_axis: %s window_axis: %s\n", fsa_bcmd, wa_bcmd);
    
    amsysfs_set_sysfs_int("/sys/class/graphics/fb0/blank", 1);
    amsysfs_set_sysfs_str("/sys/class/graphics/fb0/freescale_mode", "1");
    amsysfs_set_sysfs_str("/sys/class/graphics/fb0/free_scale_axis", fsa_bcmd);
    amsysfs_set_sysfs_str("/sys/class/graphics/fb0/window_axis", wa_bcmd);
    amsysfs_set_sysfs_str("/sys/class/graphics/fb0/free_scale", "0x10001");
    amsysfs_set_sysfs_int("/sys/class/graphics/fb0/blank", 0);
}

void reinitOsdScale(){
    LOGI("reinitOsdScale, old_free_scale_axis: %s old_window_axis: %s old_free_scale: %s\n", 
		old_free_scale_axis, old_window_axis, old_free_scale);
    amsysfs_set_sysfs_int("/sys/class/graphics/fb0/blank", 1);
    amsysfs_set_sysfs_str("/sys/class/graphics/fb0/freescale_mode", "1");
    amsysfs_set_sysfs_str("/sys/class/graphics/fb0/free_scale_axis", old_free_scale_axis);
    amsysfs_set_sysfs_str("/sys/class/graphics/fb0/window_axis", old_window_axis);
    if(!strncmp(old_free_scale, "free_scale_enable:[0x1]", 23)) {
        amsysfs_set_sysfs_str("/sys/class/graphics/fb0/free_scale", "0x10001");
    }
    else {
        amsysfs_set_sysfs_str("/sys/class/graphics/fb0/free_scale", "0x0");
    }
    amsysfs_set_sysfs_int("/sys/class/graphics/fb0/blank", 0);
}

void LunchIptv(bool isSoftFit){
    LOGI("LunchIptv isSoftFit:%d\n", isSoftFit);
    if(!isSoftFit) {
        //amsysfs_set_sysfs_str("/sys/class/graphics/fb0/video_hole", "0 0 1280 720 0 8");
        amsysfs_set_sysfs_str("/sys/class/deinterlace/di0/config", "disable");
        amsysfs_set_sysfs_int("/sys/module/di/parameters/buf_mgr_mode", 0);
    }else {
        amsysfs_set_sysfs_int("/sys/class/graphics/fb0/blank", 0);
    }
}

void QuitIptv(bool isSoftFit, bool isBlackoutPolicy){
    amsysfs_set_sysfs_int("/sys/module/di/parameters/bypass_hd", 0);
    //amsysfs_set_sysfs_str("/sys/class/graphics/fb0/video_hole", "0 0 0 0 0 0");
    if(isBlackoutPolicy)
        amsysfs_set_sysfs_int("/sys/class/video/blackout_policy", 1);
    else
        amsysfs_set_sysfs_int("/sys/class/video/disable_video", 1);
    if(!isSoftFit) {
        reinitOsdScale();
    } else {
        amsysfs_set_sysfs_int("/sys/class/graphics/fb0/blank", 0);
    }
    LOGI("QuitIptv\n");
}

int sysfs_get_long(char *path, unsigned long  *val){
    char buf[64];

    if (amsysfs_get_sysfs_str(path, buf, sizeof(buf)) == -1) {
        LOGI("unable to open file %s,err: %s", path, strerror(errno));
        return -1; 
    }   
    if (sscanf(buf, "0x%lx", val) < 1) {
        LOGI("unable to get pts from: %s", buf);
        return -1; 
    }   
    return 0;
}

int checkExist(const char *path){
    int fd;
    fd = open(path, O_RDONLY);
    if (fd >= 0) {
        close(fd);
        return 0;
    }
    return -1;
}

CTsPlayer::CTsPlayer(){
    char value[PROPERTY_VALUE_MAX] = {0};
	mIsOmxPlayer = false;
	LOGI("structure CTsPlayer mIsOmxPlayer=%d\n", mIsOmxPlayer);
    if (0 != checkExist(CTC_EXIST)) {
        LOGE(">>>>>>>>>>>>>Check!Check!" CTC_EXIST
                " not exist, check code!\n");
    }
    lp_lock_init(&mutex, NULL);

	/*
		Get some play_properties
	*/

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.buffer.time", value, "2300");
    prop_buffertime = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.audio.bufferlevel", value, "0.6");
    prop_audiobuflevel = atof(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.video.bufferlevel", value, "0.8");
    prop_videobuflevel = atof(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.audio.buffertime", value, "1000");
    prop_audiobuftime = atoi(value);
	
    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.video.buffertime", value, "1000");
    prop_videobuftime = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.show_first_frame_nosync", value, "0");
    prop_show_first_frame_nosync = atoi(value);
	
    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.softfit", value, "1");
    prop_softfit = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.blackout.policy",value,"0");
    prop_blackout_policy = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.buffersize", value, "5000");
    buffersize = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.playerwatchdog.support", value, "0");
    prop_playerwatchdog_support = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.h264.error_skip_normal", value, "3");
    H264_error_skip_normal = atoi(value);
	
    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.h264.error_skip_ff", value, "1");
    H264_error_skip_ff = atoi(value);
	
    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.h264.error_skip_reserve", value, "20");
    H264_error_skip_reserve = atoi(value);

	memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.iptv.videohide", value, "0");
    video_hide = atoi(value);

	
    LOGI("structure CTsPlayer, prop_shouldshowlog: %d, prop_buffertime: %d, prop_dumpfile: %d, audio bufferlevel: %f,video bufferlevel: %f, prop_softfit: %d,player_watchdog_support:%d\n",
    prop_shouldshowlog, prop_buffertime, prop_dumpfile, prop_audiobuflevel, prop_videobuflevel, prop_softfit,prop_playerwatchdog_support);
    LOGI("iptv.audio.buffertime = %d, iptv.video.buffertime = %d\n", prop_audiobuftime, prop_videobuftime);



	
    //char buf[64] = {0};
    memset(value, 0, PROPERTY_VALUE_MAX);
    memset(old_free_scale_axis, 0, 64);
    memset(old_window_axis, 0, 64);
    memset(old_free_scale, 0, 64);
    amsysfs_get_sysfs_str("/sys/class/graphics/fb0/free_scale_axis", old_free_scale_axis, 64);
    amsysfs_get_sysfs_str("/sys/class/graphics/fb0/window_axis", value, 64);
    amsysfs_get_sysfs_str("/sys/class/graphics/fb0/free_scale", old_free_scale, 64);
    
    LOGI("structure CTsPlayer window_axis: %s\n", value);
    char *pr = strstr(value, "[");
    if(pr != NULL) {
        int len = strlen(pr);
        int i = 0, j = 0;
        for(i=1; i<len-1; i++) {
            old_window_axis[j++] = pr[i];
        }
        old_window_axis[j] = 0;
    }

    LOGI("structure CTsPlayer free_scale_axis: %s, window_axis: %s, free_scale: %s\n", old_free_scale_axis, old_window_axis, old_free_scale);

    amsysfs_set_sysfs_int("/sys/class/video/blackout_policy", 1);
    if(amsysfs_get_sysfs_int("/sys/class/video/disable_video") == 1)
        amsysfs_set_sysfs_int("/sys/class/video/disable_video", 2);

    memset(&mAudioPara, 0, sizeof(mAudioPara));
    memset(&mVideoPara, 0, sizeof(mVideoPara));
    memset(&vPara, 0, sizeof(vPara));
    memset(&codec, 0, sizeof(codec));
	
    player_pid = -1;
    pcodec = &codec;
    codec_audio_basic_init();
    //0:normal，1:full stretch，2:4-3，3:16-9
    /*解决开机进入iptv播放，视频输出比例与配置不符问题*/
    //amsysfs_set_sysfs_int("/sys/class/video/screen_mode", 1);
    /*add end*/
	/*    //0:normal，1:full stretch，2:4-3，3:16-9
    int screen_mode = 0;
    property_get("ubootenv.var.screenmode",value,"full");
    if(!strcmp(value,"normal"))
         screen_mode = 0;
    else if(!strcmp(value,"full"))
         screen_mode = 1;
    else if(!strcmp(value,"4_3"))
         screen_mode = 2;
    else if(!strcmp(value,"16_9"))
         screen_mode = 3;
    else if(!strcmp(value,"4_3 letter box"))
        screen_mode = 7;
    else if(!strcmp(value,"16_9 letter box"))
        screen_mode = 11;
    else
        screen_mode = 1;


    amsysfs_set_sysfs_int("/sys/class/video/screen_mode", screen_mode);*/
    amsysfs_set_sysfs_int("/sys/class/tsync/enable", 1);

    //set overflow status when decode h264_4k use format h264 .
   //amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/fatal_error_reset", 1);

    v_overflows = 0;
    a_overflows = 0;
    v_underflows = 0;
    a_underflows = 0;

    last_anumdecoderErrors = 0;
    last_anumdecoder = 0;
	last_vnumdecoderErrors = 0;
	last_vnumdecoder = 0;
	
    sub_enable_output = 1;
    m_bIsPlay = false;
    m_bIsPause = false;
    pfunc_player_evt = NULL;
    m_nOsdBpp = 16;//SYS_get_osdbpp();
    m_nAudioBalance = 3;

    m_nVolume = 100;
    m_bFast = false;
	m_bStop = false;
    m_bSetEPGSize = false;
    m_bWrFirstPkg = true;
    m_StartPlayTimePoint = 0;
    m_PreviousOverflowTime = 0;
    m_isSoftFit = (prop_softfit == 1) ? true : false;
    m_isBlackoutPolicy = (prop_blackout_policy == 1) ? true : false;
    m_StopThread = false;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_create(&mThread[0], &attr, threadCheckAbend, this);
    pthread_attr_destroy(&attr);

    pthread_attr_init(&attr);
    pthread_create(&mCheckResetThread, &attr, threadCheckReset, this);
    pthread_attr_destroy(&attr);

	if (prop_async_stop) {
        pthread_create(&mThread[1], NULL, init_thread, this);
    }

    m_nMode = M_LIVE;
    LunchIptv(m_isSoftFit);
    dumpvfd = NULL;
    dumpafd = NULL;
    #if 0
    sp<IBinder> binder =defaultServiceManager()->getService(String16("media.player"));
    sp<IMediaPlayerService> service = interface_cast<IMediaPlayerService>(binder);
    /*if(service.get() != NULL){
	LOGI("CTsPlayer stopPlayerIfNeed \n");
	service->stopPlayerIfNeed();
	LOGI("CTsPlayer stopPlayerIfNeed ==end\n");
    }*/
    #endif
}

CTsPlayer::CTsPlayer(bool omx_player)
: CTsPlayer()
{
    mIsOmxPlayer = omx_player;
    LOGI("structure CTsPlayer omx_player"
            " mIsOmxPlayer=%d\n", mIsOmxPlayer);
}

CTsPlayer:: ~CTsPlayer(){
    LOGI("CTsPlayer del \n");
    if (mIsOmxPlayer) {
        LOGI("guan OmxPlayer return\n");
        return;
    }
    m_StopThread = true;
    LOGI("~CTsPlayer()\n");
    pthread_join(mCheckResetThread, NULL);
    pthread_join(mThread[0], NULL);
    if (prop_async_stop) {
        pthread_join(mThread[1], NULL);
    }

	if (prop_async_stop) {
        WAIT_EVENT(m_AStopPending);
    }

    QuitIptv(m_isSoftFit, m_isBlackoutPolicy);
	lp_lock(&mutex);
	amsysfs_set_sysfs_int(CTC_EXIST, 0);
	lp_unlock(&mutex);
    LOGI("CTsPlayer del end\n");
}

//取得播放模式,保留，暂不用
int CTsPlayer::GetPlayMode(){
    LOGI("GetPlayMode\n");
    return 1;
}

int CTsPlayer::createWindowSurface(int x,int y,int width,int height) {
	int status;

    LOGE("[%s %d] x:%d y:%d width:%d height:%d", __FUNCTION__, __LINE__, x, y, width, height);
/*	
    mSoftComposerClient = new SurfaceComposerClient;
    status = mSoftComposerClient->initCheck();
	LOGE("[%s %d] status%d", __FUNCTION__, __LINE__, status);

    mSoftControl = mSoftComposerClient->createSurface(
                String8("ctc_Surface"),
                width,
                height,
                PIXEL_FORMAT_RGBA_8888,
                0);

    if(mSoftControl != NULL){
        status = mSoftControl->isValid();
		LOGE("[%s %d] status%d", __FUNCTION__, __LINE__, status);
    }
    SurfaceComposerClient::openGlobalTransaction();
    status = mSoftControl->setLayer(INT_MAX-100);
	LOGE("[%s %d] status%d", __FUNCTION__, __LINE__, status);

    sp<IBinder> dpy = mSoftComposerClient->getBuiltInDisplay(0);
    if (dpy == NULL) {
        LOGE("SurfaceComposer::getBuiltInDisplay failed");
        return false;
    }

    DisplayInfo info;
    status_t err = mSoftComposerClient->getDisplayInfo(dpy, &info);
    if (err != NO_ERROR) {
        LOGE("SurfaceComposer::getDisplayInfo failed\n");
        return false;
    }

    float scaleX = 1;//float(info.w) / float(m_nEPGWidth);
    float scaleY = 1;//float(info.h) / float(m_nEPGHeight);

    mSoftControl->setPosition(x * scaleX, y * scaleY);
    err = mSoftControl->setMatrix(scaleX, 0.0f, 0.0f, scaleY);
    if (err != NO_ERROR) {
       LOGE("SurfaceComposer::setMatrix error");
       return false;
    }

    status = mSoftControl->show();
	LOGE("[%s %d] status%d", __FUNCTION__, __LINE__, status);
    SurfaceComposerClient::closeGlobalTransaction();

    mSoftSurface = mSoftControl->getSurface();

	if(mSoftSurface == NULL){
		LOGE("SurfaceComposer mSoftSurface NULL");
		return true;
	}

    sp<IGraphicBufferProducer> mGraphicBufProducer;
    sp<ANativeWindow> mNativeWindow;
    mGraphicBufProducer = mSoftSurface->getIGraphicBufferProducer();
    if(mGraphicBufProducer != NULL) {
        mNativeWindow = new Surface(mGraphicBufProducer);
    } else {
        LOGE("SetSurface, mGraphicBufProducer is NULL!\n");
        return true;
    }
	LOGE("[%s %d]", __FUNCTION__, __LINE__);
    native_window_set_buffer_count(mNativeWindow.get(), 4);
    native_window_set_usage(mNativeWindow.get(), GRALLOC_USAGE_HW_TEXTURE | GRALLOC_USAGE_EXTERNAL_DISP | GRALLOC_USAGE_AML_VIDEO_OVERLAY);
    native_window_set_buffers_format(mNativeWindow.get(), WINDOW_FORMAT_RGBA_8888);
*/
    return true;
}

//for conax

#define DSC_DEV_NO  0
#define FEND_DEV_NO 0
#define DMX_DEV_NO 0
#define AV_DEV_NO 0

AM_DMX_OpenPara_t dmx_para;
//AM_FEND_OpenPara_t fend_para;
AM_DSC_OpenPara_t dsc_para;
AM_AV_OpenPara_t av_para;
AM_AV_InjectPara_t inject_para;
int vch,ach;
int Dsc_dev_open = 0;
int Dsc_start_inject = 0;
int CTsPlayer::DscOpenAndSetSource(){
	int ret = AM_SUCCESS;
	LOGI("[%s %d] ", __FUNCTION__, __LINE__);

    memset(&dsc_para, 0, sizeof(dsc_para));
	memset(&dmx_para, 0, sizeof(dmx_para));
    memset(&av_para, 0, sizeof(av_para));

	if(Dsc_dev_open == 0){
   		ret = AM_AV_Open(AV_DEV_NO, &av_para);
     	if(ret != AM_SUCCESS){
			LOGI("AV dev open failed, ret:%d\n", ret);
			return ret;
		}
	     
		ret = AM_DMX_Open(DMX_DEV_NO, &dmx_para);
    	if(ret != AM_SUCCESS){
			LOGI("DMX dev open failed, ret:%d\n", ret);
			return ret;
		}
    
		ret = AM_DSC_Open(DSC_DEV_NO, &dsc_para);
		if(ret != AM_SUCCESS){
			LOGI("DSC dev open failed, ret:%d\n", ret);
			return ret;
		}
		ret = AM_DMX_SetSource(DMX_DEV_NO, AM_DMX_SRC_HIU);
		if(ret != AM_SUCCESS){
			LOGI("DMX SetSource failed, ret:%d\n", ret);
			return ret;
		}
        
    	ret = AM_AV_SetTSSource(AV_DEV_NO, AM_AV_TS_SRC_DMX0);
    	if(ret != AM_SUCCESS){
			LOGI("AV SetTSSource failed, ret:%d\n", ret);
			return ret;
    	}
	
		ret = AM_DSC_SetSource(DSC_DEV_NO, AM_DSC_SRC_DMX0);
		if(ret != AM_SUCCESS){
			LOGI("DSC SetSource failed, ret:%d\n", ret);
			return ret;
		}

		Dsc_dev_open = 1;
	}

	LOGI("[%s %d]", __FUNCTION__, __LINE__);
	return ret;
}

int CTsPlayer::iAllocateChannel(int dev_no, int *chan_id){
	int ret = AM_SUCCESS;
	LOGI("[%s %d]", __FUNCTION__, __LINE__);
	ret = AM_DSC_AllocateChannel(DSC_DEV_NO, chan_id);
	if(ret != AM_SUCCESS)
		LOGI("DSC AllocateChannel failed\n");
	return ret;
}
int CTsPlayer::iSetChannelPID(int dev_no, int chan_id, unsigned int pid){
	int ret = AM_SUCCESS;
	LOGI("[%s %d]chan_id:%d, pid:%d\n", __FUNCTION__, __LINE__,chan_id,pid);
	ret = AM_DSC_SetChannelPID(DSC_DEV_NO, chan_id, pid);
	if(ret != AM_SUCCESS)
		LOGI("DSC SetChannelPID failed\n");
	return ret;
}

int CTsPlayer::iFreeChannelAndDscClose(){
	int ret = AM_SUCCESS;
	LOGI("[%s %d]", __FUNCTION__, __LINE__);
	ret = AM_DSC_FreeChannel(DSC_DEV_NO, vch);
	if(ret != AM_SUCCESS)
		LOGI("DSC FreeChannel failed, video channel id:%d ret:%d\n",vch, ret);
	ret = AM_DSC_FreeChannel(DSC_DEV_NO, ach);
	if(ret != AM_SUCCESS)
		LOGI("DSC FreeChannel failed, audio channel id:%d ret:%d\n",ach, ret);

	ret = AM_DSC_Close(DSC_DEV_NO);
	if(ret != AM_SUCCESS)
		LOGI("DSC Close failed, ret:%d\n", ret);

    ret = AM_DMX_Close(DMX_DEV_NO);
	LOGI("[%s %d]DMX_Close ret:%d", __FUNCTION__, __LINE__,ret);
    ret = AM_AV_Close(AV_DEV_NO);
	LOGI("[%s %d]AV_Close ret:%d", __FUNCTION__, __LINE__,ret);
	
	if(Dsc_start_inject)
    	ret = AM_AV_StopInject(AV_DEV_NO);
	    LOGI("[%s %d]AV_StopInject ret:%d", __FUNCTION__, __LINE__,ret);

	Dsc_dev_open = 0;
	Dsc_start_inject = 0;
	return  ret;
}

int CTsPlayer::SetKeyforHwDescrambling(E_EncryptAlgorithm ea, E_EncryptKeyBits ekb, const uint8_t *key, 
											DSC_KeyType_t  key_type, unsigned int pid){
	int ret = AM_SUCCESS;
	AM_DSC_KeyType_t type;
	type = (AM_DSC_KeyType_t)key_type;
	LOGI("[%s %d] ea:%d, ekb:%d, key_type:%d, type:%d, pid:%d\n", __FUNCTION__, __LINE__, ea, ekb, key_type, type, pid);
	if(pid != vPara.pid && pid != a_aPara[0].pid)
		return ret;
    //LOGI("[%s %d] key:0x%x\n", __FUNCTION__, __LINE__, *key);
	
    uint8_t keyval [16] ={'\0'};
	int i = 0;
    const uint8_t *pkey = key;
	
    for(;i<16;i++) {
        keyval[i]=*pkey;
        pkey++;
		LOGI("[%s %d] i:%d keyval[i]=0x%x", __FUNCTION__, __LINE__, i, keyval[i]);
    }
	if(pid == vPara.pid){//is video pid
		ret = AM_DSC_SetKey(DSC_DEV_NO, vch, type, keyval);
		if(ret != AM_SUCCESS)
			LOGI("DSC SetKey failed, vch:%d, key_type:%d,key:%s\n",vch, key_type, keyval);
	}
	else{
		ret = AM_DSC_SetKey(DSC_DEV_NO, ach, type, keyval);
		if(ret != AM_SUCCESS)
			LOGI("DSC SetKey failed, ach:%d, key_type:%d,key:%s\n",ach, key_type, keyval);
	}
	LOGI("[%s %d]", __FUNCTION__, __LINE__);
	return ret;
}

Encrypt_info src[3]={{e_DES, 1, e_256_bits},
                    {e_AES,1,e_128_bits},
                    {e_3DES,1, e_64_bits}};

/*
  获取支持的硬解密算法及各算法支持的Key的bit数
*/
/*
int CTsPlayer::GetEncryptInfo(char *pData, int *pInputlen, int *pOutlen) {
    if(*pInputlen < sizeof(src)) {
        LOGI("[%s %d] need %d bytes but only have %d!!!\n", __FUNCTION__, __LINE__, (int)sizeof(src), *pInputlen);
        return -2;
    }
    memcpy(pData, src, sizeof(src));
    *pOutlen=(int)sizeof(src);
    LOGI("[%s %d] need %d bytes but only have %d!!!\n", __FUNCTION__, __LINE__, *pInputlen, *pOutlen);
    return AM_SUCCESS;
}*/

int CTsPlayer::SetVideoWindow(int x,int y,int width,int height){
    int epg_centre_x = 0;
    int epg_centre_y = 0;
    int old_videowindow_certre_x = 0;
    int old_videowindow_certre_y = 0;
    int new_videowindow_certre_x = 0;
    int new_videowindow_certre_y = 0;
    int new_videowindow_width = 0;
    int new_videowindow_height = 0;
    char vaxis_newx_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_newy_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_width_str[PROPERTY_VALUE_MAX] = {0};
    char vaxis_height_str[PROPERTY_VALUE_MAX] = {0};
    int vaxis_newx= -1, vaxis_newy = -1, vaxis_width= -1, vaxis_height= -1;
    int fd_axis, fd_mode;
    int x2 = 0, y2 = 0, width2 = 0, height2 = 0;
    int ret = 0;
    //const char *path_mode = "/sys/class/video/screen_mode";
    const char *path_axis = "/sys/class/video/axis";
    char bcmd[32];
    char buffer[15];
    int mode_w = 0, mode_h = 0;
	char vaule[PROPERTY_VALUE_MAX] = {0};
	
	memset(vaule, 0, PROPERTY_VALUE_MAX);
    property_get("media.contax.iptv.drm", vaule, "0");
    prop_media_contax_iptv = atoi(vaule);

    LOGI("[%s %d]parameter x:%d, y:%d, width:%d, height:%d\n", __FUNCTION__, __LINE__, x, y, width, height);
	
    OUTPUT_MODE output_mode = iGetDisplayMode();
    if(m_isSoftFit) {
        int x_b=0, y_b=0, w_b=0, h_b=0;
        int mode_x = 0, mode_y = 0, mode_width = 0, mode_height = 0;
        iGetPosition(output_mode, &mode_x, &mode_y, &mode_width, &mode_height);
        LOGI("GetPosition by outputmode mode_x: %d, mode_y: %d, mode_width: %d, mode_height: %d\n", 
                mode_x, mode_y, mode_width, mode_height);
        x_b = x + mode_x;
        y_b = y + mode_y;
        w_b = width + x_b - 1;
        h_b = height + y_b - 1;
        if (h_b < 576 && h_b % 2)
            h_b +=1;
        /*char value[PROPERTY_VALUE_MAX];
        int last_small_win_flag = 0;
        memset(value, 0, PROPERTY_VALUE_MAX);
        property_get("sys.iptv.lastsmallwindow", value, "0");
        last_small_win_flag = atoi(value);
        if((last_small_win_flag == 0)&&(mode_width == width)&&(mode_height == height))
        {
        	LOGI("full screen w -1 h -1\n");
			x_b = 0;
			y_b = 0;
			w_b = -1;
			h_b = -1;
		}	*/
        sprintf(bcmd, "%d %d %d %d", x_b, y_b, w_b, h_b);
	if(pcodec->has_sub == 1)
        subtitleSetSurfaceViewParam(x, y, width, height);
	LOGI("setvideoaxis start: %s\n", bcmd);
	if(false == m_isSurfaceWindow) {
		//ret = amsysfs_set_sysfs_str(path_axis, bcmd);
		//LOGI("setvideoaxis m_isSoftFit: false == m_isSurfaceWindow\n");
	}
        LOGI("setvideoaxis: %s\n", bcmd);
		char video_axis_x[PROPERTY_VALUE_MAX], video_axis_y[PROPERTY_VALUE_MAX], video_axis_w[PROPERTY_VALUE_MAX], video_axis_h[PROPERTY_VALUE_MAX];
		sprintf(video_axis_x,"%d",x_b);
		sprintf(video_axis_y,"%d",y_b);
		sprintf(video_axis_w,"%d",w_b);
		sprintf(video_axis_h,"%d",h_b);
		LOGI("video_axis_x: %s\n", video_axis_x);
		LOGI("video_axis_y: %s\n", video_axis_y);
		LOGI("video_axis_w: %s\n", video_axis_w);
		LOGI("video_axis_h: %s\n", video_axis_h);

		property_set("media.ctc.video_axis_x", video_axis_x);
		property_set("media.ctc.video_axis_y", video_axis_y);
		property_set("media.ctc.video_axis_w", video_axis_w);
		property_set("media.ctc.video_axis_h", video_axis_h);
		LOGI("[%s %d] end\n", __FUNCTION__, __LINE__);
        return ret;
    }

    /*adjust axis as rate recurrence*/
    GetVideoPixels(mode_w, mode_h);

    x2 = x*mode_w/m_nEPGWidth;
    width2 = width*mode_w/m_nEPGWidth;
    y2 = y*mode_h/m_nEPGHeight;
    height2 = height*mode_h/m_nEPGHeight;

    old_videowindow_certre_x = x2+int(width2/2);
    old_videowindow_certre_y = y2+int(height2/2);
    
    iGetPosition(output_mode, &vaxis_newx, &vaxis_newy, &vaxis_width, &vaxis_height);
    LOGI("output_mode: %d, vaxis_newx: %d, vaxis_newy: %d, vaxis_width: %d, vaxis_height: %d\n",
            output_mode, vaxis_newx, vaxis_newy, vaxis_width, vaxis_height);
    epg_centre_x = vaxis_newx+int(vaxis_width/2);
    epg_centre_y = vaxis_newy+int(vaxis_height/2);
    new_videowindow_certre_x = epg_centre_x + int((old_videowindow_certre_x-mode_w/2)*vaxis_width/mode_w);
    new_videowindow_certre_y = epg_centre_y + int((old_videowindow_certre_y-mode_h/2)*vaxis_height/mode_h);
    new_videowindow_width = int(width2*vaxis_width/mode_w);
    new_videowindow_height = int(height2*vaxis_height/mode_h);
    LOGI("CTsPlayer::mode_w = %d, mode_h = %d, mw = %d, mh = %d \n",
            mode_w, mode_h, m_nEPGWidth, m_nEPGHeight);

    /*if(m_nEPGWidth !=0 && m_nEPGHeight !=0) {
        amsysfs_set_sysfs_str(path_mode, "1");
    }*/

    sprintf(bcmd, "%d %d %d %d", new_videowindow_certre_x-int(new_videowindow_width/2)-1,
            new_videowindow_certre_y-int(new_videowindow_height/2)-1,
            new_videowindow_certre_x+int(new_videowindow_width/2)+1,
            new_videowindow_certre_y+int(new_videowindow_height/2)+1);            

	if(false == m_isSurfaceWindow) {
		ret = amsysfs_set_sysfs_str(path_axis, bcmd);
		LOGI("setvideoaxis false == m_isSurfaceWindow\n");
	}
    LOGI("setvideoaxis: %s\n", bcmd);

    if((width2 > 0)&&(height2 > 0)&&((width2 < (mode_w -10))||(height2< (mode_h -10))))
        amsysfs_set_sysfs_int("/sys/module/di/parameters/bypass_hd",1);
    else
        amsysfs_set_sysfs_int("/sys/module/di/parameters/bypass_hd",0);

	createWindowSurface(x, y, width, height);
	
    return ret;
}

int CTsPlayer::VideoShow(void){
	video_hide = 0;
    LOGI("VideoShow control by m_isBlackoutPolicy:%d \n", m_isBlackoutPolicy);
    //amsysfs_set_sysfs_str("/sys/class/graphics/fb0/video_hole", "0 0 1280 720 0 8");
    if(!m_isBlackoutPolicy) {
        if(amsysfs_get_sysfs_int("/sys/class/video/disable_video") == 1)
            amsysfs_set_sysfs_int("/sys/class/video/disable_video",2);
        else
            LOGW("video is enable, no need to set disable_video again\n");
    }
    return 0;
}

int CTsPlayer::VideoHide(void){
	video_hide = 1;
    LOGI("VideoHide m_isBlackoutPolicy:%d\n", m_isBlackoutPolicy);
    //amsysfs_set_sysfs_str("/sys/class/graphics/fb0/video_hole", "0 0 0 0 0 0");
    if(!m_isBlackoutPolicy)
        amsysfs_set_sysfs_int("/sys/class/video/disable_video",1);
    return 0;
}


int CTsPlayer::EnableAudioOutput()
{
    LOGI("%s, enter\n", __func__);

   lp_lock(&mutex);

#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1) {
        VmxAudioOutput (0);
    } else
#endif
    {
        codec_set_mute(pcodec, 0);
    }
    lp_unlock(&mutex);

    return CTC_SUCESS;
}

int CTsPlayer::DisableAudioOutput()
{
    LOGI("%s, enter\n", __func__);

   lp_lock(&mutex);
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1) {
        VmxAudioOutput (1);
    } else
#endif
    {
        codec_set_mute(pcodec, 1);
    }
    lp_unlock(&mutex);

    return CTC_SUCESS;
}


void CTsPlayer::InitVideo(PVIDEO_PARA_T pVideoPara){
    mVideoPara=*pVideoPara;
	if(prop_media_contax_iptv == 1){
		iAllocateChannel(DSC_DEV_NO,&vch);
		iSetChannelPID(DSC_DEV_NO,vch,mVideoPara.pid);
	}
    LOGI("InitVideo mVideoPara->pid: %d, mVideoPara->vFmt: %d, mVideoPara.nFrameRate=%d\n", mVideoPara.pid, mVideoPara.vFmt, mVideoPara.nFrameRate);
    if (mVideoPara.vFmt == -1) {
        mHaveVideo = false;
    } else {
        mHaveVideo = true;
        //LOGI("InitVideo video");
    }
    if (mVideoPara.pid == 0xffff) {
        mIsEsVideo = true;
        mIsTsStream = false;
		LOGI("InitVideo video, es stream\n");
    } else {
        mIsEsVideo = false;
        mIsTsStream = true;
		LOGI("InitVideo video, ts stream\n");
    }
	if (mHaveVideo == false && mVideoPara.pid == 0xffff) {
		mIsEsVideo = false;
		mIsTsStream = false;
	}
    if (mHaveVideo && mIsEsVideo) {
        int ret = 0;
        m_vpcodec = &m_vcodec_para;
        memset(m_vpcodec, 0, sizeof(codec_para_t));
        m_vcodec_para.has_video = 1;
        if (mVideoPara.vFmt == VFORMAT_H264) {
            m_vcodec_para.video_type = VFORMAT_H264;
            m_vcodec_para.am_sysinfo.format = VIDEO_DEC_FORMAT_H264;
        } else if (mVideoPara.vFmt == VFORMAT_MPEG12){
             //   || mVideoPara.vFmt == VIDEO_FORMAT_MPEG2) {
            m_vcodec_para.video_type = VFORMAT_MPEG12;
            m_vcodec_para.am_sysinfo.format = VIDEO_DEC_FORMAT_UNKNOW;
        } else if (mVideoPara.vFmt == VFORMAT_MPEG4) {
            m_vcodec_para.video_type = VFORMAT_MPEG4;
			m_vcodec_para.am_sysinfo.format = VIDEO_DEC_FORMAT_MPEG4_5;
        } else if ((mVideoPara.vFmt == VFORMAT_HEVC)){ //|| (mVideoPara.vFmt == CT_VFORMAT_H265)) {
            m_vcodec_para.video_type = VFORMAT_HEVC;
            m_vcodec_para.am_sysinfo.format= VIDEO_DEC_FORMAT_HEVC;
        }    
        //m_vcodec_para.am_sysinfo.param = (void *)(EXTERNAL_PTS| SYNC_OUTSIDE);
        m_vcodec_para.stream_type = STREAM_TYPE_ES_VIDEO;
        if (mVideoPara.nFrameRate != 0)
            m_vcodec_para.am_sysinfo.rate = 96000 / mVideoPara.nFrameRate;
		
        m_vcodec_para.has_audio = 0;
	    m_vcodec_para.noblock = 0;
        m_vcodec_para.has_video = 1;
        ret = codec_init(m_vpcodec);
        if(ret != 0)
        {
            LOGI("v codec init failed, ret=-0x%x", -ret);
            return;
        }
    }
}

void CTsPlayer::InitAudio(PAUDIO_PARA_T pAudioPara){
    mAudioPara = *pAudioPara;
	int count = 0;
	PAUDIO_PARA_T pAP = pAudioPara;
	
	memset(a_aPara,0,sizeof(AUDIO_PARA_T)*MAX_AUDIO_PARAM_SIZE);
    while((pAP->pid != 0)&&(count<MAX_AUDIO_PARAM_SIZE)){
	#ifdef TELECOM_VFORMAT_SUPPORT
        pAP->aFmt = changeAformat(pAP->aFmt);
	#endif
        a_aPara[count]= *pAP;
        LOGI("InitAudio pAP->pid: %d, pAP->aFmt: %d, channel=%d, samplerate=%d\n", pAP->pid, pAP->aFmt, pAP->nChannels, pAP->nSampleRate);
        pAP++;
        count++;
    }
	
	if(prop_media_contax_iptv == 1){
		iAllocateChannel(DSC_DEV_NO,&ach);
		//iSetChannelPID(DSC_DEV_NO,ach,a_aPara[0].pid);
		iSetChannelPID(DSC_DEV_NO,ach,mAudioPara.pid);	
	}
    LOGI("InitAudio mAudioPara.pid: %d, mAudioPara.aFmt: %d, channel=%d, samplerate=%d\n", mAudioPara.pid, mAudioPara.aFmt, mAudioPara.nChannels, mAudioPara.nSampleRate);

    if (mAudioPara.aFmt == -1){
        mHaveAudio = false;
    } else {
        mHaveAudio = true;
        LOGI("InitAudio mHaveAudio true\n");
    }
    if (mAudioPara.pid == 0xffff) {
        mIsEsAudio = true;
        mIsTsStream = false;
    } else {
        mIsEsAudio = false;
        mIsTsStream = true;
    }
	if (mHaveAudio == false && mAudioPara.pid == 0xffff) {
		mIsEsAudio = false;
		mIsTsStream = false;
	}
    if (mHaveAudio && mIsEsAudio) {
        int ret = 0;
        m_apcodec = &m_acodec_para;
        memset(m_apcodec, 0, sizeof(codec_para_t));
		LOGI("InitAudio audio is es\n");
        if (mAudioPara.aFmt == AFORMAT_MPEG) {
            m_apcodec->audio_type = AFORMAT_MPEG;
        } else if (mAudioPara.aFmt == AFORMAT_AAC) {
            m_apcodec->audio_type = AFORMAT_AAC;
        } else if (mAudioPara.aFmt == AFORMAT_AC3) {
            m_apcodec->audio_type = AFORMAT_AC3;
        } else if (mAudioPara.aFmt == AFORMAT_EAC3) {
            m_apcodec->audio_type = AFORMAT_EAC3;
        } 
        m_apcodec->stream_type = STREAM_TYPE_ES_AUDIO;
        LOGI("mAudioPara.aFmt=%x, m_apcodec->audio_type=%x, mAudioPara.aFmt=%d", mAudioPara.aFmt, m_apcodec->audio_type, mAudioPara.aFmt);

        m_apcodec->has_audio = 1;
        m_apcodec->noblock = 0;
        
        //Do NOT set audio info if we do not know it
        m_apcodec->audio_channels = mAudioPara.nChannels;
        m_apcodec->audio_samplerate = mAudioPara.nSampleRate;
        m_apcodec->audio_info.channels = mAudioPara.nChannels;
        m_apcodec->audio_info.sample_rate = mAudioPara.nSampleRate;

        ret = codec_init(m_apcodec);
        if(ret != 0)
        {
            LOGI("a codec init failed, ret=-0x%x", -ret);
            return;
        }
    } 
    return ;
}

#if 0
void CTsPlayer::InitVideo(PVIDEO_PARA_T pVideoPara)
{
    vPara=*pVideoPara;
#ifdef TELECOM_VFORMAT_SUPPORT
    vPara.vFmt = changeVformat(vPara.vFmt);
#endif
    LOGI("InitVideo vPara->pid: %d, vPara->vFmt: %d\n", vPara.pid, vPara.vFmt);
}

void CTsPlayer::InitAudio(PAUDIO_PARA_T pAudioPara)
{
    PAUDIO_PARA_T pAP = pAudioPara;
    int count = 0;

    LOGI("InitAudio\n");
    memset(a_aPara,0,sizeof(AUDIO_PARA_T)*MAX_AUDIO_PARAM_SIZE);
    while((pAP->pid != 0)&&(count<MAX_AUDIO_PARAM_SIZE)) {
#ifdef TELECOM_VFORMAT_SUPPORT
        pAP->aFmt = changeAformat(pAP->aFmt);
#endif
        a_aPara[count]= *pAP;
        LOGI("InitAudio pAP->pid: %d, pAP->aFmt: %d, channel=%d, samplerate=%d\n", pAP->pid, pAP->aFmt, pAP->nChannels, pAP->nSampleRate);
        pAP++;
        count++;
    }
    return ;
}
#endif

void CTsPlayer::InitSubtitle(PSUBTITLE_PARA_T pSubtitlePara){

	LOGI("[%s %d] \n", __FUNCTION__, __LINE__);
    int count = 0;
	int iner_subcount = 0;
	int i = 0;

    amsysfs_set_sysfs_int("/sys/class/subtitle/width", 0);
    amsysfs_set_sysfs_int("/sys/class/subtitle/height", 0);

    memset(sPara,0,sizeof(SUBTITLE_PARA_T)*MAX_SUBTITLE_PARAM_SIZE);
    while((pSubtitlePara->pid != 0)&&(count<MAX_SUBTITLE_PARAM_SIZE)) {
        sPara[count]= *pSubtitlePara;
        LOGI("subtitle %d: pid:%d\n",count,pSubtitlePara->pid);
        pSubtitlePara++;
        count++;
    }
    /*
    count = 0;//If count = 1,  it's out of range! sPara[] would write into vPara, and couse on video frame out.
	memset(sPara,0,sizeof(SUBTITLE_PARA_T));
	sPara[count]= *pSubtitlePara;
	*/
	for(i=0; i < count; i++){
		LOGI("InitSubtitle pid:%d,sub_type:0x%x, and set subtitle totalnum which to be confirmed:%d\n",sPara[i].pid,sPara[i].sub_type,count);
		if(sPara[i].sub_type < CODEC_ID_SCTE27)
			iner_subcount++;
		if ((sPara[i].sub_type == CODEC_ID_CLOSEDCAPTION) && (pSubtitlePara->pid == (unsigned short)-1)) {
			LOGI("means we should show cc\n");
			sub_index = i;
		}
	}
    amsysfs_set_sysfs_int("/sys/class/subtitle/total",iner_subcount);
	LOGI("[%s %d] iner_subcount:%d\n", __FUNCTION__, __LINE__, iner_subcount);
    return ;
}

void setSubType(PSUBTITLE_PARA_T pSubtitlePara){
    if(!pSubtitlePara)
        return;
    LOGI("setSubType pid:%d sub_type:0x%x\n",pSubtitlePara->pid,pSubtitlePara->sub_type);
    #if 1 
    //set subtitle type in libplayer, for android-o not support
    if (pSubtitlePara->sub_type== CODEC_ID_DVD_SUBTITLE) {
        amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",0);
    } else if (pSubtitlePara->sub_type== CODEC_ID_HDMV_PGS_SUBTITLE) {
        amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",1);
    } else if (pSubtitlePara->sub_type== CODEC_ID_XSUB) {
        amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",2);
    } else if (pSubtitlePara->sub_type == CODEC_ID_TEXT || \
                pSubtitlePara->sub_type == CODEC_ID_SSA) {
        amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",3);
    } else if (pSubtitlePara->sub_type == CODEC_ID_DVB_SUBTITLE) {
        amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",5);
    } else {
        amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",4);
    }
   #endif 
}

#define FILTER_AFMT_MPEG		(1 << 0)
#define FILTER_AFMT_PCMS16L	    (1 << 1)
#define FILTER_AFMT_AAC			(1 << 2)
#define FILTER_AFMT_AC3			(1 << 3)
#define FILTER_AFMT_ALAW		(1 << 4)
#define FILTER_AFMT_MULAW		(1 << 5)
#define FILTER_AFMT_DTS			(1 << 6)
#define FILTER_AFMT_PCMS16B		(1 << 7)
#define FILTER_AFMT_FLAC		(1 << 8)
#define FILTER_AFMT_COOK		(1 << 9)
#define FILTER_AFMT_PCMU8		(1 << 10)
#define FILTER_AFMT_ADPCM		(1 << 11)
#define FILTER_AFMT_AMR			(1 << 12)
#define FILTER_AFMT_RAAC		(1 << 13)
#define FILTER_AFMT_WMA			(1 << 14)
#define FILTER_AFMT_WMAPRO		(1 << 15)
#define FILTER_AFMT_PCMBLU		(1 << 16)
#define FILTER_AFMT_ALAC		(1 << 17)
#define FILTER_AFMT_VORBIS		(1 << 18)
#define FILTER_AFMT_AAC_LATM		(1 << 19)
#define FILTER_AFMT_APE		       (1 << 20)
#define FILTER_AFMT_EAC3		       (1 << 21)

int TsplayerGetAFilterFormat(const char *prop){
    char value[PROPERTY_VALUE_MAX];
    int filter_fmt = 0;
    /* check the dts/ac3 firmware status */
    if(access("/system/etc/firmware/audiodsp_codec_ddp_dcv.bin",F_OK) && access("/system/lib/libstagefright_soft_dcvdec.so",F_OK)){
        filter_fmt |= (FILTER_AFMT_AC3|FILTER_AFMT_EAC3);
    }
    if(access("/system/etc/firmware/audiodsp_codec_dtshd.bin",F_OK) && access("/system/lib/libstagefright_soft_dtshd.so",F_OK)){
        filter_fmt  |= FILTER_AFMT_DTS;
    }
    if(property_get(prop, value, NULL) > 0) {
        LOGI("[%s:%d]disable_adec=%s\n", __FUNCTION__, __LINE__, value);
        if(strstr(value,"mpeg") != NULL || strstr(value,"MPEG") != NULL) {
            filter_fmt |= FILTER_AFMT_MPEG;
        }
        if(strstr(value,"pcms16l") != NULL || strstr(value,"PCMS16L") != NULL) {
            filter_fmt |= FILTER_AFMT_PCMS16L;
        }
        if(strstr(value,"aac") != NULL || strstr(value,"AAC") != NULL) {
            filter_fmt |= FILTER_AFMT_AAC;
        }
        if(strstr(value,"ac3") != NULL || strstr(value,"AC#") != NULL) {
            filter_fmt |= FILTER_AFMT_AC3;
        }
        if(strstr(value,"alaw") != NULL || strstr(value,"ALAW") != NULL) {
            filter_fmt |= FILTER_AFMT_ALAW;
        }
        if(strstr(value,"mulaw") != NULL || strstr(value,"MULAW") != NULL) {
            filter_fmt |= FILTER_AFMT_MULAW;
        }
        if(strstr(value,"dts") != NULL || strstr(value,"DTS") != NULL) {
            filter_fmt |= FILTER_AFMT_DTS;
        }
        if(strstr(value,"pcms16b") != NULL || strstr(value,"PCMS16B") != NULL) {
            filter_fmt |= FILTER_AFMT_PCMS16B;
        }
        if(strstr(value,"flac") != NULL || strstr(value,"FLAC") != NULL) {
            filter_fmt |= FILTER_AFMT_FLAC;
        }
        if(strstr(value,"cook") != NULL || strstr(value,"COOK") != NULL) {
            filter_fmt |= FILTER_AFMT_COOK;
        }
        if(strstr(value,"pcmu8") != NULL || strstr(value,"PCMU8") != NULL) {
            filter_fmt |= FILTER_AFMT_PCMU8;
        }
        if(strstr(value,"adpcm") != NULL || strstr(value,"ADPCM") != NULL) {
            filter_fmt |= FILTER_AFMT_ADPCM;
        }
        if(strstr(value,"amr") != NULL || strstr(value,"AMR") != NULL) {
            filter_fmt |= FILTER_AFMT_AMR;
        }
        if(strstr(value,"raac") != NULL || strstr(value,"RAAC") != NULL) {
            filter_fmt |= FILTER_AFMT_RAAC;
        }
        if(strstr(value,"wma") != NULL || strstr(value,"WMA") != NULL) {
            filter_fmt |= FILTER_AFMT_WMA;
        }
        if(strstr(value,"wmapro") != NULL || strstr(value,"WMAPRO") != NULL) {
            filter_fmt |= FILTER_AFMT_WMAPRO;
        }
        if(strstr(value,"pcmblueray") != NULL || strstr(value,"PCMBLUERAY") != NULL) {
            filter_fmt |= FILTER_AFMT_PCMBLU;
        }
        if(strstr(value,"alac") != NULL || strstr(value,"ALAC") != NULL) {
            filter_fmt |= FILTER_AFMT_ALAC;
        }
        if(strstr(value,"vorbis") != NULL || strstr(value,"VORBIS") != NULL) {
            filter_fmt |= FILTER_AFMT_VORBIS;
        }
        if(strstr(value,"aac_latm") != NULL || strstr(value,"AAC_LATM") != NULL) {
            filter_fmt |= FILTER_AFMT_AAC_LATM;
        }
        if(strstr(value,"ape") != NULL || strstr(value,"APE") != NULL) {
            filter_fmt |= FILTER_AFMT_APE;
        }
        if(strstr(value,"eac3") != NULL || strstr(value,"EAC3") != NULL) {
            filter_fmt |= FILTER_AFMT_EAC3;
        }  
    }
    LOGI("[%s:%d]filter_afmt=%x\n", __FUNCTION__, __LINE__, filter_fmt);
    return filter_fmt;
}

void *CTsPlayer::threadSub(void *pthis){ 
    CTsPlayer *subplayer = static_cast<CTsPlayer *>(pthis);
	char value[PROPERTY_VALUE_MAX] = {0};

	LOGI("threadSub start pthis: %p\n", pthis);

    	if(hasccsub == 0 && subplayer->pcodec->has_sub == 0 && (hasSctesub == 0)) {
		LOGI("No, subtitle thread end\n");
		return NULL;
    	}

	subtitleCreat();
	//usleep(1000*1000);

	/*if (subplayer->pcodec->has_sub == 1 || (hasccsub == 1 && subplayer->m_bFast == false))
		//subtitleResetForSeek();
	else
		LOGI("threadSub, no subtitle to open\n");*/

	if (subplayer->m_firstStart) {
		if (subplayer->pcodec->has_sub == 1) {
                 LOGI("##first start, dvb sub!set type:5, total:1\n");
                 subtitleSetSubType(5, 1);   //5:dvb type 1:total
		} else {
                 subtitleSetSubType(0, 0);   //non-dvb reset sysfs
		}
             usleep(1000*30);
		subtitleOpen("", subplayer);// "" fit for api param, no matter what the path is for inner subtitle.
		LOGI("threadSub hasccsub:%d,ccsubstatus:%d hasSctesub:%d\n", hasccsub, ccsubstatus, hasSctesub);
	//	if(hasccsub == 0 || hasSctesub == 1)
	//		subtitleShow(); 
		if(hasccsub == 1 && ccsubstatus == 0)
			subtitleHide();
		subplayer->m_firstStart = false;
	}

	int sub_startdisable = 0;
	memset(value, 0, PROPERTY_VALUE_MAX);
	property_get("config.media.iptv.substartdisable", value, "0");
	sub_startdisable = atoi(value);

	if((1 == sub_startdisable) && (hasccsub == 0))
	{
		//property_set("config.media.iptv.substartdisable", "0");
		LOGI("subtile need not show now when startplay\n");
		subplayer->SubtitleShowHide(0);
	}
	subOpened = 1;
        LOGI("threadSub end\n");
        return NULL;
}

pthread_t mSetSubRatioThread;
int mSubRatioRetry = 200; //10*0.5s=5s to retry get video width and height
bool mSubRatioThreadStop = false;
void *setSubRatioAutoThread(void *pthis){
    int width = -1;
    int height = -1;
    int videoWidth = -1;
    int videoHeight = -1;
    int dispWidth = -1;
    int dispHeight = -1;
    int frameWidth = -1;
    int frameHeight = -1;

    int mode_x = 0;
    int mode_y = 0;
    int mode_width = 0;
    int mode_height = 0;
    vdec_status vdec;

    char pts_video_chr[64] = {0};
    char pts_video_chr_bac[64] = {0};
    bool pts_video_stored = false;

    do {
        amsysfs_get_sysfs_str("/sys/class/tsync/pts_video", pts_video_chr, 64);
        LOGE("CTsPlayer::setSubRatioAutoThread mSubRatioRetry:%d, pts_video_chr:%s\n",mSubRatioRetry, pts_video_chr);
        if (strlen(pts_video_chr) > 0)
        {
            LOGE("CTsPlayer::setSubRatioAutoThread mSubRatioRetry:%d, pts_video_chr:%s, pts_video_stored:%d\n",mSubRatioRetry, pts_video_chr, pts_video_stored);
            if (strcmp(pts_video_chr, "0x0") && !pts_video_stored)
            {
                pts_video_stored = true;
                strcpy(pts_video_chr_bac, pts_video_chr);
                continue;
            }
            else if (strlen(pts_video_chr_bac) > 0)
            {
                LOGE("CTsPlayer::setSubRatioAutoThread mSubRatioRetry:%d, pts_video_chr:%s, pts_video_chr_bac:%s,pts_video_stored:%d\n",mSubRatioRetry, pts_video_chr, pts_video_chr_bac, pts_video_stored);
                if (strcmp(pts_video_chr_bac, pts_video_chr))
                {
                    break;
                }
            }
        }
        //videoWidth = amsysfs_get_sysfs_int("/sys/class/video/frame_width");
        //videoHeight = amsysfs_get_sysfs_int("/sys/class/video/frame_height");
        //LOGE("CTsPlayer::setSubRatioAutoThread (videoWidth,videoHeight):(%d,%d), mSubRatioRetry:%d, pts_video:%d, pts_video_chr:%s\n",videoWidth, videoHeight, mSubRatioRetry, pts_video, pts_video_chr);

        mSubRatioRetry--;
        usleep(50000); // 0.05 s
    }while(mSubRatioRetry > 0 && !mSubRatioThreadStop);

    if (mSubRatioThreadStop)
    {
        return NULL;
    }

    videoWidth = amsysfs_get_sysfs_int("/sys/class/video/frame_width");
    videoHeight = amsysfs_get_sysfs_int("/sys/class/video/frame_height");
    LOGE("CTsPlayer::setSubRatioAutoThread 00(videoWidth,videoHeight):(%d,%d), mSubRatioRetry:%d, pts_video_chr:%s\n",videoWidth, videoHeight, mSubRatioRetry, pts_video_chr);

    DisplayInfo info;
    sp<IBinder> display(SurfaceComposerClient::getBuiltInDisplay(
                ISurfaceComposer::eDisplayIdMain));
    SurfaceComposerClient::getDisplayInfo(display, &info);
    frameWidth = info.w;
    frameHeight = info.h;
   LOGI("CTsPlayer::StartPlay (frameWidth,frameHeight):(%d,%d)\n",info.w, info.h);


    OUTPUT_MODE output_mode = iGetDisplayMode();
    iGetPosition(output_mode, &mode_x, &mode_y, &mode_width, &mode_height);
    dispWidth = mode_width - mode_x;
    dispHeight = mode_height - mode_y;
    LOGI("CTsPlayer::StartPlay (dispWidth,dispHeight):(%d,%d)\n",dispWidth, dispHeight);

    // full screen
    width = dispWidth;
    height = dispHeight;

    width = width * frameWidth / dispWidth;
    height = height * frameHeight / dispHeight;
    float ratioW = 1.000f;
    float ratioH = 1.000f;
    float ratioMax = 2.000f;
    float ratioMin = 1.250f;
    int maxW = dispWidth;
    int maxH = dispHeight;
    if (videoWidth != 0 & videoHeight != 0) {
        ratioW = ((float)width) / videoWidth;
        ratioH = ((float)height) / videoHeight;
        if (ratioW > ratioMax || ratioH > ratioMax) {
            ratioW = ratioMax;
            ratioH = ratioMax;
        }
        /*else if (ratioW < ratioMin || ratioH < ratioMin) {
            ratioW = ratioMin;
            ratioH = ratioMin;
        }*/
        LOGE("CTsPlayer::StartPlay (ratioW,ratioH):(%f,%f),(maxW,maxH):(%d,%d)\n", ratioW, ratioH, maxW, maxH);
        subtitleSetImgRatio(ratioW, ratioH, maxW, maxH);
    }
    return NULL;
}

void setSubRatioAuto(){	
#if 0
		mSubRatioThreadStop = false;
    	pthread_create(&mSetSubRatioThread, NULL, setSubRatioAutoThread, NULL);
#else
		 int width = -1;
		 int height = -1;
		 int videoWidth = -1;
		 int videoHeight = -1;
		 int dispWidth = -1;
		 int dispHeight = -1;
		 int frameWidth = -1;
		 int frameHeight = -1;
		 
		 int mode_x = 0;
		 int mode_y = 0;
		 int mode_width = 0;
		 int mode_height = 0;
	
		 DisplayInfo info;
		 sp<IBinder> display(SurfaceComposerClient::getBuiltInDisplay(
					 ISurfaceComposer::eDisplayIdMain));
		 SurfaceComposerClient::getDisplayInfo(display, &info);
		 frameWidth = info.w;
		 frameHeight = info.h;
		 LOGI("[%s %d] SurfaceComposer getDisplayInfo (frameWidth,frameHeight):(%d,%d)\n",__FUNCTION__, __LINE__, info.w, info.h);
		
		
		 OUTPUT_MODE output_mode = iGetDisplayMode();
		 iGetPosition(output_mode, &mode_x, &mode_y, &mode_width, &mode_height);
		 dispWidth = mode_width - mode_x;
		 dispHeight = mode_height - mode_y;
		 LOGI("[%s %d] iGetPosition (dispWidth,dispHeight):(%d,%d)\n", __FUNCTION__, __LINE__, dispWidth, dispHeight);
		
		 // full screen
		 width = dispWidth;
		 height = dispHeight;
		
		 width = width * frameWidth / dispWidth;
		 height = height * frameHeight / dispHeight;
		 float ratioW = 2.000f;
		 float ratioH = 2.000f;
		 float ratioMax = 2.000f;
		 float ratioMin = 1.250f;
		 int maxW = dispWidth;
		 int maxH = dispHeight;
	
		 LOGE("[%s %d] (ratioW,ratioH):(%f,%f),(maxW,maxH):(%d,%d)\n", __FUNCTION__, __LINE__, ratioW, ratioH, maxW, maxH);
		 subtitleSetImgRatio(ratioW, ratioH, maxW, maxH);
		 LOGI("[%s %d] end \n", __FUNCTION__, __LINE__);
	
#endif
}
/* 
 * player_startsync_set
 *
 * reset start sync using prop media.amplayer.startsync.mode
 * 0 none start sync
 * 1 slow sync repeate mode
 * 2 drop pcm mode
 *
 * */

int player_startsync_set(int mode){
    const char * startsync_mode = "/sys/class/tsync/startsync_mode";
    const char * droppcm_prop = "sys.amplayer.drop_pcm"; // default enable
    const char * slowsync_path = "/sys/class/tsync/slowsync_enable";
    const char * slowsync_repeate_path = "/sys/class/video/slowsync_repeat_enable";
	const char * show_firstframe = "/sys/class/video/show_first_frame_nosync";
   	const char * audio_startfirst = "sys.amplayer.audio_startfirst";
/*
    char value[PROPERTY_VALUE_MAX];
    int mode = get_sysfs_int(startsync_mode);
    int ret = property_get(startsync_prop, value, NULL);
    if (ret <= 0) {
        log_print("start sync mode prop not setting ,using default none \n");
    }
    else
        mode = atoi(value);
*/
    LOGI("start sync mode desp: 0 -none 1-slowsync repeate 2-droppcm and now parameter mode = %d\n", mode);

    if(mode == 0) // none case
    {
        amsysfs_set_sysfs_int(slowsync_path,0); 
        //property_set(droppcm_prop, "1");
        amsysfs_set_sysfs_int(slowsync_repeate_path,0); 
    }
    
    if(mode == 1) // slow sync repeat mode
    {
        amsysfs_set_sysfs_int(slowsync_path,0);
		//amsysfs_set_sysfs_int(show_firstframe,1); 
        property_set(droppcm_prop, "0");
        amsysfs_set_sysfs_int(slowsync_repeate_path,1);
		amsysfs_set_sysfs_int(show_firstframe,1); 
    }
    
    if(mode == 2) // drop pcm mode
    {
        amsysfs_set_sysfs_int(slowsync_path,0); 
        property_set(droppcm_prop, "1");
        amsysfs_set_sysfs_int(slowsync_repeate_path,0); 
    }

	 if(mode == 3) // for izzi
    {
        amsysfs_set_sysfs_int(slowsync_path,0); 
        property_set(droppcm_prop, "0");
		property_set(audio_startfirst, "1");
        amsysfs_set_sysfs_int(slowsync_repeate_path,0);
    }
    return 0;
}

bool CTsPlayer::StartPlay(){
    amsysfs_set_sysfs_int(CTC_EXIST, 1);
    registerSubtitleMiddleListener();
    int ret;
    video_disable = 0;
    audio_disable = 0;
    property_set("config.media.iptv.audiostartdisable", "0");
    property_set("config.media.iptv.videostartdisable", "0");

    char value[PROPERTY_VALUE_MAX] = {0};
    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.iptv.videohide", value, "0");
    video_hide = atoi(value);

	LOGI("[%s %d] start\n", __FUNCTION__, __LINE__);
	if (prop_async_stop) {
        WAIT_EVENT(m_AStopPending);
    }
	lp_lock(&mutex);
	m_firstStart = true;
	//subtitleCreat();
	ret = iStartPlay();
	lp_unlock(&mutex);
	
	if (1 == video_hide)
	{
		VideoHide();
	}
	LOGI("[%s %d] end\n", __FUNCTION__, __LINE__);
    return ret;
}


int CTsPlayer::StartVideo()
{
    LOGI("%s, enter\n", __func__);

	video_disable = 0;
    LOGI("StartVideo ctc_stop_video set to 0.\n");
    amsysfs_set_sysfs_int("sys/class/video/ctc_stop_video",0);
    VideoShow();
    return CTC_SUCESS;
}

int CTsPlayer::StopVideo()
{
    LOGI("%s, enter\n", __func__);
	video_disable = 1;
    int clearVideo = amsysfs_get_sysfs_int("/sys/class/video/blackout_policy");
    LOGI("StopVideo ctc_stop_video set to 1, clearVideo = %d\n", clearVideo);
    amsysfs_set_sysfs_int("sys/class/video/ctc_stop_video",1);
    if(clearVideo)
        VideoHide();
    return CTC_SUCESS;
}

int CTsPlayer::StartAudio()
{
    LOGI("%s, enter\n", __func__);

	audio_disable = 0;
    EnableAudioOutput();
    return CTC_SUCESS;
}

int CTsPlayer::StopAudio()
{
    LOGI("%s, enter\n", __func__);

	audio_disable = 1;
    DisableAudioOutput();
    return CTC_SUCESS;
}

int CTsPlayer::StartSubtitle()
{
    LOGI("%s, enter\n", __func__);
    SubtitleShowHide(1);

    if(sub_handle_ctc == NULL)
        LOGI("%s, sub_handle_ctc = NULL\n", __func__);

    if((getSubState() == 1) && (sub_handle_ctc != NULL) && (hasccsub == 0))
    {
        sub_handle_ctc(SUBTITLE_AVAIABLE, 1);
        LOGI("%s, substate 1\n", __func__);
    }
    else if((getSubState() == 0) && (sub_handle_ctc != NULL) && (hasccsub == 0))
    {
        sub_handle_ctc(SUBTITLE_UNAVAIABLE, 0);
        LOGI("%s, substate 0\n", __func__);
    }

    return CTC_SUCESS;
}

int CTsPlayer::StopSubtitle()
{
    LOGI("%s, enter\n", __func__);
    SubtitleShowHide(0);

    return CTC_SUCESS;
}

//############ClosedCaption Related information
int CTsPlayer::ClosedCaptionsSelect(int id){
   //LOGI("%s, enter\n", __func__);
    subtitleClose();
    //subtitleClose may after subtitleOpen by hidl, so delay 80ms.
    usleep(1000*80);
    char CCchannel[20] = {0};
    sprintf(CCchannel, "%d", id);
    property_set("vendor.sys.subtitleService.closecaption.channelId", CCchannel);
    subtitleOpen("", this);

   int selected = getClosedCaptionSelect();
   return selected;
}

/*返回值:
1 cc enable
0 cc disable
*/
int CTsPlayer::ClosedCaptionsEnable(){
   //LOGI("%s, enter\n", __func__);
   int ret = 0;
   ret = getClosedCaptionEnable();
   return ret;
}

int CTsPlayer::ClosedCaptionsAdded(){
   //LOGI("%s, enter\n", __func__);
   int ret = 0;
   ret = getClosedCaptionAdded();
   return ret;
}

int CTsPlayer::ClosedCaptionsRemoved(){
   //LOGI("%s, enter\n", __func__);
       int ret = 0;
   ret = getClosedCaptionRemoved();
   return ret;
}
//##########

void get_vfm_map_info(char *vfm_map)
{
    int fd;
    char *path = "/sys/class/vfm/map";
    if (!vfm_map) {
        LOGE("[get_vfm_map]Invalide parameter!");
        return;
    }    
    fd = open(path, O_RDONLY);
    if (fd >= 0) { 
        memset(vfm_map, 0, 4096); // clean buffer and read 4096 byte to avoid strlen > 4096
        int ret = read(fd, vfm_map, 4096);
        vfm_map[4095] = '\0';
        if (ret > 0) {
            LOGI("[get_vfm_map_info]vfm_map=%s strlen=%d\n", vfm_map, strlen(vfm_map));
            vfm_map[strlen(vfm_map)] = '\0';
        }
        close(fd);
    } else {
        sprintf(vfm_map, "%s", "fail");
    };   
    LOGI("last [get_vfm_map_info]vfm_map=%s\n", vfm_map);
    return ;
}

/*Check whether the decoder is occupied*/
int CTsPlayer::iCheckDecoderOccupied(){
	int ret = 0;
	char vfm_map[4096] = {0};
    char *s = NULL;
    char *p = NULL; 
    int sleep_number = 0; 
    int video_buf_used = 0;
    int audio_buf_used = 0;
    int subtitle_buf_used = 0;
    int userdata_buf_used = 0;
	do{
		   get_vfm_map_info(vfm_map);
		   s = strstr(vfm_map,"(1)");
		   p = strstr(vfm_map,"ionvideo}");
		   video_buf_used=amsysfs_get_sysfs_int("/sys/class/amstream/videobufused");
		   audio_buf_used=amsysfs_get_sysfs_int("/sys/class/amstream/audiobufused");
		   subtitle_buf_used=amsysfs_get_sysfs_int("/sys/class/amstream/subtitlebufused");
		   userdata_buf_used=amsysfs_get_sysfs_int("/sys/class/amstream/userdatabufused");
		   LOGI("s=%s,p=%s\n",s,p);
		   LOGI("buf used video:%d,audio:%d,subtitle:%d,userdata:%d\n",
			   video_buf_used,audio_buf_used,subtitle_buf_used,userdata_buf_used);
		   if((s == NULL)&&(p == NULL)&&(video_buf_used!=1)&&(audio_buf_used==0)&&
		   (subtitle_buf_used==0)&&(userdata_buf_used==0)) {
			   LOGI("not find valid,begin init\n");
		   }
		   else{
			   sleep_number++;
			   usleep(50*1000);
			   LOGI("find find find,sleep_number=%d\n",sleep_number);
			   if(sleep_number == 100){//max wait 5s
					ret = -1;
					LOGI("not find after 5s retry, so return\n");
					break;
				}
		   }
	   }while((s != NULL)||(p != NULL)||(video_buf_used == 1)||(audio_buf_used != 0) || 
	   (subtitle_buf_used != 0)||(userdata_buf_used != 0));
	return ret;
}

void CTsPlayer::iGetPlayerAttribute(){
	char value[PROPERTY_VALUE_MAX] = {0};
	char tmpfilename[1024] = "";
    static int tmpfileindex = 0;
	
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.vmx.iptv.drm", value, "0");
    prop_media_vmx_iptv = atoi(value);
	#endif

	memset(value, 0, PROPERTY_VALUE_MAX);
	property_get("iptv.shouldshowlog", value, "1");//initial the log switch
    prop_shouldshowlog = atoi(value);

	memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.dumpfile", value, "0");
    prop_dumpfile = atoi(value);

	memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.singlemode", value, "0");
    prop_singlemode = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.audiohal.ctc.reset", value, "1");
    prop_audiohal_ctc_reset = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.hasaudio", value, "1");
    hasaudio = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.hasvideo", value, "1");
    hasvideo = atoi(value);

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("sys.iptv.hasccsub", value, "0");
    hasccsub = atoi(value);
	
	memset(value, 0, PROPERTY_VALUE_MAX);
	property_get("config.media.iptv.audiostartdisable", value, "0");
	audio_disable = atoi(value);

	memset(value, 0, PROPERTY_VALUE_MAX);
	property_get("config.media.iptv.videostartdisable", value, "0");
	video_disable = atoi(value);

	memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("sys.iptv.startmode", value, "1");
    startmode = atoi(value);

	if (prop_dumpfile == 1 && dumpvfd == NULL) {
		memset(value, 0, PROPERTY_VALUE_MAX);
        property_get("iptv.dumppath", value, "/data/tmp");
        if(mIsTsStream){
            sprintf(tmpfilename, "%s/Live%d.ts", value, tmpfileindex);
            tmpfileindex++;
            dumpvfd = fopen(tmpfilename, "wb+");
        }else{
            sprintf(tmpfilename, "%s/Live%d_vwrite.data", value, tmpfileindex);
            dumpvfd = fopen(tmpfilename, "wb+");
            sprintf(tmpfilename, "%s/Live%d_awrite.data", value, tmpfileindex);
            tmpfileindex++;
            dumpafd = fopen(tmpfilename, "wb+");
        }
    }
}
void CTsPlayer::iStartDrmPlayer(){
	char vaule[PROPERTY_VALUE_MAX] = {0};
	int vmxecmpid=0,vmxemmpid=0,programnum=0;
	
	memset(vaule, 0, PROPERTY_VALUE_MAX);
	property_get("config.media.vmx.iptv.ecmpid", vaule, "0");
	vmxecmpid = atoi(vaule);

    memset(vaule, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.vmx.iptv.emmpid", vaule, "0");
    vmxemmpid = atoi(vaule);

    memset(vaule, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.vmx.iptv.programnum", vaule, "0");
    programnum = atoi(vaule);

	memset(vaule, 0, PROPERTY_VALUE_MAX); 
    property_get("config.media.vmx.iptv.certpath", vaule, "/data/data/com.zte.zteplayer/files/verimatrix/store/Verimatrix.store");
    char * certpath=strdup(vaule);
	
    memset(vaule, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.vmx.iptv.serveraddr", vaule, "client-test-3.verimatrix.com");
    char * addr=strdup(vaule);

    memset(vaule, 0, PROPERTY_VALUE_MAX);
    property_get("config.media.vmx.iptv.port", vaule, "12686");
    char * port=strdup(vaule);

    LOGE("StartPlay emm=%#x ecm=%#x , vpid/apid=%#x %#x, vfmt/afmt=%#x %#x programnum0x%x addr %s  port %s certpath %s\n", 
       vmxemmpid, vmxecmpid, pcodec->video_pid, pcodec->audio_pid, pcodec->video_type, pcodec->audio_type,programnum,addr,port, certpath);
	if(pcodec->has_audio!=1)
	     pcodec->audio_pid=0x1fff;
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
       SetParameter(addr,port,certpath,NULL);	   
       StartPlayIPTV(pcodec->video_pid, pcodec->audio_pid, vmxecmpid, vmxemmpid,pcodec->video_type,
	   	                  pcodec->audio_type,programnum, pcodec->sub_pid,pcodec->sub_type);
	#endif
	 if(addr) free(addr);
     if(port) free(port);
     if(certpath) free(certpath);
}


void CTsPlayer::iPrepareAndOpenSubtitle(){
		LOGI("[%s %d]\n",__FUNCTION__, __LINE__);
		subtitleCreat();
		//subtitleSetVideoFmt(mVideoPara.vFmt);
		subtitleSetSurfaceViewParam(s_video_axis[0], s_video_axis[1], s_video_axis[2], s_video_axis[3]);
		subtitleResetForSeek();
		subtitleOpen("", this);// "" fit for api param, no matter what the path is for inner subtitle.
		if(hasccsub == 0)
			subtitleShow();
		if(hasccsub == 1 && ccsubstatus == 0)
			subtitleHide();	
		setSubRatioAuto();
		LOGI("[%s %d] end\n",__FUNCTION__, __LINE__);
}


void CTsPlayer::iMallocLpbuffer(){
	lpbuffer_st.buffer = (unsigned char *)malloc(CTC_BUFFER_LOOP_NSIZE*buffersize);
	if(lpbuffer_st.buffer == NULL) {
		LOGI("malloc failed\n");
		lpbuffer_st.enlpflag = false;
		lpbuffer_st.rp = NULL;
		lpbuffer_st.wp = NULL;
	} else{
		LOGI("malloc success\n");
		lp_lock_init(&mutex_lp, NULL);
		lpbuffer_st.enlpflag = true;
		lpbuffer_st.rp = lpbuffer_st.buffer;
		lpbuffer_st.wp = lpbuffer_st.buffer;
		lpbuffer_st.bufferend = lpbuffer_st.buffer + CTC_BUFFER_LOOP_NSIZE*buffersize;
		lpbuffer_st.valid_can_read = 0;
		memset(lpbuffer_st.buffer, 0, CTC_BUFFER_LOOP_NSIZE*buffersize);
	}
}

static int enable_decoder_buffer(codec_para_t *pcodec)
{
#define NO_AUDIO_BUF_TIME_MS 500
    LOGI("enable decoder buffer type=%d\n", pcodec->video_type);
    if (pcodec->video_type == VFORMAT_H264) {
        amsysfs_set_sysfs_int("/sys/class/tsync/tsync_pcr_adj", NO_AUDIO_BUF_TIME_MS);
    } else if (pcodec->video_type ==  VFORMAT_MPEG12) {
        amsysfs_set_sysfs_int("/sys/class/tsync/tsync_pcr_adj", NO_AUDIO_BUF_TIME_MS);
    } else if (pcodec->video_type == VFORMAT_HEVC) {
        amsysfs_set_sysfs_int("/sys/class/tsync/tsync_pcr_adj", NO_AUDIO_BUF_TIME_MS);
    }
	return 0;
}

static int disable_decoder_buffer(void)
{
    LOGI("disable decoder buffer\n");
    amsysfs_set_sysfs_int("/sys/class/tsync/tsync_pcr_adj", 0);

    return 0;
}

static int get_audio_device_state(){
    int isA2dpDevice = 0;
    String8 mString = String8("");
    audio_io_handle_t handle = AUDIO_IO_HANDLE_NONE;
    mString = AudioSystem::getParameters(handle, (String8)"isA2dpDevice");
    //LOGI("get_audio_device_state isA2dpDevice %s", mString.string());
    if (!mString.isEmpty()) {
        sscanf(mString.string(), "isA2dpDevice=%d", &isA2dpDevice);
        if (isA2dpDevice) {
            return isA2dpDevice;
        }
    }
    return isA2dpDevice;
}

bool CTsPlayer::iStartPlay(){
    int ret;
    int filter_afmt;
    char value[PROPERTY_VALUE_MAX] = {0};

    LOGI("[%s:%d]\n", __FUNCTION__, __LINE__);
	
	numVideoDecodedFrames = 0;
    numPreVideoDecodedFrames = 0;
    
    numAudioDecodedSamples = 0;
    numPreAudioDecodedSamples = 0;

    numEncryptAudioDecodedSamples = 0;
    numPreEncryptAudioDecodedSamples = 0;
    
    audioDecoderIsRun = 0;
    encryptAudioDecoderIsRun = 0;
    videoDecoderIsRun = 0;
    
    checkVideoStateCount = 0;
    checkAudioStateCount = 0;
    checkEncryptAudioStateCount = 0;

    DisableVideoCount = 0;
    DisableAudioCount = 0;
    FrozenVideoNum = 0;
    ForzenAudioNum = 0;
    CurVideoNum = 0;
    CurAudioNum = 0;
    last_audio_state = get_audio_device_state();
    LOGI("[%s:%d] last_audio_state =%d\n", __FUNCTION__, __LINE__,last_audio_state);
    if (m_bIsPlay) {
        LOGI("[%s:%d] Already StartPlay: m_bIsPlay=%s\n", __FUNCTION__, __LINE__, (m_bIsPlay ? "true" : "false"));
        return true;
    }

	iGetPlayerAttribute();	//we get some attribute & whether dump file before start player 

    if (mIsEsVideo || mIsEsAudio) {
        ret = true;
    } else if (mIsTsStream){
        LOGI("StartPlay is TS, playmode: %s\n", prop_singlemode?"single":"multi");

	amsysfs_set_sysfs_int("/sys/class/tsync/enable", 1);
	amsysfs_set_sysfs_int("/sys/class/tsync/vpause_flag", 0); // reset vpause flag -> 0
	amsysfs_set_sysfs_int("/sys/class/video/show_first_frame_nosync", prop_show_first_frame_nosync);//keep last frame instead of show first frame
	amsysfs_set_sysfs_int("/sys/module/amvideo/parameters/horz_scaler_filter", 0xff);		
	amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/dec_control", dec_control);//force di 		
	amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/error_skip_reserve",H264_error_skip_reserve);
	amsysfs_set_sysfs_int("/sys/class/video/slowsync_flag",1);

	amsysfs_set_sysfs_int("/sys/class/video/video_global_output",1);

	amsysfs_set_sysfs_int("/sys/class/video/video_hdcp_finished", 0);

	memset(pcodec,0,sizeof(*pcodec));

	pcodec->stream_type = STREAM_TYPE_TS;

	if(prop_singlemode)
		pcodec->dec_mode = STREAM_TYPE_SINGLE;
	else
		pcodec->dec_mode = STREAM_TYPE_STREAM;
		//pcodec->dec_mode = STREAM_TYPE_FRAME;

	if((int)mVideoPara.pid != 0 && hasvideo != 0) {
		if (mVideoPara.vFmt == VFORMAT_H264) {
			pcodec->video_type = VFORMAT_H264;
			//malloc lpbuffer
			iMallocLpbuffer();
			pcodec->am_sysinfo.param = (void *)(0);
			property_set("vendor.sys.subtitleService.tvType", "2");
		} else if (mVideoPara.vFmt == VFORMAT_MPEG12) {
			pcodec->video_type = VFORMAT_MPEG12;
			property_set("vendor.sys.subtitleService.tvType", "3");
		} else if (mVideoPara.vFmt == VFORMAT_MPEG4) {
			pcodec->video_type = VFORMAT_MPEG4;
			pcodec->am_sysinfo.format= VIDEO_DEC_FORMAT_MPEG4_5;
			LOGI("VIDEO_DEC_FORMAT_MPEG4_5\n");
			property_set("vendor.sys.subtitleService.tvType", "3");
		} else if ((mVideoPara.vFmt == VFORMAT_HEVC)){// || (mVideoPara.vFmt == CT_VFORMAT_H265)) {
			pcodec->video_type = VFORMAT_HEVC;
		}else if(mVideoPara.vFmt == VFORMAT_H264_4K2K){
			pcodec->video_type = VFORMAT_H264_4K2K;
			pcodec->am_sysinfo.format = VIDEO_DEC_FORMAT_H264_4K2K;
			property_set("vendor.sys.subtitleService.tvType", "2");
		}
		else {
			LOGE("StartPlay mVideoPara.vFmt ERROR mVideoPara.vFmt=%d", mVideoPara.vFmt);
		}

		pcodec->has_video = 1;
		pcodec->video_pid = (int)mVideoPara.pid;
		pcodec->am_sysinfo.param = (void *)(0x04);
		pcodec->am_sysinfo.rate = 1;
                
		player_startsync_set(startmode);
	} else {
		pcodec->has_video = 0;
		pcodec->video_pid = -1;
	}

	
	//Buffering audio data for startplay 100ms/170ms/200ms
	memset(value, 0, PROPERTY_VALUE_MAX);
	if(!pcodec->has_video){
		property_set("media.amadec.prefilltime", "170");
		LOGI("StartPlay, for pureaudio audioprefilltime 170ms\n");
	}
	else{
		int prefilltime = 0;
    		property_get("sys.iptv.audioprefilltime", value, "0");
		prefilltime = atoi(value);
		sprintf(value,"%d",prefilltime);		
		property_set("media.amadec.prefilltime", value);
		LOGI("StartPlay, audioprefilltime value:%s\n", value);
	}

	if(IS_AUIDO_NEED_EXT_INFO(pcodec->audio_type)) {
		pcodec->audio_info.valid = 1;
		LOGI("set audio_info.valid to 1");
	}

	if(!m_bFast) {
		if(mAudioPara.pid != 0 && hasaudio != 0){
			filter_afmt = TsplayerGetAFilterFormat("media.amplayer.disable-acodecs");
			if(((1 << pcodec->audio_type) & filter_afmt) != 0) {
				LOGI("## filtered format audio_format=%d,----\n", pcodec->audio_type);
				pcodec->has_audio = 0;
			}

			pcodec->has_audio = 1;
			pcodec->audio_pid = mAudioPara.pid;
			if (mAudioPara.aFmt == AFORMAT_MPEG) {
				pcodec->audio_type = AFORMAT_MPEG;
			} else if (mAudioPara.aFmt == AFORMAT_AAC) {
				pcodec->audio_type = AFORMAT_AAC;
			} else if (mAudioPara.aFmt == AFORMAT_AC3) {
				pcodec->audio_type = AFORMAT_AC3;
			} else if (mAudioPara.aFmt == AFORMAT_EAC3) {
				pcodec->audio_type = AFORMAT_EAC3;
			} else if (mAudioPara.aFmt == AFORMAT_AAC_LATM) {
				pcodec->audio_type = AFORMAT_AAC_LATM;
			}
		}
		LOGI("pcodec->audio_samplerate: %d, pcodec->audio_channels: %d\n",
				pcodec->audio_samplerate, pcodec->audio_channels);
#ifdef PORTING_TEST
		pcodec->has_ctc = 1;
#endif
		if((int)sPara[0].pid != 0 && (int)sPara[0].sub_type <= CODEC_ID_MICRODVD) {//iner sub?
			pcodec->has_sub = 1;
			pcodec->sub_pid = (int)sPara[0].pid;
			setSubType(&sPara[0]);
			//subtitleSetSubType(5, 1);
			hasccsub = 0;
			LOGI("DVB sub, iner sub pid: %d \n", (int)sPara[0].pid);
		}

		if((int)sPara[0].pid != 0 && (int)sPara[0].sub_type == CODEC_ID_SCTE27){
			char value[PROPERTY_VALUE_MAX];
			memset(value, 0, PROPERTY_VALUE_MAX);
			LOGI("SCTE27, tvsub pid: %d and set the tvtype as 3\n", (int)sPara[0].pid);
			sprintf(value,"%d",(int)sPara[0].pid);
			hasSctesub = 1;
			property_set("vendor.sys.subtitleService.pid", value);
			property_set("vendor.sys.subtitleService.tvType", "3");
		}
		
#ifdef BUILD_WITH_VIEWRIGHT_STB
		if(prop_media_vmx_iptv == 1 && pcodec->has_sub != 1){
				pcodec->has_sub = 1;
				pcodec->sub_pid = 0x1ffe;
				amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",5);
				LOGI("vmx we need init dvb sub default for switch sub\n");
		}
#endif
		LOGI("pcodec->sub_pid: %d \n", pcodec->sub_pid);
        } else {
            pcodec->has_audio = 0;
            pcodec->audio_pid = -1;
        }
        LOGI("set vFmt:%d, aFmt:%d, vpid:%d, apid:%d\n", mVideoPara.vFmt, mAudioPara.aFmt, mVideoPara.pid, mAudioPara.pid);
        LOGI("set has_video:%d, has_audio:%d, has_sub:%d, video_pid:%d, audio_pid:%d\n", pcodec->has_video, pcodec->has_audio,
                pcodec->has_sub, pcodec->video_pid, pcodec->audio_pid);
        pcodec->noblock = 0;

		 if (!pcodec->has_audio) {
			 LOGI("has_audio is 0, disable tsync_enable\n");
			 amsysfs_set_sysfs_int("/sys/class/tsync/enable", 0);
		 }

	if(pcodec->dec_mode != STREAM_TYPE_STREAM){	
		ret = iCheckDecoderOccupied();
		if(ret < 0){
			LOGI("[%s:%d] decoder is already occupied, error to start play\n", __FUNCTION__, __LINE__);
			return false;
		}
	}

		#ifdef  BUILD_WITH_VIEWRIGHT_STB
	  	if(prop_media_vmx_iptv==1){ // startplay verimatrix stream
			iStartDrmPlayer();
			ret = 0;
   		}
   		else
		#endif
		if(prop_media_contax_iptv == 1 && pcodec->has_video == 1 && Dsc_dev_open){// startplay conax stream
        	memset(&inject_para, 0, sizeof(inject_para));
        	inject_para.vid_fmt = vPara.vFmt;
        	inject_para.aud_fmt = a_aPara[0].aFmt;
        	inject_para.pkg_fmt = PFORMAT_TS;
        	inject_para.vid_id = vPara.pid;
        	inject_para.aud_id = a_aPara[0].pid;
        	//inject_para.sub_id = sPara[0].pid;
        	//inject_para.sub_type = sfmt;

        	ret = AM_AV_StartInject(AV_DEV_NO, &inject_para);
        	if(ret != AM_SUCCESS)
        	LOGI("startplay av StartInject failed, errno:%d\n", ret);

        	Dsc_start_inject = 1;
        	LOGI("conax StartInject vFmt:%d, aFmt:%d, vpid:%d, apid:%d\n", vPara.vFmt, a_aPara[0].aFmt, vPara.pid, a_aPara[0].pid);
   		}else{	// startplay unencrypt stream
			LOGI("[%s %d] Start codec_init, pcodec->sub_handle :%d\n", __FUNCTION__, __LINE__, pcodec->sub_handle);
        		ret = codec_init(pcodec);
			codec_set_cntl_syncthresh(pcodec, pcodec->has_audio);
    		codec_set_freerun_mode(pcodec, 0);
			LOGI("[%s %d] After codec_init  ret %d\n", __FUNCTION__, __LINE__, ret);
   		}
		if(ret == 0) {
			if (m_nMode == M_LIVE) {
				if(m_isBlackoutPolicy)
					amsysfs_set_sysfs_int("/sys/class/video/blackout_policy",1);
				else
					amsysfs_set_sysfs_int("/sys/class/video/blackout_policy",0);
			}
			keep_vdec_mem = 0;
			amsysfs_set_sysfs_int("/sys/class/vdec/keep_vdec_mem", 1);

			//open subtitle
		
			pthread_attr_t attr_sub;
			pthread_attr_init(&attr_sub);
			pthread_create(&mSubThread, &attr_sub, threadSub, this);
			pthread_attr_destroy(&attr_sub);
		
			m_bIsPlay = true;
			m_bIsPause = false;
			m_bStop = false;
			LOGI("iStartPlay: codec init ok. create threadSub ok. m_bIsPlay is true\n");
		}

		m_bWrFirstPkg = true;
		m_bchangeH264to4k = false;
		writecount = 0;
		m_StartPlayTimePoint = ::android::ALooper::GetNowUs();
#ifdef PORTING_TEST
		codec_amvideo_dev_open();
#endif
		LOGI("iStartPlay: m_StartPlayTimePoint = %lld, pcodec->has_sub:%d, hasccsub:%d\n", m_StartPlayTimePoint, pcodec->has_sub, hasccsub);

        if (!pcodec->has_audio && !m_bFast) {
            LOGI("no audio need buffer data 500ms\n");
            enable_decoder_buffer(pcodec);
        }
        else
            disable_decoder_buffer(); /* not need to buffer data */

		LOGI("iStartPlay: end\n");
		return !ret;
	}else{
        LOGE("iStartPlay error is no es or ts");
       	return false;
    }
    return !ret;
}

int CTsPlayer::WriteData(PLAYER_STREAMTYPE_E type, unsigned char *pBuffer, unsigned int nSize, unsigned long int timestamp){
    int ret = -1;
    static int retry_count = 0;
    buf_status audio_buf;
    buf_status video_buf;
    float audio_buf_level = 0.00f;
    float video_buf_level = 0.00f;
    codec_para_t *pcodec = nullptr;

    if(!m_bIsPlay)
        return -1;
	lp_lock(&mutex);

 	if (type == PLAYER_STREAMTYPE_VIDEO){
        pcodec = m_vpcodec;
        while (1) {
            codec_get_vbuf_state(pcodec, &video_buf);
            //ALOGI("WriteData : video_buf.data_len=%d, timestamp=%lld", video_buf.data_len, timestamp);
            if (video_buf.data_len > 0x1000*1000) { //4M
                usleep(20*1000);
            } else {
                codec_checkin_pts(pcodec,  timestamp);
                break;
            }
        }

    } else if (type == PLAYER_STREAMTYPE_AUDIO) {
        pcodec = m_apcodec;
        while (1) {
            codec_get_abuf_state(pcodec, &audio_buf);
            //ALOGI("WriteData : audio_buf.data_len=%d, timestamp=%lld", audio_buf.data_len, timestamp);
            if (audio_buf.data_len > 0x1000*250*2) { //2M
                usleep(20*1000);
            } else {
                codec_checkin_pts(pcodec,  timestamp);
                break;
            }
        }
    } 
   // ALOGI("WriteData: nSize=%d, type=%d,audio_buf.data_len: %d, video_buf.data_len: %d,timestamp=%lld\n",  nSize, type, audio_buf.data_len, video_buf.data_len, timestamp/90);

    int temp_size = 0;
    for(int retry_count=0; retry_count<10; retry_count++) {
        ret = codec_write(pcodec, pBuffer+temp_size, nSize-temp_size);
        if((ret < 0) || (ret > nSize)) {
            if(ret < 0 && errno == EAGAIN) {
                usleep(10);
                continue;
            } else {
                ALOGI("WriteData: codec_write return %d!\n", ret);
                if(pcodec && pcodec->handle > 0){
                    ret = codec_close(pcodec);
                    ret = codec_init(pcodec);
                    if(m_bFast && (type==PLAYER_STREAMTYPE_TS)) {
                        codec_set_mode(pcodec, TRICKMODE_I);
                    }
                    ALOGI("WriteData : codec need close and reinit m_bFast=%d\n", m_bFast);
                } else {
                    ALOGI("WriteData: codec_write return error or stop by called!\n");
                    break;
                }
            }
        } else {
            temp_size += ret;
            if(temp_size >= nSize){
                temp_size = nSize;
                break;
            }
            // release 10ms to other thread, for example decoder and drop pcm
            usleep(10000);
        }
    }
    if (ret >= 0 && temp_size > ret)
        ret = temp_size; // avoid write size large than 64k size
    lp_unlock(&mutex);

    if(ret > 0) {
        if ((type == PLAYER_STREAMTYPE_VIDEO)||(type == PLAYER_STREAMTYPE_TS)){
           if(dumpvfd != NULL)     
                fwrite(pBuffer, 1, temp_size, dumpvfd);
        }else{ 
        	if(dumpafd != NULL)
                fwrite(pBuffer, 1, temp_size, dumpafd);
        }
    } else {
        ALOGI("WriteData: codec_write fail(%d),temp_size[%d] nSize[%d]\n", ret, temp_size, nSize);
        if (temp_size > 0){
            if ((type == PLAYER_STREAMTYPE_VIDEO)||(type == PLAYER_STREAMTYPE_TS)){
                if(dumpvfd != NULL)   
                    fwrite(pBuffer, 1, temp_size, dumpvfd);
            }else{
                if(dumpafd != NULL) 
                    fwrite(pBuffer, 1, temp_size, dumpafd);
            }
            return temp_size;
        } else
            return -1;
    }
    return ret;
}

static unsigned long totalSize = 0;

int CTsPlayer::WriteData(unsigned char* pBuffer, unsigned int nSize){
    int ret = -1;
    int temp_size = 0;
    static int retry_count = 0;
	int *pSize = (int *)&nSize;

	//static unsigned long totalSize = 0;
	static int64_t nowTime = 0;
	static int64_t lastTime = 0;

    if(!m_bIsPlay || m_bchangeH264to4k || m_bStop)
        return -1;
	
    lp_lock(&mutex);
	
	nowTime = ::android::ALooper::GetNowUs();
	
	//for encrypted stream Verimatrix
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
        if(prop_media_vmx_iptv==1) {
           ret = WriteDataVmx( pBuffer,  nSize, 0);
           lp_unlock(&mutex);    
           if(ret > 0) {
     		   	vmx_write_size_per_sec += ret;
               	if(writecount >= MAX_WRITE_COUNT) {
                   m_bWrFirstPkg = false;
                   writecount = 0;
               	}
              	if(m_bWrFirstPkg == true) {
                 writecount++;
               	}
           } else {
           		LOGI("verimax WriteData failed, errno:%d\n", ret);
                return -1;
           }
           return ret;		   
       }  
	#endif

	//for encrypted stream conax
	if(prop_media_contax_iptv == 1){
		   ret = AM_AV_InjectData(DSC_DEV_NO, AM_AV_INJECT_MULTIPLEX, pBuffer, pSize, -1);
           lp_unlock(&mutex);
           if(ret == AM_SUCCESS) {
		   		totalSize += *pSize;
				if(prop_shouldshowlog && ((nowTime - lastTime)/1000 >= 1000)){
		   			LOGI("conax WriteData success, nSize:%d, totalSize:%d\n", *pSize, totalSize);
					lastTime = nowTime;
				}		   	
           } else {
                if(prop_shouldshowlog && ((nowTime - lastTime)/1000 >= 1000)){
           		    LOGI("conax WriteData failed, errno:%d, totalSize:%d\n", ret, totalSize);
					lastTime = nowTime;
                }
           }
           return ret;
	}

	//for unencrypted stream
	
	ret = checkBuffLevel();
	if(ret == -1){
		lp_unlock(&mutex);
		LOGI("WriteData checkBuffLevel overflow -1\n");
		return ret;
	}
	
    for(int retry_count=0; retry_count<10; retry_count++){
            ret = codec_write(pcodec, pBuffer+temp_size, nSize-temp_size);
            if((ret < 0) || (ret > nSize)) {
                if(ret < 0 && (errno == EAGAIN)){
                    LOGI("WriteData: codec_write counts: %d\n", retry_count);
                    break;
                    /*for Mirada test APK
                    usleep(10);
                    LOGI("WriteData: codec_write return EAGAIN!\n");
                    continue;
                    */
                } else {
                    // if(pcodec->handle > 0){
                    //     ret = codec_close(pcodec);
                    //     ret = codec_init(pcodec);
                    //     if(m_bFast) {
                    //         codec_set_mode(pcodec, TRICKMODE_I);
                    //     }
                    //     LOGI("WriteData : codec need close and reinit m_bFast=%d\n", m_bFast);
                    // } else {
                        LOGI("WriteData: codec_write return error or stop by called!\n");
                        break;
                    // }
                }
            } else {
                temp_size += ret;
				totalSize += temp_size;
				if(prop_shouldshowlog && ((nowTime - lastTime)/1000 >= 1000)){
		   			LOGI("Unencrypted writeData success, nSize:%d, totalSize:%d\n", nSize, totalSize);
					lastTime = nowTime;
				}
                if(temp_size >= nSize){
                    temp_size = nSize;
                    break;
                }
                // release 10ms to other thread, for example decoder and drop pcm
                usleep(10000);
            }
    }

    lp_unlock(&mutex);

    if(ret > 0) {
        if((dumpvfd != NULL) && (temp_size > 0)) {
            fwrite(pBuffer, 1, temp_size, dumpvfd);
            LOGI("ret[%d] temp_size[%d] nSize[%d] %d!\n", ret, temp_size, nSize);
        }
        if(writecount >= MAX_WRITE_COUNT) {
            m_bWrFirstPkg = false;
            writecount = 0;
        }

        if(m_bWrFirstPkg == true) {
            writecount++;
        }
    } else {
        LOGE("Unencrypted WriteData codec_write failed(%d)\n", ret);
        return -1;
    }
    return ret;
}

bool CTsPlayer::Pause(){
      m_bIsPause = true;
#ifdef  BUILD_WITH_VIEWRIGHT_STB
     if(prop_media_vmx_iptv==1){
        return VmxPause( ) ;
     }
#endif
    codec_pause(pcodec);
    return true;
}

bool CTsPlayer::Resume(){
    m_bIsPause = false;
     
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
        return VmxResume( ) ;
    }
#endif
    codec_resume(pcodec);
    return true;
}

#define AML_VFM_MAP "/sys/class/vfm/map"

static int add_di(){
    amsysfs_set_sysfs_str(AML_VFM_MAP, "rm default");
    amsysfs_set_sysfs_str(AML_VFM_MAP, "add default decoder ppmgr deinterlace amvideo");
    return 0;
}

static int remove_di(){
    amsysfs_set_sysfs_str(AML_VFM_MAP, "rm default");
    amsysfs_set_sysfs_str(AML_VFM_MAP, "add default decoder ppmgr amvideo");
    return 0;
}
bool CTsPlayer::Fast(){
    int ret;

    ret = amsysfs_set_sysfs_int("/sys/class/video/blackout_policy", 0);
    if(ret)
        return false;
    keep_vdec_mem = 1;
	
    iStop();
    m_bFast = true;

	//di bypass
    amsysfs_set_sysfs_int("/sys/module/di/parameters/bypass_trick_mode", 2);
    amsysfs_set_sysfs_int("/sys/module/di/parameters/start_frame_drop_count",0);
	
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
        LOGI("Verimatrix Fast\n");
		if(mVideoPara.vFmt == VFORMAT_H264)
        	amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/decoder_force_trickmode", 1);
		amsysfs_set_sysfs_int("/sys/class/video/freerun_mode", 1);
        ret = iStartPlay();
        if(!ret)
            return false;
        else 
            return true;
    }
#endif

    ret = iStartPlay();
    if(!ret)
        return false;

    LOGI("Unencrypt Fast, pcodec handle: %d\n", pcodec->handle);

	//disable av-sync
    amsysfs_set_sysfs_int("/sys/class/tsync/enable", 0); 
    codec_set_freerun_mode(pcodec, 1);
	//
	
    if(pcodec->video_type == VFORMAT_HEVC) 
        ret = codec_set_mode(pcodec, TRICKMODE_I_HEVC); 
    else 
        ret = codec_set_mode(pcodec, TRICKMODE_I);
	
	if(ret != CTC_SUCESS)
		LOGI("Unencrypt Fast, set trickmode I failed, errno:%d\n", ret);
    return !ret;
}

bool CTsPlayer::StopFast(){
    int ret;

    LOGI("StopFast");
    if(pcodec->has_sub == 1)
        subtitleResetForSeek();
	
    m_bFast = false;

#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv!=1){
#endif
        ret = codec_set_mode(pcodec, TRICKMODE_NONE);
		ret = codec_set_freerun_mode(pcodec, 0);
    	if(ret){
        	LOGI("error stopfast set trickNone/freerun_mode 0 failed !\n");
    	}
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    }else {
	    amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/decoder_force_trickmode", 0);
		amsysfs_set_sysfs_int("/sys/class/video/freerun_mode", 1);
    }
#endif
    keep_vdec_mem = 1;
    iStop();
	//amsysfs_set_sysfs_int("/sys/module/di/parameters/bypass_all", 0);
    amsysfs_set_sysfs_int("/sys/module/di/parameters/bypass_trick_mode", 1);
    amsysfs_set_sysfs_int("/sys/class/tsync/enable", 1);
    ret = iStartPlay();
    if(!ret)
        return false;
    if(m_isBlackoutPolicy) {
        ret = amsysfs_set_sysfs_int("/sys/class/video/blackout_policy",1);
        if (ret)
            return false;
	}

    return true;
}
bool CTsPlayer::Stop(){
        int ret = true;
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
        if(prop_media_vmx_iptv!=1)
            codec_set_freerun_mode(pcodec, 0);
	#endif
        if(m_bFast){
           LOGI("[%s %d] iStop m_bFast is %d StopFast.\n", __FUNCTION__, __LINE__,m_bFast);
           StopFast();
        }

		if (!m_bIsPlay) {
            LOGI("already is Stoped\n");
            return true;
        }
		LOGI("[%s %d] start\n", __FUNCTION__, __LINE__);

		if (prop_async_stop) {
	        //make srue last async thread exit
	        async_flag = 1;
	        lp_lock(&mutex);
	        LOGE("[%s:%d] async stop locked\n",__FUNCTION__, __LINE__);
	        if (m_AStopPending) {
	            LOGE("[%s:%d] async stop not release err\n",__FUNCTION__, __LINE__);
	            lp_unlock(&mutex);
	            return ret;
	        }
	        m_AStopPending = 1; //enter async stop
	        pthread_cond_signal(&m_pthread_cond);
	        lp_unlock(&mutex);
	        LOGI("stop  async out\n");
	    } else {
	    	lp_lock(&mutex);
        	ret =  iStop();
			lp_unlock(&mutex);
	        LOGI("stop  sync ret:%d",ret);
	    }
		amsysfs_set_sysfs_int(CTC_EXIST, 0);
		memset(&mAudioPara, 0, sizeof(mAudioPara));
    	memset(&mVideoPara, 0, sizeof(mVideoPara));
		memset(sPara,0,sizeof(SUBTITLE_PARA_T)*MAX_SUBTITLE_PARAM_SIZE);
		LOGI("[%s %d] end\n", __FUNCTION__, __LINE__);
        return ret;
}

void * CTsPlayer::stop_thread(void )
{
    do {
        struct timeval now;
        struct timespec pthread_ts;
        LOGI("enter stop exit");
        lp_lock(&mutex);
        while (!m_AStopPending&&!m_StopThread) {
            gettimeofday(&now, NULL);
            pthread_ts.tv_sec = now.tv_sec + (20000 + now.tv_usec) / 1000000;
            pthread_ts.tv_nsec = ((20000 + now.tv_usec) * 1000) % 1000000000;
            pthread_cond_timedwait(&m_pthread_cond, &mutex, &pthread_ts);
        }
        lp_unlock(&mutex);
        if (m_AStopPending) {
            iStop();
            lp_lock(&mutex);
            if (clearlastframe_flag) {
                clearlastframe_flag = 0;
                async_flag = 0;
                ClearLastFrame();
            }
            m_AStopPending = 0;
            pthread_cond_signal(&m_pthread_cond);
            lp_unlock(&mutex);
            LOGI("stop real exit");
        }
    }while (!m_StopThread);
    LOGI("stop_thread tid :%d exit",gettid());
    return NULL;
}
void * CTsPlayer::init_thread(void *args)
{
    CTsPlayer *player = (CTsPlayer *)args;
    LOGI("enter stop_thread");
    return   player->stop_thread();

}


bool CTsPlayer::iStop(){    
    int ret = CTC_SUCESS;
    int dec_control = 0;

    DisableVideoCount = 0;
    DisableAudioCount = 0;
    FrozenVideoNum = 0;
    ForzenAudioNum = 0;
    CurVideoNum = 0;
    CurAudioNum = 0;

    property_set("vendor.sys.subtitleservice.afdValue", "0");
    
	property_set("config.media.ctc.updatevideoaxis", "0");
	amsysfs_set_sysfs_int("/sys/class/video/video_hdcp_finished", 0);

    LOGI("[%s %d] keep_vdec_mem: %d, m_bIsPlay:%d\n", __FUNCTION__, __LINE__, keep_vdec_mem, m_bIsPlay);
    amsysfs_set_sysfs_int("/sys/class/vdec/keep_vdec_mem", keep_vdec_mem);
	amsysfs_set_sysfs_int("/sys/module/amvideo/parameters/horz_scaler_filter", 2);
    amsysfs_set_sysfs_int("/sys/module/di/parameters/start_frame_drop_count",2);
    amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/error_skip_divisor", 0);
	amsysfs_set_sysfs_int("/sys/module/amvideo/parameters/toggle_count", 0);
	
	//上海电信4K小窗口向下偏移
	if (3 == amsysfs_get_sysfs_int("/sys/module/amvdec_h265/parameters/double_write_mode"))
		amsysfs_set_sysfs_int("/sys/module/amvdec_h265/parameters/double_write_mode", 0);
    //Close force to DI.
    amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/dec_control", dec_control);

    if(m_bIsPlay) {
	LOGI("m_bIsPlay is true\n");
	if(dumpvfd != NULL) {
		fclose(dumpvfd);
		dumpvfd = NULL;
	}

		// close subtitle
		if(subOpened) {
			subtitleHide();
			subtitleClose();
		}
		LOGI("subtitle closed\n");
		pthread_join(mSubThread, NULL);
		
        if(m_bIsPlay == false){ //avoid twice stop
            LOGI("Already stop return\n");
            return true;
        }

    	if (!pcodec->has_audio && !m_bFast) {
        LOGI("no audio set data buffer clear\n");
        disable_decoder_buffer();
    	}
	
	  m_bStop = true;
	  m_bFast = false;    
      m_bIsPause = false;
      m_StartPlayTimePoint = 0;
      m_PreviousOverflowTime = 0;
	  m_isSurfaceWindow = false;
		
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
        if(prop_media_vmx_iptv==1){
            StopPlayIPTV();			
        }
        else {
    #endif
		if(prop_media_contax_iptv == 1 && Dsc_dev_open)
            ret = iFreeChannelAndDscClose();
		else{
			LOGI("[%s %d] codec_close start\n", __FUNCTION__, __LINE__);
         	ret = codec_close(pcodec);
		}
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
	    }
	#endif
        pcodec->handle = -1;
        LOGI("[%s %d] ret:%d\n", __FUNCTION__, __LINE__, ret);
        m_bWrFirstPkg = true;

        if (lpbuffer_st.buffer != NULL){
            free(lpbuffer_st.buffer);
            lpbuffer_st.buffer = NULL;
            lpbuffer_st.rp = NULL;
            lpbuffer_st.wp = NULL;
            lpbuffer_st.bufferend = NULL;
            lpbuffer_st.enlpflag = 0;
            lpbuffer_st.valid_can_read = 0;
       }
#ifdef PORTING_TEST
	   codec_amvideo_dev_close();
#endif
	   m_bIsPlay = false;
    } else {
        LOGI("[%s %d] m_bIsPlay is false\n", __FUNCTION__, __LINE__);
    }
	
    v_overflows = 0;
    v_underflows = 0;
    a_underflows = 0;
    a_overflows = 0;

    last_anumdecoderErrors = 0;
    last_anumdecoder = 0;
	last_vnumdecoderErrors = 0;
	last_vnumdecoder = 0;
	
    totalSize = 0;
    hasSctesub = 0;
    subOpened = 0;
	
    return true;
}

bool CTsPlayer::Seek(){
    LOGI("Seek");
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
        LOGI("vmx iptv seek\n");
    }	
#endif
    if(m_isBlackoutPolicy)
        amsysfs_set_sysfs_int("/sys/class/video/blackout_policy",1);
    iStop();
    //usleep(500*1000);
    iStartPlay();
    return true;
}

int CTsPlayer::GetVolume(){
    float volume = 1.0f;
    int ret;

    LOGI("[%s %d] ", __FUNCTION__, __LINE__);
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
		LOGI("[%s %d] verimatrix getVolume", __FUNCTION__, __LINE__);
        return VmxGetVolume();
    }
#endif
    ret = codec_get_volume(pcodec, &volume);
    if(ret < 0) {
        return m_nVolume;
    }
    int nVolume = (int)(volume * 100);
    if(nVolume <= 0)
        return m_nVolume;
	
    LOGI("[%s %d] volume:%d \n", __FUNCTION__, __LINE__, nVolume);
    return nVolume;
}

bool CTsPlayer::SetVolume(int volume){	
    LOGI("[%s %d] volume:%d", __FUNCTION__, __LINE__, volume);

	if(0 == volume)
		audio_hide = 1;
	else
		audio_hide = 0;

#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
		LOGI("[%s %d] verimatrix setVolume", __FUNCTION__, __LINE__);
        return VmxSetVolume(volume) ;
    }
#endif
    int ret = codec_set_volume(pcodec, (float)volume/100.0);
    m_nVolume = volume;
    return true;
}

//set sound track 
//input paramerter: nAudioBlance, 1, Left Mono; 2, Right Mono; 3, Stereo; 4, Sound Mixing
bool CTsPlayer::SetAudioBalance(int nAudioBalance){
#if 0
    if((nAudioBalance < 1) && (nAudioBalance > 4))
        return false;
#endif

    m_nAudioBalance = nAudioBalance;     
#ifdef  BUILD_WITH_VIEWRIGHT_STB
     if(prop_media_vmx_iptv==1){
            return VmxSetAudioBalance(nAudioBalance) ;
     }
#endif

    if(nAudioBalance == 1) {
        LOGI("SetAudioBalance 1 Left Mono\n");
        //codec_left_mono(pcodec);
        codec_lr_mix_set(pcodec, 0);
         amsysfs_set_sysfs_str("/sys/class/amaudio/audio_channels_mask", "l");
		 audio_output_type = 1;
    } else if(nAudioBalance == 2) {
        LOGI("SetAudioBalance 2 Right Mono\n");
        //codec_right_mono(pcodec);
        codec_lr_mix_set(pcodec, 0);
        amsysfs_set_sysfs_str("/sys/class/amaudio/audio_channels_mask", "r");
		audio_output_type = 2;
    } else if(nAudioBalance == 3) {
        LOGI("SetAudioBalance 3 Stereo\n");
        //codec_stereo(pcodec);
        codec_lr_mix_set(pcodec, 0);
        amsysfs_set_sysfs_str("/sys/class/amaudio/audio_channels_mask", "s");
		audio_output_type = 3;
    } else if(nAudioBalance == 4) {
        LOGI("SetAudioBalance 4 Sound Mixing\n");
        //codec_stereo(pcodec);
        codec_lr_mix_set(pcodec, 1);
        //amsysfs_set_sysfs_str("/sys/class/amaudio/audio_channels_mask", "c");
		audio_output_type = 4;
    } else {
		audio_output_type = 0;
	}
		
    return true;
}

/*	get current sound track
	return parameter: 1, Left Mono; 2, Right Mono; 3, Stereo; 4, Sound Mixing
*/
int CTsPlayer::GetAudioBalance(){
    return m_nAudioBalance;
}

void CTsPlayer::GetVideoPixels(int& width, int& height){
    int x = 0, y = 0;
    OUTPUT_MODE output_mode = iGetDisplayMode();
    iGetPosition(output_mode, &x, &y, &width, &height);
    LOGI("[%s %d] by iGetPosition, x: %d, y: %d, width: %d, height: %d", __FUNCTION__, __LINE__, x, y, width, height);
}

bool CTsPlayer::SetRatio(int nRatio){
    char writedata[40] = {0};
    int width = 0;
    int height = 0;
    int new_x = 0;
    int new_y = 0;
    int new_width = 0;
    int new_height = 0;
    int mode_x = 0;
    int mode_y = 0;
    int mode_width = 0;
    int mode_height = 0;
    vdec_status vdec;
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
        VmxGet_vdec_state(&width,&height);
     }else {
#endif
        codec_get_vdec_state(pcodec,&vdec);
        width = vdec.width;
        height = vdec.height;
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    }
#endif

    LOGI("SetRatio width: %d, height: %d, nRatio: %d\n", width, height, nRatio);
    OUTPUT_MODE output_mode = iGetDisplayMode();
    iGetPosition(output_mode, &mode_x, &mode_y, &mode_width, &mode_height);
    
    if((nRatio != 255) && (amsysfs_get_sysfs_int("/sys/class/video/disable_video") == 1))
        amsysfs_set_sysfs_int("/sys/class/video/disable_video", 2);
    if(nRatio == 1) {	 //Full screen
        new_x = mode_x;
        new_y = mode_y;
        new_width = mode_width;
        new_height = mode_height;
        sprintf(writedata, "%d %d %d %d", new_x, new_y, new_x +new_width - 1, new_y+new_height - 1);
        amsysfs_set_sysfs_str("/sys/class/video/axis", writedata);
        return true;
    } else if(nRatio == 2) {	//Fit by width
        new_width = mode_width;
        new_height = int(mode_width*height/width);
        new_x = mode_x;
        new_y = mode_y + int((mode_height-new_height)/2);
        LOGI("SetRatio new_x: %d, new_y: %d, new_width: %d, new_height: %d\n"
                , new_x, new_y, new_width, new_height);
        sprintf(writedata, "%d %d %d %d", new_x, new_y, new_x+new_width-1, new_y+new_height-1);
        amsysfs_set_sysfs_str("/sys/class/video/axis",writedata);
        return true;
    } else if(nRatio == 3) {	//Fit by height
        new_width = int(mode_height*width/height);
        new_height = mode_height;
        new_x = mode_x + int((mode_width - new_width)/2);
        new_y = mode_y;
        LOGI("SetRatio new_x: %d, new_y: %d, new_width: %d, new_height: %d\n"
                , new_x, new_y, new_width, new_height);
        sprintf(writedata, "%d %d %d %d", new_x, new_y, new_x+new_width-1, new_y+new_height-1);
        amsysfs_set_sysfs_str("/sys/class/video/axis", writedata);
        return true;
    } else if(nRatio == 255) {
        amsysfs_set_sysfs_int("/sys/class/video/disable_video", 1);
        return true;
    }
    return false;
}

bool CTsPlayer::IsSoftFit(){
    return m_isSoftFit;
}

void CTsPlayer::SetEPGSize(int w, int h){
    LOGI("SetEPGSize: w=%d, h=%d, m_bIsPlay=%d\n", w, h, m_bIsPlay);
    m_nEPGWidth = w;
    m_nEPGHeight = h;
    if(!m_isSoftFit && !m_bIsPlay){
        InitOsdScale(m_nEPGWidth, m_nEPGHeight);
    }
}

void CTsPlayer::SwitchAudioTrack(int pid){
    int count = 0;
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(m_bIsPlay&&prop_media_vmx_iptv==1){
		 a_aPara[0].pid = pid;
         return VmxSwitchAudioTrack(pid, pcodec->audio_type) ;
     }
#endif
    while (count < MAX_AUDIO_PARAM_SIZE && (a_aPara[count].pid != pid) &&(a_aPara[count].pid != 0)) {
        count++;
    }

    if(!m_bIsPlay)
        return;

    lp_lock(&mutex);
	codec_audio_automute(pcodec->adec_priv, 1);
    codec_close_audio(pcodec);
    pcodec->audio_pid = 0xffff;

    if(codec_set_audio_pid(pcodec)) {
        LOGE("set invalid audio pid failed\n");
        lp_unlock(&mutex);
        return;
    }

    if(count < MAX_AUDIO_PARAM_SIZE) {
        pcodec->has_audio = 1;
        pcodec->audio_type = a_aPara[count].aFmt;
        pcodec->audio_pid = (int)a_aPara[count].pid;
		a_aPara[0].pid = a_aPara[count].pid;
    }
    LOGI("SwitchAudioTrack pcodec->audio_samplerate: %d, pcodec->audio_channels: %d\n", pcodec->audio_samplerate, pcodec->audio_channels);
    LOGI("SwitchAudioTrack pcodec->audio_type: %d, pcodec->audio_pid: %d\n", pcodec->audio_type, pcodec->audio_pid);

    //codec_set_audio_pid(pcodec);
    if(IS_AUIDO_NEED_EXT_INFO(pcodec->audio_type)) {
        pcodec->audio_info.valid = 1;
        LOGI("set audio_info.valid to 1");
    }

    if(codec_audio_reinit(pcodec)) {
        LOGE("reset init failed\n");
        lp_unlock(&mutex);
        return;
    }

    if(codec_reset_audio(pcodec)) {
        LOGE("reset audio failed\n");
        lp_unlock(&mutex);
        return;
    }
    codec_resume_audio(pcodec, 1);
    codec_audio_automute(pcodec->adec_priv, 0);
    lp_unlock(&mutex);
}

void CTsPlayer::SwitchSubtitle(int pid, int subtype/*, int channelid*/){
	char value[PROPERTY_VALUE_MAX];
	memset(value, 0, PROPERTY_VALUE_MAX);
	int channelid = 0;
	LOGE("[%s:%d] start, pid:%d, subtype:0x%x\n", __FUNCTION__, __LINE__, pid, subtype);

    amsysfs_set_sysfs_int("/sys/class/subtitle/width", 0);
    amsysfs_set_sysfs_int("/sys/class/subtitle/height", 0);
	
    if(m_bFast || !m_bIsPlay || m_bStop){
		LOGI( "switch subtitle,return back when fast or stop state\n");
		return;
	}
	subtitleResetForSeek();
	subtitleClose();
	//subtitleHide();
	usleep(1000*30);
	if(pid != 0 && subtype == CODEC_ID_SCTE27){//for scte27
		sprintf(value,"%d",pid);
		LOGI("scte27 pid :%s\n", value);
		property_set("vendor.sys.subtitleService.closecaption.channelId", "15");
		property_set("vendor.sys.subtitleService.tvType", "3");
		property_set("vendor.sys.subtitleService.pid", value);
		amsysfs_set_sysfs_int("/sys/class/subtitle/total",0);
		amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",0);
		subtitleSetSubType(0, 0);    //add for interface to pass
		if(pcodec->has_sub)
			pcodec->has_sub = 0;
		sPara[0].pid = pid;
		sPara[0].sub_type = subtype;
		hasSctesub = 1;		
	}else if(pid != 0 && subtype <= CODEC_ID_MICRODVD){//for inner sub
		LOGI("dvb pid :%d has_sub:%d\n", pid, pcodec->has_sub);
		
		property_set("vendor.sys.subtitleService.tvType", "-1");
		pcodec->sub_pid = pid;
		pcodec->sub_type = subtype;
		sPara[0].pid = pid;
		sPara[0].sub_type = subtype;
		//setSubType(&sPara[0]);
		subtitleSetSubType(5, 1);    //need set type, total ,open dvb
#ifdef BUILD_WITH_VIEWRIGHT_STB
		if(prop_media_vmx_iptv == 1){
			int err = 0;
			LOGI("#### vmx switch sub to dvb\n");
			err= VmxSwitchSubtitle(pid, subtype);
			amsysfs_set_sysfs_int("/sys/class/subtitle/total",1);
			setSubType(&sPara[0]);
			if(err!=0)
				 LOGE("[%s:%d]vmx set invalid sub pid failed\n", __FUNCTION__, __LINE__);
		} else
#endif
        {
		if(pcodec != NULL && !pcodec->has_sub && hasSctesub){
		       amsysfs_set_sysfs_int("/sys/class/subtitle/total",1);
		       setSubType(&sPara[0]);
			LOGI("#### scte27 switch to dvb\n");
		}else if (pcodec->has_sub == 1) {
			LOGI("#### dvb sub switch to dvb\n");
		} else {
		       amsysfs_set_sysfs_int("/sys/class/subtitle/total",1);
		       setSubType(&sPara[0]);
                    LOGI("#### no sub switch to dvb\n");
		}
		codec_set_sub_id(pcodec);
		codec_set_sub_type(pcodec);
		//set_sub_format(pcodec);
		if (pcodec->sub_handle >= 0) {
        		codec_close_sub_fd(pcodec->sub_handle);
			LOGI("### close sub fd\n");
    		}
        	codec_init_sub(pcodec);
		LOGI("### after sub codec init\n");
		}
		hasSctesub = 0;
		hasccsub = 0;
		pcodec->has_sub = 1;
		///usleep(1000*100);//for sometimes after open, close message have send to sub java layer
	}else if(subtype == CODEC_ID_CLOSEDCAPTION){
		if(pcodec->has_sub == 1){
			LOGI("[%s:%d] dvb switch to cc\n", __FUNCTION__, __LINE__);
			pcodec->has_sub = 0;
			amsysfs_set_sysfs_int("/sys/class/subtitle/total",0);
			amsysfs_set_sysfs_int("/sys/class/subtitle/subtype",0);
		}
		else if(!pcodec->has_sub && hasSctesub){
			LOGI("[%s:%d] scte27 switch to cc\n", __FUNCTION__, __LINE__);
 			hasSctesub = 0;
		}
		else if(!pcodec->has_sub)
			LOGI("[%s:%d] no sub switch to cc\n", __FUNCTION__, __LINE__);
 		subtitleSetSubType(0, 0);
		sprintf(value,"%d",channelid);
		property_set("vendor.sys.subtitleService.closecaption.channelId", value);
		property_set("vendor.sys.subtitleService.tvType", "2");
		memset(value, 0, PROPERTY_VALUE_MAX);
		sprintf(value,"%d",mVideoPara.vFmt);
		LOGI("[%s:%d] cc vfmt:%d\n", __FUNCTION__, __LINE__, mVideoPara.vFmt);
		property_set("vendor.sys.subtitleService.closecaption.vfmt", value);
		hasccsub = 1;
 	}
        subtitleOpen("", this);

	//The logic for obtaining vpts is implemented in the subtitle java layer, no need to pass from ctc !!!
	#if 0 /*will cause subtitle crash*/

	//if(hasccsub == 1 || hasSctesub == 1 || pcodec->has_sub == 1){
	if((pcodec->has_sub == 1 && subtype == CODEC_ID_DVB_SUBTITLE) || (pcodec->has_sub == 0 && subtype == CODEC_ID_SCTE27)){
		//SubtitleShowHide(1);
		subtitleShow();
		LOGI("[%s:%d] need show subtitle subtype:%x\n", __FUNCTION__, __LINE__, subtype);
	}
	#endif
	if(hasccsub == 1 && ccsubstatus == 0 && subtype == CODEC_ID_CLOSEDCAPTION)
		subtitleHide();
	LOGI("hasccsub:%d, hasSctesub:%d, has_sub:%d, ccsubstatus:%d\n", hasccsub, hasSctesub, pcodec->has_sub, ccsubstatus);
	LOGE("[%s:%d] end\n", __FUNCTION__, __LINE__);
}
void CTsPlayer::SwitchSubtitle(int pid){
	int ret;

    amsysfs_set_sysfs_int("/sys/class/subtitle/width", 0);
    amsysfs_set_sysfs_int("/sys/class/subtitle/height", 0);

	if(m_bFast || !m_bIsPlay || m_bStop){
		LOGI( "switch subtitle,return back when fast or stop state\n");
		return;
	}
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    LOGI("SwitchSubtitle be called pid is %d, vmx:%d\n", pid, prop_media_vmx_iptv);
     if(prop_media_vmx_iptv==1){
       int countvmx=0, subtype=0;
       PSUBTITLE_PARA_T pSubtitleParavmx=sPara;
       while((pSubtitleParavmx->pid != 0) && (countvmx < MAX_SUBTITLE_PARAM_SIZE)) {
            if (pSubtitleParavmx->pid == pid) {
                subtype=pSubtitleParavmx->sub_type;
                break;
            }
            countvmx++;
            pSubtitleParavmx++;
       }
       ret= VmxSwitchSubtitle(pid, subtype);
        if(ret!=0)
            LOGE("[%s:%d]vmx set invalid sub pid failed\n", __FUNCTION__, __LINE__);     			
        return  ;
     }
#else
	LOGI("SwitchSubtitle be called pid is %d,\n", pid);
#endif

    //liuzhi20170124:restart for avoiding no subtitle.
    if (pcodec->has_sub == 0 && m_StartPlayTimePoint > 0) {
        LOGI("switch subtitle but ctc inited without sub. do reset\n");
        iStop();
        iStartPlay();
        return;
    }

    if (pcodec->has_sub == 1){
		LOGE("[%s:%d]\n", __FUNCTION__, __LINE__);
        subtitleResetForSeek();
    	}
    /* first set an invalid sub id */
    pcodec->sub_pid = 0xffff;
    if(codec_set_sub_id(pcodec)) {
        LOGE("[%s:%d]set invalid sub pid failed\n", __FUNCTION__, __LINE__);
        return;
    }
    int count=0;
    PSUBTITLE_PARA_T pSubtitlePara=sPara;
	SUBTITLE_PARA_T SubtitlePara = sPara[0];
    while((pSubtitlePara->pid != 0) && (count < MAX_SUBTITLE_PARAM_SIZE)) {
		 LOGI("[%s:%d]count=%d pid=%d\n", __FUNCTION__, __LINE__,count,pSubtitlePara->pid);
        if(pSubtitlePara->pid == pid){
            setSubType(pSubtitlePara);
			sPara[0] = sPara[count];
			sPara[count] = SubtitlePara;
            break;
        }
        count++;
        pSubtitlePara++;
    }
	

	
    /* reset sub */
    pcodec->sub_pid = pid;
    if(codec_set_sub_id(pcodec)) {
        LOGE("[%s:%d]set invalid sub pid failed\n", __FUNCTION__, __LINE__);
        return;
    }

    if(codec_reset_subtile(pcodec)){
        LOGE("[%s:%d]reset subtile failed\n", __FUNCTION__, __LINE__);
    }
/*
	if (pcodec->sub_handle >= 0) {
		if(codec_close_sub_fd(pcodec->sub_handle)){
			LOGE("[%s:%d]codec_close_sub_fd failed\n", __FUNCTION__, __LINE__);
			return;
		}
	}*/

}

bool CTsPlayer::SubtitleShowHide(bool bShow){
    LOGE("[%s:%d] hasccsub:%d bShow:%d\n", __FUNCTION__, __LINE__, hasccsub, bShow);
	sub_enable_output = bShow;
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
        if(VmxHasSubtitle()){
            if(bShow)
                subtitleDisplay();
            else
                subtitleHide();
        }
        else{        
            LOGV("[%s:%d] No subtitle !\n", __FUNCTION__, __LINE__);
            return false;
	}
	return true;
    }
#endif
    if (pcodec->has_sub == 1 || hasccsub == 1 || hasSctesub == 1) {
        if (bShow) {
			ccsubstatus = 1;
            subtitleDisplay();
        } else {
            ccsubstatus = 0;
            subtitleHide();	
        }
    } else {
        LOGV("[%s:%d] No subtitle !\n", __FUNCTION__, __LINE__);
        return false;
    }

    return true;
}

void CTsPlayer::SetProperty(int nType, int nSub, int nValue){

}

int64_t CTsPlayer::GetCurrentPlayTime(){
    int64_t video_pts = 0;
    unsigned long audiopts = 0;
    unsigned long videopts = 0;
    unsigned long pcrscr = 0;
    unsigned long checkin_vpts = 0;
    
    unsigned int tmppts = 0;
    if (m_bIsPlay){
    	if ((pcodec->video_type == VFORMAT_HEVC) &&(m_bFast == true)) {
            tmppts = amsysfs_get_sysfs_int("/sys/module/amvdec_h265/parameters/h265_lookup_vpts");
            //LOGI("Fast: i only getvpts by h265_lookup_vpts :%d\n",tmppts);
    	}
    	else{
		#ifdef  BUILD_WITH_VIEWRIGHT_STB
            if(prop_media_vmx_iptv!=1)
                tmppts = codec_get_vpts(pcodec);
	    else{
				sysfs_get_long("/sys/class/tsync/pts_video",&videopts);
				tmppts = (unsigned int)videopts;
	    	}
		#else
			sysfs_get_long("/sys/class/tsync/pts_video",&videopts);
			tmppts = (unsigned int)videopts;
		#endif
    	}  	
    }
    video_pts = tmppts;
    if(m_bFast && (pcodec->video_type != VFORMAT_HEVC)){
        sysfs_get_long("/sys/class/tsync/pts_audio",&audiopts);
        sysfs_get_long("/sys/class/tsync/pts_video",&videopts);
        sysfs_get_long("/sys/class/tsync/pts_pcrscr",&pcrscr);	
        LOGI("apts:0x%x,vpts=0x%x,pcrscr=0x%x\n",audiopts,videopts,pcrscr);
        sysfs_get_long("/sys/class/tsync/checkin_vpts",&checkin_vpts);
        LOGI("In Fast last checkin_vpts=0x%x\n",checkin_vpts);
        video_pts = (int64_t)checkin_vpts;
    }
    return video_pts;
}

void CTsPlayer::leaveChannel(){
    LOGI("[%s %d] leaveChannel\n", __FUNCTION__, __LINE__);
    iStop();
}

int CTsPlayer::UpdateVmxKey(){
    int ret = 0, key = 0;
    LOGI("Update vmx key\n");
    key = 1;
    #ifdef  BUILD_WITH_VIEWRIGHT_STB
    ret = VmxUpdateKey();
    #endif
    if(ret == 0)
	LOGI("update vmx key success\n");
    else
	LOGI("update vmx key failed\n");
    return ret;
}

static int updateNative(sp<ANativeWindow> nativeWin) {
    char* vaddr;
    int ret = 0;
    ANativeWindowBuffer* buf;

    if (nativeWin.get() == NULL) {
        return 0;
    }

    int err = nativeWin->dequeueBuffer_DEPRECATED(nativeWin.get(), &buf);
	
    if(buf != NULL)
        LOGI("buf->width=%d, buf->height=%d\n",buf->width, buf->height);

    if (err != 0) {
        ALOGE("dequeueBuffer failed: %s (%d)", strerror(-err), -err);
        return -1;
    }

    return nativeWin->queueBuffer_DEPRECATED(nativeWin.get(), buf);
}

void CTsPlayer::SetSurface_ANativeWindow(ANativeWindow* pSurface){
	LOGI("SetSurface ANativeWindow pSurface: %p\n", pSurface);
	if((NULL == pSurface) || (0x0 == pSurface))
	{
		LOGI("SetSurface pSurface is 0 ,so return\n");
		return;
	}
	
    	sp<ANativeWindow> mNativeWindow;

	mNativeWindow = pSurface;
	m_isSurfaceWindow = true;

	int err;
	err = native_window_api_connect(mNativeWindow.get(), NATIVE_WINDOW_API_MEDIA);
	if(err < 0){
		LOGI("native_window_api_connect return %d\n", err);
		if(err == -22){
			property_set("iptv.ctc.setSurface", "1");
			goto next;
		}
		else
			return;
	}

	next:
    	err = native_window_set_usage(mNativeWindow.get(), am_gralloc_get_video_overlay_producer_usage());
	if(err < 0){
		LOGI("SetSurface native_window_set_usage  return %d\n", err);
		return;
	}

    	err = native_window_set_buffers_format(mNativeWindow.get(), WINDOW_FORMAT_RGBA_8888);
	if(err < 0){
		LOGI("SetSurface native_window_set_buffers_format  return %d\n", err);
		return;
	}

	updateNative(mNativeWindow);
}


void CTsPlayer::SetSurface(Surface* pSurface){
	LOGI("SetSurface pSurface: %p\n", pSurface);
	if((NULL == pSurface) || (0x0 == pSurface))
	{
		LOGI("SetSurface pSurface is 0 ,so return\n");
		return;
	}
	m_isSurfaceWindow = true;
	LOGI("SetSurface, notify hwcomposer to update video axis\n");
	property_set("config.media.ctc.updatevideoaxis", "1");

    	sp<IGraphicBufferProducer> mGraphicBufProducer;
    	sp<ANativeWindow> mNativeWindow;
    	mGraphicBufProducer = pSurface->getIGraphicBufferProducer();
    	if(mGraphicBufProducer != NULL) {
        mNativeWindow = new Surface(mGraphicBufProducer);
    	} else {
        	LOGE("SetSurface, mGraphicBufProducer is NULL!\n");
        	return;
    	}
	
	int err;
	err = native_window_api_connect(mNativeWindow.get(), NATIVE_WINDOW_API_MEDIA);
	LOGI("native_window_api_connect return %d\n", err);

    	err = native_window_set_usage(mNativeWindow.get(), am_gralloc_get_video_overlay_producer_usage());
	LOGI("SetSurface native_window_set_usage  return %d\n", err);

    	err = native_window_set_buffers_format(mNativeWindow.get(), WINDOW_FORMAT_RGBA_8888);
	LOGI("SetSurface native_window_set_buffers_format  return %d\n", err);

	updateNative(mNativeWindow);
}

void CTsPlayer::playerback_register_evt_cb(IPTV_PLAYER_EVT_CB pfunc, void *hander){
    	pfunc_player_evt = pfunc;
    	player_evt_hander = hander;
}

int CTsPlayer::checkBuffLevel(){
	int audio_delay=0, video_delay=0;
    float audio_buf_level = 0.00f, video_buf_level = 0.00f;
    buf_status audio_buf;
    buf_status video_buf;
    
    if(m_bIsPlay) {
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
      if(prop_media_vmx_iptv==1){
            return CTC_SUCESS;
    }
	#endif
	#if 1
        codec_get_abuf_state(pcodec, &audio_buf);
        codec_get_vbuf_state(pcodec, &video_buf);
        if(audio_buf.size != 0)
            audio_buf_level = (float)audio_buf.data_len / audio_buf.size;
        if(video_buf.size != 0)
            video_buf_level = (float)video_buf.data_len / video_buf.size;

		if((audio_buf_level >= MAX_WRITE_ALEVEL) || (video_buf_level >= MAX_WRITE_VLEVEL)) {
        	LOGI("checkBuffLevel : audio_buf_level= %.5f, video_buf_level=%.5f, Don't writedata()\n", 
				audio_buf_level, video_buf_level);
            if(audio_buf_level >= MAX_WRITE_ALEVEL)
                a_overflows++;
            else
                v_overflows++;
        	return -1;
    	}
		/*else if( pfunc_player_evt != NULL && (audio_buf_level <= UNDERFLOW_ALEVEL)){
			pfunc_player_evt(IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW, player_evt_hander);
			LOGI("checkBuffLevel audio low level\n");
		}
		else if(pfunc_player_evt != NULL && video_buf_level <= UNDERFLOW_VLEVEL){
			pfunc_player_evt(IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW, player_evt_hander);
			LOGI("checkBuffLevel video low level\n");
		}*/
	#else
	codec_get_audio_cur_delay_ms(pcodec, &audio_delay);
	codec_get_video_cur_delay_ms(pcodec, &video_delay);

	if(!m_bFast && m_StartPlayTimePoint > 0 && (((av_gettime() - m_StartPlayTimePoint)/1000 >= prop_buffertime)
                || (audio_delay >= prop_audiobuftime || video_delay >= prop_videobuftime))) {
            LOGI("av_gettime()=%lld, m_StartPlayTimePoint=%lld, prop_buffertime=%d\n", av_gettime(), m_StartPlayTimePoint, prop_buffertime);
            LOGI("audio_delay=%d, prop_audiobuftime=%d, video_delay=%d, prop_videobuftime=%d\n", 
		audio_delay, prop_audiobuftime, video_delay, prop_videobuftime);
            LOGI("WriteData: resume play now!\n");
            codec_resume(pcodec);
            m_StartPlayTimePoint = 0;
        }
	#endif
    }
    return CTC_SUCESS;
}

void CTsPlayer::checkBuffstate(){
    char filter_mode[PROPERTY_VALUE_MAX] = {0};
    struct vdec_status video_buf;
    if(m_bIsPlay) {      
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
		if(prop_media_vmx_iptv==1){
            return;
        }
	#endif
        codec_get_vdec_state(pcodec, &video_buf);
        if (video_buf.status & DECODER_ERROR_MASK) {
            LOGI("decoder error vdec.status: %x\n", video_buf.status);
            int is_decoder_fatal_error = video_buf.status & DECODER_FATAL_ERROR_SIZE_OVERFLOW;
            if(is_decoder_fatal_error && (pcodec->video_type == VFORMAT_H264)) {
                //change format  h264--> h264 4K
                keep_vdec_mem = 1;
                iStop();
                usleep(500*1000);
                if(property_get("ro.platform.filter.modes",filter_mode,NULL) ==  0){
                    mVideoPara.vFmt = VFORMAT_H264_4K2K;
                    iStartPlay();
                    LOGI("start play vh264_4k2k");
                }
            }
        }
    }	
}

int lastvdata_len = 0;
int video_buffer_not_change = 0;
int lastdata_len = 0;
int audio_buffer_not_change = 0;
void CTsPlayer::checkAbend(){
    int ret = 0;
    buf_status audio_buf;
    buf_status video_buf;

    static int64_t vnowTime;
    static int64_t anowTime;
    static int64_t vlastTime = ::android::ALooper::GetNowUs();
    static int64_t alastTime = ::android::ALooper::GetNowUs();
    vnowTime = ::android::ALooper::GetNowUs();
    anowTime = ::android::ALooper::GetNowUs();

    float audio_buf_level = 0.00f, video_buf_level = 0.00f;
	vdatalen = 0;
	adatalen = 0;

    char value[PROPERTY_VALUE_MAX] = {0};
    char tmpfilename[1024] = "";
    static int tmpfileindexReal = 0;

    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("iptv.dumpfile", value, "0");
    prop_dumpfile = atoi(value);

    if (prop_dumpfile == 2 && dumpvfd == NULL) {
        memset(value, 0, PROPERTY_VALUE_MAX);
        property_get("iptv.dumppath", value, "/data/tmp");
        if(mIsTsStream){
            sprintf(tmpfilename, "%s/LiveReal%d.ts", value, tmpfileindexReal);
            tmpfileindexReal++;
            dumpvfd = fopen(tmpfilename, "wb+");
        }else{
            sprintf(tmpfilename, "%s/LiveReal%d_vwrite.data", value, tmpfileindexReal);
            dumpvfd = fopen(tmpfilename, "wb+");
            sprintf(tmpfilename, "%s/LiveReal%d_awrite.data", value, tmpfileindexReal);
            tmpfileindexReal++;
            dumpafd = fopen(tmpfilename, "wb+");
        }
    }
    if(!prop_dumpfile && dumpvfd != NULL) {
            fclose(dumpvfd);
            dumpvfd = NULL;
        }

   // if(!m_bWrFirstPkg){
        bool checkAudio = true;
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
        if(prop_media_vmx_iptv==1){
			VmxGet_buf_state(&abuf_data_len, &abuf_size,&vbuf_data_len, &vbuf_size);
			audio_buf.data_len = abuf_data_len;
			audio_buf.size = abuf_size;
			video_buf.data_len = vbuf_data_len;
			video_buf.size = vbuf_size;
       // 	return;
        }else
	#endif
		{
       		codec_get_abuf_state(pcodec, &audio_buf);
        	codec_get_vbuf_state(pcodec, &video_buf);
        }

		if(audio_buf.size != 0)
            audio_buf_level = (float)audio_buf.data_len / audio_buf.size;
        if(video_buf.size != 0)
            video_buf_level = (float)video_buf.data_len / video_buf.size;

		LOGI("checkAbend: audio_buf_level= %.5f video_buf_level=%.5f, a_datalen:%d v_datalen:%d vmx_write_size_per_sec:%d\n", 
				audio_buf_level, video_buf_level, audio_buf.data_len, video_buf.data_len, vmx_write_size_per_sec);
		vmx_write_size_per_sec = 0;
		vdatalen = video_buf.data_len;
		adatalen = audio_buf.data_len;
		
        if(audio_buf_level >= MAX_WRITE_ALEVEL){
            a_overflows++;
			LOGI("checkAbend: ---audio overflow\n");
			if(pfunc_player_evt != NULL) {
                 pfunc_player_evt(IPTV_PLAYER_EVT_AUD_DEC_OVERFLOW, player_evt_hander);
				 LOGI("checkAbend: ---audio overflow and callback\n");
            }
        }
        if(video_buf_level >= MAX_WRITE_VLEVEL){
                v_overflows++;
				LOGI("checkAbend: ---video overflow\n");
				if(pfunc_player_evt != NULL) {
					pfunc_player_evt(IPTV_PLAYER_EVT_VID_DEC_OVERFLOW, player_evt_hander);
					LOGI("checkAbend: ---video overflow and callback\n");
				}	
        }
        if(pcodec->has_video) {
            if(pcodec->video_type == VFORMAT_MJPEG){
                if((video_buf.data_len < (RES_VIDEO_SIZE >> 2) || video_buf_level < UNDERFLOW_VLEVEL) && ((vnowTime - vlastTime)/1000 >= 2000)){
                    if(pfunc_player_evt != NULL) {
                        pfunc_player_evt(IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW, player_evt_hander);
						vlastTime = vnowTime;
                    }
                    checkAudio = false;
                    v_underflows++;
                    LOGI("checkAbend video low level\n");
                }
            }
            else {
                if((video_buf.data_len< RES_VIDEO_SIZE || video_buf_level < UNDERFLOW_VLEVEL) && ((vnowTime - vlastTime)/1000 >= 2000)){
                    if(pfunc_player_evt != NULL){
                        pfunc_player_evt(IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW, player_evt_hander);
						vlastTime = vnowTime;
                    }
                    checkAudio = false;
                    v_underflows++;
                    LOGI("checkAbend video low level\n");
                }
                else if(/*((video_buf_level < 0.0009) || (video_buffer_not_change >= 10))
                    &&*/ (video_buffer_not_change >= 2) && ((vnowTime - vlastTime)/1000 >= 1000))
                {
                    if (pfunc_player_evt != NULL) {
                        pfunc_player_evt(IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW, player_evt_hander);
                    }
                    LOGI("#####checkAbend video low level\n");
                    v_underflows++;
                    vlastTime = vnowTime;
                }
            }
        }
        if(lastvdata_len == video_buf.data_len) {
            video_buffer_not_change++;
        } else {
            video_buffer_not_change = 0;
            lastvdata_len = video_buf.data_len;
        }

        if(pcodec->has_audio /*&& checkAudio*/) {
            if((audio_buf.data_len < RES_AUDIO_SIZE/* || audio_buf_level < UNDERFLOW_ALEVEL*/) && ((anowTime - alastTime)/1000 >= 2000)){
                if(pfunc_player_evt != NULL){
                    pfunc_player_evt(IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW, player_evt_hander);
					alastTime = anowTime;
                }
                a_underflows++;
                LOGI("checkAbend audio low level\n");
            }else if(/*(audio_buf_level < 0.0009) &&*/ (audio_buffer_not_change >= 2) && ((anowTime - alastTime)/1000 >= 1000)){
                if (pfunc_player_evt != NULL) {
                    pfunc_player_evt(IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW, player_evt_hander);
                }
				LOGI("#####checkAbend audio low level\n");
				a_underflows++;
				alastTime = anowTime;
            }
            
        }
		if(lastdata_len == audio_buf.data_len) {
            audio_buffer_not_change++;
		} else {
            audio_buffer_not_change = 0;
			lastdata_len = audio_buf.data_len;
		}
   // }
}

bool CTsPlayer::iReset(){
	LOGI("[%s %d] before try mutex\n", __FUNCTION__, __LINE__);
	lp_lock(&mutex);
	LOGI("[%s %d] run restart\n", __FUNCTION__, __LINE__);
	iStop();
	iStartPlay();
	LOGI("[%s %d] end restart\n", __FUNCTION__, __LINE__);
	lp_unlock(&mutex);
	LOGI("[%s %d] after release mutex\n", __FUNCTION__, __LINE__);
	return true;
}
void CTsPlayer::checkVdecstate(){
    struct vdec_status video_status;
    int audio_datalen;
    int vpts_invalid = 0, apts_invalid = 0;
    AM_AV_VideoStatus_t vdecstatus;
    AM_AV_AudioStatus_t adecstatus;
    memset(&video_status, 0, sizeof(video_status));
    memset(&vdecstatus, 0, sizeof(vdecstatus));
    memset(&adecstatus, 0, sizeof(adecstatus));
	
    if(m_bIsPlay) {
	#ifdef  BUILD_WITH_VIEWRIGHT_STB
        if(prop_media_vmx_iptv==1){
	    //VmxGet_buf_state(&abuf_data_len, &abuf_size,&vbuf_data_len, &vbuf_size);
#ifdef PORTING_TEST
	    VmxGet_dec_state(&adecstatus,&vdecstatus);
	    video_status.status = vdecstatus.status;
#endif
	    video_status.width = vdecstatus.src_w;
	    video_status.height = vdecstatus.src_h;
            //return;
        }else
	#endif
	{
            codec_get_vdec_state(pcodec, &video_status);
	}
	//####for invalid pts callback
#ifdef PORTING_TEST
	vpts_invalid = codec_amvideo_pts_discontinue(1, VIDEO);
	apts_invalid = codec_amvideo_pts_discontinue(1, AUDIO);
#endif

		if(vpts_invalid){
			LOGI("checkVdecstate: we found vpts not discontinue\n");
			if(pfunc_player_evt != NULL){
					pfunc_player_evt(IPTV_PLAYER_EVT_VID_INVALID_TIMESTAMP, player_evt_hander);
#ifdef PROTING_TEST
					codec_amvideo_pts_discontinue(0, VIDEO);
#endif
					LOGI("checkVdecstate: vpts not discontinue and callback\n");
			}
		}
		
		if(apts_invalid){
			LOGI("checkVdecstate: we found apts not discontinue\n");
			if(pfunc_player_evt != NULL){
					pfunc_player_evt(IPTV_PLAYER_EVT_AUD_INVALID_TIMESTAMP, player_evt_hander);
#ifdef PROTING_TEST
					codec_amvideo_pts_discontinue(0, AUDIO);
#endif
					LOGI("checkVdecstate: apts not discontinue and callback\n");
			}
		}
		
	//#### for invalid data callback
		
		if((video_status.status & DECODER_FATAL_ERROR_SIZE_OVERFLOW) &&
				   (pcodec->video_type == VFORMAT_HEVC) &&
				   (video_status.width > 4096 || video_status.height > 2304)) {
					 LOGI("oversize! restart decoder!!! vdec.status: %x width : %d height: %d\n", video_status.status, video_status.width, video_status.height);
					 if(pfunc_player_evt != NULL)
					 	pfunc_player_evt(IPTV_PLAYER_EVT_VID_OTHER, player_evt_hander);
					 iReset();
			 		 return ;
	    }
		
		if (video_status.status & DECODER_ERROR_MASK) {
			//LOGE("1 decoder vdec.status: %x width : %d height: %d video_type:%d\n", 
				//video_status.status, video_status.width, video_status.height, pcodec->video_type);
			if(video_status.status & DECODER_FATAL_ERROR_UNKNOW){
				if(pfunc_player_evt != NULL)
					pfunc_player_evt(IPTV_PLAYER_EVT_VID_INVALID_DATA, player_evt_hander);
				
				if((pcodec->video_type == VFORMAT_HEVC) && 
					(video_status.width > 1920 || video_status.height > 1080)){
						//  LOGI("decoder fatal error ctcplayer should need restart \n ");
						//	iReset();
						LOGI("h265 fatal error should driver to reset,not ctcplayer \n ");
						return;
				}else if(pcodec->video_type == VFORMAT_H264){
					int app_reset_support  = amsysfs_get_sysfs_int("/sys/module/amvdec_h264/parameters/fatal_error_reset");
                	if(app_reset_support){
               			LOGI("fatal_error_reset=1,DECODER_FATAL_ERROR_UNKNOW happened force reset decoder\n ");
                    	amsysfs_set_sysfs_int("/sys/module/amvdec_h264/parameters/decoder_force_reset", 1);
                	}
				}
				

			}
		}

		if(video_status.status == 0x200003f && pcodec->video_type == VFORMAT_H264){
				if(pfunc_player_evt != NULL)
					pfunc_player_evt(IPTV_PLAYER_EVT_VID_OTHER, player_evt_hander);
				LOGE("[%s %d] restart decoder\n", __FUNCTION__, __LINE__);
				iReset();
				return;
		}
		#if 0
        //monitor buffer staus ,overflow more than 2s reset player,if support 
        if (prop_playerwatchdog_support && !m_bIsPause){
            codec_get_abuf_state(pcodec, &audio_buf);
            codec_get_vbuf_state(pcodec, &video_buf);
            if (audio_buf.size != 0)
            audio_buf_level = (float)audio_buf.data_len / audio_buf.size;
            if (video_buf.size != 0)
            video_buf_level = (float)video_buf.data_len / video_buf.size;
            if ((audio_buf_level >= MAX_WRITE_ALEVEL) || (video_buf_level >= MAX_WRITE_VLEVEL)) {
                LOGI("checkVdecstate : audio_buf_level= %.5f, video_buf_level=%.5f\n", audio_buf_level, video_buf_level);
                if (m_PreviousOverflowTime == 0)
                    m_PreviousOverflowTime  = av_gettime();
                if ((av_gettime()-m_PreviousOverflowTime) >= 2000000){
                    LOGI("buffer  overflow more than 2s ,reset  player\n ");
                    iStop();
                    //usleep(500*1000);
                    iStartPlay();
                }
            }else{
                m_PreviousOverflowTime = 0;
            }
        	}
		#endif
    } 
}

void CTsPlayer::checkVideostate()
{
	numVideoDecodedFrames = amsysfs_get_sysfs_int("/sys/class/vdec/dec_frame_count");
	// LOGV("threadCheckAbend numVideoDecodedFrames=%d, numPreVideoDecodedFrames=%d, checkVideoStateCount=%d\n", numVideoDecodedFrames, numPreVideoDecodedFrames, checkVideoStateCount);

	if(numVideoDecodedFrames > numPreVideoDecodedFrames)
	{
		//LOGV("threadCheckAbend videoDecoderIsRun is true\n");
		videoDecoderIsRun = 1;
		checkVideoStateCount = 0;
	}
	else
	{
	    if(checkVideoStateCount < 1 && numVideoDecodedFrames != 0)
    	{
    		//LOGV("threadCheckAbend videoDecoderIsRun is true, checkVideoStateCount=%d\n", checkVideoStateCount);
    		videoDecoderIsRun = 1;
    		checkVideoStateCount++;
    	}
        else
        {
    		//LOGV("threadCheckAbend videoDecoderIsRun is false\n");
		    videoDecoderIsRun = 0;
        }
	}

	numPreVideoDecodedFrames = numVideoDecodedFrames;
}


void CTsPlayer::checkAudiostate()
{
	adec_status audioinfostatus;
	codec_get_adec_state(pcodec, &audioinfostatus);
#ifdef PORTING_TEST
	numAudioDecodedSamples = audioinfostatus.numDecodedSamples;
#endif

	// LOGV("threadCheckAbend numDecodedSamples =%d, numPreAudioDecodedSamples=%d, checkAudioStateCount=%d\n", numAudioDecodedSamples, numPreAudioDecodedSamples, checkAudioStateCount);

	if(numAudioDecodedSamples > numPreAudioDecodedSamples)
	{
		//LOGV("threadCheckAbend audioDecoderIsRun is true\n");
		audioDecoderIsRun = 1;
		checkAudioStateCount = 0;
	}
	else
	{
	    if(checkAudioStateCount < 1 && numAudioDecodedSamples != 0)
        {
        	//LOGV("threadCheckAbend audioDecoderIsRun is true, checkAudioStateCount=%d\n", checkAudioStateCount);
        	audioDecoderIsRun = 1;
        	checkAudioStateCount++;
        }
        else
        {
		    //LOGV("threadCheckAbend audioDecoderIsRun is false\n");
    		audioDecoderIsRun = 0;
        }
	}

	numPreAudioDecodedSamples = numAudioDecodedSamples;
}

void CTsPlayer::checkEncryptAudiostate()
{
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    AM_AV_AudioStatus_t aStat;
    memset(&aStat, 0, sizeof(AM_AV_AudioStatus_t));
#ifdef PORTING_TEST
    AM_AV_GetAudioStatus(AV_DEV_NO, &aStat);
    numEncryptAudioDecodedSamples = aStat.numDecodedSamples;
#endif

	LOGV("threadCheckAbend checkEncryptAudiostate numEncryptDecodedSamples =%d, numPreEncryptAudioDecodedSamples=%d, checkEncryptAudioStateCount=%d\n", numEncryptAudioDecodedSamples, numPreEncryptAudioDecodedSamples, checkEncryptAudioStateCount);

	if(numEncryptAudioDecodedSamples > numPreEncryptAudioDecodedSamples)
	{
		//LOGV("threadCheckAbend encryptAudioDecoderIsRun is true\n");
		encryptAudioDecoderIsRun = 1;
		checkEncryptAudioStateCount = 0;
	}
	else
	{
	    if(checkEncryptAudioStateCount < 1 && numEncryptAudioDecodedSamples != 0)
    	{
    		//LOGV("threadCheckAbend encryptAudioDecoderIsRun is true, checkEncryptAudioStateCount=%d\n", checkEncryptAudioStateCount);
    		encryptAudioDecoderIsRun = 1;
    		checkEncryptAudioStateCount++;
    	}
        else
        {
    		//LOGV("threadCheckAbend encryptAudioDecoderIsRun is false\n");
		    encryptAudioDecoderIsRun = 0;
        }
	}

	numPreEncryptAudioDecodedSamples = numEncryptAudioDecodedSamples;
#endif
}

void *CTsPlayer::threadCheckAbend(void *pthis){
    LOGV("threadCheckAbend start pthis: %p\n", pthis);
    CTsPlayer *tsplayer = static_cast<CTsPlayer *>(pthis);
	//int checkStateCount = 0;
    do {
		if (tsplayer->m_bIsPlay && !tsplayer->m_bStop) {
			/*checkStateCount++;
			if(checkStateCount >= 10) {
                #ifdef  BUILD_WITH_VIEWRIGHT_STB
                    if(prop_media_vmx_iptv == 1){
                        tsplayer->checkEncryptAudiostate();
                    }else
                #endif
                    {
                        tsplayer->checkAudiostate();
                    }
				tsplayer->checkVideostate();
				checkStateCount = 0;
			}*/

			#ifdef  BUILD_WITH_VIEWRIGHT_STB
				if(prop_media_vmx_iptv == 1){
					if(1 == amsysfs_get_sysfs_int("/sys/class/video/video_hdcp_finished"))
					{
						amsysfs_set_sysfs_int("/sys/class/video/video_hdcp_finished", 0);
						LOGV("update ecm now\n");
						VmxUpdateEcm();
					}
				}
            #endif
		}

        //sleep(2);
        //tsplayer->checkBuffLevel();
        if (tsplayer->m_bIsPlay && !tsplayer->m_bStop){
           /* if (lp_trylock(&tsplayer->mutex) != 0) {
                continue;
            }*/
            tsplayer->checkVdecstate();
            checkcount++;
            if(checkcount >= 40) {
                tsplayer->checkAbend();
                checkcount = 0;
            }
           // lp_unlock(&tsplayer->mutex);
        }
        usleep(50 * 1000);
    }
    while(!m_StopThread);
    LOGV("threadCheckAbend end\n");
    return NULL;
}

void CTsPlayer::restartAudio() {
    last_audio_state = get_audio_device_state();
    LOGI("enter restartAudio last_audio_state=%d\n",last_audio_state);
    if (!m_bIsPlay)
        return;
    lp_lock(&mutex);
#ifdef BUILD_WITH_VIEWRIGHT_STB
       if (m_bIsPlay && prop_media_vmx_iptv == 1) {
           VmxSwitchAudioTrack(pcodec->audio_pid, pcodec->audio_type);
           lp_unlock(&mutex);
           return;
       }
#endif
    codec_audio_automute(pcodec->adec_priv, 1);
    codec_close_audio(pcodec);
    LOGI("restartAudio pcodec->audio_type: %d, pcodec->audio_pid: %d\n", pcodec->audio_type, pcodec->audio_pid);
    if (IS_AUIDO_NEED_EXT_INFO(pcodec->audio_type)) {
        pcodec->audio_info.valid = 1;
        LOGI("set audio_info.valid to 1");
    }
    if (codec_audio_reinit(pcodec)) {
        LOGE("reset init failed\n");
        lp_unlock(&mutex);
        return;
    }
    if (codec_reset_audio(pcodec)) {
        LOGE("reset audio failed\n");
        lp_unlock(&mutex);
        return;
    }
    codec_resume_audio(pcodec, 1);
    codec_audio_automute(pcodec->adec_priv, 0);
    lp_unlock(&mutex);
    LOGI("restartAudio end!\n");
    return;
}

void *CTsPlayer::threadCheckReset(void *pthis) {
    LOGI("threadCheckReset start pthis: %p\n", pthis);
    CTsPlayer *tsplayer = static_cast<CTsPlayer *>(pthis);
    do {
        if (tsplayer->m_bIsPlay && !tsplayer->m_bStop) {
           if(prop_audiohal_ctc_reset && hasaudio && tsplayer->pcodec->has_audio){
              int temp_state = get_audio_device_state();
              //LOGI("threadCheckReset temp_state=%d last_audio_state=%d\n",temp_state,last_audio_state );
              if(temp_state != last_audio_state){
                 //do repaly while the isA2dpDevice state change
                tsplayer->restartAudio();
              }
           }
        }
        usleep(100 * 1000);
    } while(!m_StopThread);
    LOGI("threadCheckReset end\n");
    return NULL;
}

/*ZTEDSP 20140905 合入760D 代码原始接口切换音轨有问题*/
void CTsPlayer::SwitchAudioTrack_ZTE(PAUDIO_PARA_T pAudioPara)
{
    int count = 0;

    LOGI( "Begin CTsPlayer::SwitchAudioTrack_ZTE().\n");
	if(!m_bIsPlay){
		 LOGI( "m_bIsPlay false.return back!!\n");
		 return;
	}
	
    if(NULL == pAudioPara || pAudioPara->aFmt == AFORMAT_UNKNOWN || 0 == pAudioPara->pid  )
    {	
   		LOGI( "SwitchAudioTrack pAudioPara->audio_type:%d pAudioPara->audio_pid:%d\n",pAudioPara->aFmt,pAudioPara->pid);
		LOGI( "audio info is error when switch_AudioTrack.\n");
	    return ;
	}
	mAudioPara = *pAudioPara;

	lp_lock(&mutex);
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){	    
            VmxSwitchAudioTrack(pAudioPara->pid, pcodec->audio_type) ; 
			lp_unlock(&mutex);
            return ;
     }
#endif

    codec_audio_automute(pcodec->adec_priv, 1);
    codec_close_audio(pcodec);
    pcodec->audio_pid = 0xffff;

    if (codec_set_audio_pid(pcodec))
    {
        lp_unlock(&mutex);
        LOGI( "set invalid audio pid failed\n");
        return;
    }

    if(count < MAX_AUDIO_PARAM_SIZE)
    {
        pcodec->has_audio=1;
        pcodec->audio_type= pAudioPara->aFmt;
        pcodec->audio_pid=pAudioPara->pid;
        //pcodec->audio_samplerate=a_aPara[count].nSampleRate;
        //pcodec->audio_channels=a_aPara[count].nChannels;
    }

    LOGI(  "SwitchAudioTrack pcodec->audio_samplerate:%d pcodec->audio_channels:%d\n",pcodec->audio_samplerate,pcodec->audio_channels);
    LOGI(  "SwitchAudioTrack pcodec->audio_type:%d pcodec->audio_pid:%d\n",pcodec->audio_type,pcodec->audio_pid);
    //codec_set_audio_pid(pcodec);
    if(IS_AUIDO_NEED_EXT_INFO(pcodec->audio_type))
    {
        pcodec->audio_info.valid = 1;
        LOGI("set audio_info.valid to 1");
    }

    if (codec_audio_reinit(pcodec))
    {
        lp_unlock(&mutex);
        LOGI( "reset init failed\n");
        return;
    }

    if (codec_reset_audio(pcodec))
    {
        lp_unlock(&mutex);
        LOGI( "reset audio failed\n");
        return;
    }

    codec_resume_audio(pcodec, 1);
    lp_unlock(&mutex);
    codec_audio_automute(pcodec->adec_priv, 0);
    LOGI( "End   CTsPlayer::SwitchAudioTrack_ZTE().\n");

    return ;
}
/*end add*/

void CTsPlayer::SwitchVideoTrack(PVIDEO_PARA_T pVideoPara){
	if(!m_bIsPlay){
		 LOGI( "SwitchVideoTrack m_bIsPlay false.return back!!\n");
		 return;
	}
	mVideoPara = *pVideoPara;
	LOGI("SwitchVideoTrack, vfmt:%d, vpid:%d\n", mVideoPara.vFmt, mVideoPara.pid);
	iReset();//simulation switch channel
	return;
}

void CTsPlayer::ClearLastFrame()
{
    int ret = -1;
	clearlastframe_flag = 1;
    LOGI( "[%s %d] Begin set disable_video(1--->0) !!!, async_flag=%d\n", __FUNCTION__, __LINE__, async_flag);
	if (async_flag) {
        async_flag = 0;
    } else {
	    amsysfs_set_sysfs_int("/sys/class/video/disable_video", 1);
	    int fd_fb0 = open("/dev/amvideo", O_RDWR);
	    if (fd_fb0 >= 0)
	    {
			//fix this cuse kernel crash 
	        //ret = ioctl(fd_fb0, AMSTREAM_IOC_CLEAR_VBUF, NULL);
	        ret = ioctl(fd_fb0, AMSTREAM_IOC_CLEAR_VIDEO, NULL);
	        close(fd_fb0);
	    }
	    amsysfs_set_sysfs_int("/sys/class/video/disable_video", 0);
    }

    LOGI( "[%s %d] End set disable_video(1--->0) !!!\n", __FUNCTION__, __LINE__);
    return;
}

void CTsPlayer::BlackOut(int EarseLastFrame)
{
    LOGI( "[%s %d]Vod set blackout_policy, EarseLastFrame=%d.\n", __FUNCTION__, __LINE__, EarseLastFrame);
    amsysfs_set_sysfs_int("/sys/class/video/blackout_policy", EarseLastFrame);
    return;
}

bool CTsPlayer::SetErrorRecovery(int mode)
{
	if((mode < 0)||(mode > 3)){
		LOGI( "[%s %d] set ErrorCorrection failed! mode:%d\n", __FUNCTION__, __LINE__, mode);
		 return false;
	}
	if(mVideoPara.vFmt == VFORMAT_H264){
		LOGI( "[%s %d] set H264 error_recovery_mode:%d\n", __FUNCTION__, __LINE__, mode);
		amsysfs_set_sysfs_int( "/sys/module/amvdec_h264/parameters/error_recovery_mode", mode);		
	}
	else if(mVideoPara.vFmt == VFORMAT_MPEG12){
		LOGI( "[%s %d] set mpeg12 error_frame_skip_level:%d\n", __FUNCTION__, __LINE__, mode);
		amsysfs_set_sysfs_int( "/sys/module/amvdec_mpeg12/parameters/error_frame_skip_level", mode);
	}
    return true;
}

void CTsPlayer::GetAvbufStatus(PAVBUF_STATUS pstatus)
{
	buf_status audio_buf;
	buf_status video_buf;

	//LOGI( "This is CTsPlayer::GetAvbufStatus(). pstatus=%d.\n", pstatus);

	if(pstatus == NULL)
	{
		return;
	}
#ifdef  BUILD_WITH_VIEWRIGHT_STB
	if(prop_media_vmx_iptv==1){
		pstatus->abuf_size = abuf_size;
		pstatus->abuf_data_len = abuf_data_len;
		pstatus->abuf_free_len = abuf_size - abuf_data_len;
		pstatus->vbuf_size = vbuf_size;
		pstatus->vbuf_data_len = vbuf_data_len;
		pstatus->vbuf_free_len = vbuf_size - vbuf_free_len;
		return ;
	}
#endif
	codec_get_abuf_state(pcodec,&audio_buf);
	codec_get_vbuf_state(pcodec,&video_buf);

	pstatus->abuf_size = audio_buf.size;
	pstatus->abuf_data_len = audio_buf.data_len;
	pstatus->abuf_free_len = audio_buf.free_len;
	pstatus->vbuf_size = video_buf.size;
	pstatus->vbuf_data_len = video_buf.data_len;
	pstatus->vbuf_free_len = video_buf.free_len;

	return;
}

//FOR Multicast tools
void CTsPlayer::SetVideoHole(int x,int y,int w,int h){
    char  bcmd[32]={0};
    LOGI( "This is CTsPlayer::SetVideoHole(). x=%d, y=%d, w=%d, h=%d.\n", x, y, w, h);
    //amsysfs_set_sysfs_str( FB0_VIDEO_HOLE_PATH, "0 0 1280 720 0 8");//修改视频播放按应用键返回launcher但显示静止视频的问题
    sprintf(bcmd, "%d %d %d %d 0 8", x,y,w,h);
    amsysfs_set_sysfs_str( "/sys/class/graphics/fb0/video_hole", bcmd);
    return;
}
void CTsPlayer::writeScaleValue(){
    LOGI( "This is CTsPlayer::writeScaleValue().\n");
    amsysfs_set_sysfs_str( "/sys/class/graphics/fb0/request2XScale", "16 1226 690");

    return;
}
//

//get media info

int CTsPlayer::GetRealTimeFrameRate()
{
    int nRTfps = 0;

    nRTfps = amsysfs_get_sysfs_int("/sys/class/video/current_fps");
    if (nRTfps > 0)
        LOGI( "realtime fps:%d\n", nRTfps);

    return nRTfps;
}

int CTsPlayer::GetVideoFrameRate()
{
    int nVideoFrameRate = 0;
    struct vdec_status video_status;
#ifdef  BUILD_WITH_VIEWRIGHT_STB
    if(prop_media_vmx_iptv==1){
            return VmxGetVideoFrameRate() ;
     }
#endif
    if (NULL != pcodec)
    {
        codec_get_vdec_state(pcodec, &video_status);
        nVideoFrameRate = video_status.fps;
        // LOGI("video frame rate:%d\n", nVideoFrameRate);
    }

    return nVideoFrameRate;
}
int CTsPlayer::GetVideoDropNumber()
{
	int drop_number = 0;
	drop_number = amsysfs_get_sysfs_int("/sys/class/video/video_drop_number");
	LOGI("video drop number = %d\n",drop_number);

	return drop_number;
}
int CTsPlayer::GetVideoTotalNumber()
{
        int total_number = 0;
        total_number = amsysfs_get_sysfs_int("/sys/class/video/total_frame_number");
        LOGI("video total number = %d\n",total_number);

        return total_number;
}
int CTsPlayer::GetVideoPTS()
{
	int pts = 0;
	pts = codec_get_vpts(pcodec);
	LOGE("video pts = %u\n",pts);
	return pts;
}

int CTsPlayer::GetCurrentVidPTS(unsigned long long *pPTS)
{   
	int ret = -1;
	unsigned long  videopts;
	ret = sysfs_get_long("/sys/class/tsync/pts_video", &videopts);   
    //LOGI("current_pts:%lld\n", videopts);
    *pPTS = (unsigned long long)videopts;
    return ret;
}

int CTsPlayer::GetAVPTSDiff(int *avdiff)
{
	int ret = -1;
	int diff = 0;
	unsigned long videopts;
	unsigned long audiopts;
	if(sysfs_get_long("/sys/class/tsync/pts_video", &videopts))
		return -1;
	if(sysfs_get_long("/sys/class/tsync/pts_audio", &audiopts))
		return -1;
	
	diff = (int)audiopts - videopts;
	*avdiff = diff;
	//LOGV("GetAVPTSDiff: avdiff=%d\n",diff);
	return 0;
	
}

void CTsPlayer::GetVideoInfo (int *width,int *height ,int *ratio,int *frameformat)
{
    int rVideoRatio = 0;
    int rVideoHeight = 0;
    int rVideoWidth = 0;
    int videoWH = 0;
	int rVideo_format = 0;
	char tVideoFFMode[64] = {0};

    rVideoHeight = amsysfs_get_sysfs_int("/sys/class/video/frame_height");
    rVideoWidth = amsysfs_get_sysfs_int("/sys/class/video/frame_width");
	amsysfs_get_sysfs_str("/sys/class/deinterlace/di0/frame_format",tVideoFFMode,64);
	LOGE("tVideoFFMode:%s\n", tVideoFFMode);
    if(rVideoWidth > 0 && rVideoHeight > 0)
        videoWH =(float)rVideoWidth / (float)rVideoHeight;
    if (videoWH < 1.34) {
        rVideoRatio = 0;
    } else if(videoWH > 1.7) {
        rVideoRatio = 1;
    }

	if(!strcmp(tVideoFFMode, "progressive")) {
        rVideo_format = 1;
    } else {
        rVideo_format = 0;
    }	
	
	*width = rVideoWidth;
	*height = rVideoHeight;
	*ratio = rVideoRatio;
	*frameformat = rVideo_format;
}

int CTsPlayer::GetPlayerInstanceNo() {
    return 0;
}

void CTsPlayer::ExecuteCmd(const char* cmd_str)
{
    return;
}

int CTsPlayer::GetChannelConfiguration(int numChannels, int acmod){
	int channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO;
    	if(numChannels == 1)
		channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_MONO;
	else if(numChannels == 2)
		channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO;
	else if(numChannels == 3){
			if(acmod == 3)
				channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R;
			else if(acmod == 4)
				channelConfiguration =	TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_S;
			else
				channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_STEREO;
	}else if(numChannels == 4){
			if(acmod == 5)
				channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_S;
			else if(acmod == 6)
				channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_S;
			else
				channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_R_SL_RS;
	}else if(numChannels == 5)
			channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_L_C_R_SL_SR;
	else if(numChannels == 6)
			channelConfiguration =	TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_5_1;
	else if(numChannels == 8)
			channelConfiguration = TIF_HAL_PLAYBACK_AUDIO_SOURCE_CHANNEL_CONFIGURATION_7_1;
	return channelConfiguration;
}

int video_length_change = 3;
int video_frame_change = 10;
int audio_length_change = 3;
int audio_frame_change = 10;
int sub_count = 0;
void CTsPlayer::GetDecodeFrameInfo(void *DecoderInfo, TIF_HAL_PlaybackType_t eType)
{
    static int64_t nowTime = 0;
    static int64_t lastTime = ::android::ALooper::GetNowUs();
    static int64_t video_invalid_lastTime = ::android::ALooper::GetNowUs();
    static int64_t audio_invalid_lastTime = ::android::ALooper::GetNowUs();
    static int lastvdatalen = 0;
    static int lastadatalen = 0;
    static int firstin = 1;
    nowTime = ::android::ALooper::GetNowUs();

    if (NULL == DecoderInfo)
    {
        LOGE("GetDecodeFrameInfo , DecoderInfo is NULL!\n");
        return;
    }

	if(TIF_HAL_PLAYBACK_TYPE_ALL == eType)
	{
		TIF_HAL_PlaybackStatus_t* info = (TIF_HAL_PlaybackStatus_t*)DecoderInfo;

		//video
        LOGV("GetDecodeFrameInfo , pcodec->has_video =%d, video_disable=%d\n", pcodec->has_video, video_disable);
		if((pcodec->has_video) && (0 == video_disable)){

			info->video.statistics.numDecodedFrames = amsysfs_get_sysfs_int("/sys/class/vdec/dec_frame_count");

            // LOGE("info->video.statistics.numDecodedFrames=%d, videoDecoderIsRun=%d\n",info->video.statistics.numDecodedFrames, videoDecoderIsRun);
            
            /*if((info->video.statistics.numDecodedFrames > 0) && (videoDecoderIsRun == 1))
			{
				info->video.enabled = m_bIsPlay? 1:0;
            }*/

            info->video.enabled = m_bIsPlay? 1:0;

			info->video.info.height = amsysfs_get_sysfs_int("/sys/class/video/frame_height");
			info->video.info.width = amsysfs_get_sysfs_int("/sys/class/video/frame_width");

            int framerate = 0;
			#ifdef  BUILD_WITH_VIEWRIGHT_STB
        		if(prop_media_vmx_iptv==1){
                     framerate = VmxGetVideoFrameRate();
        	    }else
			#endif
                 {
					struct vdec_status video_status;
					memset(&video_status, 0, sizeof(video_status));
					codec_get_vdec_state(pcodec, &video_status);
                    framerate = video_status.fps;
					// error_mask = video_status.status;
                 }
             // LOGE("framerate=%d", framerate);
             if(framerate == 23){
                 info->video.info.frameRate = 23.97;
             }else if(framerate == 29){
                 info->video.info.frameRate = 29.97;
             }else if(framerate == 59){
                 info->video.info.frameRate = 59.97;
             }else{
                 info->video.info.frameRate = (float)framerate;
             }

			char frame_aspect_ratio[64] = {0};
			amsysfs_get_sysfs_str("/sys/class/video/frame_aspect_ratio", frame_aspect_ratio, 64);
			//LOGE("GetDecodeFrameInfo , frame_aspect_ratio=%s\n", frame_aspect_ratio);

			if(!strncmp(frame_aspect_ratio, "0x90", 4) || !strncmp(frame_aspect_ratio, "0x8c", 4)){ // 16:9
				if(info->video.info.height <= 480 && 
					info->video.info.width <= 720)
					info->video.info.pixelAspectRatio =(float)(16.0*(info->video.info.height))/(9.0*(info->video.info.width));
				else
					info->video.info.pixelAspectRatio = 1.0;
			}
			else{ //4:3 0xc0	
				if(info->video.info.height <= 480 && 
					info->video.info.width <= 720)
					info->video.info.pixelAspectRatio = (float)(4.0*(info->video.info.height))/(3.0*(info->video.info.width));
				else
					info->video.info.pixelAspectRatio = 1.0;
			}
			
			char afdValue[PROPERTY_VALUE_MAX] = {0};
            int afd = 0;
            memset(afdValue, 0, PROPERTY_VALUE_MAX);
            property_get("vendor.sys.subtitleservice.afdValue", afdValue, "0");
            afd = atoi(afdValue);
            // LOGE("-------afd------=%d\n", afd);
            info->video.info.activeFormatDescription = afd;

			if(0 == video_hide)
			{
                DisableVideoCount += (CurVideoNum - FrozenVideoNum);
                int first_frame_toggled = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/first_frame_toggled");
                int new_frame_count = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/new_frame_count");
				info->video.statistics.numDisplayedFrames = new_frame_count + first_frame_toggled - DisableVideoCount;
                FrozenVideoNum = info->video.statistics.numDisplayedFrames;
                CurVideoNum = info->video.statistics.numDisplayedFrames;

				LOGV("DisableVideoCount = %d, CurVideoNum = %d, FrozenVideoNum = %d, first_frame_toggled = %d, new_frame_count = %d, numDisplayedFrames = %d\n", 
					DisableVideoCount, CurVideoNum, FrozenVideoNum, first_frame_toggled, new_frame_count, info->video.statistics.numDisplayedFrames);
			}
            else
            {
                int first_frame_toggled = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/first_frame_toggled");
                int new_frame_count = amsysfs_get_sysfs_int("/sys/module/amvideo/parameters/new_frame_count");
                info->video.statistics.numDisplayedFrames = FrozenVideoNum;
                CurVideoNum = new_frame_count + first_frame_toggled - DisableVideoCount;
            }

			LOGV("DisableVideoCount = %d, CurVideoNum = %d, FrozenVideoNum = %d\n", DisableVideoCount, CurVideoNum, FrozenVideoNum);

			info->video.statistics.numDecoderErrors = amsysfs_get_sysfs_int("/sys/class/vdec/dec_lostframe_count");//by
			info->video.statistics.numDecoderOverflows = v_overflows;
			info->video.statistics.numDecoderUnderflows = v_underflows;
			info->video.statistics.numFramesDropped = amsysfs_get_sysfs_int("/sys/class/vdec/dec_lostframe_count");

			if(pfunc_player_evt != NULL && info->video.enabled){
				//LOGI("video nowTime:%lld, lastTime:%lld\n", nowTime/1000, lastTime/1000);
				if(info->video.statistics.numDecodedFrames == 0 && ((nowTime - lastTime)/1000 >= 5000) && vdatalen != 0){
					//vpid set to apid or video pid is right but codec type is error, we need report invalid data
					pfunc_player_evt(IPTV_PLAYER_EVT_VID_INVALID_DATA, player_evt_hander);
					LOGI("####report video invalid data evt by 1 case\n");
					lastTime = nowTime;
				}

                /*LOGI("last_vnumdecoder:%d numDecodedFrames:%d nowTime:%lld video_invalid_lastTime:%lld vdatalen:%d lastvdatalen:%d", 
					last_vnumdecoder, info->video.statistics.numDecodedFrames, nowTime, video_invalid_lastTime, vdatalen, lastvdatalen);*/

				if(info->video.statistics.numDecodedFrames != 0  && 
					(info->video.statistics.numDecodedFrames == last_vnumdecoder) &&
					((nowTime - video_invalid_lastTime)/1000 >= 8000) &&
					(video_length_change <= 0) && (video_frame_change <= 0)){
					/*if(firstin){
						lastvdatalen = vdatalen;
						firstin = 0;
						LOGI("###for save lastvdatalen= %d\n", lastvdatalen);
					}*/

					pfunc_player_evt(IPTV_PLAYER_EVT_VID_INVALID_DATA, player_evt_hander);
					LOGI("report video invalid data evt by 2 case, vdatalen =%d, lastvdatalen =%d\n", vdatalen, lastvdatalen);

                    video_invalid_lastTime = nowTime;
				}

				if(info->video.statistics.numDecoderErrors > last_vnumdecoderErrors){
					pfunc_player_evt(IPTV_PLAYER_EVT_VID_INVALID_DATA, player_evt_hander);
					LOGI("report video invalid data evt by 3 case\n");
				}
			}
			last_vnumdecoderErrors = info->video.statistics.numDecoderErrors;
			if (last_vnumdecoder == info->video.statistics.numDecodedFrames)
                video_frame_change--;
			else {
                video_frame_change = 10;
				last_vnumdecoder = info->video.statistics.numDecodedFrames;
			}

            if (lastvdatalen == vdatalen) {
                video_length_change = 1;
            } else {
                video_length_change--;
				lastvdatalen = vdatalen;
			}

			LOGV("GetDecodeFrameInfo , video, enabled=%d\n", info->video.enabled);
			LOGV("GetDecodeFrameInfo , video, numDecodedFrames=%d\n", info->video.statistics.numDecodedFrames);
			LOGV("GetDecodeFrameInfo , video, numDisplayedFrames=%d\n", info->video.statistics.numDisplayedFrames);
			LOGV("GetDecodeFrameInfo , video, numDecoderErrors=%d\n", info->video.statistics.numDecoderErrors);
			LOGV("GetDecodeFrameInfo , video, numDecoderOverflows=%d\n", info->video.statistics.numDecoderOverflows);
			LOGV("GetDecodeFrameInfo , video, numDecoderUnderflows=%d\n", info->video.statistics.numDecoderUnderflows);
			LOGV("GetDecodeFrameInfo , video, numFramesDropped=%d\n", info->video.statistics.numFramesDropped);
			LOGV("GetDecodeFrameInfo , video, height=%d\n", info->video.info.height);
			LOGV("GetDecodeFrameInfo , video, width=%d\n", info->video.info.width);
			LOGV("GetDecodeFrameInfo , video, frameRate=%f\n", info->video.info.frameRate);
			LOGV("GetDecodeFrameInfo , video, pixelAspectRatio=%f\n", info->video.info.pixelAspectRatio);
			LOGV("GetDecodeFrameInfo , video, activeFormatDescription=%d\n", info->video.info.activeFormatDescription);
		}

		//audio
        LOGV("GetDecodeFrameInfo , pcodec->has_audio =%d, audio_disable=%d\n", pcodec->has_audio, audio_disable);
		if((pcodec->has_audio) && (0 == audio_disable)){
			#ifdef  BUILD_WITH_VIEWRIGHT_STB
			if(prop_media_vmx_iptv == 1){
				AM_AV_AudioStatus_t aStat;
				memset(&aStat, 0, sizeof(AM_AV_AudioStatus_t));
				AM_AV_GetAudioStatus(AV_DEV_NO, &aStat);
#ifdef PROTING_TEST
				info->audio.statistics.numDecodedSamples = aStat.numDecodedSamples;
#endif
                // LOGE("info->audio.statistics.numDecodedSamples=%d, encryptAudioDecoderIsRun=%d\n", info->audio.statistics.numDecodedSamples, encryptAudioDecoderIsRun);
				
                /*if((info->audio.statistics.numDecodedSamples > 0) && 
					(encryptAudioDecoderIsRun == 1))
                {
					info->audio.enabled = m_bIsPlay? 1:0;
                }*/

                info->audio.enabled = m_bIsPlay? 1:0;

#ifdef PROTING_TEST
				info->audio.info.numChannels = aStat.originchannels;
				info->audio.info.channelConfiguration = (TIF_HAL_Playback_AudioSourceChannelConfiguration_t)GetChannelConfiguration(info->audio.info.numChannels, aStat.acmod);
#endif
				info->audio.info.sampleRate = aStat.sample_rate; 
				//LOGE("for vmx decodernum %d\n", aStat.numDecodedSamples);
				if(0 == audio_hide)
				{
                     DisableAudioCount += (CurAudioNum - ForzenAudioNum);
#ifdef PROTING_TEST
                     info->audio.statistics.numOutputSamples = aStat.numOutputSamples - DisableAudioCount;
#endif
                     ForzenAudioNum = info->audio.statistics.numOutputSamples;
                     CurAudioNum = info->audio.statistics.numOutputSamples;

#ifdef PROTING_TEST
					 LOGE("Vmx DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d, aStat.numOutputSamples = %d, numOutputSamples = %d\n", 
					 	DisableAudioCount, CurAudioNum, ForzenAudioNum, aStat.numOutputSamples, info->audio.statistics.numOutputSamples);
#endif
				}
                else
                {
                    info->audio.statistics.numOutputSamples = ForzenAudioNum;
#ifdef PROTING_TEST
                    CurAudioNum = aStat.numOutputSamples - DisableAudioCount;
#endif
                }

				LOGV("Vmx DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d\n", DisableAudioCount, CurAudioNum, ForzenAudioNum);

                if((info->audio.info.sampleRate == 0) && (info->audio.statistics.numOutputSamples > 0))
                {
                    if( mAudioPara.nSampleRate != 0)
                        info->audio.info.sampleRate = mAudioPara.nSampleRate;
                    else
                        info->audio.statistics.numOutputSamples = 0;
                }

#ifdef PROTING_TEST
				info->audio.statistics.numDecoderErrors = aStat.numDecoderErrors;
#endif
				info->audio.statistics.numDecoderOverflows = a_overflows;
				info->audio.statistics.numDecoderUnderflows = a_underflows;
#ifdef PROTING_TEST
				info->audio.statistics.numSamplesDiscarded = aStat.numSamplesDiscarded;
#endif
			}else
			#endif
			{
				//for unencrypt
				adec_status ainfostatus;
				memset(&ainfostatus, 0, sizeof(ainfostatus));
				codec_get_adec_state(pcodec, &ainfostatus);

#ifdef PROTING_TEST
				info->audio.statistics.numDecodedSamples = ainfostatus.numDecodedSamples;
#endif
                // LOGE("info->audio.statistics.numDecodedSamples=%d, audioDecoderIsRun=%d", info->audio.statistics.numDecodedSamples, audioDecoderIsRun);

				/*
                if((info->audio.statistics.numDecodedSamples > 0) && (audioDecoderIsRun == 1))
				{
					info->audio.enabled = m_bIsPlay? 1:0;
                }
                */

                info->audio.enabled = m_bIsPlay? 1:0;

#ifdef PROTING_TEST
				info->audio.info.numChannels = ainfostatus.originchannels;
				info->audio.info.channelConfiguration = (TIF_HAL_Playback_AudioSourceChannelConfiguration_t)GetChannelConfiguration(info->audio.info.numChannels, ainfostatus.acmod);
#endif

                 info->audio.info.sampleRate = ainfostatus.sample_rate;
				 
				if(0 == audio_hide)
				{
                     DisableAudioCount += (CurAudioNum - ForzenAudioNum);
#ifdef PROTING_TEST
                     info->audio.statistics.numOutputSamples = ainfostatus.numOutputSamples - DisableAudioCount;
#endif
                     ForzenAudioNum = info->audio.statistics.numOutputSamples;
                     CurAudioNum = info->audio.statistics.numOutputSamples;

#ifdef PROTING_TEST
					  LOGV("DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d, ainfostatus.numOutputSamples = %d, numOutputSamples = %d\n", 
					 	DisableAudioCount, CurAudioNum, ForzenAudioNum, ainfostatus.numOutputSamples, info->audio.statistics.numOutputSamples);
#endif
				
				}
                else
                {
                    info->audio.statistics.numOutputSamples = ForzenAudioNum;
#ifdef PROTING_TEST
                    CurAudioNum = ainfostatus.numOutputSamples - DisableAudioCount;
#endif
                }

				LOGV("DisableAudioCount = %d, CurAudioNum = %d, ForzenAudioNum = %d\n", DisableAudioCount, CurAudioNum, ForzenAudioNum);

                 
                if((info->audio.info.sampleRate == 0) && (info->audio.statistics.numOutputSamples > 0))
                {
                    // LOGI("#####audio numOutputSamples:%d, audio sampleRate:%d\n", info->audio.statistics.numOutputSamples, mAudioPara.nSampleRate);
                    if( mAudioPara.nSampleRate != 0)
                        info->audio.info.sampleRate = mAudioPara.nSampleRate;
                    else
                        info->audio.statistics.numOutputSamples = 0;
                    // LOGI("#####audio sampleRate:%d\n", info->audio.info.sampleRate);
                }

#ifdef PROTING_TEST
				info->audio.statistics.numDecoderErrors = ainfostatus.numDecoderErrors;
#endif
				info->audio.statistics.numDecoderOverflows = a_overflows;
				info->audio.statistics.numDecoderUnderflows = a_underflows;
#ifdef PROTING_TEST
				info->audio.statistics.numSamplesDiscarded = ainfostatus.numSamplesDiscarded;
#endif
			}

			if(pfunc_player_evt != NULL && info->audio.enabled){
					if(info->audio.statistics.numDecodedSamples == 0 && 
						(/*(adatalen != 0) || */(pcodec->audio_pid == pcodec->video_pid) || (pcodec->audio_pid == pcodec->sub_pid))){
						//apid set to vpid or apid is right but codec type is error, we need report invalid data
						pfunc_player_evt(IPTV_PLAYER_EVT_AUD_INVALID_DATA, player_evt_hander);
						LOGI("report audio invalid data evt by 1 case\n");
					}else if(info->audio.statistics.numDecoderErrors > last_anumdecoderErrors){
						pfunc_player_evt(IPTV_PLAYER_EVT_AUD_INVALID_DATA, player_evt_hander);
						LOGI("report audio invalid data evt by 2 case\n");
					}
                    if(info->audio.statistics.numDecodedSamples != 0  && 
                    (info->audio.statistics.numDecodedSamples == last_anumdecoder) &&
                    ((nowTime - audio_invalid_lastTime)/1000 >= 8000) &&
                    (audio_length_change <= 0) && (audio_frame_change <= 0)){

                    pfunc_player_evt(IPTV_PLAYER_EVT_AUD_INVALID_DATA, player_evt_hander);
                    LOGI("report audio invalid data evt by 3 case, adatalen =%d, lastadatalen =%d\n", adatalen, lastadatalen);

                    video_invalid_lastTime = nowTime;
                }
			}
			last_anumdecoderErrors = info->audio.statistics.numDecoderErrors;

            if (last_anumdecoder == info->audio.statistics.numDecodedSamples)
                audio_frame_change--;
            else {
                audio_frame_change = 10;
                last_anumdecoder = info->audio.statistics.numDecodedSamples;
            }

            if (lastadatalen == adatalen) {
                audio_length_change = 1;
            } else {
                audio_length_change--;
                lastadatalen = adatalen;
            }
			
            LOGV("GetDecodeFrameInfo , audio, enabled=%d\n", info->audio.enabled);
			LOGV("GetDecodeFrameInfo , audio, numDecodedSamples=%d\n", info->audio.statistics.numDecodedSamples);
			LOGV("GetDecodeFrameInfo , audio, numOutputSamples=%d\n", info->audio.statistics.numOutputSamples);
			LOGV("GetDecodeFrameInfo , audio, numDecoderErrors=%d\n", info->audio.statistics.numDecoderErrors);
			LOGV("GetDecodeFrameInfo , audio numDecoderOverflows=%d\n", info->audio.statistics.numDecoderOverflows);
			LOGV("GetDecodeFrameInfo , audio, numDecoderUnderflows=%d\n", info->audio.statistics.numDecoderUnderflows);
			LOGV("GetDecodeFrameInfo , audio, numSamplesDiscarded=%d\n", info->audio.statistics.numSamplesDiscarded);
			LOGV("GetDecodeFrameInfo , audio channelConfiguration=%d\n", info->audio.info.channelConfiguration);
			LOGV("GetDecodeFrameInfo , audio, numChannels=%d\n", info->audio.info.numChannels);
			LOGV("GetDecodeFrameInfo , audio, sampleRate=%d\n", info->audio.info.sampleRate);
	   }

	   //subtitle
	   if(pcodec->has_sub == 1 || hasSctesub)
		{
			// LOGE("GetDecodeFrameInfo , getSubState is ok");
			if(sub_enable_output)
			{
				info->subtitles.enabled = 1;
            }

			info->subtitles.info.height = amsysfs_get_sysfs_int("/sys/class/subtitle/height");
			info->subtitles.info.width = amsysfs_get_sysfs_int("/sys/class/subtitle/width");
			if((int)sPara[0].pid != 0 && (int)sPara[0].sub_type == CODEC_ID_SCTE27){
				//info->subtitles.info.height = (unsigned int)subtitleGetSubHeight();
				//info->subtitles.info.width = (unsigned int)subtitleGetSubWidth;
				memcpy(info->subtitles.info.iso639Code, subtitleGetLanguage(), 3);
                if(!strcmp(info->subtitles.info.iso639Code, "\0"))
                {
                    info->subtitles.info.height = 0;
                    info->subtitles.info.width = 0;
                }
			}

            if(info->subtitles.info.height > 0 && sub_count <= 2)
                sub_count++;
            else
                sub_count = 0;

            if(getSubState() && sub_count == 1)
            {
                if(sub_handle_ctc)
                {
                    sub_handle_ctc(SUBTITLE_AVAIABLE, 0);
                    LOGI("%s, ###MIRADA_IZZI Subtitle_CTsplayer Callback State = %d\n", __func__, 0);
                }
            }

			//memcpy(info->subtitles.info.iso639Code, subtitleGetLanguage(), 3);
			info->subtitles.statistics.numDecoderErrors = amsysfs_get_sysfs_int("/sys/class/subtitle/errnums");
			/* if(info->subtitles.statistics.numDecoderErrors > last_snumdecoderErrors)
				s_err_reason = TIF_HAL_PLAYBACK_ERROR_REASON_OTHER;
			last_snumdecoderErrors = info->subtitles.statistics.numDecoderErrors;*/

			LOGV("GetDecodeFrameInfo , subtitle, enabled=%d\n", info->audio.statistics.numSamplesDiscarded);
			LOGV("GetDecodeFrameInfo , subtitle, numDecoderErrors=%d\n", info->subtitles.statistics.numDecoderErrors);
			LOGV("GetDecodeFrameInfo , subtitle, height=%d\n", info->subtitles.info.height);
			LOGV("GetDecodeFrameInfo , subtitle, width=%d\n", info->subtitles.info.width);
			LOGV("GetDecodeFrameInfo , subtitle, iso639Code=%s\n", info->subtitles.info.iso639Code);
		}
	}
	else if(TIF_HAL_PLAYBACK_TYPE_VIDEO == eType)
	{
		//reserved.
	}
	else if(TIF_HAL_PLAYBACK_TYPE_AUDIO == eType)
	{
		//reserved.
	}
	else if(TIF_HAL_PLAYBACK_TYPE_SUBTITLES == eType)
	{
		//reserved.
	}
	else
	{
		LOGE("type error!\n");
	}

	return;
}

int CTsPlayer::SetEcmCallback(tSetEcmCallback cb)
{
#ifdef  BUILD_WITH_VIEWRIGHT_STB
	VmxSetEcmCallback(cb);
#endif

	return 0;
}

void CTsPlayer::SetSubtitleCallback(SUBTITLELIS sub)
{
    sub_handle_ctc = sub;
    LOGE("SetSubtitleCallback in CTsPlayer");
    subtitle_register(sub);
    return;
}

int CTsPlayer::GetRatio()
{
    int pixelAspectRatio = 0;
    char frame_aspect_ratio[64] = {0};
    amsysfs_get_sysfs_str("/sys/class/video/frame_aspect_ratio", frame_aspect_ratio, 64);
    //LOGE("GetDecodeFrameInfo , frame_aspect_ratio=%s\n", frame_aspect_ratio);

    if(!strncmp(frame_aspect_ratio, "0x90", 4) || !strncmp(frame_aspect_ratio, "0x8c", 4))
    { // 16:9
        pixelAspectRatio = 1;
    }
    else
    { //4:3 0xc0    
        pixelAspectRatio = 2;
    }

    return pixelAspectRatio;
}

int CTsPlayer::GetHDMIState(){
	char HdmiState[64] = {0};
	int Hdmi_int = 0;
	amsysfs_get_sysfs_str("/sys/class/amhdmitx/amhdmitx0/hdmi/state", HdmiState, 64);
	
	if(!strncmp(HdmiState, "HDMI=1", 6))
	{
		LOGE("HdmiState strcmp 1\n");
		Hdmi_int = 1;
	}
	else
	{
		LOGE("HdmiState strcmp 0\n");
		Hdmi_int = 0;
	}
	
	return Hdmi_int;
}

void CTsPlayer::SetAudioDelay(char* mode)
{
    LOGE("setprop persist.vendor.dvb.audio_delay = %s \n", mode);
    amsysfs_set_sysfs_str("/sys/class/video/video_delay_val", mode);
}
