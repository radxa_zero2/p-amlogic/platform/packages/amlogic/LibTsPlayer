#define LOG_TAG "subtitleMiddleClient-CTC"

//#include <ISubTitleService.h>
#include "subtitleservice.h"
#include "CTsPlayer.h"
#include "Amsysfsutils.h"

#include <binder/Binder.h>
//#include <binder/IServiceManager.h>
#include "SubtitleServerHidlClient.h"
#include "subtitleMiddleCallback.h"
#include <utils/Atomic.h>
#include <utils/Log.h>
#include <utils/RefBase.h>
#include <utils/String8.h>
#include <utils/String16.h>
#include <utils/threads.h>
#include <unistd.h>


using namespace android;

#define STR_LEN 256

namespace ctc_subtitle {
static sp<SubtitleServerHidlClient> service = NULL;
static Mutex amgLock;
CTsPlayer* ctc_mp = NULL;
sp<EventCallback> spEventCB;

SUBTITLELIS substate_handle = NULL;
unsigned int sub_en = 0;

static const sp<SubtitleServerHidlClient>& getSubtitleService()
{
    Mutex::Autolock _l(amgLock);
    if (service == nullptr) {
        service = new SubtitleServerHidlClient();
    }
    ALOGE_IF(service == 0, "no subtitle_service!!");
	//ALOGE("find ISubtitle service...");
    return service;
}

void subtitleShow()
{
	ALOGE("subtitleShow");
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleShow();
    }
    return;
}

int getSubtitleCurrentPos()
{
    int pos = 0;
       int sub_type = -1;
	int firstvpts = amsysfs_get_sysfs_ulong("/sys/class/tsync/firstvpts");
	//ALOGE("firstvpts :%d, subtitleGetTypeDetial:%d,subtitleGetType():%d\n",
	//	amsysfs_get_sysfs_ulong("/sys/class/tsync/firstvpts"),subtitleGetTypeDetial(),subtitleGetType());
	if (firstvpts == 0) {
		pos = 0;
	} else {
              sub_type = amsysfs_get_sysfs_int("/sys/class/subtitle/subtype");
	       ALOGE("getSubtitleCurrentPos sub_type:%d", sub_type);
		if (sub_type == 5){//dvb subtitle: 5
			pos = (ctc_mp->GetCurrentPlayTime()/90);
		} else {
			pos = ((ctc_mp->GetCurrentPlayTime() - firstvpts)/90);
		}
	}
	return pos;
}

void subtitleOpenIdx(int idx) {
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleOpenIdx(idx);
    }
    return;
}

void subtitleClose()
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleClose();
    }
    return;
}

int subtitleGetTotal()
{
    int ret = -1;
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        ret = subser->subtitleGetTotal();
    }
    return ret;
}

void subtitleNext()
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleNext();
    }
    return;
}

void subtitlePrevious()
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitlePrevious();
    }
    return;
}

void subtitleShowSub(int pos)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        //ALOGE("subtitleShowSub pos:%d\n", pos);
        subser->subtitleShowSub(pos);
    }
    return;
}

void subtitleOption()
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleOption();
    }
    return;
}

int subtitleGetType()
{
    int ret = 0;
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        ret = subser->subtitleGetType();
    }
    return ret;
}

char* subtitleGetTypeStr()
{
    static char* ret = "";
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        //@@ret = String8(subser->getTypeStr()).string();
    }
    return ret;
}

int subtitleGetTypeDetial()
{
    int ret = 0;
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        ret = subser->subtitleGetTypeDetial();
    }
    return ret;
}

void subtitleSetTextColor(int color)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleSetTextColor(color);
    }
    return;
}

void subtitleSetTextSize(int size)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleSetTextSize(size);
    }
    return;
}

void subtitleSetGravity(int gravity)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleSetGravity(gravity);
    }
    return;
}

void subtitleSetTextStyle(int style)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleSetTextStyle(style);
    }
    return;
}

void subtitleSetPosHeight(int height)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleSetPosHeight(height);
    }
    return;
}

void subtitleSetImgRatio(float ratioW, float ratioH, int maxW, int maxH)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleSetImgRatio(ratioW, ratioH, maxW, maxH);
    }
    return;
}

void subtitleSetSurfaceViewParam(int x, int y, int w, int h)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    ALOGE("subtitleSetSurfaceViewParam 00 x:%d y:%d w:%d h:%d\n",x,y,w,h);
    if(subser != 0){
        //ALOGE("subtitleSetSurfaceViewParam 01\n");
        subser->subtitleSetSurfaceViewParam(x, y, w, h);
    }
    return;
}

void subtitleClear()
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleClear();
    }
    return;
}

void subtitleResetForSeek()
{
	ALOGE("subtitleResetForSeek");
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleResetForSeek();
    }
    return;
}

void subtitleHide()
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleHide();
    }
    return;
}

void subtitleDisplay()
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleDisplay();
    }
    return;
}

char* subtitleGetCurName()
{
    static char ret[STR_LEN] = {0,};
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        /*String16 value;
        value = subser->getCurName();
        memset(ret, 0, STR_LEN);
        strcpy(ret, String8(value).string());*/
    }
    return ret;
}

char* subtitleGetName(int idx)
{
    static char ret[STR_LEN] = {0,};
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        /*String16 value;
        value = subser->getName(idx);
        memset(ret, 0, STR_LEN);
        strcpy(ret, String8(value).string());*/
    }
    return ret;
}

const char* subtitleGetLanguage()
{
    ALOGE("subtitleGetLanguage");
    static char ret[STR_LEN] = {0,};
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        std::string value;
        value = subser->subtitleGetLanguage(0);
        //memset(ret, 0, STR_LEN);
        //strcpy(ret, subser->subtitleGetLanguage(0));
        //if (value != nullptr) {
            ALOGE("subtitleGetLanguage -iso639Code-languege:%s", value.c_str());
            strncpy(ret, value.c_str(), STR_LEN-1);
            return ret;
        //}
    }
    return NULL;
}

void subtitleLoad(char* path)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        //subser->load(String16(path));
    }
    return;
}

void subtitleCreat()
{
    ALOGE("subtitleCreat");
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleCreat();
    }
    ALOGE("subtitleCreated");
    return;
}

void subtitleOpen(char* path, void *pthis)
{
    ALOGE("subtitleOpen");
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    ctc_mp = static_cast<CTsPlayer *>(pthis);
    if(subser != 0) {
#ifdef BUILD_IN_ZTE_SOURCE && ANDROID_PLATFORM_SDK_VERSION == 28
        subser->subtitleOpen(path, getSubtitleCurrentPos);
#else
        subser->subtitleOpen(path, getSubtitleCurrentPos, nullptr);
#endif
    }
    return;
}

void subtitleSetSubPid(int pid)
{
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        subser->subtitleSetSubPid(pid);
    }
    return;
}

int subtitleGetSubHeight()
{
    int ret = 0;
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        ret = subser->subtitleGetSubHeight();
    }
    return ret;
}

int subtitleGetSubWidth()
{
    int ret = 0;
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        ret = subser->subtitleGetSubWidth();
    }
    return ret;
}

void subtitleSetSubType(int type, int total)
{
    ALOGD("[registerSubtitleMiddleListener]type:%d,total:%d", type, total);
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
#ifdef BUILD_IN_ZTE_SOURCE && ANDROID_PLATFORM_SDK_VERSION == 28
        subser->subtitleSetSubType(type, total);
#else
        subser->subtitleSetSubType(type);
#endif
    }
    return;
}

void registerSubtitleMiddleListener()
{
    ALOGD("[registerSubtitleMiddleListener]");
    const sp<SubtitleServerHidlClient>& subser = getSubtitleService();
    if(subser != 0){
        spEventCB = new EventCallback();
        subser->setListener(spEventCB);
    }
    return;
}

unsigned int getSubState()
{
	ALOGD("sub getSubState, sub_en=%d",sub_en);
	return sub_en;
}

int cc_enable = 0;
int cc_index = 0;
int cc_removed = 0;
int cc_added = 0;
unsigned int getClosedCaptionEnable()
{
   ALOGD("getClosedCaptionEnable, cc_enable=%d",cc_enable);
   return cc_enable;
}

unsigned int getClosedCaptionSelect()
{
   ALOGD("getClosedCaptionSelect, cc_index=%d",cc_index);
   return cc_index;
}

unsigned int getClosedCaptionRemoved(){

   return cc_removed;
}

unsigned int getClosedCaptionAdded(){
   return cc_added;

}


#if 0
void EventCallback::notify (const subtitle_parcel_t &parcel) {
    //ALOGD("eventcallback notify parcel.msgType = %d", parcel.msgType);
    if (parcel.msgType == EVENT_ONSUBTITLEDATA_CALLBACK) {
         ALOGD("subtitleMiddleClient notify parcel.msgType = %d, event:%d, id:%d", parcel.msgType, parcel.bodyInt[0], parcel.bodyInt[1]);
        if(parcel.bodyInt[0] == 1){
           cc_enable = 1;//cc available
           cc_added = 1;
           cc_index = parcel.bodyInt[1];
		   substate_handle(CC_ADD, cc_index);
        }else if(parcel.bodyInt[0] == 0){
           cc_removed = 1;
           cc_enable = 0; // cc unavailable
           cc_index = parcel.bodyInt[1];
		   substate_handle(CC_REMOVED, cc_index);
        }

    } else if (parcel.msgType == EVENT_ONSUBTITLEAVAILABLE_CALLBACK) {
        ALOGD("subtitleMiddleClient notify parcel.msgType = %d, available:%d", parcel.msgType, parcel.bodyInt[0]);
       
        /*Subtitle Error Type
          ERROR_DECODER_TIMEOUT = 2;
          ERROR_DECODER_TIMEERROR = 3;
          ERROR_DECODER_INVALIDDATA = 4;
          ERROR_DECODER_LOSEDATA = 5;
          ERROR_DECODER_END = 6;*/

        if (substate_handle != NULL)
        {
            switch (parcel.bodyInt[0])
            {
                case 1:
                    substate_handle(SUBTITLE_AVAIABLE, 0);
                    sub_en = 1;
                    ALOGD("###MIRADA_IZZI Subtitle Callback State = %d\n", parcel.bodyInt[0]);
                    break;
                case 3:
                    substate_handle(SUBTITLE_UNAVAIABLE, 3);
                    ALOGD("###MIRADA_IZZI Subtitle Callback State = %d\n", parcel.bodyInt[0]);
                    break;
                case 4:
                    substate_handle(SUBTITLE_UNAVAIABLE, 4);
                    ALOGD("###MIRADA_IZZI Subtitle Callback State = %d\n", parcel.bodyInt[0]);
                    break;
                case 5:
                    substate_handle(SUBTITLE_UNAVAIABLE, 100);
                    ALOGD("###MIRADA_IZZI Subtitle Callback State = %d\n", parcel.bodyInt[0]);
                default:
                break;
            }
        }
    }
}
#endif

void subtitle_register(SUBTITLELIS sub)
{
    ALOGD("subtitle_register start");
    substate_handle = sub;
    ALOGD("subtitle_register end");
}

}
