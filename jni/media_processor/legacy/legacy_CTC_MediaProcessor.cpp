/*
 * author: bo.cao@amlogic.com
 * date: 2012-07-20
 * wrap original source code for CTC usage
 */

#define LOG_TAG "legacy_CTC_MediaProcessor"
#include <CTC_MediaProcessor.h>
#include <legacy_CTC_MediaProcessor.h>
#include "../CTC_Log.h"
#include "../CTC_Utils.h"
#include "CTC_MediaControl.h"

#define NOT_IMPLEMENTED() ALOGW("%s() Not Implemented!", __FUNCTION__)

using android::OK;

CTC_MediaProcessor* GetMediaProcessor(int use_omx_decoder, int isDrm)
{
    LOGD("GetMediaProcessor decoder ID %d \n", use_omx_decoder);
    return new CTC_MediaProcessor(use_omx_decoder);
}

CTC_MediaProcessor::CTC_MediaProcessor(int use_omx_decoder)
{
    MLOG("use_omx_decoder:%d", use_omx_decoder);

    aml::CTC_InitialParameter initParam;
    initParam.userId = 0;
    initParam.useOmx = use_omx_decoder;
    initParam.isEsSource = 0;
    initParam.flags = 0;
    initParam.extensionSize = 0;

    ctc_MediaControl = aml::GetMediaProcessor(&initParam);
}

CTC_MediaProcessor::~CTC_MediaProcessor()
{
    MLOG();

    if (ctc_MediaControl != nullptr) {
        delete ctc_MediaControl;
        ctc_MediaControl = nullptr;
    }
}

int CTC_MediaProcessor::GetMediaControlVersion()
{
    return aml::GetMediaProcessorVersion();
}

int CTC_MediaProcessor::GetPlayMode()
{
    int result = ctc_MediaControl->GetPlayMode();
    return result;
}

bool CTC_MediaProcessor::StartPlay()
{
    bool result = ctc_MediaControl->StartPlay();
    return result;
}

bool CTC_MediaProcessor::StartRender()
{
    NOT_IMPLEMENTED();
    return 0;
}

int CTC_MediaProcessor::WriteData(unsigned char* pBuffer, unsigned int nSize)
{
    int result = ctc_MediaControl->WriteData(pBuffer, nSize);
    return result;
}

int CTC_MediaProcessor::WriteData(PLAYER_STREAMTYPE_E type, uint8_t* pBuffer, uint32_t nSize, int64_t pts)
{
    aml::PLAYER_STREAMTYPE_E streamType = aml::PLAYER_STREAMTYPE_TS;
    switch (type) {
    case PLAYER_STREAMTYPE_AUDIO:
        streamType = aml::PLAYER_STREAMTYPE_AUDIO;
        break;

    case PLAYER_STREAMTYPE_VIDEO:
        streamType = aml::PLAYER_STREAMTYPE_VIDEO;
        break;

    case PLAYER_STREAMTYPE_SUBTITLE:
        streamType = aml::PLAYER_STREAMTYPE_SUBTITLE;
        break;

    default:
        break;
    }

    int result = ctc_MediaControl->WriteData(streamType, pBuffer, nSize, pts);
    return result;
}

int CTC_MediaProcessor::SetVideoWindow(int x, int y, int width, int height)
{
    int result = ctc_MediaControl->SetVideoWindow(x, y, width, height);
    return result;
}

int CTC_MediaProcessor::VideoShow()
{
    int result = ctc_MediaControl->VideoShow();
    return result;
}

int CTC_MediaProcessor::VideoHide()
{
    int result = ctc_MediaControl->VideoHide();
    return result;
}

void CTC_MediaProcessor::InitVideo(PVIDEO_PARA_T pVideoPara)
{
    aml::VIDEO_PARA_T paras[MAX_VIDEO_PARAM_SIZE];
    memset(paras, 0, sizeof(paras));

    aml::VIDEO_PARA_T* para = paras;
    int count = 0;
    while (pVideoPara->pid != 0 && count < MAX_VIDEO_PARAM_SIZE) {
        para->pid = pVideoPara->pid;  //pid
        para->nVideoWidth = pVideoPara->nVideoWidth;
        para->nVideoHeight = pVideoPara->nVideoHeight;
        para->nFrameRate = pVideoPara->nFrameRate;
        para->vFmt = pVideoPara->vFmt;
        para->nExtraSize = pVideoPara->nExtraSize;
        para->pExtraData = pVideoPara->pExtraData;

        pVideoPara++;
        para++;
        count++;
    }

    ctc_MediaControl->InitVideo(paras);
}

void CTC_MediaProcessor::InitAudio(PAUDIO_PARA_T pAudioPara)
{
    aml::AUDIO_PARA_T paras[MAX_AUDIO_PARAM_SIZE];
    memset(paras, 0, sizeof(paras));

    aml::AUDIO_PARA_T* para = paras;
    int count = 0;
    while ((pAudioPara->pid != 0 || pAudioPara->nSampleRate != 0) && count < MAX_AUDIO_PARAM_SIZE) {
        para->pid = pAudioPara->pid;  //pid
        para->nChannels = pAudioPara->nChannels;
        para->nSampleRate = pAudioPara->nSampleRate;
        para->aFmt = pAudioPara->aFmt;
        para->block_align = 0;
        para->bit_per_sample = 0;
        para->nExtraSize = pAudioPara->nExtraSize;
        para->pExtraData = pAudioPara->pExtraData;

        pAudioPara++;
        para++;
        count++;
    }

    ctc_MediaControl->InitAudio(paras);
}

bool CTC_MediaProcessor::Pause()
{
    bool result = ctc_MediaControl->Pause();
    return result;
}

bool CTC_MediaProcessor::Resume()
{
    bool result = ctc_MediaControl->Resume();
    return result;
}

bool CTC_MediaProcessor::Fast()
{
    bool result = ctc_MediaControl->Fast();
    return result;
}

bool CTC_MediaProcessor::StopFast()
{
    bool result = ctc_MediaControl->StopFast();
    return result;
}

bool CTC_MediaProcessor::Stop()
{
    bool result = ctc_MediaControl->Stop();
    return result;
}

bool CTC_MediaProcessor::Seek()
{
    bool result = ctc_MediaControl->Seek();
    return result;
}

bool CTC_MediaProcessor::SetVolume(int volume)
{
    bool result = ctc_MediaControl->SetVolume((float)volume);

    return result;
}

int CTC_MediaProcessor::GetVolume()
{
    int result = ctc_MediaControl->GetVolume();
    return result;
}

bool CTC_MediaProcessor::SetRatio(int nRatio)
{
    bool result = ctc_MediaControl->SetRatio((aml::CONTENTMODE_E)nRatio);

    return result;
}

int CTC_MediaProcessor::GetAudioBalance()
{
    int result = ctc_MediaControl->GetAudioBalance();
    return result;
}

bool CTC_MediaProcessor::SetAudioBalance(int nAudioBalance)
{
    bool result = ctc_MediaControl->SetAudioBalance((aml::ABALANCE_E)nAudioBalance);

    return result;
}

void CTC_MediaProcessor::GetVideoPixels(int& width, int& height)
{
    ctc_MediaControl->GetVideoPixels(width, height);
    return;
}

bool CTC_MediaProcessor::IsSoftFit()
{
    bool result = ctc_MediaControl->IsSoftFit();
    return result;
}

void CTC_MediaProcessor::SetEPGSize(int w, int h)
{
    ctc_MediaControl->SetEPGSize(w, h);
    return;
}

void CTC_MediaProcessor::SetSurface_ANativeWindow(ANativeWindow* pSurface)
{
    ctc_MediaControl->SetSurface_ANativeWindow(pSurface);
}

void CTC_MediaProcessor::SetSurface(android::Surface* pSurface)
{
    ctc_MediaControl->SetSurface(pSurface);
    return;
}

long CTC_MediaProcessor::GetCurrentPlayTime()
{
    long currentPlayTime = 0;
    currentPlayTime = ctc_MediaControl->GetCurrentPlayTime();  //ms
    return currentPlayTime;
}

void CTC_MediaProcessor::InitSubtitle(PSUBTITLE_PARA_T sParam)
{
    aml::SUBTITLE_PARA_T paras[MAX_SUBTITLE_PARAM_SIZE];
    memset(paras, 0, sizeof(paras));

    aml::SUBTITLE_PARA_T* para = paras;
    int count = 0;
    while (sParam->pid != 0 && count < MAX_SUBTITLE_PARAM_SIZE) {
        para->sub_type = convertSubType((SUB_TYPE)sParam->sub_type);
        aml::setSubtitleId(para, para->sub_type, sParam->pid);

        sParam++;
        para++;
        count++;
    }

    ctc_MediaControl->InitSubtitle(paras);
}

bool CTC_MediaProcessor::SubtitleShowHide(bool bShow)
{
    bool result = ctc_MediaControl->SubtitleShowHide(bShow);
    return result;
}

void CTC_MediaProcessor::SwitchAudioTrack_ZTE(PAUDIO_PARA_T pAudioPara)
{
    aml::AUDIO_PARA_T para;
    memset(&para, 0, sizeof(para));

    para.pid = pAudioPara->pid;  //pid
    para.nChannels = pAudioPara->nChannels;
    para.nSampleRate = pAudioPara->nSampleRate;
    para.aFmt = pAudioPara->aFmt;
    para.block_align = 0;
    para.bit_per_sample = 0;
    para.nExtraSize = pAudioPara->nExtraSize;
    para.pExtraData = pAudioPara->pExtraData;

    ctc_MediaControl->SwitchAudioTrack(para.pid, &para);
}

void CTC_MediaProcessor::SwitchVideoTrack(PVIDEO_PARA_T pVideoPara)
{
    aml::VIDEO_PARA_T para;
    memset(&para, 0, sizeof(para));

    para.pid = pVideoPara->pid;  //pid
    para.nVideoWidth = pVideoPara->nVideoWidth;
    para.nVideoHeight = pVideoPara->nVideoHeight;
    para.nFrameRate = pVideoPara->nFrameRate;
    para.vFmt = pVideoPara->vFmt;
    para.nExtraSize = pVideoPara->nExtraSize;
    para.pExtraData = pVideoPara->pExtraData;

    ctc_MediaControl->SwitchVideoTrack(para.pid, &para);
}

void CTC_MediaProcessor::ClearLastFrame(void)
{
    ctc_MediaControl->SetStopMode(false);
}

void CTC_MediaProcessor::BlackOut(int EarseLastFrame)
{
    ctc_MediaControl->SetStopMode(!EarseLastFrame);
}

bool CTC_MediaProcessor::SetErrorRecovery(int mode)
{
    NOT_IMPLEMENTED();

    return true;
}

void CTC_MediaProcessor::GetAvbufStatus(PAVBUF_STATUS pstatus)
{
    aml::AVBUF_STATUS avStatus;

    int ret = ctc_MediaControl->GetBufferStatus(&avStatus);
    if (ret == OK) {
        pstatus->abuf_data_len = avStatus.abuf_data_len;
        pstatus->abuf_size = avStatus.abuf_size;
        pstatus->abuf_free_len = avStatus.abuf_size - avStatus.abuf_data_len;
        pstatus->vbuf_data_len = avStatus.vbuf_data_len;
        pstatus->vbuf_size = avStatus.vbuf_size;
        pstatus->vbuf_free_len = avStatus.vbuf_size - avStatus.vbuf_data_len;
    }
}

int CTC_MediaProcessor::GetRealTimeFrameRate()
{
    NOT_IMPLEMENTED();

    return 0;
}

int CTC_MediaProcessor::GetVideoFrameRate()
{
    NOT_IMPLEMENTED();

    return 0;
}

int CTC_MediaProcessor::GetCurrentVidPTS(unsigned long long* pPTS)
{
    *pPTS = ctc_MediaControl->GetCurrentPts(aml::PLAYER_STREAMTYPE_VIDEO);

    return 0;
}

void CTC_MediaProcessor::GetVideoInfo(int* width, int* height, int* ratio, int* frameformat)
{
    aml::VIDEO_INFO_T vinfo;
    memset(&vinfo, 0, sizeof(vinfo));

    ctc_MediaControl->GetStreamInfo(&vinfo, nullptr, nullptr);
    *width = vinfo.nVideoWidth;
    *height = vinfo.nVideoHeight;
    *ratio = vinfo.nAspect;
    *frameformat = vinfo.interlaced;  //interlace or progressive
}

int CTC_MediaProcessor::GetAVPTSDiff(int* avdiff)
{
    //int result = ctc_MediaControl->playerback_getStatusInfo(IPTV_PLAYER_ATTR_VIDAUDDIFF,avdiff);
    NOT_IMPLEMENTED();
    return 0;
}

void CTC_MediaProcessor::playerback_register_evt_cb(IPTV_PLAYER_EVT_CB pfunc, void* hander)
{
    mEventCb = pfunc;
    mEventHandler = hander;

    ctc_MediaControl->playerback_register_evt_cb([](void* handler, aml::IPTV_PLAYER_EVT_E evt, uint32_t param1, uint32_t param2) {
        CTC_MediaProcessor* processor = (CTC_MediaProcessor*)handler;
        return processor->internalEventCb(evt, param1, param2);
    }, this);
}

void CTC_MediaProcessor::RegisterParamEvtCb(void* hander, IPTV_PLAYER_PARAM_Evt_e enEvt, IPTV_PLAYER_PARAM_EVENT_CB pfunc)
{
    //ctc_MediaControl->RegisterParamEvtCb (hander,enEvt ,pfunc);
    NOT_IMPLEMENTED();
    return;
}

int CTC_MediaProcessor::playerback_getStatusInfo(IPTV_ATTR_TYPE_e enAttrType, int* value)
{
    //int result = ctc_MediaControl->playerback_getStatusInfo (enAttrType ,value);
    NOT_IMPLEMENTED();
    return 0;
}

int CTC_MediaProcessor::GetPlayerInstanceNo(void)
{
    //int result = ctc_MediaControl->GetPlayerInstanceNo();
    //LOGD("lxy GetPlayerInstanceNo %d\n",result);
    NOT_IMPLEMENTED();
    return 0;
}

void CTC_MediaProcessor::ExecuteCmd(const char* cmd)
{
    //ctc_MediaControl->ExecuteCmd(cmd);
    //LOGD("ExecuteCmd cmd %s\n",cmd);
    NOT_IMPLEMENTED();
    return;
}

int CTC_MediaProcessor::StartVideo()
{
    ctc_MediaControl->SetParameter(aml::CTC_KEY_PARAMETER_START_VIDEO, nullptr);

    return 0;
}

int CTC_MediaProcessor::StopVideo()
{
    ctc_MediaControl->SetParameter(aml::CTC_KEY_PARAMETER_STOP_VIDEO, nullptr);

    return 0;
}

int CTC_MediaProcessor::StartAudio()
{
    ctc_MediaControl->SetParameter(aml::CTC_KEY_PARAMETER_START_AUDIO, nullptr);

    return 0;
}

int CTC_MediaProcessor::StopAudio()
{
    ctc_MediaControl->SetParameter(aml::CTC_KEY_PARAMETER_STOP_AUDIO, nullptr);

    return 0;
}

int CTC_MediaProcessor::StartSubtitle()
{
    ctc_MediaControl->SetParameter(aml::CTC_KEY_PARAMETER_START_SUBTITLE, nullptr);

    return 0;
}

int CTC_MediaProcessor::StopSubtitle()
{
    ctc_MediaControl->SetParameter(aml::CTC_KEY_PARAMETER_STOP_SUBTITLE, nullptr);

    return 0;
}

int CTC_MediaProcessor::EnableAudioOutput()
{
    int result = ctc_MediaControl->EnableAudioOutput();

    return result;
}

int CTC_MediaProcessor::DisableAudioOutput()
{
    int result = ctc_MediaControl->DisableAudioOutput();

    return result;
}

int CTC_MediaProcessor::GetVideoPTS()
{
    return ctc_MediaControl->GetCurrentPts(aml::PLAYER_STREAMTYPE_VIDEO);
}

int CTC_MediaProcessor::SetSurfaceVideo(int x, int y, int width, int height)
{
    //int result = ctc_MediaControl->SetSurfaceVideo(x, y, width, height);
    NOT_IMPLEMENTED();
    return 0;
}

int GetMediaProcessorVersion()
{
    return 2;
}

int CTC_MediaProcessor::ClosedCaptionsSelect(int id)
{
    aml::SUBTITLE_PARA_T para;
    memset(&para, 0, sizeof(para));

    para.sub_type = aml::CC;
    para.cc_param.Channel_ID = id;

    ctc_MediaControl->SwitchSubtitle(0x1FFF, &para);
    return 0;
}

int CTC_MediaProcessor::ClosedCaptionsEnable()
{
    int ccEnable = 0;

    ctc_MediaControl->GetParameter(aml::CTC_KEY_PARAMETER_CC_ENABLE, &ccEnable);

    return ccEnable;
}

void CTC_MediaProcessor::GetDecodeFrameInfo(void* DecoderInfo, TIF_HAL_PlaybackType_t eType)
{
    if (eType == TIF_HAL_PLAYBACK_TYPE_ALL) {
        TIF_HAL_PlaybackStatus_t* info = (TIF_HAL_PlaybackStatus_t*)DecoderInfo;
        memset(info, 0, sizeof(*info));

        aml::VIDEO_INFO_T vinfo;
        aml::AUDIO_INFO_T ainfo;
        aml::SUBTITLE_INFO_T sinfo;
        ctc_MediaControl->GetStreamInfo(&vinfo, &ainfo, &sinfo);

        info->video.enabled = vinfo.enabled;
        info->video.info.width = vinfo.nVideoWidth;
        info->video.info.height = vinfo.nVideoHeight;
        info->video.info.frameRate = vinfo.nFrameRate;
        info->video.info.pixelAspectRatio = vinfo.nAspect;
        info->video.info.activeFormatDescription = vinfo.activeFormatDescription;
        info->video.statistics.numDecodedFrames = vinfo.nDecoded;
        info->video.statistics.numDisplayedFrames = vinfo.nDisplayed;
        info->video.statistics.numDecoderErrors = vinfo.nDecoded;
        info->video.statistics.numDecoderOverflows = vinfo.nDecoderOverflows;
        info->video.statistics.numDecoderUnderflows = vinfo.nDecoderUnderflows;
        info->video.statistics.numFramesDropped = vinfo.nFramesDropped;

        info->audio.enabled = ainfo.enabled;
        info->audio.info.numChannels = ainfo.nChannels;
        info->audio.info.sampleRate = ainfo.nSampleRate;
        info->audio.info.channelConfiguration = (TIF_HAL_Playback_AudioSourceChannelConfiguration_t)ainfo.channelConfiguration;
        info->audio.statistics.numDecodedSamples = ainfo.nDecoded;
        info->audio.statistics.numOutputSamples = ainfo.nOutputSamples;
        info->audio.statistics.numDecoderErrors = ainfo.nDecoderErrors;
        info->audio.statistics.numDecoderOverflows = ainfo.nDecoderOverflows;
        info->audio.statistics.numDecoderUnderflows = ainfo.nDecoderUnderFlows;
        info->audio.statistics.numSamplesDiscarded = ainfo.nSamplesDiscarded;


        info->subtitles.enabled = sinfo.enabled;
        info->subtitles.info.width = sinfo.width;
        info->subtitles.info.height = sinfo.height;
        memcpy(info->subtitles.info.iso639Code, sinfo.iso639Code, sizeof(info->subtitles.info.iso639Code));
        info->subtitles.statistics.numDecoderErrors = sinfo.nDecodeErrors;
    } else if (TIF_HAL_PLAYBACK_TYPE_VIDEO == eType) {
        //reserved.
    } else if (TIF_HAL_PLAYBACK_TYPE_AUDIO == eType) {
        //reserved.
    } else if (TIF_HAL_PLAYBACK_TYPE_SUBTITLES == eType) {
        //reserved.
    } else {
        LOGE("type error!\n");
    }
}

int CTC_MediaProcessor::SetEcmCallback(tSetEcmCallback cb)
{
    NOT_IMPLEMENTED();

    return 0;
}

void CTC_MediaProcessor::SetSubtitleCallback(SUBTITLELIS sub)
{
    mSubtitleCb = sub;
}

int CTC_MediaProcessor::GetRatio()
{
    aml::VIDEO_INFO_T vinfo;
    ctc_MediaControl->GetStreamInfo(&vinfo, nullptr, nullptr);

    return vinfo.nAspect;
}

int CTC_MediaProcessor::GetHDMIState()
{
    int HdmiState = 0;
    ctc_MediaControl->GetParameter(aml::CTC_KEY_PARAMETER_HDMI_STATE, &HdmiState);
    LOGD("CTC_MediaProcessor HdmiState = %d \n", HdmiState);

    return HdmiState;
}

int CTC_MediaProcessor::ClosedCaptionsRemoved()
{
    int ccRemoved = 0;
    ctc_MediaControl->GetParameter(aml::CTC_KEY_PARAMETER_CC_REMOVED, &ccRemoved);

    return ccRemoved;
}

void CTC_MediaProcessor::SwitchSubtitle(int pid, int type)
{
    aml::SUBTITLE_PARA_T para;
    memset(&para, 0, sizeof(para));

    para.sub_type = convertSubType((SUB_TYPE)type);
    aml::setSubtitleId(&para, para.sub_type, pid);

    ctc_MediaControl->SwitchSubtitle(pid, &para);
}

void CTC_MediaProcessor::SetAudioDelay(char* mode)
{
    ctc_MediaControl->SetParameter(aml::CTC_KEY_PARAMETER_AUDIO_DELAY, mode);
}

////////////////////////////////////////////////////////////////////////////////
aml::SubtitleType CTC_MediaProcessor::convertSubType(SUB_TYPE subType)
{
    aml::SubtitleType amlSubType;

    switch (subType) {
    case CODEC_ID_DVB_SUBTITLE:
        amlSubType = aml::DVB_SUBTITLE;
        break;

    case CODEC_ID_DVB_TELETEXT:
        amlSubType = aml::TELETEXT;
        break;

    case CODEC_ID_SCTE27:
        amlSubType = aml::SCTE27;
        break;

    case CODEC_ID_CLOSEDCAPTION:
        amlSubType = aml::CC;
        break;

    default:
        amlSubType = aml::UNKNOWN_SUBTYPE;
        ALOGW("Unknown sub_type:%#x", subType);
        break;
    }

    return amlSubType;
}

void CTC_MediaProcessor::internalEventCb(aml::IPTV_PLAYER_EVT_E evt, uint32_t param1, uint32_t param2)
{
    switch (evt) {
    case aml::IPTV_PLAYER_EVT_FIRST_PTS:
        if (mEventCb)
            mEventCb(IPTV_PLAYER_EVT_FIRST_PTS, mEventHandler);
        break;

    case aml::IPTV_PLAYER_EVT_Subtitle_Available:
        if (mSubtitleCb)
            mSubtitleCb(SUBTITLE_AVAIABLE, 0);
        break;

    default:
        ALOGW("unhandled event:%#x", evt);
        break;
    }
}
