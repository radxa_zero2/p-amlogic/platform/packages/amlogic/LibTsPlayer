//#define LOG_NDEBUG 0
#define LOG_TAG "CTC_FdDataSource"
#include <utils/Log.h>
#include <utils/CallStack.h>
#include <sys/stat.h>
#include <sys/eventfd.h>
#include "FdDataSource.h"
#include "AmFFmpegByteIOAdapter.h"

#include <media/stagefright/foundation/ALooper.h>
#include <unistd.h>
#include <poll.h>

#define POLL_NOTIFY_FD_INDEX    0
#define POLL_DATA_FD_INDEX      1

using namespace android;

FdDataSource::FdDataSource(int fd, int64_t offset, int64_t length)
: mFd(fd)
, mOffset(offset)
, mLength(length)
{
    ALOGI("fd:%d, init mFd:%d", fd, mFd);

    if (mFd >= 0) {
        struct stat s;
        if (fstat(fd, &s) == 0) {
            if (S_ISREG(s.st_mode)) {
                if (mLength < 0) {
                    mLength = s.st_size;
                    ALOGV("initial mLength to filesize:%lld", mLength);
                }

                if (mOffset + mLength > s.st_size) {
                    mLength = s.st_size - mOffset;
                }
            } else if (S_ISFIFO(s.st_mode)) {
                ALOGI("pipe source! initial size:%lld", s.st_size);
                mIsPipe = true;
            }
        }

        mInitCheck = OK;
    }

    mNotifyFd = eventfd(0, EFD_NONBLOCK);
    ALOGI("notify_fd:%d", mNotifyFd);
}

FdDataSource::~FdDataSource()
{
    ALOGI("%s", __FUNCTION__);

    close();

    if (mNotifyFd >= 0) {
        ::close(mNotifyFd);
        ALOGI("close notify_fd:%d", mNotifyFd);
        mNotifyFd = -1;
    }
}

ssize_t FdDataSource::readAt(void *data, size_t size, off64_t offset)
{
    uint8_t* buffer = (uint8_t*)data;
    ssize_t ret = 0;
    ssize_t readSize = 0;
    int i;

    if (mStopped || mFd < 0) {
        ALOGV("mStopped:%d, fd=%d", mStopped, mFd);
        return 0;
    }

    if (offset != mOffset) {
        ALOGV("readAt discontinue:%lld, mOffset:%lld", offset, mOffset);
        seekTo(offset);
    }

    if (mLength > 0 && offset >= mLength) {
        ALOGV("request offset exceed length!");
        return 0;
    }

    //onReadBegin();

    struct pollfd fds[2];
    fds[0].fd = mNotifyFd;
    fds[0].events = POLLIN | POLLERR;
    fds[0].revents = 0;

    fds[1].fd = mFd;
    fds[1].events = POLLIN | POLLERR;
    fds[1].revents = 0;
    ret = poll(fds, 2, 1000);
    if (ret < 0) {
        ALOGE("poll failed:%s, mFd:%d", strerror(errno), mFd);
        goto exit;
    } else if (ret == 0) {
        ALOGE("poll timeout! mFd:%d", mFd);
        goto exit;
    }

    i = 2;

    while (i--) {
        if (fds[i].revents == 0)
            continue;

        switch (i) {
        case POLL_NOTIFY_FD_INDEX:
        {
            ALOGI("handle notify_fd event, revents:%d, ret:%d, request size:%d, return readSize:%d",
                    fds[i].revents, ret, size, readSize);
            if (fds[i].revents & POLLIN) {
                int64_t count;
                int readLen = ::read(mNotifyFd, &count, sizeof(int64_t));
                if (readLen < 0) {
                    ALOGE("read notify_fd failed with %d", errno);
                }
            }
            mStopped = true;
            
            //CallStack("DEBUG");
            goto exit;
        }
        break;

        case POLL_DATA_FD_INDEX:
        {
doRead:
            readSize = ::read(mFd, buffer, size);
            if (readSize <= 0) {
                ALOGE("readAt return %d, errno:%d", readSize, errno);
                if (errno == EINTR) {
                    goto doRead;
                }
            } else {
                mOffset += readSize;
            }
        }
        break;
        }
    }

exit:
    //onReadEnd(readSize);
    return readSize >= 0 ? readSize : -errno;
}

status_t FdDataSource::getSize(off64_t *size)
{
    ALOGV("getSize mLength: %lld", mLength);

    *size = mLength;
    return OK;
}

void FdDataSource::setFlags(uint32_t flags)
{
#if 0
    MiniDataSource::setFlags(flags);

    if (mFlags & kNonBlock) {
        ALOGI("set fd nonblock mode");
        if (fcntl(mFd, F_SETFL, fcntl(mFd, F_GETFL) | O_NONBLOCK) < 0) {
            ALOGW("set fd nonblock mode failed! %s", strerror(errno));
        }
    } else if (!(mFlags & kNonBlock)) {
        ALOGI("clear fd nonblock mode");
        if (fcntl(mFd, F_SETFL, fcntl(mFd, F_GETFL) &~ O_NONBLOCK) < 0) {
            ALOGW("clear fd nonblock mode failed! %s", strerror(errno));
        }
    }
#endif
}

void FdDataSource::flush()
{
#if 0
    if (mIsPipe) {
        int bufferSize = 4096;
        uint8_t dummyBuffer[bufferSize];
        ssize_t ret = 0;
        int64_t flushBegin = ALooper::GetNowUs();

        uint32_t flags = mFlags;
        setFlags(flags | MiniDataSource::kNonBlock);

        for (;;) {
            ret = ::read(mFd, dummyBuffer, bufferSize);
            if (ret == 0) {
                ALOGI("flush return EOF");
                break;
            } else if (ret < 0) {
                ALOGI("flush return %d", -errno);
                break;
            }

            if (ALooper::GetNowUs() - flushBegin > kFlushTimeout) {
                ALOGW("flush timeout!!!");
                break;
            }
        }

        setFlags(flags);
    }
#endif

    mStopped = false;

    //MiniDataSource::flush();
    
    ALOGI("flush finished!");
}

int64_t FdDataSource::getCachedSize()
{
    ssize_t size = -1;
    status_t err = OK;

    if (mFd >= 0) {
        struct stat s;
        err = fstat(mFd, &s);
        if (err < 0) {
            ALOGE("fstat failed with %d", errno);
        } else {
            size = s.st_size;
        }
    }

    return size;
}

void FdDataSource::disconnect()
{
    ALOGI("signal notify event, notify_fd:%d", mNotifyFd);

    mIsDisconnecting = true;

    if (mNotifyFd >= 0) {
        int64_t count = 1;

        if (::write(mNotifyFd, &count, sizeof(int64_t)) < 0) {
            ALOGE("signal notify_fd failed with %d", errno);
        }
    } else {
        //TODO: fall back to do this...
        close();
    }
}

void FdDataSource::finishDisconnect()
{
    ALOGI("%s", __FUNCTION__);

    int64_t count = 0;
    int ret = ::read(mNotifyFd, &count, sizeof(int64_t));
    if (ret > 0) {
        ALOGI("clear notify event!");
    }

    if (mIsDisconnecting) {
        mIsDisconnecting = false;

        mStopped = true;
        close();
    }
}

void FdDataSource::close()
{
#if 1
    if (mFd >= 0) {
        ALOGI("close mFd %d", mFd);
        ::close(mFd);
        mFd = -1;
    }
#endif
}

void FdDataSource::setFd(int fd)
{
    close();
    mFd = fd;

    ALOGI("set mFd:%d", mFd);
}

void FdDataSource::seekTo(off64_t offset)
{
    int ret = 0;

    mOffset = offset;
    if ((ret = ::lseek64(mFd, offset, SEEK_SET)) < 0) {
        ALOGE("lseek64 failed: %d, offset:%lld", ret, offset);    
    }
}

