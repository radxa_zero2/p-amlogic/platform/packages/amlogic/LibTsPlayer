/* 
 * Copyright (C) 2019 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "CTCMediaPlayer"

#include <cutils/properties.h>
#include <sys/stat.h>
#include <gui/Surface.h>
#include <media/ammediaplayerext.h>
#include <media/stagefright/foundation/AHandlerReflector.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/ALooper.h>
#include <media/stagefright/Utils.h>
#include <utils/String16.h>
#include <CTC_MediaProcessor.h>
#include "../media_processor/CTC_Log.h"
#include "CTCMediaPlayer.h"
#include "../media_processor/CTC_Utils.h"
#include "SourceFeeder.h"
#include <amports/aformat.h>
#include <amports/vformat.h>
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
}

namespace android {

// key for media statistics
static const char *kKeyPlayer = "CTCMediaPlayer";
// attrs for media statistics
    // NB: these are matched with public Java API constants defined
    // in frameworks/base/media/java/android/media/MediaPlayer.java
    // These must be kept synchronized with the constants there.
static const char *kPlayerVMime = "android.media.mediaplayer.video.mime";
static const char *kPlayerVCodec = "android.media.mediaplayer.video.codec";
static const char *kPlayerWidth = "android.media.mediaplayer.width";
static const char *kPlayerHeight = "android.media.mediaplayer.height";
static const char *kPlayerFrames = "android.media.mediaplayer.frames";
static const char *kPlayerFramesDropped = "android.media.mediaplayer.dropped";
static const char *kPlayerAMime = "android.media.mediaplayer.audio.mime";
static const char *kPlayerACodec = "android.media.mediaplayer.audio.codec";
static const char *kPlayerDuration = "android.media.mediaplayer.durationMs";
static const char *kPlayerPlaying = "android.media.mediaplayer.playingMs";
static const char *kPlayerError = "android.media.mediaplayer.err";
static const char *kPlayerErrorCode = "android.media.mediaplayer.errcode";

    // NB: These are not yet exposed as public Java API constants.
static const char *kPlayerErrorState = "android.media.mediaplayer.errstate";
static const char *kPlayerDataSourceType = "android.media.mediaplayer.dataSource";
//
static const char *kPlayerRebuffering = "android.media.mediaplayer.rebufferingMs";
static const char *kPlayerRebufferingCount = "android.media.mediaplayer.rebuffers";
static const char *kPlayerRebufferingAtExit = "android.media.mediaplayer.rebufferExit";

struct CTCMediaPlayerReceiver : public ISourceReceiver
{
    explicit CTCMediaPlayerReceiver(const std::shared_ptr<aml::CTC_MediaProcessor>& ctc)
    : mCtc(ctc)
    {

    }

    int GetWriteBuffer(SourceFeeder::MediaType type, uint8_t** pBuffer, size_t* nSize) override {
        return mCtc->GetWriteBuffer(getStreamType(type), pBuffer, nSize);
    }

    int WriteData(uint8_t* pBuffer, size_t nSize) override {
        return mCtc->WriteData(pBuffer, nSize);
    }

    int WriteData(SourceFeeder::MediaType type, uint8_t* pBuffer, uint32_t nSize, int64_t pts) override {
        return mCtc->WriteData(getStreamType(type), pBuffer, nSize, pts);
    }

private:
    aml::PLAYER_STREAMTYPE_E getStreamType(SourceFeeder::MediaType type) {
        aml::PLAYER_STREAMTYPE_E streamType = aml::PLAYER_STREAMTYPE_TS;
        switch (type) {
        case SourceFeeder::MEDIA_TYPE_VIDEO:
            streamType = aml::PLAYER_STREAMTYPE_VIDEO;
            break;

        case SourceFeeder::MEDIA_TYPE_AUDIO:
            streamType = aml::PLAYER_STREAMTYPE_AUDIO;
            break;

        case SourceFeeder::MEDIA_TYPE_SUBTITLE:
            streamType = aml::PLAYER_STREAMTYPE_SUBTITLE;
            break;

        default:
            break;
        }

        return streamType;
    }

    std::shared_ptr<aml::CTC_MediaProcessor> mCtc;
};

///////////////////////////////////////////////////////////////////////////////
CTCMediaPlayer::CTCMediaPlayer(pid_t pid)
{
    ALOGD("CTCMediaPlayer(%p) created, clientPid(%d)", this, pid);

    memset(mVideoParas, 0, sizeof(mVideoParas));
    memset(mAudioParas, 0, sizeof(mAudioParas));
    memset(mSubtitleParas, 0, sizeof(mSubtitleParas));

    // set up an analytics record
#if ANDROID_PLATFORM_SDK_VERSION <= 28
    mAnalyticsItem = new MediaAnalyticsItem(kKeyPlayer);
#else
    mAnalyticsItem = MediaAnalyticsItem::create(kKeyPlayer);
#endif

    initConfig();

    mHandler = new AHandlerReflector<CTCMediaPlayer>(this);

    memset(&mStreamInfo, 0, sizeof(mStreamInfo));
}

CTCMediaPlayer::~CTCMediaPlayer()
{
    ALOGV("~CTCMediaPlayer(%p)", this);

    if (mLooper) {
        if (mSourceFeeder) {
            mLooper->unregisterHandler(mSourceFeeder->id());
        }

        if (mHandler) {
            mLooper->unregisterHandler(mHandler->id());
        }

        mLooper->stop();
    }

    for (int i = 0; i < mStreamInfo.stream_info.total_video_num; i++) {
        if (mStreamInfo.video_info[i] != NULL) {
            free(mStreamInfo.video_info[i]);
            mStreamInfo.video_info[i] = NULL;
        }
    }

    for (int i = 0; i < mStreamInfo.stream_info.total_audio_num; i++) {
        if (mStreamInfo.audio_info[i] != NULL) {
            free(mStreamInfo.audio_info[i]);
            mStreamInfo.audio_info[i] = NULL;
        }
    }

    if (mAnalyticsItem) {
        delete mAnalyticsItem;
        mAnalyticsItem = nullptr;
    }

    MLOG();
}

status_t CTCMediaPlayer::initCheck()
{
    MLOG();

    initLooper();

    return OK;
}

status_t CTCMediaPlayer::setUID(uid_t uid)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::setDataSource(const sp<IMediaHTTPService>& httpService,
        const char* url,
        const KeyedVector<String8, String8> *headers)
{
    MLOG("url:%s", url);

    AutoMutex _l(mLock);

    mUrl = url;
    if (!strncasecmp(url, "udp://", 6)) {
        mIsLive = true;
    }

    mSourceFeeder = new SourceFeeder(url);
    mState = STATE_UNPREPARED;

    return OK;
}

status_t CTCMediaPlayer::setDataSource(int fd, int64_t offset, int64_t length)
{
    MLOG("fd:%d, offset:%lld, length:%lld", fd, offset, length);

    const size_t SIZE = 256;
    char buffer[SIZE];
    snprintf(buffer, SIZE, "/proc/%d/fd/%d", getpid(), fd);
    struct stat s;
    if (lstat(buffer, &s) == 0) {
        if ((s.st_mode & S_IFMT) == S_IFLNK) {
            char linkto[256];
            int len = readlink(buffer, linkto, sizeof(linkto));
            if (len > 0) {
                if (len > 255) {
                    linkto[252] = '.';
                    linkto[253] = '.';
                    linkto[254] = '.';
                    linkto[255] = 0;
                } else {
                    linkto[len] = 0;
                }

                ALOGI("fd_url:%s", linkto);
            }
        }
    }

    AutoMutex _l(mLock);
    mFd = ::dup(fd);

    mSourceFeeder = new SourceFeeder(mFd, offset, length);
    mState = STATE_UNPREPARED;

    return OK;
}

status_t CTCMediaPlayer::setDataSource(const sp<IStreamSource> &source)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::setDataSource(const sp<DataSource>& dataSource)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::setVideoSurfaceTexture(const sp<IGraphicBufferProducer>& bufferProducer)
{
    MLOG();

    sp<AMessage> msg = new AMessage(kWhatSetVideoSurface, mHandler);

    if (bufferProducer == nullptr) {
        msg->setObject("surface", NULL);
    } else {
        msg->setObject("surface", new Surface(bufferProducer, true));
    }

    msg->post();

    return OK;
}

status_t CTCMediaPlayer::getBufferingSettings(BufferingSettings* buffering)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::setBufferingSettings(const BufferingSettings& buffering)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::prepare()
{
    MLOG();

    sp<AMessage> msg = new AMessage(kWhatPrepare, mHandler);
    sp<AMessage> response;
    status_t err;

    msg->postAndAwaitResponse(&response);
    CHECK(response->findInt32("err", &err));

    return err;
}

status_t CTCMediaPlayer::prepareAsync()
{
    MLOG();

    sp<AMessage> msg = new AMessage(kWhatPrepare, mHandler);
    msg->post();

    return OK;
}

status_t CTCMediaPlayer::start()
{
    MLOG();

    sp<AMessage> msg = new AMessage(kWhatStart, mHandler);
    msg->post();

    return OK;
}

status_t CTCMediaPlayer::stop()
{
    MLOG();

    sp<AMessage> msg = new AMessage(kWhatStop, mHandler);
    msg->post();

    return OK;
}

status_t CTCMediaPlayer::pause()
{
    MLOG();

    sp<AMessage> msg = new AMessage(kWhatPause, mHandler);
    msg->post();

    return OK;
}

bool CTCMediaPlayer::isPlaying()
{
    return mState == STATE_RUNNING;
}

status_t CTCMediaPlayer::setPlaybackSettings(const AudioPlaybackRate& rate)
{
    MLOG();

    return true;
}

status_t CTCMediaPlayer::getPlaybackSettings(AudioPlaybackRate* rate)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::setSyncSettings(const AVSyncSettings& sync, float videoFpsHint)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::getSyncSettings(AVSyncSettings* sync, float* videoFps)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::seekTo(int msec, MediaPlayerSeekMode mode)
{
    int64_t seekTimeUs = msec * 1000ll;

    sp<AMessage> msg = new AMessage(kWhatSeek, mHandler);
    msg->setInt64("seekTimeUs", seekTimeUs);
    msg->setInt32("mode", mode);

    msg->post();

    return OK;
}

status_t CTCMediaPlayer::getCurrentPosition(int* msec)
{
    AutoMutex _l(mLock);

    int64_t pts = AV_NOPTS_VALUE;
    if (mProcessor) {
        pts = mProcessor->GetCurrentPts(aml::PLAYER_STREAMTYPE_VIDEO);
    }

    int64_t currentUsec = AV_NOPTS_VALUE;
    if (pts != AV_NOPTS_VALUE) {
        currentUsec = (pts&0xFFFFFFFF) * 100 / 9;
    }

    if (mStartTime == AV_NOPTS_VALUE) {
        int64_t ffStartPts = mSourceFeeder->getStartTime() * 9 / 100;

        ALOGW("first pts:%lld(%#llx), ffStartPts:%lld(%#llx), ffStartTime:%lld",
              pts, pts, ffStartPts, ffStartPts, mSourceFeeder->getStartTime());

        int64_t durationUs = mSourceFeeder->getDuration();

        mStartTime = (ffStartPts & 0xFFFFFFFF) * 100 / 9;
        if (currentUsec != AV_NOPTS_VALUE && currentUsec - mStartTime > durationUs) {
            mStartTime = currentUsec;
        }
    }

    int64_t nowUs = 0;
    if (mStartTime != AV_NOPTS_VALUE && currentUsec != AV_NOPTS_VALUE) {
        nowUs = currentUsec - mStartTime;
    }

    ALOGV("currentUsec:%.2fs, mStartTime:%.2fs, position:%.2fs", currentUsec/1e6, mStartTime/1e6, nowUs/1e6);

    if (msec) {
        *msec = nowUs / 1000;
    }

    return OK;
}

status_t CTCMediaPlayer::getDuration(int *msec)
{
    //MLOG();

    int64_t durationUs = 0;

    if (mSourceFeeder) {
        durationUs = mSourceFeeder->getDuration();
    }

    *msec = durationUs / 1000;

    return OK;
}

status_t CTCMediaPlayer::reset()
{
    MLOG();

    sp<AMessage> msg = new AMessage(kWhatReset, mHandler);

    status_t err;
    sp<AMessage> response;
    msg->postAndAwaitResponse(&response);
    CHECK(response->findInt32("err", &err));

    MLOG();
    return err;
}

status_t CTCMediaPlayer::notifyAt(int64_t mediaTimeUs)
{
    MLOG();

    return OK;
}

status_t CTCMediaPlayer::setLooping(int loop)
{
    MLOG();

    mLooping = loop;

    return OK;
}

player_type CTCMediaPlayer::playerType()
{
    return (player_type)CTC_MEDIAPLAYER;
}

status_t CTCMediaPlayer::invoke(const Parcel& request, Parcel* reply)
{
    if (reply == nullptr) {
        ALOGE("reply is a NULL pointer");
        return BAD_VALUE;
    }

    int32_t methodId;
    status_t ret = request.readInt32(&methodId);
    if (ret != OK) {
        ALOGE("Failed to retrieve the requested method to invoke");
        return ret;
    }

    ALOGV("methodId:%d", methodId);
    switch (methodId) {
    case INVOKE_ID_GET_TRACK_INFO:
    {
        onGetTrackInfo(reply);
    }
    break;

    case INVOKE_ID_GET_AM_TRACK_INFO:
    {
        onGetMediaInfo(reply);
    }
    break;

    case INVOKE_ID_SELECT_TRACK:
    {
        int trackIndex = request.readInt32();
        int msec = 0;

        getCurrentPosition(&msec);
        onSelectTrack(trackIndex, true, msec * 1000ll);
    }
    break;

    case INVOKE_ID_UNSELECT_TRACK:
    {
        int trackIndex = request.readInt32();
        onSelectTrack(trackIndex, false, 0xdeadbeef /* not used */);
    }
    break;

    case INVOKE_ID_GET_SELECTED_TRACK:
    {
        int32_t type = request.readInt32();
        onGetSelectedTrack(type, reply);
    }
    break;

    default:
        return INVALID_OPERATION;
    }

    return OK;
}

void CTCMediaPlayer::setAudioSink(const sp<AudioSink>& audioSink)
{
    MLOG();
}

status_t CTCMediaPlayer::setParameter(int key, const Parcel& request)
{
    MLOG("key:%d", key);

    switch (key) {
    case KEY_PARAMETER_AML_PLAYER_SET_TS_OR_ES:
    {
        int isTs = request.readInt32();
        MLOG("isTs:%d", isTs);
        mSetStreamType = isTs ? aml::PLAYER_STREAMTYPE_TS : aml::PLAYER_STREAMTYPE_ES;
    }
    break;

    case KEY_PARAMETER_CTC_PLAYER_SET_USE_OMX:
    {
        int useOmx = request.readInt32();
        mUseOmx = useOmx;
    }
    break;

    case KEY_PARAMETER_CTC_PLAYER_SET_EVENT_CB:
    {
        int64_t pEventCb = request.readInt64();
        int64_t handler = request.readInt64();
        ctc_func_player_evt = (aml::IPTV_PLAYER_EVT_CB)pEventCb;
        ctc_player_evt_hander = (void*)handler;
    }
    break;

    default:
    ALOGW("unhandled key:%#x", key);
        break;
    }

    return OK;
}

static constexpr int FOURCC(unsigned char c1, unsigned char c2, unsigned char c3, unsigned char c4)
{
        return ((c1) << 24 | (c2) << 16 | (c3) << 8 | (c4));
}

status_t CTCMediaPlayer::getParameter(int key, Parcel* reply)
{
    MLOG();

    if (key == FOURCC('m','t','r','X')) {
        // mtrX -- a play on 'metrics' (not matrix)
        // gather current info all together, parcel it, and send it back
        updateMetrics("api");
        mAnalyticsItem->writeToParcel(reply);
        return OK;
    }

    return INVALID_OPERATION;
}

status_t CTCMediaPlayer::getMetadata(const media::Metadata::Filter& ids, Parcel* records)
{
    MLOG();

    using media::Metadata;

    Metadata meta(records);

    meta.appendBool(Metadata::kPauseAvailable, true);
    meta.appendBool( Metadata::kSeekBackwardAvailable, true);
    meta.appendBool(Metadata::kSeekForwardAvailable, true);
    meta.appendBool(Metadata::kSeekAvailable, true);

    return OK;
}

status_t CTCMediaPlayer::dump(int fd, const Vector<String16> &args) const
{
    AString logString(" CTCMediaPlayer\n");
    char buf[256] = {0};

    ALOGI("%s", logString.c_str());
    AString url;
    if (mFd >= 0) {
        url = nameForFd(mFd);
    } else if (!mUrl.empty()) {
        url = mUrl;
    }
    snprintf(buf, sizeof(buf), "url:%s\n", url.c_str());
    logString.append(buf);

    if (fd >= 0) {
        int outFd = dup(fd);
        if (outFd >= 0) {
            FILE* out = fdopen(outFd, "w");
            fprintf(out, "%s", logString.c_str());
            fclose(out);
            out = NULL;
        }
    }

    return OK;
}

void CTCMediaPlayer::onMessageReceived(const sp<AMessage>& msg)
{
    switch (msg->what()) {
    case kWhatSetVideoSurface:
    {
        sp<RefBase> obj;
        CHECK(msg->findObject("surface", &obj));
        sp<Surface> surface = static_cast<Surface*>(obj.get());

        mSurface = surface;
    }
    break;

    case kWhatPrepare:
    {
        status_t err = onPrepare();

        sp<AReplyToken> replyID;
        if (msg->senderAwaitsResponse(&replyID)) {
            sp<AMessage> response = new AMessage;
            response->setInt32("err", err);
            response->postReply(replyID);
        } else {
            if (err == OK) {
                notifyListenser(MEDIA_PREPARED);
            } else {
                notifyListenser(MEDIA_ERROR, err);
            }
        }
    }
    break;

    case kWhatStart:
    {
        onStart();
    }
    break;

    case kWhatPause:
    {
        onPause();
    }
    break;

    case kWhatStop:
    {
        onStop();
    }
    break;

    case kWhatSeek:
    {
        int64_t seekTimeUs;
        int32_t mode;
        CHECK(msg->findInt64("seekTimeUs", &seekTimeUs));
        CHECK(msg->findInt32("mode", &mode));

        onSeek(seekTimeUs, (MediaPlayerSeekMode)mode, true);
    }
    break;

    case kWhatReset:
    {
        status_t err = onReset();

        sp<AReplyToken> replyID;
        CHECK(msg->senderAwaitsResponse(&replyID));
        
        sp<AMessage> response = new AMessage;
        response->setInt32("err", err);
        response->postReply(replyID);
    }
    break;

    default:
        TRESPASS();
    }
}

///////////////////////////////////////////////////////////////////////////////
void CTCMediaPlayer::initConfig()
{
    bool tsStream = property_get_bool("media.ctcmediaplayer.ts-stream", false);
    mSetStreamType = tsStream ? aml::PLAYER_STREAMTYPE_TS : aml::PLAYER_STREAMTYPE_ES;
}

void CTCMediaPlayer::initLooper()
{
    if (mLooper == nullptr) {
        mLooper = new ALooper;
        AString looperName = AStringPrintf("CTCMediaPlayer");
        mLooper->setName(looperName.c_str());
        mLooper->start();

        mLooper->registerHandler(mHandler);
    }
}

status_t CTCMediaPlayer::onPrepare()
{
    MLOG();
    AutoMutex _l(mLock);

    status_t err = OK;

    if (mSourceFeeder == nullptr) {
        return INVALID_OPERATION;
    }

    bool isTsFormat = mSourceFeeder->isTsFormat();
    if ((isTsFormat && mSetStreamType == aml::PLAYER_STREAMTYPE_ES) ||
        (!isTsFormat && mSetStreamType == aml::PLAYER_STREAMTYPE_TS)) {
        mStreamType = aml::PLAYER_STREAMTYPE_ES;
        ALOGI("isTsFormat:%d, mSetStreamType:%s", isTsFormat, aml::streamType2Str(mSetStreamType));
    } else {
        mStreamType = mSetStreamType;
    }

    SourceFeeder::SourceMode sourceMode = SourceFeeder::SOURCE_MODE_ES;
    if (mStreamType == aml::PLAYER_STREAMTYPE_TS) {
        sourceMode = SourceFeeder::SOURCE_MODE_TS;
    }
    mSourceFeeder->setSourceMode(sourceMode);

    if (mSourceFeeder->prepare(mLooper) != OK) {
        return INVALID_OPERATION;
    }

    if (initCTC() != OK) {
        return INVALID_OPERATION;
    }

    mState = STATE_PREPARED;

    return err;
}

status_t CTCMediaPlayer::onStart()
{
    MLOG("state:%d", mState);

    AutoMutex _l(mLock);

    switch (mState) {
    case STATE_PAUSED:
    case STATE_STOPPED:
        mProcessor->Resume();
        break;

    default:
        break;
    }

    mState = STATE_RUNNING;

    return OK;
}

status_t CTCMediaPlayer::onPause()
{
    MLOG("state:%d", mState);

    AutoMutex _l(mLock);

    if (mState == STATE_RUNNING) {
        mProcessor->Pause();
    }

    mState = STATE_PAUSED;

    return OK;
}

status_t CTCMediaPlayer::onStop()
{
    AutoMutex _l(mLock);

    if (mProcessor) {
        mProcessor->Pause();
    }

    mState = STATE_STOPPED;

    return OK;
}

status_t CTCMediaPlayer::onSeek(int64_t seekTimeUs, MediaPlayerSeekMode mode, bool needNotify)
{
    MLOG("seekTimeUs:%lld", seekTimeUs);

    status_t err = OK;

    int flags = AVSEEK_FLAG_ANY;
    switch (mode) {
    case android::MediaTrack::ReadOptions::SEEK_PREVIOUS_SYNC:
        flags = AVSEEK_FLAG_BACKWARD;
        break;

    case android::MediaTrack::ReadOptions::SEEK_CLOSEST_SYNC:
    case android::MediaTrack::ReadOptions::SEEK_CLOSEST:
        flags = AVSEEK_FLAG_CLOSEST_SYNC;
        break;

    case android::MediaTrack::ReadOptions::SEEK_NEXT_SYNC:
    case android::MediaTrack::ReadOptions::SEEK_FRAME_INDEX:
        flags = AVSEEK_FLAG_ANY;
        break;

    default:
        break;
    }

    AutoMutex _l(mLock);

    if (mSourceFeeder) {
        mSourceFeeder->stop();
        mProcessor->Seek();
        mSourceFeeder->seekTo(seekTimeUs, flags);
        mSourceFeeder->start();
    }

    if (needNotify) {
        notifyListenser_l(MEDIA_SEEK_COMPLETE);
    }

    return OK;
}

status_t CTCMediaPlayer::onReset()
{
    MLOG();

    AutoMutex _l(mLock);

    if (mSourceFeeder) {
        mSourceFeeder->stop();
    }

    if (mProcessor) {
        mProcessor->Stop();
    }

    mState = STATE_IDLE;

    return OK;
}

status_t CTCMediaPlayer::initCTC()
{
    MLOG();

    aml::CTC_InitialParameter initParam;
    initParam.userId = -1;
    initParam.useOmx = mUseOmx;
    initParam.isEsSource = mStreamType == aml::PLAYER_STREAMTYPE_ES;
    initParam.flags = 0;
    initParam.extensionSize = 0;
    mProcessor.reset(aml::GetMediaProcessor(&initParam));
    if (mProcessor == nullptr) {
        ALOGE("GetMediaProcessor failed!");
        return INVALID_OPERATION;
    }
    
    auto& videoTracks = mSourceFeeder->getVideoTracks();
    mVideoParasCount = std::min(videoTracks.size(), (size_t)MAX_VIDEO_PARAM_SIZE);
    for (size_t i = 0; i < mVideoParasCount; ++i) {
        convertToVPara(videoTracks[i].mStream, mVideoParas + i);
    }

    auto& audioTracks = mSourceFeeder->getAudioTracks();
    mAudioParasCount = std::min(audioTracks.size(), (size_t)MAX_AUDIO_PARAM_SIZE);
    for (size_t i = 0; i < mAudioParasCount; ++i) {
        convertToAPara(audioTracks[i].mStream, mAudioParas + i);
    }

    auto& subtitleTracks = mSourceFeeder->getSubtitleTracks();
    mSubtitleParasCount = std::min(subtitleTracks.size(), (size_t)MAX_SUBTITLE_PARAM_SIZE);
    for (size_t i = 0; i < mSubtitleParasCount; ++i) {
        convertToSPara(subtitleTracks[i].mStream, mSubtitleParas + i);
    }

    if (mVideoParasCount > 0) {
        mProcessor->InitVideo(mVideoParas);
    }

    if (mAudioParasCount > 0) {
        mProcessor->InitAudio(mAudioParas);
    }

    if (mSubtitleParasCount > 0) {
        mProcessor->InitSubtitle(mSubtitleParas);
    }

    if (mSurface) {
        mProcessor->SetSurface(mSurface.get());
    }

    if (mIsLive) {
        mProcessor->initSyncSource(aml::CTC_SYNCSOURCE_PCR);
    }

    if (!mProcessor->StartPlay()) {
        ALOGE("StartPlay failed!");
        return INVALID_OPERATION;
    }

    mProcessor->playerback_register_evt_cb(ctc_func_player_evt, ctc_player_evt_hander);
    mSourceFeeder->setSourceReceiver(new CTCMediaPlayerReceiver(mProcessor));
    mSourceFeeder->start();

    MLOG();
    return OK;
}


static void writeTrackInfo(Parcel* reply, SourceFeeder::Track* track)
{
    AVMediaType type = (AVMediaType)track->mMediaType;
    int32_t trackType = avMediaType2TrackType(type);
    AString mime;

    if (trackType == MEDIA_TRACK_TYPE_AUDIO) {
        mime = "audio/";
    } else if (trackType == MEDIA_TRACK_TYPE_VIDEO) {
        mime = "video/";
    } else {
        mime = track->mime;
    }

    reply->writeInt32(2); // write something non-zero
    reply->writeInt32(trackType);
    reply->writeString16(String16(mime.c_str()));
    reply->writeString16(String16(track->lang.c_str()));

    if (trackType == MEDIA_TRACK_TYPE_SUBTITLE) {
        int32_t isAuto, isDefault, isForced;

        isAuto = track->isAuto;
        isDefault = track->isDefault;
        isForced = track->isForced;

        reply->writeInt32(isAuto);
        reply->writeInt32(isDefault);
        reply->writeInt32(isForced);
    }
}

status_t CTCMediaPlayer::onGetTrackInfo(Parcel* reply)
{
    size_t inbandTracks = mSourceFeeder->getTrackCount();

    //write track count
    reply->writeInt32(inbandTracks);

    for (size_t i = 0; i < inbandTracks; ++i) {
        writeTrackInfo(reply, mSourceFeeder->getTrackInfo(i));
    }

    return OK;
}

status_t CTCMediaPlayer::onSelectTrack(size_t trackIndex, bool select, int64_t timeUs)
{
    ALOGI("selectTrack trackIndex:%d, select:%d, timeUs:%lld", trackIndex, select, timeUs);

    AutoMutex _l(mLock);

    SourceFeeder::Track* track = mSourceFeeder->getTrackInfo(trackIndex);
    AVMediaType mediaType = track->mStream->codecpar->codec_type;

    mSourceFeeder->stop();
    mSourceFeeder->selectTrack(trackIndex, select, timeUs);

    if (mediaType == AVMEDIA_TYPE_AUDIO) {
        mProcessor->SwitchAudioTrack(track->mStream->id, NULL);
    } else if (mediaType == AVMEDIA_TYPE_SUBTITLE) {
        mProcessor->SwitchSubtitle(track->mStream->id, NULL);
    }

    mSourceFeeder->start();

    return OK;
}

status_t CTCMediaPlayer::onGetSelectedTrack(int32_t type, Parcel* reply)
{
    AVMediaType mediaType = trackType2AvMediaType((media_track_type)type);

    ssize_t selectedTrack = mSourceFeeder->getSelectedTrack(mediaType);

    reply->writeInt32(selectedTrack);

    return OK;
}

void CTCMediaPlayer::notifyListenser(int msg, int ext1, int ext2, const Parcel* in)
{
    AutoMutex _l(mLock);
    notifyListenser_l(msg, ext1, ext2, in);
}

void CTCMediaPlayer::notifyListenser_l(int msg, int ext1, int ext2, const Parcel* in)
{
    switch (msg) {
    case MEDIA_PREPARED:
        ALOGI("MEDIA_PREPARED");
        break;

    case MEDIA_SEEK_COMPLETE:
        ALOGI("MEDIA_SEEK_COMPLETE");
        break;

    case MEDIA_ERROR:
        break;

    default:
        break;
    }

    mLock.unlock();
    sendEvent(msg, ext1, ext2, in);
    mLock.lock();
}

void CTCMediaPlayer::updateMetrics(const char* where)
{
    SourceFeeder::Track* track = nullptr;

    if (mSourceFeeder->getTrackCount() > 0) {
        for (size_t i = 0; i < mSourceFeeder->getTrackCount(); ++i) {
            track = mSourceFeeder->getTrackInfo(i);

            AString mime = track->mime;

            AString name = "unknown";
            name = avcodec_get_name(track->mStream->codec->codec_id);
            ALOGI("index:%d, mime:%s, codec_name:%s", i, mime.c_str(), name.c_str());

            if (mime.startsWith("video/")) {
                int32_t width, height;
                mAnalyticsItem->setCString(kPlayerVMime, mime.c_str());
                if (!name.empty()) {
                    mAnalyticsItem->setCString(kPlayerVCodec, name.c_str());
                }

                mAnalyticsItem->setInt32(kPlayerWidth, track->mStream->codec->width);
                mAnalyticsItem->setInt32(kPlayerHeight, track->mStream->codec->height);

                int64_t numFramesTotal = 0;
                int64_t numFramesDropped = 0;

                mAnalyticsItem->setInt64(kPlayerFrames, numFramesTotal);
                mAnalyticsItem->setInt64(kPlayerFramesDropped, numFramesDropped);
            } else if (mime.startsWith("audio/")) {
                mAnalyticsItem->setCString(kPlayerAMime, mime.c_str());
                if (!name.empty()) {
                    mAnalyticsItem->setCString(kPlayerACodec, name.c_str());
                }
            }
        }
    }

    // always provide duration and playing time, even if they have 0/unknown values.

    // getDuration() uses mLock for mutex -- careful where we use it.
    int duration_ms = -1;
    getDuration(&duration_ms);
    mAnalyticsItem->setInt64(kPlayerDuration, duration_ms);

    //mPlayer->updateInternalTimers();

    int64_t mPlayingTimeUs = 0;

    mAnalyticsItem->setInt64(kPlayerPlaying, (mPlayingTimeUs+500)/1000 );
    mAnalyticsItem->setCString(kPlayerDataSourceType, "ffmpeg-source");
}

#define INVALID_TS_PROG_NUM -1
status_t CTCMediaPlayer::updateMediaInfo(void)
{
    ALOGV("updateMediaInfo");
    maudio_info_t *ainfo;
    mvideo_info_t *vinfo;
    int cur_video_index = 0;
    int cur_audio_index = 0;
    mStreamInfo.stream_info.total_video_num = 0;
    mStreamInfo.stream_info.total_audio_num = 0;
    mStreamInfo.stream_info.cur_sub_index   = -1;
    mStreamInfo.is_multi_prog = 0;
    mStreamInfo.ts_programe_info.programe_num = 0;

/*
    if (mSource->isStreaming()) {
        sp<AMessage> aformat= mSource->getFormat(true);  //audio
        sp<AMessage> vformat= mSource->getFormat(false);   // video
        if (vformat != NULL) {
            vinfo = (mvideo_info_t *)malloc(sizeof(mvideo_info_t));
            memset(vinfo, 0, sizeof(mvideo_info_t));
            int32_t codecid=-1,width=-1,height=-1,bitrate=-1;
            int64_t duration = -1;
            vinfo->index = 0;
            if (vformat->findInt32("codec-id", &codecid)) {
                vinfo->id = codecid;
            }
            if (vformat->findInt32("width", &width)) {
                vinfo->width = width;
            }
            if (vformat->findInt32("height", &height)) {
                vinfo->height = height;
            }
            if (vformat->findInt64("durationUs", &duration)) {
                 vinfo->duartion = duration;
            }
            if (vformat->findInt32("bit-rate", &bitrate)) {
                vinfo->bit_rate = bitrate;
            }
            vinfo->format  = (vformat_t)0;
            vinfo->aspect_ratio_num = 0;
            vinfo->aspect_ratio_den = 0;
            vinfo->frame_rate_num   = 0;
            vinfo->frame_rate_den   = 0;
            vinfo->video_rotation_degree = 0;
            mStreamInfo.video_info[mStreamInfo.stream_info.total_video_num] = vinfo;
            mStreamInfo.stream_info.total_video_num++;
        }
        if (aformat != NULL) {
            ainfo = (maudio_info_t *)malloc(sizeof(maudio_info_t));
            memset(ainfo, 0, sizeof(maudio_info_t));
            int32_t codecid=-1, bitrate=-1, samplerate=-1, channelcount=-1;
            int64_t duration=-1;
            ainfo->index = 0;
            AString mime;
            if (aformat->findInt32("codec-id", &codecid)) {
                ainfo->id = codecid;
            }
            if (aformat->findString("mime", &mime)) {
                if (mime == MEDIA_MIMETYPE_AUDIO_DTSHD) {
                    ALOGI("mime:%s",MEDIA_MIMETYPE_AUDIO_DTSHD);
                    mStrCurrentAudioCodec = "DTSHD";
                    ainfo->id = CODEC_ID_DTS;
                }else if (mime == MEDIA_MIMETYPE_AUDIO_OPUS) {
                     ALOGI("mime:%s",MEDIA_MIMETYPE_AUDIO_OPUS);
                     ainfo->id = CODEC_ID_OPUS;
                }
            }
            if (aformat->findInt32("bit-rate", &bitrate)) {
                ainfo->bit_rate = bitrate;
            }
            if (aformat->findInt32("channel-count", &channelcount)) {
                ainfo->channel = channelcount;
            }
            if (aformat->findInt32("sample-rate", &samplerate)) {
                ainfo->sample_rate = samplerate;
            }
            if (vformat->findInt64("durationUs", &duration)) {
                ainfo->duration = duration;
            }
            if (ainfo->id  > 0)
                ainfo->aformat      = audioTypeConvert((enum CodecID)ainfo->id);
            ALOGV("aformat %d",ainfo->aformat);
            mStreamInfo.audio_info[mStreamInfo.stream_info.total_audio_num] = ainfo;
            mStreamInfo.stream_info.total_audio_num++;

        }
        mStreamInfo.stream_info.cur_video_index = cur_video_index;
        mStreamInfo.stream_info.cur_audio_index = cur_audio_index;
        mStreamInfo.stream_info.cur_sub_index   = -1;
    }else

*/
    {
        int64_t duration = 0;
        //if (OK == mSource->getDuration(&duration)) {
            duration = mSourceFeeder->getDuration();
            mStreamInfo.stream_info.duration = duration / 1000000; //us to sec
        //}
        int track_num = mSourceFeeder->getTrackCount();
      for (size_t i = 0; i < track_num; i++) {
          SourceFeeder::Track* track = mSourceFeeder->getTrackInfo(i);
            AString mime;
            //format->findString("mime", &mime);
            mime = track->mime;

            if (track->mStream->codecpar->codec_id == AV_CODEC_ID_MP2) {
                mime = "audio/mp2";
                ALOGW("change mime to %s adapt for movieplayer!", mime.c_str());
            }

            int32_t prog_num = INVALID_TS_PROG_NUM;
            //if (format->findInt32("program-num", &prog_num)) {
                //ALOGV("[%s:%d]get prognum=%d\n", __FUNCTION__, __LINE__, prog_num);
            //}

            if (mime.startsWith("video/")) {
                vinfo = (mvideo_info_t *)malloc(sizeof(mvideo_info_t));
                memset(vinfo, 0, sizeof(mvideo_info_t));

                int32_t codecid,width,height,bitrate;
                int64_t duration;
                vinfo->index       = i;
                vinfo->id = track->mStream->codec->codec_id;
                //if (format->findInt32("width", &width)) {
                    //vinfo->width = width;
                //}
                vinfo->width = track->mStream->codec->width;
                //if (format->findInt32("height", &height)) {
                    //vinfo->height = height;
                //}
                vinfo->height = track->mStream->codec->height;
                //if (format->findInt64("durationUs", &duration)) {
                     //vinfo->duartion = duration;
                //}
                vinfo->duartion = av_rescale_q(track->mStream->duration, track->mStream->time_base, AV_TIME_BASE_Q);
                //if (format->findInt32("bit-rate", &bitrate)) {
                    //vinfo->bit_rate = bitrate;
                //}
                vinfo->bit_rate = track->mStream->codec->bit_rate;
                vinfo->format      = aml::convertToVFormat(vinfo->id);
                vinfo->aspect_ratio_num = 0;
                vinfo->aspect_ratio_den = 0;
                vinfo->frame_rate_num   = 0;
                vinfo->frame_rate_den   = 0;
                vinfo->video_rotation_degree = 0;
                mStreamInfo.video_info[mStreamInfo.stream_info.total_video_num] = vinfo;
                //if (INVALID_TS_PROG_NUM != prog_num) {
                    //mStreamInfo.video_info[mStreamInfo.stream_info.total_video_num]->prog_num = prog_num;
                //}
                mStreamInfo.stream_info.total_video_num++;
                AVMediaType trackType = trackType2AvMediaType(MEDIA_TRACK_TYPE_VIDEO);
                if (i == mSourceFeeder->getSelectedTrack(trackType)) {
                    cur_video_index = i;
                    mStreamInfo.ts_programe_info.cur_prognum = prog_num;
                }
                AString name;
                //if (format->findString("program-name", &name)) {
                    //ALOGV("program-name %s",name.c_str());
                    //int num = mStreamInfo.ts_programe_info.programe_num;
                    //mStreamInfo.ts_programe_info.ts_programe_detail[num].video_pid = i;
                    //strcpy(mStreamInfo.ts_programe_info.ts_programe_detail[num].programe_name, name.c_str());
                    //mStreamInfo.ts_programe_info.programe_num++;
                //}
            } else if (mime.startsWith("audio/")) {
                ainfo = (maudio_info_t *)malloc(sizeof(maudio_info_t));
                memset(ainfo, 0, sizeof(maudio_info_t));
                int32_t codecid, bitrate, samplerate, channelcount;
                int64_t duration;
                ainfo->index     = i;
                //if (format->findInt32("codec-id", &codecid)) {
                    //ainfo->id = codecid;
                //}
                ainfo->id = track->mStream->codec->codec_id;
                //if (format->findInt32("bit-rate", &bitrate)) {
                    //ainfo->bit_rate = bitrate;
                //}
                ainfo->bit_rate = track->mStream->codec->bit_rate;
                //if (format->findInt32("channel-count", &channelcount)) {
                    //ainfo->channel = channelcount;
                //}
                ainfo->channel = track->mStream->codec->channels;
                //if (format->findInt32("sample-rate", &samplerate)) {
                    //ainfo->sample_rate = samplerate;
                //}
                ainfo->sample_rate = track->mStream->codec->sample_rate;
                //if (format->findInt64("durationUs", &duration)) {
                    //ainfo->duration = duration;
                //}
                ainfo->duration = av_rescale_q(track->mStream->duration, track->mStream->time_base, AV_TIME_BASE_Q);
                //if (mime == MEDIA_MIMETYPE_AUDIO_OPUS) {
                     //ALOGI("mime:%s",MEDIA_MIMETYPE_AUDIO_OPUS);
                     //ainfo->id = CODEC_ID_OPUS;
                //}
                ainfo->aformat      = aml::convertToAFormat(ainfo->id);
                ALOGV("ainfo->id: %#x, ainfo->aformat:%d", ainfo->id, ainfo->aformat);
                mStreamInfo.audio_info[mStreamInfo.stream_info.total_audio_num] = ainfo;
                if (INVALID_TS_PROG_NUM != prog_num) {
                    //mStreamInfo.audio_info[mStreamInfo.stream_info.total_audio_num]->prog_num = prog_num;
                }
                mStreamInfo.stream_info.total_audio_num++;
                AVMediaType trackType = trackType2AvMediaType(MEDIA_TRACK_TYPE_AUDIO);
                if (i == mSourceFeeder->getSelectedTrack(trackType)) {
                    cur_audio_index = i;
                    //if (mime == MEDIA_MIMETYPE_AUDIO_DTSHD) {
                        //ALOGI("mime:%s",MEDIA_MIMETYPE_AUDIO_DTSHD);
                        //mStrCurrentAudioCodec = "DTSHD";
                    //} else {
                        //mStrCurrentAudioCodec = NULL;
                     //}
                }
                strncpy(ainfo->audio_mime, mime.c_str(), sizeof(ainfo->audio_mime)-1);
                ainfo->audio_mime[sizeof(ainfo->audio_mime)-1] = '\0';
            }
        }

        int prog_vnum_tmp = 0;
        if (mStreamInfo.ts_programe_info.programe_num > 1) {
            ALOGI("Multiple Programme\n");
            mStreamInfo.is_multi_prog  = 1;
        }

        for (int prog_cnt_tmp  = 0; prog_cnt_tmp < mStreamInfo.ts_programe_info.programe_num; prog_cnt_tmp++) {
            int prog_anum_tmp = 0;
            prog_vnum_tmp = mStreamInfo.video_info[prog_cnt_tmp]->prog_num;
            ALOGI("[%s:%d] video prog_num=%d,cnt=%d\n",__FUNCTION__, __LINE__, prog_vnum_tmp, prog_cnt_tmp);
            mStreamInfo.video_info[prog_cnt_tmp]->audio_info.prog_audio_num = 0;
            for (int audio_cnt_tmp = 0; audio_cnt_tmp < mStreamInfo.stream_info.total_audio_num; audio_cnt_tmp++) {
                prog_anum_tmp = mStreamInfo.audio_info[audio_cnt_tmp]->prog_num;
                if (prog_vnum_tmp == prog_anum_tmp) {
                    ALOGI("[%s:%d] audio prog_num=%d\n",__FUNCTION__, __LINE__, prog_anum_tmp);
                    ts_prog_audio_info *pinfo = &mStreamInfo.video_info[prog_cnt_tmp]->audio_info;
                    pinfo->prog_audio_index_list[pinfo->prog_audio_num] = audio_cnt_tmp;
                    pinfo->prog_audio_num ++;
                    mStreamInfo.audio_info[audio_cnt_tmp]->prog_video_index = prog_cnt_tmp;
                }
            }
        }

        mStreamInfo.stream_info.cur_video_index = cur_video_index;
        mStreamInfo.stream_info.cur_audio_index = cur_audio_index;
        mStreamInfo.stream_info.cur_sub_index   = -1;
    }

    return OK;
}

status_t CTCMediaPlayer::onGetMediaInfo(Parcel* reply)
{
    //Mutex::Autolock autoLock(mLock);
    ALOGI("CTCMediaPlayer::getMediaInfo");
    int datapos=reply->dataPosition();
    updateMediaInfo();
    //filename
    reply->writeString16(String16("-1"));
    //duration
    if (mStreamInfo.stream_info.duration > 0)
        reply->writeInt32(mStreamInfo.stream_info.duration);
    else
        reply->writeInt32(-1);

    reply->writeString16(String16("null"));

    //bitrate
      if (mStreamInfo.stream_info.bitrate > 0)
        reply->writeInt32(mStreamInfo.stream_info.bitrate);
    else
        reply->writeInt32(-1);

    //filetype
    reply->writeInt32(mStreamInfo.stream_info.type);

    /*select info*/
    reply->writeInt32(mStreamInfo.stream_info.cur_video_index);
    reply->writeInt32(mStreamInfo.stream_info.cur_audio_index);
    reply->writeInt32(mStreamInfo.stream_info.cur_sub_index);
    ALOGV("--cur video:%d cur audio:%d cur sub:%d \n",mStreamInfo.stream_info.cur_video_index,mStreamInfo.stream_info.cur_audio_index, mStreamInfo.stream_info.cur_sub_index);
    /*build video info*/
    reply->writeInt32(mStreamInfo.stream_info.total_video_num);
    for (int i = 0;i < mStreamInfo.stream_info.total_video_num; i ++) {
        reply->writeInt32(mStreamInfo.video_info[i]->index);
        reply->writeInt32(mStreamInfo.video_info[i]->id);
        reply->writeString16(String16(player_vformat2str((vformat_t)mStreamInfo.video_info[i]->format)));
        reply->writeInt32(mStreamInfo.video_info[i]->width);
        reply->writeInt32(mStreamInfo.video_info[i]->height);
        ALOGV("--video index:%d id:%d totlanum:%d width:%d height:%d \n",mStreamInfo.video_info[i]->index,mStreamInfo.video_info[i]->id,mStreamInfo.stream_info.total_video_num,mStreamInfo.video_info[i]->width,mStreamInfo.video_info[i]->height);
    }

    /*build audio info*/
    if (mStreamInfo.is_multi_prog) {
        int ts_total_audio_num = 0;
        for (int i = 0; i < mStreamInfo.ts_programe_info.programe_num; i ++) {
            if (mStreamInfo.video_info[i]->prog_num == mStreamInfo.ts_programe_info.cur_prognum) {
                ts_total_audio_num = mStreamInfo.video_info[i]->audio_info.prog_audio_num;
                break;
            }
        }
        reply->writeInt32(ts_total_audio_num);
    }
    else {
        reply->writeInt32(mStreamInfo.stream_info.total_audio_num);
    }

    for (int i = 0; i < mStreamInfo.stream_info.total_audio_num; i ++) {
        if (mStreamInfo.is_multi_prog && mStreamInfo.ts_programe_info.cur_prognum != \
            mStreamInfo.audio_info[i]->prog_num) {
            continue;
        }

        reply->writeInt32(mStreamInfo.audio_info[i]->index);
        reply->writeInt32(mStreamInfo.audio_info[i]->id);
        reply->writeInt32(mStreamInfo.audio_info[i]->aformat);
        reply->writeInt32(mStreamInfo.audio_info[i]->channel);
        reply->writeInt32(mStreamInfo.audio_info[i]->sample_rate);
        reply->writeString16(String16(mStreamInfo.audio_info[i]->audio_mime));
        ALOGV("--audio index:%d id:%d totlanum:%d channel:%d samplerate:%d aformat=%d\n",mStreamInfo.audio_info[i]->index,mStreamInfo.audio_info[i]->id,mStreamInfo.stream_info.total_audio_num,mStreamInfo.audio_info[i]->channel, mStreamInfo.audio_info[i]->sample_rate, mStreamInfo.audio_info[i]->aformat);
    }

    /*build subtitle info*/
    reply->writeInt32(0);
    reply->writeInt32(mStreamInfo.ts_programe_info.programe_num);
    for (int i = 0; i < mStreamInfo.ts_programe_info.programe_num; i++) {
        reply->writeInt32(mStreamInfo.ts_programe_info.ts_programe_detail[i].video_pid);
        reply->writeString16(String16(mStreamInfo.ts_programe_info.ts_programe_detail[i].programe_name));
        ALOGV("--programe i:%d, id:%d programe_name:%s\n", i,
                                mStreamInfo.ts_programe_info.ts_programe_detail[i].video_pid,
                                mStreamInfo.ts_programe_info.ts_programe_detail[i].programe_name);
    }
    reply->setDataPosition(datapos);
    return OK;
}




}
