/* 
 * Copyright (C) 2019 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "SourceFeeder"
#include "../media_processor/CTC_Log.h"
#include "../media_processor/CTC_Utils.h"
#include "SourceFeeder.h"
#include "FdDataSource.h"
#include "AmFFmpegByteIOAdapter.h"
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/hexdump.h>
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
}

using namespace android;

#define ADTS_HEADER_SIZE 7

static constexpr int kFeedChunkSize = 50 * 188;

///////////////////////////////////////////////////////////////////////////////
SourceFeeder::SourceFeeder(const char* url)
: mUrl(url)
{
    MLOG("mUrl:%s", mUrl.c_str());

    init();
}

SourceFeeder::SourceFeeder(int fd, int64_t offset, int64_t length)
: mUrl("")
{
    mFdDataSource = new FdDataSource(fd, offset, length);
    mSourceAdapter = new AmFFmpegByteIOAdapter();
    mSourceAdapter->init(mFdDataSource);

    init();
}

SourceFeeder::~SourceFeeder()
{
    MLOG();

    if (mFpTs != nullptr) {
        ::fclose(mFpTs);
        mFpTs = nullptr;
    }

    if (mFpEsAudio != nullptr) {
        ::fclose(mFpEsAudio);
        mFpEsAudio = nullptr;
    }

    if (mFpEsVideo != nullptr) {
        ::fclose(mFpEsVideo);
        mFpEsVideo = nullptr;
    }

    delete[] mFeedBuffer;
    if (mFeedEsPacket) {
        av_freep(&mFeedEsPacket);
    }

    if (mFilteredPacket) {
        av_freep(&mFilteredPacket);
    }

    if (mVideoBsf) {
        av_bsf_free(&mVideoBsf);
    }

    if (mAudioBsf) {
        av_bsf_free(&mAudioBsf);
    }

    avformat_close_input(&mFFmpegCtx);
}

void SourceFeeder::init()
{
    MLOG();

    mFeedBuffer = new uint8_t[kFeedChunkSize];
    mFeedEsPacket = av_mallocz(sizeof(AVPacket));
    mFilteredPacket = av_mallocz(sizeof(AVPacket));
}

void SourceFeeder::setSourceReceiver(const sp<ISourceReceiver>& receiver)
{
    mReceiver = receiver;
}

void SourceFeeder::setSourceMode(SourceMode mode)
{
    mSourceMode = mode;
}

status_t SourceFeeder::prepare(const android::sp<android::ALooper>& looper)
{
    looper->registerHandler(this);

    int dump = property_get_int32("media.ctcmediaplayer.dump", 0);
    if (dump != 0) {
        if (mSourceMode == SOURCE_MODE_TS) {
            mFpTs = ::fopen("/data/ctcmediaplayer_dump.ts", "wb");
            if (mFpTs == nullptr) {
                ALOGW("open dump for TS failed! %s", strerror(errno));
            }
        } else if (mSourceMode == SOURCE_MODE_ES) {
            if (dump & 1) {
                mFpEsAudio = ::fopen("/data/ctcmediaplayer_dump.audio", "wb");
                if (mFpEsAudio == nullptr) {
                    ALOGW("open dump for audio ES failed! %s", strerror(errno));
                }
            }

            if (dump & 2) {
                mFpEsVideo = ::fopen("/data/ctcmediaplayer_dump.video", "wb");
                if (mFpEsVideo == nullptr) {
                    ALOGW("open dump for video ES failed! %s", strerror(errno));
                }
            }
        }
    }

    av_register_all();
    //av_log_set_level(AV_LOG_DEBUG);

    mFFmpegCtx = avformat_alloc_context();
    mFFmpegCtx->flags |= AVFMT_FLAG_KEEP_SIDE_DATA;

    if (mSourceAdapter) {
        ALOGI("use AmFFmpegByteIOAdapter");
        mFFmpegCtx->pb = mSourceAdapter->getContext();
    }

    int err = avformat_open_input(&mFFmpegCtx, mUrl.c_str(), nullptr, nullptr);
    if (err != 0) {
        char errBuf[AV_ERROR_MAX_STRING_SIZE];
        ALOGE("avformat_open_input failed: url:%s, err:%d, %s", mUrl.c_str(), err, av_make_error_string(errBuf, sizeof(errBuf), err));
        return err;
    }

    AVInputFormat* tsFormat = av_find_input_format("mpegts");
    if (mFFmpegCtx->iformat == tsFormat) {
        mIsTsFormat = true;
    } else {
        mIsTsFormat = false;
    }
    MLOG("input format is%s TS, name:%s", mIsTsFormat ? "" : " NOT", mFFmpegCtx->iformat->name);


    err = avformat_find_stream_info(mFFmpegCtx, nullptr);
    if (err != 0) {
        char errBuf[AV_ERROR_MAX_STRING_SIZE];
        ALOGE("avformat_find_stream_info failed: err:%d, %s", err, av_make_error_string(errBuf, sizeof(errBuf), err));
        return err;
    }

    ALOGI("mFFmpegCtx:%p, nb_streams:%d, nb_programs:%d", mFFmpegCtx, mFFmpegCtx->nb_streams, mFFmpegCtx->nb_programs);
    av_dump_format(mFFmpegCtx, -1, nullptr, 0);

    AVStream** streams = mFFmpegCtx->streams;
    size_t *p_stream_index = nullptr;
    size_t nb_streams = mFFmpegCtx->nb_streams;
    if (mIsTsFormat && mFFmpegCtx->nb_programs > 1) {
        int program_index = property_get_int32("media.ctcmediaplayer.program_index", 0);
        if (program_index >= mFFmpegCtx->nb_programs) {
            ALOGW("program_index:%d exceed, reset to 0!", program_index);
            program_index = 0;
        }
        AVProgram* program = mFFmpegCtx->programs[program_index];
        p_stream_index = program->stream_index;
        nb_streams = program->nb_stream_indexes;
        mStartTime = program->start_time;
        ALOGI("program start_time:%lld, ffmpeg context start_time:%lld", program->start_time, mFFmpegCtx->start_time);
    } else {
        mStartTime = mFFmpegCtx->start_time;
    }

    ALOGI("mStartTime:%lld, duration:%lld", mStartTime, mFFmpegCtx->duration);

    for (size_t i = 0; i < nb_streams; ++i) {
        AVStream* st = p_stream_index ? streams[p_stream_index[i]] : streams[i];

        Track track;
        track.mIndex = i;
        track.mStream = st;
        track.mime = aml::CTC_convertCodecIdToMimeType(st->codec);
        AVDictionaryEntry* dict_entry = av_dict_get(st->metadata, "language", nullptr, 0);
        if (dict_entry != nullptr) {
            ALOGV("lang:%s", dict_entry->value);
            track.lang = dict_entry->value;
        } else {
            track.lang = avcodec_get_name(st->codecpar->codec_id);
        }
        track.mMediaType = (MediaType)st->codecpar->codec_type;

        if (st->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            ALOGI("video pid:%d, fmt:%s\n", st->id, avcodec_get_name(st->codecpar->codec_id));
            ALOGI("    video stream timebase:%d %d, extradata:%p, extradata_size:%d", st->time_base.num, st->time_base.den, st->codecpar->extradata, st->codecpar->extradata_size);

            mVideoTracks.push_back(track);
            if (mVideoTrack.mStream == nullptr) {
                mVideoTrack = track;
            }

            initBsf(mVideoBsf, st);
        } else if (st->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
            ALOGI("audio pid:%d, fmt:%s\n", st->id, avcodec_get_name(st->codecpar->codec_id));
            ALOGI("    audio stream timebase:%d %d", st->time_base.num, st->time_base.den);
            ALOGI("    profile:%d, sample rate:%d, channels:%d, extradata:%p, extradata_size:%d", st->codecpar->profile, st->codecpar->sample_rate, st->codecpar->channels,
                    st->codecpar->extradata, st->codecpar->extradata_size);
            hexdump(st->codecpar->extradata, st->codecpar->extradata_size);

            mAudioTracks.push_back(track);
            if (mAudioTrack.mStream == nullptr) {
                mAudioTrack = track;
            }

            initBsf(mAudioBsf, st);
        } else if (st->codecpar->codec_type == AVMEDIA_TYPE_SUBTITLE) {
            ALOGI("subtitle pid:%d, fmt:%s\n", st->id, avcodec_get_name(st->codecpar->codec_id));
            hexdump(st->codecpar->extradata, st->codecpar->extradata_size);

            mSubtitleTracks.push_back(track);
            if (mSubtitleTrack.mStream == nullptr) {
                mSubtitleTrack = track;
            }
        } else if (st->codecpar->codec_type == AVMEDIA_TYPE_UNKNOWN) {
            ALOGI("unknown codec_type, codec_tag:%#x", st->codecpar->codec_tag);
            if (st->codecpar->codec_tag == 0x82) {
                dict_entry = av_dict_get(st->metadata, "language", nullptr, 0);
                if (dict_entry != nullptr) {
                    track.lang = dict_entry->value;
                } else {
                    track.lang = "scte27";
                }

                track.mMediaType = (MediaType)AVMEDIA_TYPE_SUBTITLE;
                mSubtitleTracks.push_back(track);
                if (mSubtitleTrack.mStream == nullptr) {
                    mSubtitleTrack = track;
                }
            }
        } else {
            ALOGI("other codec_type: %d", st->codecpar->codec_type);
        }

        mSources.push_back(std::move(track));
    }

    mBitrate = mFFmpegCtx->bit_rate;
    if (mBitrate > 0) {
        int feedFreq = mBitrate / (kFeedChunkSize*8);
        mFeedPeriodUs = 1000000 / feedFreq;
        mFeedPeriodUs >>= 1;
        ALOGI("mFeedDurationUs: %.2fms", mFeedPeriodUs/1e3);
    }

    return OK;
}

status_t SourceFeeder::start()
{
    MLOG("mReceiver:%p", mReceiver.get());

    if (mSourceMode == SOURCE_MODE_UNKNOWN) {
        mSourceMode = mIsTsFormat ? SOURCE_MODE_TS: SOURCE_MODE_ES;
        ALOGW("init mSourceMode to %d", mSourceMode);
    }

    sp<AMessage> msg = new AMessage(kWhatFeedData, this);
    msg->setInt32("generation", mFeedGeneration);
    msg->post();

    return OK;
}

status_t SourceFeeder::stop()
{
    MLOG();

#if 0
    sp<AMessage> msg = new AMessage(kWhatStop, this);

    sp<AMessage> response;
    msg->postAndAwaitResponse(&response);
#else
    ++mFeedGeneration;
#endif

    return OK;
}

status_t SourceFeeder::seekTo(int64_t seekTimeUs, int flags)
{
    MLOG("seekTimeUs:%lld, startTime:%lld, ffStartTime:%lld", seekTimeUs, mStartTime, mFFmpegCtx->start_time);

    int ret = 0;
    int64_t timeUs = mStartTime + seekTimeUs;

    ret = doSeek(timeUs, flags);
    if (ret < 0) {
        return ret;
    }

    mFeedRemainSize = 0;
    av_packet_unref((AVPacket*)mFeedEsPacket);
    av_packet_unref((AVPacket*)mFilteredPacket);

    av_bsf_free(&mVideoBsf);
    av_bsf_free(&mAudioBsf);

    initBsf(mVideoBsf, mVideoTrack.mStream);
    initBsf(mAudioBsf, mAudioTrack.mStream);

    mVideoCsdWritten = false;

    return ret;
}

android::status_t SourceFeeder::doSeek(int64_t timeUs, int flags)
{
    int ret = -1;
    int64_t timestamp = timeUs;

    if (mVideoTrack.mStream) {
        timestamp = av_rescale_q(timeUs, AV_TIME_BASE_Q, mVideoTrack.mStream->time_base);
        ret = av_seek_frame(mFFmpegCtx, mVideoTrack.mStream->index, timestamp, flags);
        if (ret < 0) {
            ALOGW("av_seek_frame seek video failed, seekTimeUs:%lld, startTime:%lld", timeUs,
                  mStartTime);
            return ret;
        }
    } else if (mAudioTrack.mStream) {
        timestamp = av_rescale_q(timeUs, AV_TIME_BASE_Q, mAudioTrack.mStream->time_base);
        ret = av_seek_frame(mFFmpegCtx, mAudioTrack.mStream->index, timestamp, flags);
        if (ret < 0) {
            ALOGW("av_seek_frame seek audio failed, seekTimeUs:%lld, startTime:%lld", timeUs,
                  mStartTime);
            return ret;
        }
    } else if (mSubtitleTrack.mStream) {
        timestamp = av_rescale_q(timeUs, AV_TIME_BASE_Q, mSubtitleTrack.mStream->time_base);
        ret = av_seek_frame(mFFmpegCtx, mSubtitleTrack.mStream->index, timestamp, flags);
        if (ret < 0) {
            ALOGW("av_seek_frame seek subtitle failed, seekTimeUs:%lld, startTime:%lld", timeUs,
                  mStartTime);
            return ret;
        }
    }

    return ret;
}

bool SourceFeeder::isTsFormat() const
{
    return mIsTsFormat;
}

const std::vector<SourceFeeder::Track>& SourceFeeder::getVideoTracks() const
{
    return mVideoTracks;
}

const std::vector<SourceFeeder::Track>& SourceFeeder::getAudioTracks() const
{
    return mAudioTracks;
}

const std::vector<SourceFeeder::Track>& SourceFeeder::getSubtitleTracks() const
{
    return mSubtitleTracks;
}

int64_t SourceFeeder::getDuration() const
{
    int64_t duration = 0;

    if (mFFmpegCtx) {
        duration = mFFmpegCtx->duration;
    }

    return duration;
}

int64_t SourceFeeder::getStartTime() const
{
    return mStartTime;
}

int64_t SourceFeeder::getFirstVideoPts() const
{
    return mFirstVideoPts;
}

size_t SourceFeeder::getTrackCount() const
{
    return mSources.size();
}

SourceFeeder::Track* SourceFeeder::getTrackInfo(size_t trackIndex)
{
    Track& track = mSources.at(trackIndex);

    return &track;
}

android::status_t SourceFeeder::selectTrack(size_t trackIndex, bool select, int64_t timeUs)
{
    ALOGI("select track, trackIndex:%d, select:%d, timeUs:%lld", trackIndex, select, timeUs);

    int ret;
    Track& selectedTrack = mSources.at(trackIndex);
    bool selected = false;
    int64_t streamTimeUs = INT64_MIN;

    if (select) {
        if (selectedTrack.mStream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            if (mAudioTrack.mIndex != trackIndex) {
                selected = true;
                ALOGI("select audio track: %d ==> %d", mAudioTrack.mIndex, trackIndex);

                mAudioTrack = selectedTrack;
                streamTimeUs = mLastAudioPts;

                av_bsf_free(&mAudioBsf);
                initBsf(mAudioBsf, mAudioTrack.mStream);

            }
        }  else if (selectedTrack.mStream->codec->codec_type == AVMEDIA_TYPE_SUBTITLE) {
            if (mSubtitleTrack.mIndex != trackIndex) {
                selected = true;
                ALOGI("select subtitle track: %d ==> %d", mSubtitleTrack.mIndex, trackIndex);

                mSubtitleTrack = selectedTrack;
                streamTimeUs = mLastSubtitlePts;
            }
        }

        if (selected) {
            timeUs += mStartTime;
            ALOGI("after selectTrack, streamTimeUs:%f, currentPositionUs:%f", streamTimeUs/1e6, timeUs/1e6);

            int64_t time = av_rescale_q(timeUs, AV_TIME_BASE_Q, selectedTrack.mStream->time_base);
            ret = av_seek_frame(mFFmpegCtx, selectedTrack.mStream->index, time, AVSEEK_FLAG_BACKWARD);
            if (ret < 0) {
                ALOGW("av_seek_frame failed with %d after select track!", ret);
            }
        }
    }


    return android::OK;
}

ssize_t SourceFeeder::getSelectedTrack(int32_t type) const
{
    const Track* track = nullptr;

    switch (type) {
    case AVMEDIA_TYPE_AUDIO:
        track = &mAudioTrack;
        break;

    case AVMEDIA_TYPE_VIDEO:
        track = &mVideoTrack;
        break;

    case AVMEDIA_TYPE_SUBTITLE:
        track = &mSubtitleTrack;
        break;

    default:
        break;
    }

    return track ? track->mIndex : -1;
}

void SourceFeeder::onMessageReceived(const sp<AMessage>& msg)
{
    switch (msg->what()) {
    case kWhatFeedData:
    {
        int32_t generation;
        CHECK(msg->findInt32("generation", &generation));
        if (generation != mFeedGeneration) {
            break;
        }

        int64_t delayUs = onFeedData();
        if (delayUs >= 0) {
            msg->post(delayUs);
        }
    }
    break;

    default:
        TRESPASS();
    }
}

void SourceFeeder::initBsf(AVBSFContext*& bsf, AVStream* st)
{
    bsf = nullptr;
    if (st == nullptr) {
        return;
    }

    const AVBitStreamFilter* filter = nullptr;
    int err = 0;

    if (st->codec->codec_id == AV_CODEC_ID_HEVC) {
        filter = av_bsf_get_by_name("hevc_mp4toannexb");
    } else if (st->codec->codec_id == AV_CODEC_ID_H264) {
        filter = av_bsf_get_by_name("h264_mp4toannexb");
    }

    if (filter) {
        err = av_bsf_alloc(filter, &bsf);
        if (err < 0) {
            ALOGE("alloc bsf %s failed!", filter->name);
        }

        avcodec_parameters_copy(bsf->par_in, st->codecpar);
        bsf->time_base_in = st->time_base;
        err = av_bsf_init(bsf);
        if (err < 0) {
            ALOGE("av_bsf_init failed, err = %d", err);

            av_bsf_free(&bsf);
        }
    }
}

int64_t SourceFeeder::onFeedData()
{
    mFeedBeginTimeUs = android::ALooper::GetNowUs();
    status_t err = OK;

    if (mSourceMode == SOURCE_MODE_TS) {
        err = feedTs();
    } else {
        err = feedEs();
    }

    err = err >= 0 ? OK : err;

    return calcFeedDelay(err);
}

status_t SourceFeeder::feedTs()
{
    int ret = OK;

    if (mFeedRemainSize == 0) {
        ret = avio_read(mFFmpegCtx->pb, mFeedBuffer, kFeedChunkSize);
        if (ret < 0) {
            ALOGW("avio_read return %#x, %s", ret, av_err2str(ret));

            if (ret == AVERROR_EOF) {
                ret = avio_seek(mFFmpegCtx->pb, 0, SEEK_SET);
                if (ret != 0) {
                    ALOGE("avio_seek failed!");
                }
            }

            return ret;
        } else {
            mFeedRemainSize = ret;

            if (mFpTs && ::fwrite(mFeedBuffer, 1, mFeedRemainSize, mFpTs) != mFeedRemainSize) {
                ALOGW("write dump TS failed!");
            }
        }
    }

    if (mReceiver && mFeedRemainSize > 0) {
        ret = mReceiver->WriteData(mFeedBuffer, mFeedRemainSize);
        if (ret >= 0) {
            mTotalFeedSize += mFeedRemainSize;
            ALOGV("WriteData size:%d, ret:%d, mTotalFeedSize:%lld", mFeedRemainSize, ret, mTotalFeedSize);

            mFeedRemainSize = 0;        //writeData successfully
        }

    }

    return ret;
}

status_t SourceFeeder::feedEs()
{
    int ret = OK;
    AVPacket *pkt = (AVPacket*)mFeedEsPacket;

    while (pkt->data == nullptr) {
        ret = av_read_frame(mFFmpegCtx, pkt);
        if (ret < 0) {
            ALOGE("av_read_frame ret:%#x, %s", ret, av_err2str(ret));

            if (ret == AVERROR_EOF) {
                ret = doSeek(mStartTime, AVSEEK_FLAG_CLOSEST_SYNC);
                if (ret != 0) {
                    ALOGE("[%d] av_seek_frame failed! ret = %d", __LINE__, ret);
                }
            }

            return ret;
        }

        ++mTotalEsPackets;

        AVStream* st = mFFmpegCtx->streams[pkt->stream_index];
        bool drop = false;
        if (st->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
            if (st != mAudioTrack.mStream) {
                drop = true;
            }
        } else if ( st->codec->codec_type == AVMEDIA_TYPE_SUBTITLE) {
            if (st != mSubtitleTrack.mStream) {
                drop = true;
            }
        } else if (st->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            if (st != mVideoTrack.mStream) {
                drop = true;
            }
        }

        if (drop) {
            //ALOGV("drop pkt!");
            av_free_packet(pkt);
        }
    }

    CHECK(pkt->data);
    AVBSFContext* bsf = nullptr;
    AVStream* st = mFFmpegCtx->streams[pkt->stream_index];
    int64_t lastPts = av_rescale_q(pkt->pts, st->time_base, AV_TIME_BASE_Q);
    if (st->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
        bsf = mVideoBsf;
        mLastVideoPts = lastPts;
        if (pkt->pts != AV_NOPTS_VALUE && mFirstVideoPts == INT64_MIN) {
            mFirstVideoPts = lastPts;
            if (mFirstVideoPts - mStartTime > mFFmpegCtx->duration) {
                ALOGW("mStartTime maybe incorrect, set to first vpts!");
                mStartTime = mFirstVideoPts;
            }
        }
    } else if (st->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
        mLastAudioPts = lastPts;
    } else if (st->codecpar->codec_type == AVMEDIA_TYPE_SUBTITLE) {
        mLastSubtitlePts = lastPts;
    }

    if (bsf) {
        ret = av_bsf_send_packet(bsf, pkt);
        if (ret < 0) {
            if (ret != -EAGAIN) {
                ALOGW("av_bsf_send_packet failed, ret = %d", ret);
            }
        } else {
            av_free_packet(pkt);
        }

        pkt = (AVPacket*)mFilteredPacket;
        if (pkt->data == nullptr) {
            ret = av_bsf_receive_packet(bsf, pkt);
            if (ret < 0) {
                ALOGI("av_bsf_receive_packet return %d", ret);
                return ret;
            }
        }
    }

    if (st->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
        if (st->codec->codec_id == AV_CODEC_ID_AAC) {
            if (pkt->data[0] != 0xFF || (pkt->data[1]&0xF0) != 0xF0) {
                const char* adts = constructADTS(pkt->size);

                AVPacket old_pkt = *pkt;
                av_init_packet(pkt);

                av_new_packet(pkt, old_pkt.size + ADTS_HEADER_SIZE);
                av_packet_copy_props(pkt, &old_pkt);
                memcpy(pkt->data, adts, ADTS_HEADER_SIZE);
                memcpy(pkt->data+ADTS_HEADER_SIZE, old_pkt.data, old_pkt.size);

                av_free_packet(&old_pkt);
            }
        }
    }

    ALOGV("Es pkt:%p, size:%d, pts:%lld, index:%d, mTotalEsPackets:%lld", pkt->data, pkt->size, pkt->pts, pkt->stream_index, mTotalEsPackets);

    AVStream* stream = mFFmpegCtx->streams[pkt->stream_index];

    FILE* fp = nullptr;
    if (stream->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
        fp = mFpEsAudio;
    } else if (stream->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
        fp = mFpEsVideo;
    }

    if (fp && ::fwrite(pkt->data, 1, pkt->size, fp) != pkt->size) {
        ALOGW("dump %s ES failed!", av_get_media_type_string(stream->codec->codec_type));
    }

    if (mReceiver) {
        int64_t timestamp = 0;
        if (pkt->pts != AV_NOPTS_VALUE) {
            timestamp = av_rescale_q(pkt->pts, stream->time_base, {1, 90000});
        }

        uint8_t* pBuffer = nullptr;
        uint32_t nSize = 0;
        if (stream->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            if (!mVideoCsdWritten) {
                mVideoCsdWritten = true;

                int extradata_size = stream->codecpar->extradata_size;
                if (extradata_size > 0) {
                    uint8_t* extradata = stream->codecpar->extradata;
                    bool insertCsd = true;
                    if (pkt->size >= extradata_size && !memcmp(pkt->data, extradata, extradata_size)) {
                        insertCsd = false;
                        ALOGI("video packet include csd already!");
                    }

                    if (insertCsd) {
                        ret = av_grow_packet(pkt, stream->codecpar->extradata_size);
                        if (ret < 0) {
                            ALOGW("av_grow_packet failed with %d", ret);
                        } else {
                            ALOGI("video insert csd, size:%d", extradata_size);
                            memmove(pkt->data + extradata_size, pkt->data, pkt->size);
                            memcpy(pkt->data, stream->codecpar->extradata, extradata_size);
                        }
                    }
                }
            }

            ret = mReceiver->GetWriteBuffer(MEDIA_TYPE_VIDEO, &pBuffer, &nSize);
            if (ret == OK) {
                //ALOGI("SoftWriteData type:%d, index:%d, pts:%lld(%lldms), data:%p, size:%d, mTotalEsPackets:%lld", stream->codec->codec_type, pkt->stream_index, timestamp, timestamp/90, pkt->data, pkt->size, mTotalEsPackets);
                ret = mReceiver->WriteData(MEDIA_TYPE_VIDEO, pkt->data, pkt->size, timestamp);
            } else if (ret != -EAGAIN) {
                ret = OK;
            }
        } else if (stream->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
            //ALOGI("SoftWriteData type:%d, index:%d, pts:%lld(%lldms), data:%p, size:%d, mTotalEsPackets:%lld", stream->codec->codec_type, pkt->stream_index, timestamp, timestamp/90, pkt->data, pkt->size, mTotalEsPackets);
            ret = mReceiver->GetWriteBuffer(MEDIA_TYPE_AUDIO, &pBuffer, &nSize);
            if (ret == OK) {
                ret = mReceiver->WriteData(MEDIA_TYPE_AUDIO, pkt->data, pkt->size, timestamp);
            } else if (ret != -EAGAIN) {
                ret = OK;
            }
        }  else if (stream->codec->codec_type == AVMEDIA_TYPE_SUBTITLE) {
            //ALOGI("SoftWriteData type:%d, index:%d, pts:%lld(%lldms), data:%p, size:%d, mTotalEsPackets:%lld", stream->codec->codec_type, pkt->stream_index, timestamp, timestamp/90, pkt->data, pkt->size, mTotalEsPackets);

            ret = mReceiver->GetWriteBuffer(MEDIA_TYPE_SUBTITLE, &pBuffer, &nSize);
            if (ret == OK) {
                ret = mReceiver->WriteData(MEDIA_TYPE_SUBTITLE, pkt->data, pkt->size, timestamp);
            } else if (ret != -EAGAIN) {
                ret = OK;
            }
        } else {
            ret = OK;
        }

        ALOGV("SoftWriteData ret:%d", ret);

        if (ret >= 0) {
            av_free_packet(pkt);
        }
    }

    return ret;
}

int64_t SourceFeeder::calcFeedDelay(status_t err)
{
    int64_t delayUs = err != OK ? 60 * 1000 : 1*1000;
#if 0
    int64_t feedCostUs = ALooper::GetNowUs() - mFeedBeginTimeUs;

    delayUs = std::max(mFeedPeriodUs - feedCostUs, (int64_t)0);
#endif

    return delayUs;
}

static bool getSampleRateTableIndex(int sampleRate, uint8_t* tableIndex) {
    static const int kSampleRateTable[] = {
        96000, 88200, 64000, 48000, 44100, 32000,
        24000, 22050, 16000, 12000, 11025, 8000
    };
    const int tableSize =
        sizeof(kSampleRateTable) / sizeof(kSampleRateTable[0]);

    *tableIndex = 0;
    for (int index = 0; index < tableSize; ++index) {
        if (sampleRate == kSampleRateTable[index]) {
            ALOGV("Sample rate: %d and index: %d",
                sampleRate, index);
            *tableIndex = index;
            return true;
        }
    }

    ALOGE("Sampling rate %d bps is not supported", sampleRate);
    return false;
}

const char* SourceFeeder::constructADTS(size_t frameSize)
{
    static char header[ADTS_HEADER_SIZE];
    char* p = header;

    memset(p, 0, ADTS_HEADER_SIZE);

    const uint8_t kFieldId = 0;
    const uint8_t kMpegLayer = 0;
    const uint8_t kProtectionAbsense = 1;  // 1: kAdtsHeaderLength = 7

    *p++ = 0xFF;

    *p |= 0xF0;
    *p |= kFieldId << 3;
    *p |= kMpegLayer << 1;
    *p |= kProtectionAbsense;
    ++p;

    uint8_t kProfileCode;
    uint8_t kSampleFreqIndex;
    uint8_t kChannelConfigCode;
    const uint8_t* extraData = mAudioTrack.mStream->codecpar->extradata;
    if (extraData) {
        kProfileCode = (extraData[0] & 0xF8) >> 3;
        if (kProfileCode == 5 || kProfileCode == 29) {
            kProfileCode = (extraData[2] & 0x7C) >> 2;
        }
        --kProfileCode;

        kSampleFreqIndex = (extraData[0]&0x07)<<1 | (extraData[1]&0x80)>>7;
        kChannelConfigCode = (extraData[1]&0x78)>>3;
    } else {
        kProfileCode = mAudioTrack.mStream->codecpar->profile;
        getSampleRateTableIndex(mAudioTrack.mStream->codecpar->sample_rate, &kSampleFreqIndex);
        kChannelConfigCode = mAudioTrack.mStream->codecpar->channels;
    }

    const uint8_t kPrivateStream = 0;
    *p |= kProfileCode << 6;
    *p |= kSampleFreqIndex << 2;
    *p |= kPrivateStream << 1;
    *p |= (kChannelConfigCode>>2) & 1;
    ++p;

    const uint8_t kCopyright = 0;
    const uint32_t kFrameLength = frameSize + ADTS_HEADER_SIZE;
    *p |= (kChannelConfigCode&3)<<6;
    *p |= kCopyright << 2;
    *p |= (kFrameLength & 0x1800) >> 11;
    ++p;

    *p++ |= (kFrameLength & 0x07F8) >> 3;

    const uint32_t kBufferFullness = 0x7FF;  // VBR
    *p |= (kFrameLength&7)<<5;
    *p |= (kBufferFullness&0x07C0)>>6;
    ++p;

    const uint8_t kFrameCount = 0;
    *p |= (kBufferFullness&0x3F) << 2;
    *p |= kFrameCount;

    //hexdump(header, ADTS_HEADER_SIZE);

    return header;
}
