#include "CTCMediaInfo.h"

using namespace android;

AVMediaType trackType2AvMediaType(android::media_track_type trackType)
{
    AVMediaType type = AVMEDIA_TYPE_UNKNOWN;

    switch (trackType) {
    case MEDIA_TRACK_TYPE_AUDIO:
        type = AVMEDIA_TYPE_AUDIO;
        break;

    case MEDIA_TRACK_TYPE_VIDEO:
        type = AVMEDIA_TYPE_VIDEO;
        break;

    case MEDIA_TRACK_TYPE_SUBTITLE:
        type = AVMEDIA_TYPE_SUBTITLE;
        break;

    default:
        break;
    }

    return type;
}

android::media_track_type avMediaType2TrackType(AVMediaType type)
{
    android::media_track_type trackType = MEDIA_TRACK_TYPE_UNKNOWN;

    switch (type) {
    case AVMEDIA_TYPE_AUDIO:
        trackType = MEDIA_TRACK_TYPE_AUDIO;
        break;

    case AVMEDIA_TYPE_VIDEO:
        trackType = MEDIA_TRACK_TYPE_VIDEO;
        break;

    case AVMEDIA_TYPE_SUBTITLE:
        trackType = MEDIA_TRACK_TYPE_SUBTITLE;
        break;

    default:
        break;
    }

    return trackType;
}

const char* player_vformat2str(vformat_t value)
{
    switch (value) {
    case VFORMAT_MPEG12:
        return "MPEG12";

    case VFORMAT_MPEG4:
        return "MPEG4";

    case VFORMAT_H264:
        return "H264";

    case VFORMAT_HEVC:
        return "HEVC";

    case VFORMAT_MJPEG:
        return "MJPEG";

    case VFORMAT_REAL:
        return "REAL";

    case VFORMAT_JPEG:
        return "JPEG";

    case VFORMAT_VC1:
        return "VC1";

    case VFORMAT_AVS:
        return "AVS";

    case VFORMAT_SW:
        return "SW";

    case VFORMAT_H264MVC:
        return "H264MVC";

    case VFORMAT_H264_4K2K:
        return "H264_4K2K";

    default:
        return "NOT_SUPPORT VFORMAT";
    }
    return NULL;
}

