/* 
 * Copyright (C) 2019 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#ifndef _SOURCE_FEEDER_H_
#define _SOURCE_FEEDER_H_

#include <media/stagefright/foundation/ABase.h>
#include <media/stagefright/foundation/AHandler.h>
#include <media/stagefright/foundation/AMessage.h>
#include <memory>
#include <string>
#include <vector>

struct AVFormatContext;
struct AVStream;
struct AVBitStreamFilter;
struct AVBSFContext;

class FdDataSource;
class AmFFmpegByteIOAdapter;
struct ISourceReceiver;

class SourceFeeder : public android::AHandler
{
public:
    //keep sync with ffmpeg
    enum MediaType {
        MEDIA_TYPE_UNKNOWN = -1,  ///< Usually treated as AVMEDIA_TYPE_DATA
        MEDIA_TYPE_VIDEO,
        MEDIA_TYPE_AUDIO,
        MEDIA_TYPE_DATA,  ///< Opaque data information usually continuous
        MEDIA_TYPE_SUBTITLE,
        MEDIA_TYPE_ATTACHMENT,  ///< Opaque data information usually sparse
        MEDIA_TYPE_NB
    };

    struct Track {
        size_t mIndex = INT32_MAX;
        AVStream* mStream = nullptr;
        MediaType mMediaType = MEDIA_TYPE_UNKNOWN;

        android::AString mime = "unknown";
        android::AString lang = "unknown";

        //subtitle
        bool isAuto = false;
        bool isDefault = false;
        bool isForced = false;
    };

    enum SourceMode {
        SOURCE_MODE_UNKNOWN,
        SOURCE_MODE_ES,
        SOURCE_MODE_TS,
    };

public:
    explicit SourceFeeder(const char* url);
    SourceFeeder(int fd, int64_t offset, int64_t length);
    void setSourceReceiver(const android::sp<ISourceReceiver>& receiver);
    void setSourceMode(SourceMode mode);
    android::status_t prepare(const android::sp<android::ALooper>& looper);
    android::status_t start();
    android::status_t stop();
    android::status_t seekTo(int64_t seekTimeUs, int flags);

    bool isTsFormat() const;
    const std::vector<Track>& getVideoTracks() const;
    const std::vector<Track>& getAudioTracks() const;
    const std::vector<Track>& getSubtitleTracks() const;

    int64_t getDuration() const;
    int64_t getStartTime() const;
    int64_t getFirstVideoPts() const;
    size_t getTrackCount() const;
    Track* getTrackInfo(size_t trackIndex);
    android::status_t selectTrack(size_t trackIndex, bool select, int64_t timeUs);
    ssize_t getSelectedTrack(int32_t type) const;

protected:
    ~SourceFeeder();
    void onMessageReceived(const android::sp<android::AMessage>& msg);

private:
    enum {
        kWhatFeedData   = 'feeD',
        kWhatStop       = 'stop',
    };

    void init();
    int64_t onFeedData();
    android::status_t feedTs();
    android::status_t feedEs();
    android::status_t doSeek(int64_t timeUs, int flags);
    int64_t calcFeedDelay(android::status_t err);
    const char* constructADTS(size_t frameSize);
    void initBsf(AVBSFContext*& bsf, AVStream* st);

    std::string mUrl;

    android::sp<FdDataSource> mFdDataSource;
    android::sp<AmFFmpegByteIOAdapter> mSourceAdapter;

    android::sp<ISourceReceiver> mReceiver;

    SourceMode mSourceMode = SOURCE_MODE_UNKNOWN;

    AVFormatContext* mFFmpegCtx = nullptr;
    bool mIsTsFormat = true;
    int64_t mStartTime = INT64_MIN;

    std::vector<Track> mVideoTracks;
    std::vector<Track> mAudioTracks;
    std::vector<Track> mSubtitleTracks;

    std::vector<Track> mSources;
    Track mAudioTrack;
    Track mVideoTrack;
    Track mSubtitleTrack;

    bool mVideoCsdWritten = false;


    int64_t mBitrate = 0;
    int64_t mFeedPeriodUs = -1;
    int64_t mFeedBeginTimeUs = -1;

    int32_t mFeedGeneration = 0;

    uint8_t* mFeedBuffer = nullptr;
    size_t mFeedRemainSize = 0;
    int64_t mTotalFeedSize = 0;
    int64_t mTotalEsPackets = 0;

    void* mFeedEsPacket = nullptr;
    void* mFilteredPacket = nullptr;

    FILE* mFpTs = nullptr;
    FILE* mFpEsAudio = nullptr;
    FILE* mFpEsVideo = nullptr;

    AVBSFContext* mAudioBsf = nullptr;
    AVBSFContext* mVideoBsf = nullptr;

    int64_t mLastAudioPts = INT64_MIN;
    int64_t mLastVideoPts = INT64_MIN;
    int64_t mLastSubtitlePts = INT64_MIN;

    int64_t mFirstVideoPts = INT64_MIN;

    DISALLOW_EVIL_CONSTRUCTORS(SourceFeeder);
};


struct ISourceReceiver : public android::RefBase
{
    virtual ~ISourceReceiver() {}
    virtual int GetWriteBuffer(SourceFeeder::MediaType type, uint8_t** pBuffer, uint32_t* nSize) = 0;
    virtual int WriteData(uint8_t* pBuffer, uint32_t nSize) = 0;
    virtual int WriteData(SourceFeeder::MediaType type, uint8_t* pBuffer, uint32_t nSize, int64_t pts) = 0;
};







#endif
