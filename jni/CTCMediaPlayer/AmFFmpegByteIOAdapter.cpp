/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "CTC_AmFFmpegByteIOAdapter"
#include <utils/Log.h>

#include <assert.h>
#include <stdint.h>
#include <unistd.h>
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avio.h>
#include <libavformat/avio_internal.h>
}

#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/ALooper.h>
#include "FdDataSource.h"
#include "AmFFmpegByteIOAdapter.h"

#ifndef FF_INPUT_BUFFER_PADDING_SIZE
#define FF_INPUT_BUFFER_PADDING_SIZE 32
#endif

static uint32_t kAdapterBufferSize = 32768;

using namespace android;

AmFFmpegByteIOAdapter::AmFFmpegByteIOAdapter()
    : mInitCheck(false),
      mContext(NULL),
      mNextReadPos(0) {
      mEndFlag = 0;
}

AmFFmpegByteIOAdapter::~AmFFmpegByteIOAdapter() {
    ALOGI("%s:%d", __FUNCTION__, __LINE__);

    avio_close(mContext);

    ALOGI("%s:%d", __FUNCTION__, __LINE__);
}

bool AmFFmpegByteIOAdapter::init(const sp<FdDataSource>& source) {
    // Make certain the parameters the called passed are reasonable.
    if (NULL == source.get()) {
        ALOGE("Input source should not be NULL.");
        return false;
    }

    // Already inited?  If so, this is an error.
    if (mInitCheck) {
        ALOGW("Adapter is already initialized.");
        return false;
    }

    memset(&mProtocol, 0, sizeof(URLProtocol));
    mProtocol.url_read = urlRead;
    mProtocol.url_seek = urlSeek;
    mProtocol.flags = AVIO_FLAG_READ;

    URLContext* urlContext = NULL;
    urlContext = (URLContext*)av_mallocz(sizeof(URLContext));
    urlContext->prot = &mProtocol;
    urlContext->flags = mProtocol.flags;
    urlContext->priv_data = this;

    int ret = ffio_fdopen(&mContext, urlContext);
    if (ret < 0) {
        ALOGE("ffio_fdopen failed %d", ret);
    } else {
        mInitCheck = true;
        ALOGI("## create mContext:%p", mContext);
    }

    if (mInitCheck) {
        mSource = source;
    }

    return mInitCheck;
}

// On success, the number of bytes read is returned. On error, -1 is returned.
int AmFFmpegByteIOAdapter::read(uint8_t* buf, int amt) {

    /*[SE][BUG][IPTV-145][chengshun.wang] When end flag is true, return -1, avoid block in pipe read*/
    if (!mInitCheck || NULL == buf || mEndFlag == 1) {
        ALOGI("read source has been force stoped!");
        return AVERROR_EOF;
    }
    if (!amt) {
        ALOGI("read size is 0!");
        return 0;
    }

    ssize_t result = 0;

    result = mSource->readAt(buf, amt, mNextReadPos);
    if (result > 0) {
        mNextReadPos += result;
    }

    return static_cast<int>(result) != 0 ? static_cast<int>(result) : AVERROR_EOF;
}

// Upon successful completion, returns the current position after seeking.
// If whence is AVSEEK_SIZE, returns the size of underlying source.
// Otherwise, -1 is returned.
int64_t AmFFmpegByteIOAdapter::seek(int64_t offset, int whence) {
    // TODO: ChrumiumHTTPDataSource::initCheck() sometimes returns non-OK value
    // even if it is connected.
    // if (!mInitCheck || OK != mSource->initCheck()) {
    if (!mInitCheck) {
        return -1;
    }

    int64_t target = -1;
    int64_t size = 0;
    bool sizeSupported = (OK == mSource->getSize(&size));

    switch (whence) {
    case SEEK_SET:
        target = offset;
        break;
    case SEEK_CUR:
        target = mNextReadPos + offset;
        break;
    case SEEK_END:
        if (sizeSupported) {
            target = size + offset;
        }
        break;
    case AVSEEK_SIZE:
        return size;
    default:
        ALOGE("Invalid seek whence (%d) in ByteIOAdapter::Seek", whence);
    }

    if ((target < 0) || (target > size)) {
        ALOGW("Invalid seek request to %lld (size: %lld).", target, size);
        return -1;
    }

    mNextReadPos = target;

    return target;
}

int32_t AmFFmpegByteIOAdapter::staticRead(void* thiz, uint8_t* buf, int amt) {
    CHECK(thiz);
    return static_cast<AmFFmpegByteIOAdapter *>(thiz)->read(buf, amt);
}

int32_t AmFFmpegByteIOAdapter::staticWrite(void* thiz, uint8_t* buf, int amt) {
    ALOGE("Write operation is not allowed.");
    return -1;
}

int64_t AmFFmpegByteIOAdapter::staticSeek(
        void* thiz, int64_t offset, int whence) {
    CHECK(thiz);
    return static_cast<AmFFmpegByteIOAdapter *>(thiz)->seek(offset, whence);
}

int AmFFmpegByteIOAdapter::urlRead(URLContext* h, uint8_t* buf, int size)
{
    AmFFmpegByteIOAdapter* thiz = (AmFFmpegByteIOAdapter*)h->priv_data;

    return thiz->read(buf, size);
}


int64_t AmFFmpegByteIOAdapter::urlSeek(URLContext* h, int64_t offset, int whence)
{
    AmFFmpegByteIOAdapter* thiz = (AmFFmpegByteIOAdapter*)h->priv_data;

    return thiz->seek(offset, whence);
}

void AmFFmpegByteIOAdapter::disconnect() {
    ALOGI("disconnect");
    mEndFlag = 1;

    if (mSource != NULL) {
        mSource->disconnect();
    }
}

void AmFFmpegByteIOAdapter::flush()
{
    ALOGI("%s", __FUNCTION__);

    //mNextReadPos = 0;
    mEndFlag = 0;
}


