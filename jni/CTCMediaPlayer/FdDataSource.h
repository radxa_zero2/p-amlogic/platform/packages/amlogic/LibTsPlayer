#ifndef _FD_DATASOURCE_H
#define _FD_DATASOURCE_H

#include <media/stagefright/foundation/ABase.h>
#include <utils/Mutex.h>
#include <atomic>
#include <utils/RefBase.h>


class FdDataSource : public android::RefBase {
public:
    FdDataSource(int fd, int64_t offset, int64_t length);
    ssize_t readAt(void *data, size_t size, off64_t offset);
    android::status_t getSize(off64_t *size);
    virtual void setFlags(uint32_t flags);
    void close();
    void disconnect();
    void finishDisconnect();
    void setFd(int fd);
    int64_t getCachedSize();
    void flush();
    
protected:
    ~FdDataSource();

private:
    static constexpr int kFlushTimeout = 100 * 1000; //us

    void seekTo(off64_t offset);

    int mInitCheck = android::NO_INIT;
    int mFd;
    int64_t mOffset = 0;
    int64_t mLength = -1;
    uint32_t mFlags = 0;

    bool mIsPipe = false;
    std::atomic<bool> mIsDisconnecting{false};
    bool mStopped = false;
    int mNotifyFd;

    DISALLOW_EVIL_CONSTRUCTORS(FdDataSource);
};

#endif

