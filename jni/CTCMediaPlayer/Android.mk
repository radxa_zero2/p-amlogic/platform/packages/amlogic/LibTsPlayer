LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= CTCMediaPlayer.cpp \
	CTCMediaPlayerFactory.cpp \
	SourceFeeder.cpp \
	FdDataSource.cpp \
	AmFFmpegByteIOAdapter.cpp \
	CTCMediaInfo.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include \
	$(TOP)/hardware/amlogic/media/amcodec/include \
	$(TOP)/vendor/amlogic/common/external/ffmpeg \
	$(TOP)/frameworks/av/media/libmediaplayerservice \
	$(TOP)/vendor/amlogic/common/frameworks/av/mediaextconfig/include

LOCAL_CFLAGS := -Wno-deprecated-declarations -Wno-unused-parameter -Wno-unused-variable \
	-DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

#LOCAL_HEADER_LIBRARIES := libmedia_headers

LOCAL_SHARED_LIBRARIES := libamffmpeg \
	libstagefright_foundation   \
	libui						\
	libgui						\
	liblog						\
	libbinder					\
	libutils					\
	libcutils					\
	libmedia					\

LOCAL_MODULE := libctcmediaplayer
LOCAL_MODULE_TAGS := optional
include $(BUILD_STATIC_LIBRARY)

###############################################################################
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	SourceFeeder.cpp \
	FdDataSource.cpp \
	AmFFmpegByteIOAdapter.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include \
	$(TOP)/hardware/amlogic/media/amcodec/include \
	$(TOP)/vendor/amlogic/common/external/ffmpeg \
	$(TOP)/vendor/amlogic/common/frameworks/av/mediaextconfig/include

LOCAL_CFLAGS := -Wno-deprecated-declarations -Wno-unused-parameter -Wno-unused-variable \
	-DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

#LOCAL_HEADER_LIBRARIES := libmedia_headers

LOCAL_SHARED_LIBRARIES := libamffmpeg.vendor \
	libstagefright_foundation   \
	libui						\
	libgui						\
	liblog						\
	libbinder					\
	libutils					\
	libcutils					\

LOCAL_MODULE := libctcmediaplayer.vendor
LOCAL_MODULE_TAGS := optional
LOCAL_VENDOR_MODULE := true
include $(BUILD_STATIC_LIBRARY)

