#ifndef _CTC_MEDIAPLAYER_H_
#define _CTC_MEDIAPLAYER_H_

#include <media/MediaPlayerInterface.h>
#include <media/stagefright/foundation/ABase.h>
#include <media/stagefright/foundation/AString.h>
#include <memory>
#include <media/MediaAnalyticsItem.h>
#include <CTC_Common.h>
#include "CTCMediaInfo.h"

namespace aml { 
class CTC_MediaProcessor;
}
class SourceFeeder;
class ITsPlayer;

namespace android {

struct ALooper;
template <typename T>
struct AHandlerReflector;

class CTCMediaPlayer : public MediaPlayerInterface
{
public:
    explicit CTCMediaPlayer(pid_t pid);
    virtual status_t initCheck() override;
    status_t setUID(uid_t uid) override;
	virtual status_t setDataSource(
            const sp<IMediaHTTPService> &httpService,
            const char *url,
            const KeyedVector<String8, String8> *headers) override;
	virtual status_t setDataSource(int fd, int64_t offset, int64_t length) override;
	virtual status_t setDataSource(const sp<IStreamSource> &source) override;
	virtual status_t setDataSource(const sp<DataSource>& dataSource) override;
	virtual status_t setVideoSurfaceTexture(
            const sp<IGraphicBufferProducer> &bufferProducer) override;
	virtual status_t getBufferingSettings(
            BufferingSettings* buffering /* nonnull */) override;
	virtual status_t setBufferingSettings(const BufferingSettings& buffering) override;
	virtual status_t prepare() override;
    virtual status_t prepareAsync() override;
    virtual status_t start() override;
    virtual status_t stop() override;
    virtual status_t pause() override;
    virtual bool isPlaying() override;
    virtual status_t setPlaybackSettings(const AudioPlaybackRate &rate) override;
    virtual status_t getPlaybackSettings(AudioPlaybackRate *rate) override;
    virtual status_t setSyncSettings(const AVSyncSettings &sync, float videoFpsHint) override;
    virtual status_t getSyncSettings(AVSyncSettings *sync, float *videoFps) override;
    virtual status_t seekTo(
            int msec, MediaPlayerSeekMode mode = MediaPlayerSeekMode::SEEK_PREVIOUS_SYNC) override;
    virtual status_t getCurrentPosition(int *msec) override;
    virtual status_t getDuration(int *msec) override;
    virtual status_t reset() override;
    virtual status_t notifyAt(int64_t mediaTimeUs) override;
    virtual status_t setLooping(int loop) override;
    virtual player_type playerType() override;
    virtual status_t invoke(const Parcel &request, Parcel *reply) override;
    virtual void setAudioSink(const sp<AudioSink> &audioSink) override;
    virtual status_t setParameter(int key, const Parcel &request) override;
    virtual status_t getParameter(int key, Parcel *reply) override;

    virtual status_t getMetadata(
            const media::Metadata::Filter& ids, Parcel *records) override;

    virtual status_t dump(int fd, const Vector<String16> &args) const override;

protected:
    friend struct AHandlerReflector<CTCMediaPlayer>;
    virtual ~CTCMediaPlayer();
    void onMessageReceived(const sp<AMessage>& msg);

private:
    enum State {
        STATE_IDLE,
        STATE_UNPREPARED,
        STATE_PREPARED,
        STATE_RUNNING,
        STATE_PAUSED,
        STATE_STOPPED,
    };

    enum {
        kWhatSetVideoSurface    = 'svsF',
        kWhatPrepare            = 'pare',
        kWhatStart              = 'star',
        kWhatPause              = 'pase',
        kWhatStop               = 'stop',
        kWhatSeek               = 'seek',
        kWhatReset              = 'rset',
    };

    void initConfig();
    void initLooper();
    status_t onPrepare();
    status_t onStart();
    status_t onPause();
    status_t onStop();
    status_t onSeek(int64_t seekTimeUs, MediaPlayerSeekMode mode, bool needNotify);
    status_t onReset();
    status_t initCTC();

    void notifyListenser(int msg, int ext1=0, int ext2=0, const android::Parcel* in=NULL);
    void notifyListenser_l(int msg, int ext1=0, int ext2=0, const android::Parcel* in=NULL);

    status_t onGetTrackInfo(Parcel* reply);
    status_t onGetMediaInfo(Parcel* reply);
    status_t updateMediaInfo();	
    status_t onSelectTrack(size_t trackIndex, bool select, int64_t timeUs);
    status_t onGetSelectedTrack(int32_t type, Parcel* reply);

    void updateMetrics(const char *where);

    media_info_t mStreamInfo;

    sp<ALooper> mLooper;

    aml::VIDEO_PARA_T mVideoParas[MAX_VIDEO_PARAM_SIZE];
    aml::AUDIO_PARA_T mAudioParas[MAX_AUDIO_PARAM_SIZE];
    aml::SUBTITLE_PARA_T mSubtitleParas[MAX_SUBTITLE_PARAM_SIZE];
    size_t mVideoParasCount = 0;
    size_t mAudioParasCount = 0;
    size_t mSubtitleParasCount = 0;

    std::shared_ptr<aml::CTC_MediaProcessor> mProcessor;

    android::sp<SourceFeeder> mSourceFeeder;

    sp<Surface> mSurface;
    sp<AHandlerReflector<CTCMediaPlayer>> mHandler;

    aml::PLAYER_STREAMTYPE_E mSetStreamType;
    aml::PLAYER_STREAMTYPE_E mStreamType;
    bool mUseOmx = false;

    MediaAnalyticsItem *mAnalyticsItem = nullptr;

    mutable Mutex mLock;
    State mState = STATE_IDLE;
    bool mLooping = false;
    android::AString mUrl;
    int mFd = -1;
    bool mIsLive = false;

    int64_t mStartTime = AV_NOPTS_VALUE;

private:
    // for ctc begin
    aml::IPTV_PLAYER_EVT_CB ctc_func_player_evt = nullptr;
    void* ctc_player_evt_hander = nullptr;

    // for ctc end

	DISALLOW_EVIL_CONSTRUCTORS(CTCMediaPlayer);
};


}


#endif


