#ifndef _CTC_MEDIAINFO_H_
#define _CTC_MEDIAINFO_H_

#include <amports/aformat.h>
#include <amports/vformat.h>
#include <media/mediaplayer_common.h>
extern "C" {
#include <libavformat/avformat.h>
}

#define MSG_SIZE                    64
#define MAX_VIDEO_STREAMS           10
#define MAX_AUDIO_STREAMS           16
#define MAX_SUB_INTERNAL            64
#define MAX_SUB_EXTERNAL            24
#define MAX_SUB_STREAMS             (MAX_SUB_INTERNAL + MAX_SUB_EXTERNAL)
#define MAX_PLAYER_THREADS          32

#define CALLBACK_INTERVAL           (300)

typedef struct {
    int video_pid;
    int audio_track_num;
    int audio_pid[MAX_AUDIO_STREAMS]; //max audio streams is 16 per programe
    char programe_name[64];
} ts_programe_detail_t;

typedef struct {
    int prog_audio_num;
    int prog_audio_index_list[MAX_AUDIO_STREAMS];
} ts_prog_audio_info;

typedef enum {
    ACOVER_NONE   = 0,
    ACOVER_JPG    ,
    ACOVER_PNG    ,
} audio_cover_type;

typedef struct {
    char title[512];
    char author[512];
    char album[512];
    char comment[512];
    char year[4];
    int track;
    char genre[32];
    char copyright[512];
    audio_cover_type pic;
} audio_tag_info;


typedef enum {
    UNKNOWN_FILE    = 0,
    AVI_FILE        = 1,
    MPEG_FILE       = 2,
    WAV_FILE        = 3,
    MP3_FILE        = 4,
    AAC_FILE        = 5,
    AC3_FILE        = 6,
    RM_FILE         = 7,
    DTS_FILE        = 8,
    MKV_FILE        = 9,
    MOV_FILE        = 10,
    MP4_FILE        = 11,
    FLAC_FILE       = 12,
    H264_FILE       = 13,
    M2V_FILE        = 14,
    FLV_FILE        = 15,
    P2P_FILE        = 16,
    ASF_FILE        = 17,
    STREAM_FILE     = 18,
    APE_FILE        = 19,
    AMR_FILE        = 20,
    AVS_FILE        = 21,
    PMP_FILE        = 22,
    OGM_FILE            = 23,
    HEVC_FILE       = 24,
    DRA_FILE        = 25,
    FILE_MAX,
} pfile_type;

typedef struct {
    int index;
    int id;
    int channel;
    int sample_rate;
    int bit_rate;
    aformat_t aformat;
    int duration;
    audio_tag_info *audio_tag;
    char language[128];
    char audio_mime[128];
    int prog_num;
    int prog_video_index;
} maudio_info_t;

typedef struct {
    int index;
    int id;
    int width;
    int height;
    int aspect_ratio_num;
    int aspect_ratio_den;
    int frame_rate_num;
    int frame_rate_den;
    int bit_rate;
    vformat_t format;
    int duartion;
    unsigned int video_rotation_degree;
    int prog_num;
    ts_prog_audio_info audio_info;
} mvideo_info_t;

typedef struct {
    int index;
    int id;
    char internal_external; //0:internal_sub 1:external_sub
    unsigned short width;
    unsigned short height;
    unsigned int sub_type;
    char resolution;
    long long subtitle_size;
    char sub_language[128];
} msub_info_t;

typedef struct {
    char *filename;
    int  duration;
    long long  file_size;
    pfile_type type;
    int bitrate;
    int has_video;
    int has_audio;
    int has_sub;
    int nb_streams;
    int total_video_num;
    int cur_video_index;
    int total_audio_num;
    int cur_audio_index;
    int total_sub_num;
    int cur_sub_index;
    int seekable;
    int drm_check;
    int adif_file_flag;
} mstream_info_t;

typedef struct {
    int programe_num;
    ts_programe_detail_t ts_programe_detail[MAX_VIDEO_STREAMS];
    int cur_prognum;
} ts_programe_info_t;

typedef struct {
    mstream_info_t stream_info;
    mvideo_info_t *video_info[MAX_VIDEO_STREAMS];
    maudio_info_t *audio_info[MAX_AUDIO_STREAMS];
    msub_info_t *sub_info[MAX_SUB_STREAMS];
    ts_programe_info_t ts_programe_info;
    int is_multi_prog;
} media_info_t;





AVMediaType trackType2AvMediaType(android::media_track_type trackType);
android::media_track_type avMediaType2TrackType(AVMediaType type);
const char* player_vformat2str(vformat_t value);





#endif
