#define LOG_TAG "testLooper"
#include <utils/Log.h>
#include <media/stagefright/foundation/AHandler.h>
#include <media/stagefright/foundation/ALooper.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/hexdump.h>
#include <stdio.h>
#include <utils/Looper.h>

using namespace android;

struct MyHandler : AHandler
{
    MyHandler() = default;

    enum {
        kWhatStart = 'star',
    };

    void init();
    void start();

protected:
    void onMessageReceived(const sp<AMessage>& msg);

private:
    int eventCallback(int fd, int events);
    sp<Looper> mLooper;


    MyHandler(const MyHandler&) = delete;
    MyHandler& operator= (const MyHandler&);
};

void MyHandler::init()
{
    mLooper = new Looper(true);

    int fd = STDIN_FILENO;

    int status = ::fcntl(fd, F_GETFL);
    status |= O_NONBLOCK;
    ::fcntl(fd, F_SETFL, status);

    mLooper->addFd(fd, 0, Looper::EVENT_INPUT,
            [](int fd, int events, void* data) {
            MyHandler* h = (MyHandler*)data;
            return h->eventCallback(fd, events);
        }, this);
}

void MyHandler::start()
{
    sp<AMessage> msg = new AMessage(kWhatStart, this);
    msg->post();
}

void MyHandler::onMessageReceived(const sp<AMessage>& msg)
{
    switch (msg->what()) {
    case kWhatStart:
    {
        int ret = mLooper->pollOnce(-1);
        ALOGI("pollOnce return %d", ret);
        msg->post(0);
    }
    break;

    }
}

//////////////////////////////////
int MyHandler::eventCallback(int fd, int events)
{
    ALOGI("fd:%d, events:%d", fd, events);

    for (;;) {
        uint8_t buf[1024];
        int ret = ::read(fd, buf, sizeof(buf));
        if (ret < 0) {
            ALOGI("read fd:%d return %d, errno:%d", fd, ret, errno);
            break;
        }

        AString s;
        hexdump(buf, ret, 0, &s);
        ALOGI("read: %s", s.c_str());
    }

    return 1;
}


int main(int argc, char *argv[])
{
    sp<MyHandler> handler = new MyHandler;
    sp<ALooper> looper = new ALooper;
    looper->registerHandler(handler);
    looper->setName("hello");
    looper->start();

    handler->init();
    handler->start();

    for (;;) {

    }

    return 0;
}
