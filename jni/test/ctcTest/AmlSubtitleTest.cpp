#include "Module.h"
#include "CTCTest.h"
#include <iostream>
#include <CTC_MediaProcessor.h>


///////////////////////////////////////////////////////////////////////////////
struct AmlSubtitleModule : Module {
    explicit AmlSubtitleModule(const Command*commands);
    virtual ~AmlSubtitleModule();

    virtual void usage();
    virtual int parseArguments(int argc, char** argv);
    virtual int initCTC();
    virtual int runCommand(const std::string& cmdBuf);

private:
    AmlSubtitleModule(const AmlSubtitleModule&) = delete;
    AmlSubtitleModule& operator=(const AmlSubtitleModule&) = delete;
};


///////////////////////////////////////////////////////////////////////////////
#define STR_TO_ENUM(s) {#s, s}

static int close_SubtitleShowHide(void* _mp, const std::vector<std::string>& args __unused)
{
    int ret = 0;
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;

    ret = mp->SubtitleShowHide(false);
    printf("call playerback_getStatusInfo return %d\n", ret);

    return 0;
}

static int open_SubtitleShowHide(void* _mp, const std::vector<std::string>& args __unused)
{
    int ret = 0;
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;

    ret = mp->SubtitleShowHide(true);
    printf("call playerback_getStatusInfo return %d\n", ret);

    return 0;
}

int param[10] = {0};
int i = 0;
static int cmd_SwitchSubtitle(void* _mp, const std::vector<std::string>& args)
{
    i = 0;
    for_each(args.begin(), args.end(), [](const std::string& p){printf("##:size=%u,%s\n", p.size(), p.c_str()); param[i++] = atoi(p.c_str()); });

    std::cout << "type:" << param[0] << "pid:" << param[1] << "vfmt:" << param[2] <<std::endl;

    aml::SUBTITLE_PARA_T para;
    if (param[0] == 2) {
        para.sub_type                       = aml::CC;
        para.cc_param.vfmt                  = (vformat_t)param[2];
        para.cc_param.Channel_ID            = param[1];
    } else if (param[0] == 3) {
        para.sub_type                       = aml::SCTE27;
        para.scte27_param.SCTE27_PID        = param[1];
    } else if (param[0] == 4) {
        para.sub_type                       = aml::DVB_SUBTITLE;
        para.dvb_sub_param.Sub_PID          = param[1];
        para.dvb_sub_param.Composition_Page = 1;
        para.dvb_sub_param.Ancillary_Page   = 1;
    }

    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;

    if (param[0] == 2 || param[0] == 3) {
        mp->InitSubtitle(&para);
    }

    mp->SwitchSubtitle(param[1], nullptr);

    return 0;
}

static int cmd_setSubtitleViewWindow(void* _mp, const std::vector<std::string>& args)
{
    i = 0;
    for_each(args.begin(), args.end(), [](const std::string& p){printf("##:size=%u,%s\n", p.size(), p.c_str()); param[i++] = atoi(p.c_str()); });

    std::cout << "x:" << param[0] << "y:" << param[1] << "width:" << param[2] << "height:" << param[3] << std::endl;

    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;
    //mp->setSubtitleViewWindow(param[0], param[1], param[2], param[3]);
    (void)mp;

    return 0;
}


static struct Command ctcTestCommands[] = {
    {"closeSubtitleShowHide", 0, {close_SubtitleShowHide}, "subtitle show or hide"},
    {"openSubtitleShowHide", 0, {open_SubtitleShowHide}, "subtitle show or hide"},
    {"SwitchSubtitle", 1, {cmd_SwitchSubtitle}, "switch subtitle"},
    {"setSubtitleViewWindow", 1, {cmd_setSubtitleViewWindow}, "set subtitle view window"},
    {nullptr, 0, {nullptr}, ""}
};

static struct ModuleDescriptor moduleDesc {
    .name = "amlsubtitle",
    .module = new AmlSubtitleModule(ctcTestCommands),
};

AUTO_REGISTER_MODULE(&moduleDesc);

///////////////////////////////////////////////////////////////////////////////
AmlSubtitleModule::AmlSubtitleModule(const Command* commands)
: Module(commands)
{

}

AmlSubtitleModule::~AmlSubtitleModule()
{

}

void AmlSubtitleModule::usage()
{
    Module::usage();
}

int AmlSubtitleModule::parseArguments(int argc, char** argv)
{
    int ret = 0;

    ret = Module::parseArguments(argc, argv);

    return ret;
}

int AmlSubtitleModule::initCTC()
{
    printf("register default_ctc_cb\n");

    static_cast<aml::CTC_MediaProcessor*>(mCtc)->playerback_register_evt_cb(aml::default_ctc_cb, this);

    return 0;
}

int AmlSubtitleModule::runCommand(const std::string& cmdBuf)
{
    int ret = 0;

    ret = Module::runCommand(cmdBuf);

    return ret;
}


