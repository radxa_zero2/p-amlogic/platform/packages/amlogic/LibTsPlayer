#include "Module.h"
#include <legacy_CTC_MediaProcessor.h>

extern void default_ctc_cb(IPTV_PLAYER_EVT_e evt, void* handler __unused);

///////////////////////////////////////////////////////////////////////////////
struct ZTEModule : Module {
    explicit ZTEModule(const Command*commands);
    virtual ~ZTEModule();

    virtual const char* moduleName() const;
    virtual void usage();
    virtual int parseArguments(int argc, char** argv);
    virtual int initCTC();
    virtual int runCommand(const std::string& cmdBuf);

private:
    ZTEModule(const ZTEModule&) = delete;
    ZTEModule& operator=(const ZTEModule&) = delete;
};

///////////////////////////////////////////////////////////////////////////////
static int cmd_switchSubtitle(void* _mp, const std::vector<std::string>& args)
{
    int pid = std::stoi(args[0]);
    std::cout << "subtitle pid: " << pid << std::endl;

    ::CTC_MediaProcessor* mp = (::CTC_MediaProcessor*)_mp;
    mp->SwitchSubtitle(pid, aml::UNKNOWN_SUBTYPE);

    return 0;
}


static struct Command ctcTestCommands[] = {
    {"switchSubtitle", 1, {cmd_switchSubtitle}, "switch subtitle"},
    {nullptr, 0, {nullptr}, ""}
};

static struct ModuleDescriptor moduleDesc {
    .name = "zte",
    .module = new ZTEModule(ctcTestCommands),
};

AUTO_REGISTER_MODULE(&moduleDesc);

///////////////////////////////////////////////////////////////////////////////
ZTEModule::ZTEModule(const Command* commands)
: Module(commands)
{

}

ZTEModule::~ZTEModule()
{

}

const char* ZTEModule::moduleName() const
{
    return moduleDesc.name;
}

void ZTEModule::usage()
{
    Module::usage();
}

int ZTEModule::parseArguments(int argc, char** argv)
{
    int ret = 0;

    ret = Module::parseArguments(argc, argv);

    return ret;
}

int ZTEModule::initCTC()
{
    printf("register default_ctc_cb\n");

    static_cast<::CTC_MediaProcessor*>(mCtc)->playerback_register_evt_cb(default_ctc_cb, this);

    return 0;
}

int ZTEModule::runCommand(const std::string& cmdBuf)
{
    int ret = 0;

    ret = Module::runCommand(cmdBuf);

    return ret;
}

