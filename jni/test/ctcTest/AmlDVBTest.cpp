#include "Module.h"
#include <string.h>
#include <CTC_MediaProcessor.h>

struct AmlDVBModule : Module {
    explicit AmlDVBModule(const Command* commands);
    virtual ~AmlDVBModule();

    virtual const char* moduleName() const;
    virtual void usage();
    virtual int parseArguments(int argc, char** argv);
    virtual int initCTC();
    virtual int runCommand(const std::string& cmdBuf);

private:
    aml::DVB_CAS_TYPE_E mCaType = aml::AMLDVB_CA_TYPE_CLEAR;
    int mEcmPid = 0x1FFF;
    int mEmmPid = 0x1FFF;

    AmlDVBModule(const AmlDVBModule&) = delete;
    AmlDVBModule& operator=(const AmlDVBModule&) = delete;
};

static struct Command ctcTestCommands[] = {
    {nullptr, 0, {nullptr}, ""}
};

static struct ModuleDescriptor moduleDesc {
    .name = "amldvb",
    .module = new AmlDVBModule(ctcTestCommands),
};

AUTO_REGISTER_MODULE(&moduleDesc);

///////////////////////////////////////////////////////////////////////////////
AmlDVBModule::AmlDVBModule(const Command* commands)
: Module(commands)
{

}

AmlDVBModule::~AmlDVBModule()
{

}

const char* AmlDVBModule::moduleName() const
{
    return moduleDesc.name;
}

void AmlDVBModule::usage()
{
    Module::usage();

    printf("    -<ca type>, eg:vmx\n");
    printf("    -ecm <pid>\n");
    printf("    -emm <pid>\n");
}

int AmlDVBModule::parseArguments(int argc, char** argv)
{
    int ret = 0;
    int processed = 0;

    for (int i = 0; i < argc; ++i) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
            case 'v':
            {
                if (strcmp(argv[i]+1, "vmx") == 0) {
                    mCaType = aml::AMLDVB_CA_TYPE_VMX;

                    rearrangeOptions(argv, i, 1, processed);
                }
            }
            break;

            case 'e':
            {
                if (strcmp(argv[i]+1, "ecm") == 0) {
                    if (i < argc-1) {
                        mEcmPid = strtol(argv[++i], nullptr, 0);
                        printf("ecm pid = %d\n", mEcmPid);
                        rearrangeOptions(argv, i-1, 2, processed);
                    } else {
                        rearrangeOptions(argv, i, 1, processed);
                    }
                } else if (strcmp(argv[i]+1, "emm") == 0) {
                    if (i < argc-1) {
                        mEmmPid = strtol(argv[++i], nullptr, 0);
                        printf("emm pid = %d\n", mEmmPid);
                        rearrangeOptions(argv, i-1, 2, processed);
                    } else {
                        rearrangeOptions(argv, i, 1, processed);
                    }
                }
            }
            break;

            default:
                break;
            }
        }
    }

    argc -= processed;
    argv += processed;

    ret = Module::parseArguments(argc, argv);

    return ret;
}

int AmlDVBModule::initCTC()
{
    aml::DVB_CAS_INIT_PARA para;

    para.type = mCaType;
    if (para.type == aml::AMLDVB_CA_TYPE_VMX) {
        para.p_vmx_init.ecmpid = mEcmPid;
        para.p_vmx_init.emmpid = mEmmPid;
    }

    printf("InitCAS, ca type = %d\n", para.type);
    static_cast<aml::CTC_MediaProcessor*>(mCtc)->InitCAS(&para);

    return 0;
}

int AmlDVBModule::runCommand(const std::string& cmdBuf)
{
    int ret = 0;

    ret = Module::runCommand(cmdBuf);

    return ret;
}
