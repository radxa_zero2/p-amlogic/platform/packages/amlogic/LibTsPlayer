#define LOG_TAG "CTCModule"
#include "Module.h"
#include "CTCTest.h"
#include <CTC_MediaProcessor.h>
#include<mutex>
#include <map>
#include <utils/RefBase.h>
#include <utils/Log.h>
#include <media/stagefright/foundation/ADebug.h>

using namespace android;

///////////////////////////////////////////////////////////////////////////////
struct CTCModule : Module {
    explicit CTCModule(const Command*commands);
    virtual ~CTCModule();

    virtual const char* moduleName() const;
    virtual void usage();
    virtual int parseArguments(int argc, char** argv);
    virtual int initCTC();
    virtual int runCommand(const std::string& cmdBuf);

private:
    struct SectionFilterContext : public android::RefBase {
        SectionFilterContext(int pid) : mPid(pid) {}
        ~SectionFilterContext() {}

        int mPid = 0x1FFF;
        void* channel = nullptr;
        void* filter = nullptr;
    };

    struct Section {
        Section(const uint8_t* buffer_, size_t size_)
        : buffer(buffer_)
        , size(size_)
        {
        }

        const uint8_t* data() const {
            return buffer + bufferIndex;
        }

        size_t dataSize() const {
            return size;
        }

        bool empty() const {
            return size == 0;
        }

        const uint8_t* advance(int offset) {
            bufferIndex += offset;
            size -= offset;

            return buffer + bufferIndex;
        }

    private:
        const uint8_t* buffer = nullptr;
        int size = 0;
        int bufferIndex = 0;
    };

    int addSectionFilter(int pid, aml::SECTION_FILTER_CB cb);
    int removeSectionFilter(int pid);

    static int patCb(size_t size, uint8_t* data, void* userData);
    static int pmtCb(size_t size, uint8_t* data, void* userData);

    struct PMTStream {
        int straemType;
        int streamPid;
    };
    void onPmtParsed(const std::vector<PMTStream>& streams);

    std::mutex mLock;
    std::map<int, android::sp<SectionFilterContext>> mSectionFilters;
    int mFilterId = 0;

private:
    aml::CTC_SyncSource mAvSyncSource = aml::CTC_SYNCSOURCE_AUDIO;

    CTCModule(const CTCModule&) = delete;
    CTCModule& operator=(const CTCModule&) = delete;
};


///////////////////////////////////////////////////////////////////////////////
static int cmd_videoShow(void* _mp, const std::vector<std::string>& args __unused)
{
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;
    mp->VideoShow();

    return 0;
}


static int cmd_videoHide(void* _mp, const std::vector<std::string>& args __unused)
{
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;
    mp->VideoHide();

    return 0;
}

static int cmd_videoStart(void* _mp, const std::vector<std::string>& args __unused)
{
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;
    mp->SetParameter(aml::CTC_KEY_PARAMETER_START_VIDEO, nullptr);

    return 0;
}

static int cmd_videoStop(void* _mp, const std::vector<std::string>& args __unused)
{
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;
    mp->SetParameter(aml::CTC_KEY_PARAMETER_STOP_VIDEO, nullptr);

    return 0;
}

static int cmd_videoSwitch(void* _mp, const std::vector<std::string>& args __unused)
{
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;

    int pid = std::stoi(args[0]);
    int vfmt = std::stoi(args[1]);
    printf("pid:%d, vfmt:%d\n", pid, vfmt);

    aml::VIDEO_PARA_T vPara{};
    vPara.pid = pid;
    vPara.vFmt = (vformat_t)vfmt;

    mp->SwitchVideoTrack(pid, &vPara);

    return 0;
}

static int cmd_getBufferStatus(void* _mp, const std::vector<std::string>& args __unused)
{
    aml::AVBUF_STATUS bufStatus;

    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;
    mp->GetBufferStatus(&bufStatus);
    printf("audio buf_size:%d, level:%d, buffered:%dms\n \
            video buf_size:%d, level:%d, buffered:%dms\n",
            bufStatus.abuf_size, bufStatus.abuf_data_len, bufStatus.abuf_ms,
            bufStatus.vbuf_size, bufStatus.vbuf_data_len, bufStatus.vbuf_ms);

    return 0;
}

static int cmd_setSpeed(void* _mp, const std::vector<std::string>& args)
{
    aml::CTC_MediaProcessor* mp = (aml::CTC_MediaProcessor*)_mp;

    float speed = std::stof(args[0]);

    aml::CTC_PLAYSPEED_OPT_S options;
    options.enPlaySpeedDirect = aml::CTC_PLAYSPEED_DIRECT_FORWARD;
    options.u32SpeedInteger = (uint32_t)speed;
    options.u32SpeedDecimal = (uint32_t)(speed*1000)%1000;
    mp->SetParameter(aml::CTC_KEY_PARAMETER_PLAYSPEED, &options);

    return 0;
}

static struct Command ctcTestCommands[] = {
    {"videoShow", 0, {cmd_videoShow}, "show video!"},
    {"videoHide", 0, {cmd_videoHide}, "hide video!"},
    {"videoStart", 0, {cmd_videoStart}, "start video!"},
    {"videoStop", 0, {cmd_videoStop}, "stop video!"},
    {"switchVideo", 2, {cmd_videoSwitch}, "switch video track!"},
    {"getBufferStatus", 0, {cmd_getBufferStatus}, "get buffer status!"},
    {"setspeed", 1, {cmd_setSpeed}, "set playback rate!"},
    {nullptr, 0, {nullptr}, ""}
};

static struct ModuleDescriptor moduleDesc {
    .name = "ctc",
    .module = new CTCModule(ctcTestCommands),
};

AUTO_REGISTER_MODULE(&moduleDesc);

///////////////////////////////////////////////////////////////////////////////
CTCModule::CTCModule(const Command* commands)
: Module(commands)
{

}

CTCModule::~CTCModule()
{

}

const char* CTCModule::moduleName() const
{
    return moduleDesc.name;
}

void CTCModule::usage()
{
    Module::usage();

    printf("    -pcr: set pcr as sync source.\n");
}

int CTCModule::parseArguments(int argc, char** argv)
{
    int ret = 0;
    int processed = 0;

    for (int i = 0; i < argc; ++i) {
        if (argv[i][0] == '-') {
            if (!strcmp(argv[i], "-pcr")) {
                printf("pcr sync mode!\n");
                mAvSyncSource = aml::CTC_SYNCSOURCE_PCR;
                rearrangeOptions(argv, i, 1, processed);
            }
        }
    }

    argc -= processed;
    argv += processed;

    ret = Module::parseArguments(argc, argv);

    return ret;
}

int CTCModule::initCTC()
{
    if (isLive()) {
        mAvSyncSource = aml::CTC_SYNCSOURCE_PCR;
    }

    static_cast<aml::CTC_MediaProcessor*>(mCtc)->initSyncSource(mAvSyncSource);

    printf("register default_ctc_cb\n");
    static_cast<aml::CTC_MediaProcessor*>(mCtc)->playerback_register_evt_cb(aml::default_ctc_cb, this);

    addSectionFilter(0, &CTCModule::patCb);

    return 0;
}

int CTCModule::runCommand(const std::string& cmdBuf)
{
    int ret = 0;

    ret = Module::runCommand(cmdBuf);

    return ret;
}

int CTCModule::addSectionFilter(int pid, aml::SECTION_FILTER_CB cb)
{
    sp<SectionFilterContext> context = new SectionFilterContext(pid);
    aml::CTC_MediaProcessor* ctc = static_cast<aml::CTC_MediaProcessor*>(mCtc);
    context->channel = ctc->DMX_CreateChannel(pid);
    int ret = ctc->DMX_OpenChannel(context->channel);
    if (ret < 0) {
        ALOGE("open channel pid:%d failed!", pid);
        return ret;
    }

    context->filter = ctc->DMX_CreateFilter(cb, this, mFilterId++);
    ret = ctc->DMX_AttachFilter(context->filter, context->channel);

    {
        std::lock_guard<std::mutex> _l(mLock);
        mSectionFilters.emplace(pid, context);
    }

    return 0;
}

int CTCModule::removeSectionFilter(int pid)
{
    aml::CTC_MediaProcessor* ctc = static_cast<aml::CTC_MediaProcessor*>(mCtc);
    sp<SectionFilterContext> context;

    {
        std::lock_guard<std::mutex> _l(mLock);
        auto it = mSectionFilters.find(pid);
        context = it->second;
    }

    if (context->filter != nullptr) {
        ctc->DMX_DetachFilter(context->filter, context->channel);
        ctc->DMX_DestroyFilter(context->filter);
        context->filter = nullptr;
    }

    if (context->channel != nullptr) {
        ctc->DMX_CloseChannel(context->channel);
        ctc->DMX_DestroyChannel(context->channel);
        context->channel = nullptr;
    }

    {
        std::lock_guard<std::mutex> _l(mLock);
        int ret = mSectionFilters.erase(context->mPid);
        ALOGI("pid:%d, %d sections removed!", pid, ret);
    }

    return 0;
}

int CTCModule::patCb(size_t size, uint8_t* data, void* userData)
{
    CTCModule* thiz = (CTCModule*)userData;

    ALOGI("pat cb, size:%d", size);
    Section section(data, size);
    const uint8_t* p = section.data();
    int section_syntax_indicator = (p[1]&0x80) >> 7;
    CHECK(section_syntax_indicator == 1);
    int section_length = (p[1] & 0x0F) << 4 | p[2];
    CHECK(section_length <= 4093);
    ALOGI("section_length = %d, size:%d", section_length, size);

    p = section.advance(3);
    //int transport_stream_id = p[0]<<8 | p[1];
    p = section.advance(5);
    int numPrograms = (section.dataSize() - 4)/4;

    int program_number = 0;
    int program_map_PID = 0x1FFF;
    for (int i = 0; i < numPrograms; ++i) {
        program_number = p[0]<<4 | p[1];

        if (program_number != 0) {
            program_map_PID = (p[2]&0x01F) << 8 | p[3];
            ALOGI("programNumber:%d, program_map_PID = %d\n", program_number, program_map_PID);
        } else {
            //int network_PID = (p[2]&0x1F) << 8 | p[3];
        }

        p = section.advance(4);
    }

    if (program_map_PID != 0x1FFF) {
        thiz->removeSectionFilter(0);
        thiz->addSectionFilter(program_map_PID, pmtCb);
    }

    return 0;
}

int CTCModule::pmtCb(size_t size, uint8_t* data, void* userData)
{
    CTCModule* thiz = (CTCModule*)userData;

    ALOGI("pmt cb, size:%d", size);
    Section section(data, size);
    const uint8_t* p = section.data();
    //int table_id = p[0];
    int section_syntax_indicator = (p[1]&0x80) >> 7;
    CHECK(section_syntax_indicator == 1);
    int section_length = (p[1] & 0x0F) << 4 | p[2];
    CHECK(section_length <= 4093);
    ALOGI("section_length = %d, size:%d", section_length, size);

    p = section.advance(3);
    int programNumber = p[0]<<8 | p[1];
    p = section.advance(5);

    int pcr_pid = (p[0]&0x1F)<<8 | p[1];
    //results.pcrPid = pcr_pid;
    int program_info_length = (p[2]&0x0F) << 8 | p[3];
    CHECK(program_info_length < 1024);
    p = section.advance(4);

    //skip program info
    p = section.advance(program_info_length);
    int infoBytesRemaining = section.dataSize() - 4;

    std::vector<PMTStream> pmtStreams;
    while (infoBytesRemaining >= 5) {
        int stream_type = p[0];
        int elementary_pid = (p[1]&0x1F) << 8 | p[2];
        int es_info_length = (p[3]&0x0F) << 8 | p[4];
        p = section.advance(5 + es_info_length);
        infoBytesRemaining -= 5 + es_info_length;

        ALOGI("straem_type:%d, pid:%d(%#x)", stream_type, elementary_pid, elementary_pid);
        pmtStreams.push_back({stream_type, elementary_pid});
    }

    thiz->onPmtParsed(pmtStreams);

    return 0;
}

void CTCModule::onPmtParsed(const std::vector<PMTStream>& pmtStreams)
{
    aml::CTC_MediaProcessor* ctc = static_cast<aml::CTC_MediaProcessor*>(mCtc);

    for (auto& p : pmtStreams) {
        switch (p.straemType) {
        //AVC
        case 27:
        //HEVC
        case 36:
            {
                if (mVideoPid != 0x1FFF && mVideoPid != p.streamPid) {
                    ALOGI("video PID changed:%d -> %d", mVideoPid, p.streamPid);
                    aml::VIDEO_PARA_T vPara{};
                    vPara.pid = p.streamPid;
                    if (p.straemType == 27) {
                        vPara.vFmt = VFORMAT_H264;
                    } else if (p.straemType == 36) {
                        vPara.vFmt = VFORMAT_HEVC;
                    }
                    ctc->SwitchVideoTrack(p.streamPid, &vPara);
                }
                mVideoPid = p.streamPid;
            }
            break;

        //AAC
        case 15:
        // AC-3
        case 129:
            {
                if (mAudioPid != 0x1FFF && mAudioPid != p.streamPid) {
                    ALOGI("audio PID changed:%d -> %d", mAudioPid, p.streamPid);
                    aml::AUDIO_PARA_T aPara{};
                    aPara.pid = p.streamPid;
                    if (p.straemType == 15) {
                        aPara.aFmt = AFORMAT_AAC;
                    } else if (p.straemType == 129) {
                        aPara.aFmt = AFORMAT_AC3;
                    }
                    ctc->SwitchAudioTrack(p.streamPid, &aPara);
                }
                mAudioPid = p.streamPid;
            }
            break;

        default:
            ALOGE("unknown straem type:%d", p.straemType);
            break;
        }
    }
}


