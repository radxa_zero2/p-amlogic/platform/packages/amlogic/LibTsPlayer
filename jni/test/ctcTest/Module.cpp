#include "Module.h"
#include <algorithm>


///////////////////////////////////////////////////
int cmd_showHelp(Module* module, const std::vector<std::string>& args __unused)
{
    module->printCommands();
    return 0;
}

//builtin commands
static Command builtinCommands[] = {
    {"help", 0, .m_fn = cmd_showHelp, "show help"},
    {nullptr, 0, {nullptr}, ""}
};

///////////////////////////////////////////////////////////////////////////////
Module::Module(const Command* commands)
: mCommands(commands)
, mBuiltinCommands(builtinCommands)
{
}

Module::~Module()
{

}

void Module::printCommands()
{
    doPrintCommands(mBuiltinCommands, true);
    doPrintCommands(mCommands);
}

void Module::usage()
{
    printf("Usage: ctctest %s <filepath> <options>\n\
options:\n\
    -e(ES mode)\n\
    -l(legacy mode)\n\
    -o(omx)\n\
    -v <pid>\n\
    -a <pid>\n\
    -s <pid>\n\
    -h(show help)\n\n", moduleName());
}

int Module::parseArguments(int argc, char** argv)
{
    for (int i = 0; i < argc; ++i) {
        if (argv[i][0] == '-') {
            if (argv[i][2] != '\0') {
                continue;
            }

            switch (argv[i][1]) {
            case 'e':
                mFeedEs = true;
                break;

            case 'l':
                printf("legacy mode!\n");
                mLegacyMode = true;
                break;

            case 'o':
                mUseOmx = true;
                break;

            case 'v':
                if (i < argc-1) {
                    mVideoPid = strtol(argv[++i], nullptr, 0);
                    printf("video pid = %d\n", mVideoPid);
                }
                break;

            case 'a':
                if (i < argc-1) {
                    mAudioPid = strtol(argv[++i], nullptr, 0);
                    printf("audio pid = %d\n", mAudioPid);
                }
                break;

            case 's':
                if (i < argc-1) {
                    mSubtitlePid = strtol(argv[++i], nullptr, 0);
                    printf("subtitle pid = %d\n", mSubtitlePid);
                }
                break;

            case 'h':
                usage();
                break;

            default:
                printf("unknown argument:%c\n", argv[i][1]);
                break;
            }
        } else {
            mFile = argv[i];
        }
    }

    if (mFile.empty()) {
        printf("no file to play!\n");
        return -1;
    } else {
        if (!strncasecmp(mFile.c_str(), "udp://", 6)) {
            mIsLive = true;
        }
    }

    return 0;
}

int Module::initCTC()
{
    return 0;
}

int Module::runCommand(const std::string& cmdBuf)
{
    std::vector<std::string> result;

    result = split(cmdBuf, result);
    if (result.empty()) {
        return -1;
    }

    //for_each(result.begin(), result.end(), [](std::string& p){printf("##:size=%u,%s\n", p.size(), p.c_str()); });

    std::string cmdName = *result.begin();
    const struct Command* command = nullptr;

    command = findCommand(cmdName, mBuiltinCommands);
    if (command != nullptr) {
        result.erase(result.begin());

        if (result.size() < command->argCount) {
            return -1;
        }

        return command->m_fn(this, result);
    } else {
        command = findCommand(cmdName, mCommands);
    }

    if (command != nullptr) {
        result.erase(result.begin());

        if (result.size() < command->argCount) {
            return -1;
        }

        return command->fn(mCtc, result);
    } else {
        printf("Unknown command: %s\n", cmdName.c_str());
        return -1;
    }
}

static bool isDelimiter(const std::string& delimiters, char c)
{
    return delimiters.find(c) != std::string::npos;
}

std::vector<std::string>& Module::split(const std::string& str, std::vector<std::string>& result)
{
    std::string::size_type pos, prev = 0;
    std::string s;
    std::string cmdDelimiter = " ";
    std::string argDelimiter = ", ";

    pos = str.find_first_of(cmdDelimiter);

    if (pos == std::string::npos) {
        result.emplace_back(std::move(str));
        return result;
    }

    result.emplace_back(str, 0, pos);
    prev = pos + 1;

    while ((pos = str.find_first_of(argDelimiter, prev)) != std::string::npos) {
        if (pos > prev) {
            size_t b = prev;
            size_t e = pos;
            while (b < e) {
                if (isspace(str[b]) || isDelimiter(argDelimiter, str[b]))        ++b;
                else if (isspace(str[e]) || isDelimiter(argDelimiter, str[e]))   --e;
                else                        break;
            }

            if (b <= e) {
                result.emplace_back(str, b, e - b + 1);
            }
        }

        prev = pos + 1;
    }

    while (prev < str.size()) {
        if (isspace(str[prev]) || isDelimiter(argDelimiter, str[prev]))
            ++prev;
        else
            break;
    }

    if (prev < str.size())
        result.emplace_back(str, prev, str.size() - prev);

    return result;
}

const Command* Module::findCommand(const std::string& cmdName, const Command* commandTable)
{
    if (commandTable == nullptr)
        return nullptr;

    const struct Command* command = commandTable;
    const struct Command* result  = nullptr;

    while (command && command->name) {
        if (command->name == cmdName) {
            result = command;
            break;
        }

        ++command;
    }

    return result;
}

void Module::doPrintCommands(const Command* pCommands, bool printHeader)
{
    if (pCommands == nullptr) return;

    const struct Command* command = pCommands;

    if (printHeader && command && command->name) {
        printf("commands help:\n%20s | %s | %-s\n", "Command Name", "Args count", "Help");
    }

    while (command && command->name) {
        const char* help = command->help ? command->help : "";
        printf("%20s | %10u | %s\n", command->name, command->argCount, help);

        ++command;
    }
}

void Module::rearrangeOptions(char** argv, int parsed, int len, int& processed)
{
    char* cur = nullptr;

    for (size_t i = 0; i < len; ++i, ++parsed) {
        cur = argv[parsed];

        for (int j = parsed; j > processed; --j) {
            argv[j] = argv[j-1];
        }
        argv[processed++] = cur;
    }
}


///////////////////////////////////////////////////
//all test modules;
std::map<std::string, Module*> g_modules;

void registerModule(ModuleDescriptor* desc)
{
    //printf("registerModule: %s\n", desc->name);

    if (g_modules.find(desc->name) != g_modules.end()) {
        printf("module %s already registerd!\n", desc->name);
        return;
    }

    g_modules.emplace(desc->name, desc->module);
}
