#include "Module.h"
#include "CTCTest.h"
#include <CTC_MediaProcessor.h>

///////////////////////////////////////////////////////////////////////////////
struct AmlProbeModule : Module {
    explicit AmlProbeModule(const Command*commands);
    virtual ~AmlProbeModule();

    virtual const char* moduleName() const;
    virtual void usage();
    virtual int parseArguments(int argc, char** argv);
    virtual int initCTC();
    virtual int runCommand(const std::string& cmdBuf);

private:
    AmlProbeModule(const AmlProbeModule&) = delete;
    AmlProbeModule& operator=(const AmlProbeModule&) = delete;
};


///////////////////////////////////////////////////////////////////////////////
#define STR_TO_ENUM(s) {#s, s}

#if 0
static struct IPTV_PLAYER_ATTR_TABLE {
    const char* str;
    IPTV_ATTR_TYPE_e attr;
} iptvPlayerAttrTable[] = {
    STR_TO_ENUM(IPTV_PLAYER_ATTR_VID_ASPECT),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_VID_RATIO),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_VID_SAMPLETYPE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_VIDAUDDIFF),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_VID_BUF_SIZE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_VID_USED_SIZE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_AUD_BUF_SIZE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_AUD_USED_SIZE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_AUD_SAMPLERATE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_AUD_BITRATE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_AUD_CHANNEL_NUM),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_VID_FRAMERATE ),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_V_HEIGHT),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_V_WIDTH),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_STREAM_BITRATE),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_CATON_TIMES),
    STR_TO_ENUM(IPTV_PLAYER_ATTR_CATON_TIME),
};
#endif

static int cmd_getStatusInfo(void* mp __unused, const std::vector<std::string>& args)
{
    std::string strAttr = *args.begin();
    //IPTV_ATTR_TYPE_e attr = (IPTV_ATTR_TYPE_e)-1;
    bool valid = false;

    //for (size_t i = 0; i < sizeof(iptvPlayerAttrTable)/sizeof(*iptvPlayerAttrTable); ++i) {
        //if (strAttr == iptvPlayerAttrTable[i].str) {
            //attr = iptvPlayerAttrTable[i].attr;
            //valid = true;
            //break;
        //}
    //}

    int ret     = -1;
    int value   = -1;
    if (valid) {
        //ret = mp->playerback_getStatusInfo(attr, &value);
        //printf("call playerback_getStatusInfo return %d, value=%d\n", ret, value);
    } else {
        printf("Invalid attr: %s\n", strAttr.c_str());
    }

    return 0;
}

static struct Command ctcTestCommands[] = {
    {"getStatusInfo", 1, {cmd_getStatusInfo}, "get status info"},
    {nullptr, 0, {nullptr}, ""}
};

static struct ModuleDescriptor moduleDesc {
    .name = "amlprobe",
    .module = new AmlProbeModule(ctcTestCommands),
};

AUTO_REGISTER_MODULE(&moduleDesc);

///////////////////////////////////////////////////////////////////////////////
AmlProbeModule::AmlProbeModule(const Command* commands)
: Module(commands)
{

}

AmlProbeModule::~AmlProbeModule()
{

}

const char* AmlProbeModule::moduleName() const
{
    return moduleDesc.name;
}

void AmlProbeModule::usage()
{
    Module::usage();
}

int AmlProbeModule::parseArguments(int argc, char** argv)
{
    int ret = 0;

    ret = Module::parseArguments(argc, argv);

    return ret;
}

int AmlProbeModule::initCTC()
{
    printf("register default_ctc_cb\n");

    static_cast<aml::CTC_MediaProcessor*>(mCtc)->playerback_register_evt_cb(aml::default_ctc_cb, this);

    return 0;
}

int AmlProbeModule::runCommand(const std::string& cmdBuf)
{
    int ret = 0;

    ret = Module::runCommand(cmdBuf);

    return ret;
}

