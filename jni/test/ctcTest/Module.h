#ifndef _MODULE_H_
#define _MODULE_H_

#include <string>
#include <vector>
#include <map>

struct Module;

struct Command {
    const char* name;
    size_t argCount;
    union {
        int (*fn)(void* mp, const std::vector<std::string>& args);
        int (*m_fn)(Module* mp, const std::vector<std::string>& args);
    };
    const char* help;
};

struct Module {
    explicit Module(const Command* commands);
    virtual ~Module() = 0;

    const char* file() const { return mFile.c_str(); }
    bool isLive() const {return mIsLive;}
    bool isFeedEs() const { return mFeedEs; }
    bool isLegacyMode() const {return mLegacyMode;}
    bool useOmx() const { return mUseOmx; }
    int audioPid() const { return mAudioPid; }
    int videoPid() const { return mVideoPid; }
    int subtitlePid() const { return mSubtitlePid; }
    void setAudioPid(int pid) {mAudioPid = pid;}
    void setVideoPid(int pid) {mVideoPid = pid;}
    void setSubtitlePid(int pid) {mSubtitlePid = pid;}

    void setCTCHandler(void* ctc) { mCtc = ctc; }
    void printCommands();

    virtual const char* moduleName() const {return "<module name>"; }
    virtual void usage();
    virtual int parseArguments(int argc, char** argv);
    virtual int initCTC();
    virtual int runCommand(const std::string& cmdBuf);
    static std::vector<std::string>& split(const std::string& str, std::vector<std::string>& result);

protected:
    const Command* findCommand(const std::string& cmdName, const Command* commandTable);
    void doPrintCommands(const Command* pCommands, bool printHeader = false);
    void rearrangeOptions(char** argv, int parsed, int len, int& processed);

    const Command* mCommands = nullptr;
    bool mIsLive = false;
    bool mFeedEs = false;
    bool mLegacyMode = false;
    bool mUseOmx = false;
    int mAudioPid = 0x1FFF;
    int mVideoPid = 0x1FFF;
    int mSubtitlePid = 0x1FFF;
    std::string mFile;
    void* mCtc = nullptr;

private:
    const Command* mBuiltinCommands = nullptr;
    Module(const Module&) = delete;
    Module& operator=(const Module&) = delete;
};

struct ModuleDescriptor {
    const char* name;
    Module* module;
};

extern std::map<std::string, Module*> g_modules;

void registerModule(ModuleDescriptor* desc);

#define AUTO_REGISTER_MODULE(desc) \
    namespace { \
        struct A { \
            A() { \
                registerModule((desc)); \
            } \
        } _a; \
    }








#endif
