//#define LOG_NDEBUG 0
#define LOG_TAG "CTCTest"
#include <utils/Log.h>
#include <utils/RefBase.h>
#include <media/stagefright/foundation/AHandler.h>
#include <media/stagefright/foundation/ABase.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/SurfaceUtils.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
#include <gui/Surface.h>
#include <gui/SurfaceComposerClient.h>
#include <gui/ISurfaceComposer.h>
#endif
#include <iostream>
#include <libgen.h>
#include <math.h>
#include <binder/ProcessState.h>
#include <sys/syscall.h>
#include <CTCMediaPlayer/SourceFeeder.h>
#include <ui/DisplayInfo.h>

#include <CTC_MediaProcessor.h>
#include <media_processor/CTC_Utils.h>
#include <AM_MPP/AmlHalPlayer.h>
#include <system/window.h>

#include "Module.h"

extern "C" {
#include <libavformat/avformat.h>
}

//using namespace android;
using android::AHandler;
using android::sp;
using android::AMessage;
using android::OK;
using android::AString;
using android::status_t;

static bool g_quit = false;

#define ENUM_TO_STR(e) case e: return #e; break
///////////////////////////////////////////////////////////////////////////////
// environment and utils
static void set_sysfs_str(const char* path, const char* value)
{
    int fd;

    fd = ::open(path, O_WRONLY);
    if (fd < 0) {
        printf("open %s failed, %s\n", path, strerror(errno));
        return;
    }

    if (::write(fd, value, strlen(value)+1) < 0) {
        printf("write %s failed, %s\n", path, strerror(errno));
    }

    ::close(fd);
}

struct Env
{
    static const int APPLICATION_MEDIA_SUBLAYER = -2;
    static const int NORMAL_LAYER = 0;
    static const int DEFAULT_DIGHOLE_LAYER = 1;

    Env() {}
    int createSurface();
    void controlSurface(int zorder);
    void controlSurface(int left, int top, int right, int bottom);
    void updateSurface();
    static void openOsd();
    static void closeOsd();

#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
    sp<android::SurfaceComposerClient> mComposerClient;
    sp<android::SurfaceControl> mSurfaceControl;
    sp<android::Surface> mSurface;

    sp<android::SurfaceControl> mSurfaceControl2;
    sp<android::Surface> mSurface2;
#endif

    int mDisplayWidth = 1920;
    int mDisplayHeight = 1080;

    int mSurfaceWidth  = 1920;
    int mSurfaceHeight = 1080;

    std::vector<SourceFeeder::Track> mVideoTracks;
    std::vector<SourceFeeder::Track> mAudioTracks;
    std::vector<SourceFeeder::Track> mSubtitleTracks;

private:
    Env(const Env&) = delete;
    Env& operator=(const Env&) = delete;
};

int Env::createSurface()
{
#if !defined(__ANDROID_VNDK__)
    mComposerClient = new android::SurfaceComposerClient;
    CHECK_EQ(mComposerClient->initCheck(), OK);

    sp<android::IBinder> displayToken  = nullptr;
#if ANDROID_PLATFORM_SDK_VERSION == 28
    displayToken = mComposerClient->getBuiltInDisplay(android::ISurfaceComposer::eDisplayIdMain);
#elif ANDROID_PLATFORM_SDK_VERSION >= 29
    displayToken = mComposerClient->getInternalDisplayToken();
#endif

    android::DisplayInfo displayInfo;
    CHECK_EQ(OK, mComposerClient->getDisplayInfo(displayToken, &displayInfo));
    //printf("displayInfo:%d x %d\n", displayInfo.w, displayInfo.h);
    mDisplayWidth = displayInfo.w;
    mDisplayHeight = displayInfo.h;

    mSurfaceWidth   = mDisplayWidth >> 1;
    mSurfaceHeight  = mDisplayHeight >> 1;

    mSurfaceControl = mComposerClient->createSurface(android::String8("ctcTest"), mSurfaceWidth, mSurfaceHeight, android::PIXEL_FORMAT_RGB_565, 0);
    CHECK(mSurfaceControl->isValid());
    mSurface = mSurfaceControl->getSurface();

    android::SurfaceComposerClient::Transaction()
        .setFlags(mSurfaceControl, android::layer_state_t::eLayerOpaque, android::layer_state_t::eLayerOpaque)
#if ANDROID_PLATFORM_SDK_VERSION >= 29
        .setCrop_legacy(mSurfaceControl, android::Rect(mSurfaceWidth, mSurfaceHeight))
#endif
        .show(mSurfaceControl)
        .apply();

    mSurfaceControl2 = mComposerClient->createSurface(android::String8("ctcTest-ui"), mDisplayWidth, mDisplayHeight, android::PIXEL_FORMAT_RGBA_8888, 0);
    CHECK(mSurfaceControl2->isValid());
    mSurface2 = mSurfaceControl2->getSurface();
    int ret = native_window_api_connect(mSurface2.get(), NATIVE_WINDOW_API_CPU);
    if (ret < 0) {
        ALOGE("mSurface2 connect failed with %d!", ret);
    }

    mSurface2->allocateBuffers();

    android::SurfaceComposerClient::Transaction()
#if ANDROID_PLATFORM_SDK_VERSION >= 29
        .setCrop_legacy(mSurfaceControl2, android::Rect(mDisplayWidth, mDisplayHeight))
#endif
        .setLayer(mSurfaceControl2, Env::DEFAULT_DIGHOLE_LAYER)
        .show(mSurfaceControl2)
        .apply();

    updateSurface();
#endif

    return 0;
}

void Env::controlSurface(int zorder)
{
#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
    auto transcation = android::SurfaceComposerClient::Transaction();

    transcation.setLayer(mSurfaceControl, zorder);
    transcation.setLayer(mSurfaceControl2, zorder);

    transcation.apply();
#endif
}

void Env::controlSurface(int left, int top, int right, int bottom)
{
#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
    auto transcation = android::SurfaceComposerClient::Transaction();

    if (left >= 0 && top >= 0) {
        transcation.setPosition(mSurfaceControl, left, top);
    }

    if (right > left && bottom > top) {
        int width = right - left;
        int height = bottom - top;
        transcation.setSize(mSurfaceControl, width, height);
#if ANDROID_PLATFORM_SDK_VERSION >= 29
        transcation.setCrop_legacy(mSurfaceControl, android::Rect(width, height));
#endif
    }

    transcation.apply();
#endif
}

void Env::updateSurface()
{
#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
    ANativeWindow* nativeWindow = static_cast<ANativeWindow*>(mSurface2.get());

    int err = 0;
    ANativeWindowBuffer* buf;
    err = nativeWindow->dequeueBuffer_DEPRECATED(nativeWindow, &buf);
    if (err != 0) {
        printf("dequeueBuffer failed:%d\n", err);
        return;
    }

    nativeWindow->lockBuffer_DEPRECATED(nativeWindow, buf);
    sp<android::GraphicBuffer> graphicBuffer = android::GraphicBuffer::from(buf);

    char* vaddr;
    graphicBuffer->lock(1, (void **)&vaddr);
    if (vaddr != nullptr) {
        memset(vaddr, 0x0, graphicBuffer->getWidth() * graphicBuffer->getHeight() * 4); /*to show video in osd hole...*/
    }
    graphicBuffer->unlock();
    graphicBuffer.clear();
    nativeWindow->queueBuffer_DEPRECATED(nativeWindow, buf);
#endif
}

void Env::openOsd()
{
//    set_sysfs_str("/sys/class/graphics/fb0/osd_display_debug", "1");
//    set_sysfs_str("/sys/class/graphics/fb0/blank", "0");
}

void Env::closeOsd()
{
//    set_sysfs_str("/sys/class/graphics/fb0/osd_display_debug", "1");
//    set_sysfs_str("/sys/class/graphics/fb0/blank", "1");
}

static std::string fmtTime()
{
    struct timeval tv;
    struct tm* tm;
    gettimeofday(&tv, nullptr);

    tm = localtime(&tv.tv_sec);

    char buf[40];
    snprintf(buf, sizeof(buf), "%02d-%02d %02d:%02d:%02d.%03d",
            tm->tm_mon + 1,
            tm->tm_mday,
            tm->tm_hour,
            tm->tm_min,
            tm->tm_sec,
            (int)tv.tv_usec/1000
            );

	return std::string(buf);
}

struct CTCInterface : public ISourceReceiver
{
    virtual ~CTCInterface() {}
    virtual void* getHandler() const = 0;
    virtual void InitVideo(std::vector<SourceFeeder::Track>& tracks);
    virtual void InitAudio(std::vector<SourceFeeder::Track>& tracks);
    virtual void InitSubtitle(std::vector<SourceFeeder::Track>& tracks);
    virtual void SetSurface(android::Surface* pSurface);
    virtual void SetSurface_ANativeWindow(ANativeWindow* pSurface);
    virtual bool StartPlay();
    virtual bool Stop();
};

///////////////////////////////////////////////////////////////////////////////
// for legacy ZTE CTC interface
#ifndef  __ANDROID_VNDK__
#include <legacy_CTC_MediaProcessor.h>

static const char* iptvPlayerEvt2Str(IPTV_PLAYER_EVT_e evt)
{
    static char buf[20];
    snprintf(buf, sizeof(buf), "Unknown IPTV_PLAYER_EVT_e:%#x", evt);

    switch (evt) {
        ENUM_TO_STR(IPTV_PLAYER_EVT_FIRST_PTS);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VOD_EOS);
        ENUM_TO_STR(IPTV_PLAYER_EVT_ABEND);
        ENUM_TO_STR(IPTV_PLAYER_EVT_PLAYBACK_ERROR);

        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_FRAME_ERROR);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_DISCARD_FRAME);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_DEC_OVERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_DEC_UNDERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_INVALID_TIMESTAMP);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_INVALID_DATA);
        ENUM_TO_STR(IPTV_PLAYER_EVT_VID_OTHER);

        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_FRAME_ERROR);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_DISCARD_FRAME);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_DEC_OVERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_DEC_UNDERFLOW);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_INVALID_TIMESTAMP);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_INVALID_DATA);
        ENUM_TO_STR(IPTV_PLAYER_EVT_AUD_OTHER);

        ENUM_TO_STR(IPTV_PLAYER_EVT_SUB_INVALID_TIMESTAMP);
        ENUM_TO_STR(IPTV_PLAYER_EVT_SUB_INVALID_DATA);
        ENUM_TO_STR(IPTV_PLAYER_EVT_SUB_OTHER);
    default:
        return buf;
    };

    return buf;
}

void default_ctc_cb(IPTV_PLAYER_EVT_e evt, void* handler __unused)
{
	std::string t = fmtTime();

    pid_t pid = getpid();
    pid_t tid = syscall(__NR_gettid);

    printf("%s %d %d evt:%s\n", t.c_str(), pid, tid, iptvPlayerEvt2Str(evt));

    //if (evt == IPTV_PLAYER_EVT_FIRST_PTS) {
        //Env::closeOsd();
    //}
}

static void convertToVPara(AVStream* st, VIDEO_PARA_T* vPara)
{
    vPara->pid = st->id;
    vPara->vFmt = aml::convertToVFormat(st->codecpar->codec_id);
    vPara->nVideoWidth = st->codecpar->width;
    vPara->nVideoHeight = st->codecpar->height;
    vPara->nFrameRate = av_q2d(st->avg_frame_rate);
    vPara->pExtraData = st->codecpar->extradata;
    vPara->nExtraSize = st->codecpar->extradata_size;
}

static void convertToAPara(AVStream* st, AUDIO_PARA_T* aPara)
{
    aPara->pid = st->id;
    aPara->aFmt = aml::convertToAFormat(st->codecpar->codec_id);
    aPara->nChannels = st->codecpar->channels;
    aPara->nSampleRate = st->codecpar->sample_rate;
    aPara->pExtraData = st->codecpar->extradata;
    aPara->nExtraSize = st->codecpar->extradata_size;
}

static void convertToSPara(AVStream* st, SUBTITLE_PARA_T* sPara)
{
    sPara->pid = st->id;
    sPara->sub_type = st->codecpar->codec_id;
    sPara->stream_index = 0;
}

struct ZTE_CTCInterface : public CTCInterface
{
    ZTE_CTCInterface(bool isEsSource __unused, bool useOmx) {
        mCtc.reset(GetMediaProcessor(useOmx, 0));
    }

    void* getHandler() const override {return mCtc.get();}
    void InitVideo(std::vector<SourceFeeder::Track>& tracks) override {
        ::VIDEO_PARA_T paras[MAX_VIDEO_PARAM_SIZE];
        memset(paras, 0, sizeof(paras));
        for (size_t i = 0; i < tracks.size(); ++i) {
            if (i == MAX_VIDEO_PARAM_SIZE)
                break;

            ::convertToVPara(tracks[i].mStream, paras + i);
        }

        mCtc->InitVideo(paras);
    }

    void InitAudio(std::vector<SourceFeeder::Track>& tracks) override {
        ::AUDIO_PARA_T paras[MAX_AUDIO_PARAM_SIZE];
        memset(paras, 0, sizeof(paras));

        for (size_t i = 0; i < tracks.size(); ++i) {
            if (i == MAX_AUDIO_PARAM_SIZE)
                break;

        ::convertToAPara(tracks[i].mStream, paras + i);
        }

        mCtc->InitAudio(paras);
    }

    void InitSubtitle(std::vector<SourceFeeder::Track>& tracks) override {
        ::SUBTITLE_PARA_T paras[MAX_SUBTITLE_PARAM_SIZE];
        memset(paras, 0, sizeof(paras));

        for (size_t i = 0; i < tracks.size(); ++i) {
            if (i == MAX_SUBTITLE_PARAM_SIZE)
                break;

            ::convertToSPara(tracks[i].mStream, paras + i);
        }

        mCtc->InitSubtitle(paras);
    }

    void SetSurface(android::Surface* pSurface) override {
        mCtc->SetSurface(pSurface);
    }

    void SetSurface_ANativeWindow(    ANativeWindow* pSurface) override {
        mCtc->SetSurface_ANativeWindow(pSurface);
    }

    bool StartPlay() override {
        return mCtc->StartPlay();
    }

    bool Stop() override {
        return mCtc->Stop();
    }

    int GetWriteBuffer(SourceFeeder::MediaType type __unused, uint8_t** pBuffer, uint32_t* nSize __unused) override {
        *pBuffer = nullptr;
        return 0;
    }

    int WriteData(uint8_t* pBuffer, uint32_t nSize) override {
        return mCtc->WriteData(pBuffer, nSize);
    }

    int WriteData(SourceFeeder::MediaType type, uint8_t* pBuffer, uint32_t nSize, int64_t pts) override {
        return mCtc->WriteData(getStreamType(type), pBuffer, nSize, pts);
    }

private:
    ::PLAYER_STREAMTYPE_E getStreamType(SourceFeeder::MediaType type) {
        ::PLAYER_STREAMTYPE_E streamType = ::PLAYER_STREAMTYPE_TS;
        switch (type) {
        case SourceFeeder::MEDIA_TYPE_VIDEO:
            streamType = ::PLAYER_STREAMTYPE_VIDEO;
            break;

        case SourceFeeder::MEDIA_TYPE_AUDIO:
            streamType = ::PLAYER_STREAMTYPE_AUDIO;
            break;

        case SourceFeeder::MEDIA_TYPE_SUBTITLE:
            streamType = ::PLAYER_STREAMTYPE_SUBTITLE;
            break;

        default:
            break;
        }

        return streamType;
    }

    std::shared_ptr<CTC_MediaProcessor> mCtc;
};
#endif

///////////////////////////////////////////////////
namespace aml {
void default_ctc_cb(void* handler __unused, aml::IPTV_PLAYER_EVT_E evt, uint32_t param1, uint32_t param2)
{
	std::string t = fmtTime();

    pid_t pid = getpid();
    pid_t tid = syscall(__NR_gettid);

    printf("%s %d %d evt:%s, param1:%d, param2:%d\n", t.c_str(), pid, tid,
            aml::iptvPlayerEvt2Str(evt), param1, param2);

#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
    if (evt == IPTV_PLAYER_EVT_FIRST_PTS) {
        Env::closeOsd();
    }
#endif
}

}

struct Canonical_CTCInterface : public CTCInterface
{
    Canonical_CTCInterface(bool isEsSource, bool useOmx, bool isLegacyMode) {
        aml::CTC_InitialParameter initParam;
        initParam.userId = 0;
        initParam.useOmx = useOmx;
        initParam.isEsSource = isEsSource;
        initParam.flags = isLegacyMode ? CTC_LEGACY_MODE : 0;
        initParam.extensionSize = 0;
        mCtc.reset(aml::GetMediaProcessor(&initParam));
    }

    void* getHandler() const override {return mCtc.get();}
    void InitVideo(std::vector<SourceFeeder::Track>& tracks) override {
        aml::VIDEO_PARA_T paras[MAX_VIDEO_PARAM_SIZE];
        memset(paras, 0, sizeof(paras));

        for (size_t i = 0; i < tracks.size(); ++i) {
            if (i == MAX_VIDEO_PARAM_SIZE)
                break;

            aml::convertToVPara(tracks[i].mStream, paras + i);
        }

        mCtc->InitVideo(paras);
    }

    void InitAudio(std::vector<SourceFeeder::Track>& tracks) override {
        aml::AUDIO_PARA_T paras[MAX_AUDIO_PARAM_SIZE];
        memset(paras, 0, sizeof(paras));

        for (size_t i = 0; i < tracks.size(); ++i) {
            if (i == MAX_AUDIO_PARAM_SIZE)
                break;

            aml::convertToAPara(tracks[i].mStream, paras + i);
        }

        mCtc->InitAudio(paras);
    }

    void InitSubtitle(std::vector<SourceFeeder::Track>& tracks) override {
        aml::SUBTITLE_PARA_T paras[MAX_SUBTITLE_PARAM_SIZE];
        memset(paras, 0, sizeof(paras));

        for (size_t i = 0; i < tracks.size(); ++i) {
            if (i == MAX_SUBTITLE_PARAM_SIZE)
                break;

            aml::convertToSPara(tracks[i].mStream, paras + i);
        }

        mCtc->InitSubtitle(paras);
    }

    void SetSurface(android::Surface* pSurface) override {
        mCtc->SetSurface(pSurface);
    }

    void SetSurface_ANativeWindow(ANativeWindow* pSurface) override {
        mCtc->SetSurface_ANativeWindow(pSurface);
    }

    bool StartPlay() override {
        return mCtc->StartPlay();
    }

    bool Stop() override {
        return mCtc->Stop();
    }

    int GetWriteBuffer(SourceFeeder::MediaType type, uint8_t** pBuffer, uint32_t* nSize) override {
        return mCtc->GetWriteBuffer(getStreamType(type), pBuffer, nSize);
    }

    int WriteData(uint8_t* pBuffer, uint32_t nSize) override {
        return mCtc->WriteData(pBuffer, nSize);
    }

    int WriteData(SourceFeeder::MediaType type, uint8_t* pBuffer, uint32_t nSize, int64_t pts) override {
        return mCtc->WriteData(getStreamType(type), pBuffer, nSize, pts);
    }


private:
    aml::PLAYER_STREAMTYPE_E getStreamType(SourceFeeder::MediaType type) {
        aml::PLAYER_STREAMTYPE_E streamType = aml::PLAYER_STREAMTYPE_TS;
        switch (type) {
        case SourceFeeder::MEDIA_TYPE_VIDEO:
            streamType = aml::PLAYER_STREAMTYPE_VIDEO;
            break;

        case SourceFeeder::MEDIA_TYPE_AUDIO:
            streamType = aml::PLAYER_STREAMTYPE_AUDIO;
            break;

        case SourceFeeder::MEDIA_TYPE_SUBTITLE:
            streamType = aml::PLAYER_STREAMTYPE_SUBTITLE;
            break;

        default:
            break;
        }

        return streamType;
    }

    std::shared_ptr<aml::CTC_MediaProcessor> mCtc;
};

///////////////////////////////////////////////////
static void showCommandHelp()
{
    printf("    commands: osd: show osd\n"
           "              video: show video\n"
           "              zorder <zordr>: adjust zorder\n"
           "              resize <left, top, right, bottom>: adjust axis\n"
           "              info:show file info\n");
}

static bool processCommand(const std::string& cmdBuf, Env* env, CTCInterface* ctc __unused)
{
    bool ret = true;

    std::vector<std::string> args;
    args = Module::split(cmdBuf, args);
    std::string cmd = *args.begin();
    args.erase(args.begin());

    if (cmd == "osd") {
        env->controlSurface(Env::APPLICATION_MEDIA_SUBLAYER);
    } else if (cmd == "video") {
        env->controlSurface(Env::NORMAL_LAYER);
    } else if (cmd == "zorder") {
        if (args.size() == 1) {
            int zorder = std::stoi(args[0]);
            env->controlSurface(zorder);
        }
    } else if (cmd == "resize") {
        int x = -1;
        int y = -1;
        int width = -1;
        int height = -1;

        if (args.size() == 4) {
            x = std::stoi(args[0]);
            y = std::stoi(args[1]);
            width = std::stoi(args[2]);
            height = std::stoi(args[3]);
            env->controlSurface(x, y, width, height);
        }
    } else if (cmd == "info") {
        std::vector<SourceFeeder::Track>* videoTracks = &env->mVideoTracks;
        std::vector<SourceFeeder::Track>* audioTracks = &env->mAudioTracks;
        std::vector<SourceFeeder::Track>* subtitleTracks = &env->mSubtitleTracks;

        if (!videoTracks->empty()) printf("videos:\n");
        std::for_each(videoTracks->begin(), videoTracks->end(), [](SourceFeeder::Track& t) {
            printf("  pid:%d, fmt:%s\n", t.mStream->id, avcodec_get_name(t.mStream->codecpar->codec_id));
        });

        if (!audioTracks->empty()) printf("audios:\n");
        std::for_each(audioTracks->begin(), audioTracks->end(), [](SourceFeeder::Track& t) {
            printf("  pid:%d, fmt:%s\n", t.mStream->id, avcodec_get_name(t.mStream->codecpar->codec_id));
        });

        if (!subtitleTracks->empty()) printf("subtitles:\n");
        std::for_each(subtitleTracks->begin(), subtitleTracks->end(), [](SourceFeeder::Track& t) {
            printf("  pid:%d, fmt:%s\n", t.mStream->id, avcodec_get_name(t.mStream->codecpar->codec_id));
        });
    } else {
        ret = false;
    }

    return ret;
}

int main(int argc, char *argv[])
{
    sp<android::ProcessState> proc(android::ProcessState::self());
    android::ProcessState::self()->startThreadPool();

    int ret = 0;
    const char* ctcTestName = basename(argv[0]);

    if (argc < 2) {
        const char* ctcVersionString = nullptr;
        aml::GetMediaProcessorVersion(&ctcVersionString);
        printf("CTC version: %s\n%s\n", ctcVersionString, aml::ctcVersion().c_str());

        extern std::string liveplayerVersion();
        const char* amlHalPlayerVersionString = nullptr;
        aml::AML_AVPLAY_GetVersion(&amlHalPlayerVersionString);
        printf("AmlHalPlayer version: %s\n%s\n", amlHalPlayerVersionString, liveplayerVersion().c_str());

        printf("Usage: %s <module_name> <args>\n", ctcTestName);
        showCommandHelp();
        printf("    use \'-h\' argument to show more helps.\n");
        printf("current supported modules:\n");
        for (auto&p : g_modules) {
            printf("\t%s\n", p.first.c_str());
        }
        printf("\n");

        return 0;
    }

    const char* moduleName = nullptr;
    size_t moduleNameIndex = 0;
    for (int i = 1; i < argc; ++i) {
        if (argv[i][0] != '-') {
            if (g_modules.find(argv[i]) != g_modules.end()) {
                moduleNameIndex = i;
                break;
            }
        }
    }

    if (moduleNameIndex != 0) {
        moduleName = argv[moduleNameIndex];
        argc -= moduleNameIndex + 1;
        argv += moduleNameIndex + 1;
    } else {
        moduleName = "ctc";
        --argc;
        ++argv;
    }

    Module* module = g_modules.at(moduleName);
    ret = module->parseArguments(argc, argv);
    if (ret < 0) {
        return -1;
    }


#if defined(__ANDROID_VNDK__)
    if (!module->useOmx()) {
        fprintf(stderr, "please choose omx mode(-o) under vendor!\n");
        return 0;
    }
#endif

    signal(SIGINT, [](int sig) {
        printf("%s\n", strsignal(sig));
        g_quit = true;
    });

    Env env;

    sp<android::ALooper> looper = new android::ALooper();
    AString looperName = android::AStringPrintf("CTCTest");
    looper->setName(looperName.c_str());
    looper->start();

//start:
    int err = 0;
    sp<SourceFeeder> sourceFeeder = new SourceFeeder(module->file());
    sourceFeeder->setSourceMode(module->isFeedEs() ? SourceFeeder::SOURCE_MODE_ES : SourceFeeder::SOURCE_MODE_TS);
    if (sourceFeeder->prepare(looper) != OK) {
        return -1;
    }

#if !defined(__ANDROID_VNDK__) || defined(HAS_VENDOR_GUI)
    ret = atexit(&Env::openOsd);
    if (ret < 0) {
        ALOGE("call atexit failed! errno=%d", errno);
        return -1;
    }
#endif

    env.mVideoTracks = sourceFeeder->getVideoTracks();
    size_t videoParasCount = std::min(env.mVideoTracks.size(), (size_t)MAX_VIDEO_PARAM_SIZE);

    env.mAudioTracks = sourceFeeder->getAudioTracks();
    size_t audioParasCount = std::min(env.mAudioTracks.size(), (size_t)MAX_AUDIO_PARAM_SIZE);

    env.mSubtitleTracks = sourceFeeder->getSubtitleTracks();
    size_t subtitleParasCount = std::min(env.mSubtitleTracks.size(), (size_t)MAX_SUBTITLE_PARAM_SIZE);

    if (module->videoPid() != 0 && videoParasCount > 0) {
        for (size_t i = 0; i < videoParasCount; ++i) {
            if (env.mVideoTracks[i].mStream->id == module->videoPid()) {
                std::swap(env.mVideoTracks[0], env.mVideoTracks[i]);
                break;
            }
        }
    }

    if (module->audioPid() != 0 && audioParasCount > 0) {
        for (size_t i = 0; i < audioParasCount; ++i) {
            if (env.mAudioTracks[i].mStream->id == module->audioPid()) {
                std::swap(env.mAudioTracks[0], env.mAudioTracks[i]);
                break;
            }
        }
    }

    if (module->subtitlePid() != 0 && subtitleParasCount > 0) {
        for (size_t i = 0; i < subtitleParasCount; ++i) {
            if (env.mSubtitleTracks[i].mStream->id == module->subtitlePid()) {
                std::swap(env.mSubtitleTracks[0], env.mSubtitleTracks[i]);
                break;
            }
        }
    }

    bool canonicalCTC = strcmp(module->moduleName(), "zte");
    printf("%s mode\n", canonicalCTC ? "canonical" : "zte");

    sp<CTCInterface> ctc = nullptr;
    if (canonicalCTC) {
        ctc = new Canonical_CTCInterface(module->isFeedEs(), module->useOmx(), module->isLegacyMode());
    } else {
#ifndef __ANDROID_VNDK__
        ctc = new ZTE_CTCInterface(module->isFeedEs(), module->useOmx());
#endif
    }

    if (videoParasCount > 0) {
        ctc->InitVideo(env.mVideoTracks);
        module->setVideoPid(env.mVideoTracks[0].mStream->id);
    }

    if (audioParasCount > 0) {
        ctc->InitAudio(env.mAudioTracks);
        module->setAudioPid(env.mAudioTracks[0].mStream->id);
    }

    if (subtitleParasCount > 0) {
        ctc->InitSubtitle(env.mSubtitleTracks);
        module->setSubtitlePid(env.mSubtitleTracks[0].mStream->id);
    }

    env.createSurface();
#if !defined(__ANDROID_VNDK__)
    if ((err = native_window_api_connect(env.mSurface.get(), NATIVE_WINDOW_API_MEDIA)) != OK) {
        printf("nativeWindowConnect failed: %d\n", err);
    }
    ctc->SetSurface_ANativeWindow(env.mSurface.get());
#endif

    module->setCTCHandler(ctc->getHandler());
    module->initCTC(); //extra initialization

    if (!ctc->StartPlay()) {
        printf("start play failed!\n");
        return -1;
    }

    sourceFeeder->setSourceReceiver(ctc.get());
    sourceFeeder->start();

    bool prompt = false;
    char promptBuf[50];
    snprintf(promptBuf, sizeof(promptBuf), "%s >", ctcTestName);

    std::string lastCommand;

    while (!g_quit) {
        if (tcgetpgrp(0) != getpid()) {
            usleep(10*1000);
            continue;
        }

        if (prompt) {
            prompt = false;
            fprintf(stderr, "%s", promptBuf);
        }

        struct pollfd fds;
        fds.fd = STDIN_FILENO;
        fds.events = POLL_IN;
        fds.revents = 0;
        int ret = ::poll(&fds, 1, 1000);
        if (ret < 0) {
            //printf("poll STDIN_FILENO failed! %d\n", -errno);
        } else if (ret > 0) {
            if (fds.revents & POLL_ERR) {
                printf("poll error!\n");
                continue;
            } else if (!(fds.revents & POLL_IN)) {
                continue;
            }

            prompt = true;

            char buffer[100]{0};
            int len = ::read(STDIN_FILENO, buffer, sizeof(buffer));
            if (len <= 0) {
                printf("read failed! %d\n", -errno);
                continue;
            }
            buffer[len-1] = '\0';
            //printf("read buf:%s, size:%d\n", buffer, len);

            std::string buf(buffer);
            size_t b = 0;
            size_t e = buf.size();
            while (b < e) {
                if (isspace(buf[b])) ++b;
                else if (isspace(buf[e])) --e;
                else break;
            }

            if (b < e) {
                buf = buf.substr(b, e - b);
                lastCommand = buf;
            } else if (b == e && !lastCommand.empty()) {
                buf = lastCommand;
            } else {
                continue;
            }

            if (processCommand(buf, &env, ctc.get())) {
                continue;
            }

            module->runCommand(buf);
        }
    }

    printf("exiting...");
    sourceFeeder->stop();
    looper->unregisterHandler(sourceFeeder->id());
    looper->stop();
    sourceFeeder.clear();

    if (ctc) {
        ALOGI("ctc stopping...");
        ctc->Stop();
        ctc.clear();
    }

    printf("exited!\n");

    return 0;
}
