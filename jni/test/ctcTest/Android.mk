LOCAL_PATH:= $(call my-dir)

###########################################################
CTC_TEST_SRCS := \
	CTCTest.cpp \
	Module.cpp \
	CTCIntfTest.cpp \
	ZTEIntfTest.cpp \
	AmlSubtitleTest.cpp \
	AmlProbeTest.cpp \
	AmlDVBTest.cpp

###############################################################################
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= $(CTC_TEST_SRCS)

HARDWARE_PATH := $(TOP)/hardware/amlogic

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../include \
	$(LOCAL_PATH)/../../ \
	$(HARDWARE_PATH)/media/amcodec/include \
	$(HARDWARE_PATH)/media/amavutils/include \
	$(TOP)/vendor/amlogic/common/external/ffmpeg

LOCAL_CFLAGS := -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

LOCAL_SHARED_LIBRARIES :=       \
	libamffmpeg  				\
	libCTC_MediaProcessor		\
	libliveplayer               \
	libstagefright_foundation   \
	libstagefright				\
	libui						\
	libgui						\
	liblog						\
	libbinder					\
	libutils					\
	libcutils					\

LOCAL_MODULE := ctctest
LOCAL_MODULE_TAGS := optional

#LOCAL_SANITIZE := address

include $(BUILD_EXECUTABLE)

###############################################################################
ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 29))
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= $(filter-out ZTEIntfTest.cpp, $(CTC_TEST_SRCS))

HARDWARE_PATH := $(TOP)/hardware/amlogic

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../include \
	$(LOCAL_PATH)/../../ \
	$(HARDWARE_PATH)/media/amcodec/include \
	$(HARDWARE_PATH)/media/amavutils/include \
	$(TOP)/vendor/amlogic/common/external/ffmpeg \

LOCAL_CFLAGS := -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION) -Wno-unused-parameter \
	-Wno-unused-variable
ifeq ($(USE_VENDOR_GUI), true)
LOCAL_CFLAGS := \
	-DNO_INPUT \
	-DHAS_VENDOR_GUI
endif

LOCAL_SHARED_LIBRARIES :=       \
	libamffmpeg.vendor			\
	libCTC_MediaProcessor.vendor \
	libliveplayer.vendor        \
	libstagefright_foundation   \
	libui						\
	liblog						\
	libbinder					\
	libutils					\
	libcutils
ifeq ($(USE_VENDOR_GUI), true)
LOCAL_SHARED_LIBRARIES += \
	libgui_vendor
endif

LOCAL_MODULE := ctctest.vendor
LOCAL_MODULE_TAGS := optional

#LOCAL_SANITIZE := address

LOCAL_VENDOR_MODULE := true
include $(BUILD_EXECUTABLE)
endif

