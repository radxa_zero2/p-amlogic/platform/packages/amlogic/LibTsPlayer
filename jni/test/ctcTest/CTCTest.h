#ifndef _CTC_TEST_H_
#define _CTC_TEST_H_

#include <CTC_Common.h>

namespace aml {
void default_ctc_cb(void* handler, aml::IPTV_PLAYER_EVT_E evt, uint32_t param1, uint32_t param2);
}


#endif
