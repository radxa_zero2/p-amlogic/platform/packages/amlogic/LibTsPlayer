LOCAL_PATH := $(call my-dir)

lib_dir :=

define is-module-source-exist
$(shell find $(TOP)/vendor/amlogic -maxdepth 4 -type d -name $(1) -exec echo OK \; 2>/dev/null)
endef

include $(CLEAR_VARS)
LOCAL_MODULE := libAmIptvMedia
LOCAL_SRC_FILES := android_$(PLATFORM_SDK_VERSION)/libAmIptvMedia
LOCAL_MODULE_SUFFIX := .so
LOCAL_SRC_FILES := $(lib_dir)$(LOCAL_SRC_FILES)$(LOCAL_MODULE_SUFFIX)
ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 28))
LOCAL_SHARED_LIBRARIES += libsystemcontrolclient
endif
ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 29))
LOCAL_SHARED_LIBRARIES += libffmpeg_ctc
LOCAL_SHARED_LIBRARIES += libsystemcontrolservice
endif
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT)/lib/
ifneq ($(call is-module-source-exist, iptv-middlewave),OK)
include $(BUILD_PREBUILT)
endif

ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 28))

include $(CLEAR_VARS)
LOCAL_MODULE := amctcTest
LOCAL_SRC_FILES := android_$(PLATFORM_SDK_VERSION)/amctcTest
LOCAL_SHARED_LIBRARIES += libAmIptvMedia
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_PATH := $(TARGET_OUT)/bin/
ifneq ($(call is-module-source-exist, iptv-middlewave),OK)
include $(BUILD_PREBUILT)
endif

include $(CLEAR_VARS)
LOCAL_MODULE := MultiMediaPlayer
LOCAL_SRC_FILES := android_$(PLATFORM_SDK_VERSION)/MultiMediaPlayer_inside_1.0_11397c1_release_201910211442.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := .apk
LOCAL_CERTIFICATE := platform
include $(BUILD_PREBUILT)

endif

ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \= 29))

include $(CLEAR_VARS)
LOCAL_MODULE := MultiMediaPlayer
LOCAL_SRC_FILES := android_$(PLATFORM_SDK_VERSION)/MultiMediaPlayer_inside_1.0_5b860cb_release_202002172055.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := .apk
LOCAL_CERTIFICATE := platform
include $(BUILD_PREBUILT)

endif


include $(CLEAR_VARS)
LOCAL_MODULE := libliveplayer
LOCAL_SRC_FILES := android_$(PLATFORM_SDK_VERSION)/libliveplayer
LOCAL_MODULE_SUFFIX := .so
LOCAL_SRC_FILES := $(lib_dir)$(LOCAL_SRC_FILES)$(LOCAL_MODULE_SUFFIX)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT)/lib/
ifneq ($(call is-module-source-exist, liveplayer),OK)
include $(BUILD_PREBUILT)
endif

ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 29))
include $(CLEAR_VARS)
LOCAL_MODULE := libliveplayer.vendor
LOCAL_SRC_FILES := android_$(PLATFORM_SDK_VERSION)/vendor/libliveplayer.vendor
LOCAL_MODULE_SUFFIX := .so
LOCAL_SRC_FILES := $(lib_dir)$(LOCAL_SRC_FILES)$(LOCAL_MODULE_SUFFIX)
LOCAL_SHARED_LIBRARIES := libamcodec.vendor
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR)/lib/
ifneq ($(call is-module-source-exist, liveplayer),OK)
include $(BUILD_PREBUILT)
endif
endif

ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 28))

include $(CLEAR_VARS)
LOCAL_MODULE := libamlCtcCas
LOCAL_SRC_FILES := android_$(PLATFORM_SDK_VERSION)/libamlCtcCas
LOCAL_MODULE_SUFFIX := .so
LOCAL_SRC_FILES := $(lib_dir)$(LOCAL_SRC_FILES)$(LOCAL_MODULE_SUFFIX)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT)/lib/

ifneq ($(call is-module-source-exist, liveplayer),OK)
include $(BUILD_PREBUILT)
endif

endif

